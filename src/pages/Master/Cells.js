import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withFormik } from "formik"
import * as Yup from "yup";
import Typography from '@material-ui/core/Typography';
import AgTable from "./../../components/AgTable";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from "@material-ui/core/Grid";
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Export from "@material-ui/icons/CloudUploadOutlined";
import Import from "@material-ui/icons/CloudDownloadOutlined";


//your styles here
const styles = theme => ({
    //action style
    dialogStyles: {
        paperWidthSm: '300px',
        textAlign: 'center'
    },
    toolbarIcon: {
        margin: '10px'
    },
    toolbar: {
        margin: '5px'
    },
    //page styles
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        marginLeft: "65px"
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
    card: {
        minWidth: 320,
        "width": "15px",
        "padding": "50px"
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        maxWidth: 400,
        width: "100%"
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
});

const Form = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset
    } = props
    return (

        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <FormControl className={classes.formControl}>
                <InputLabel error={errors.department && (touched.department)} htmlFor="department-simple">Department</InputLabel>
                <Select
                    value={values.department}
                    onChange={handleChange}
                    inputProps={{
                        name: 'department',
                        id: 'department-simple',
                    }}
                    error={errors.department && (touched.department)}
                    FormHelperTextProps={{
                        error: errors.department && (touched.department)
                    }}
                    helperText={
                        errors.department && (touched.department)
                            ? errors.department
                            : null
                    }
                >
                    <MenuItem value={0}>Maintenance</MenuItem>
                    <MenuItem value={1}>New Product Development</MenuItem>
                    <MenuItem value={2}>Production</MenuItem>
                    <MenuItem value={3}>Quality</MenuItem>
                    <MenuItem value={4}>Supplier Quality Assurance</MenuItem>
                </Select>
            </FormControl>

            <FormControl className={classes.formControl}>
                <TextField
                    name="id"
                    placeholder="Enter ID"
                    label="ID"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.id}
                    error={errors.id && (touched.id)}
                    FormHelperTextProps={{
                        error: errors.id && (touched.id)
                    }}
                    helperText={
                        errors.id && (touched.id)
                            ? errors.id
                            : null
                    }
                />
            </FormControl>

            <FormControl className={classes.formControl}>
                <TextField
                    name="name" //should be declared as you would declare a variable
                    placeholder="Enter Name" // Help text for user to understand what is to be input
                    label="Name" // Label when form is empty
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.name}
                    error={
                        errors.name && (touched.name)
                    }
                    value={values.name}
                    FormHelperTextProps={{
                        error:
                            errors.name && (touched.name)
                    }}
                    helperText={
                        (errors.name && (touched.name))
                            ? errors.name
                            : null
                    }
                />
            </FormControl>
            <DialogActions>
                <Button variant="contained" type="submit" color="primary" className={classes.button}>
                    Save
  </Button>
                <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                    Cancel
  </Button>
            </DialogActions>
        </form>

    );

})

const CellsForm = withFormik({
    mapPropsToValues: () => ({ department: '', id: '', name: '' }),
    validationSchema: Yup.object().shape({
        department: Yup.string("Department is required and should be a String").required("Department Required"),
        id: Yup.string("ID is required and should be a String").required("ID Required"),
        name: Yup.string("Name is required and should be a String").required("Name Required")
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'BasicForm',
})(Form)

class CellsPage extends React.PureComponent {
    state = {
        dialogOpen: false
    }

    handleClickOpen = () => {
        this.setState({ dialogOpen: true });
    };

    handleClose = () => {
        this.setState({ dialogOpen: false });
    };

    componentDidMount() {
        console.log("gridapi", this.gridApi)
    }

    onGridReady = (params) => {
        params.api.sizeColumnsToFit()
        this.gridParams = params
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

    exportData = () => {
        this.gridApi.exportDataAsCsv(this.gridParams);
    }

    render() {
        const columnDefs = [{
            headerName: "Sr.No.", field: "srno", sortable: true, filter: true, width: 50
        }, {
            headerName: "Id", field: "id", sortable: true, filter: true, width: 100
        }, {
            headerName: "Description", field: "description", sortable: true, filter: true
        }]
        const rowData = [{
            srno: 1, id: "Main", description: "opexworks"
        }, {
            srno: 2, id: "Main", description: "opexworks"
        }, {
            srno: 3, id: "Main", description: "opexworks"
        }]
        const { classes } = this.props
        return (
            <Fragment>
                <Dialog
                    className={classes.dialogStyles}
                    open={this.state.dialogOpen}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                    maxWidth="sm"
                >
                    <DialogTitle id="form-dialog-title">Add a Cell</DialogTitle>
                    <DialogContent className={classes.dialogStyles}>
                        <CellsForm {...this.props} />
                    </DialogContent>
                </Dialog>
                <Grid
                    container
                    direction="row"
                    justify="space-between"
                    flx
                >
                    <Grid item xs>
                        <div className={classes.toolbar}>
                            <Fab size={"small"} variant="extended" color="primary" onClick={this.handleClickOpen} className={classes.toolbarIcon}>
                                <AddIcon />&nbsp;
                                Add a Cell
                        </Fab>

                            <Fab onClick={this.exportData} size={"small"} variant="extended" color="primary" className={classes.toolbarIcon}>
                                <Import />&nbsp;
                                Export
                        </Fab>

                            <Fab size={"small"} variant="extended" color="primary" className={classes.toolbarIcon}>
                                <Export /> &nbsp;
                                Import
                        </Fab>
                        </div>
                    </Grid>
                </Grid>
                <Paper>
                    <AgTable onGridReady={this.onGridReady} columnDefs={columnDefs} rowData={rowData} />
                    `                </Paper>
            </Fragment >)
    }
}

export default withStyles(styles)(CellsPage);