import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withFormik, Field } from "formik"
import * as Yup from "yup";
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import { DatePicker } from 'material-ui-pickers';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';

//your styles here

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        margin: theme.spacing.unit,
        width: "100%",
        maxWidth: 400
    },
    textField2: {
        margin: theme.spacing.unit,
        width: "100%",
        maxWidth: 400
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
    card1: {
        maxWidth: "850px",
        width: "100%",
        padding: 30,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20
    },
    card2: {
        minWidth: 375,
        maxWidth: "850px",
        width: "45px",
        padding: 30,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        maxWidth: 400,
        width: "100%"
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
    datepicker: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: "100%",
        maxWidth: 400
    },
    group: {
        margin: '1px',
        display: 'flex',
        flexDirection: 'row'
    },
    file: {
        marginTop: 20,
        marginBottom: 20
    },
    typoGraphy: {
        marginTop: theme.spacing.unit * 2,
    }
});

// Register 

const RegisterForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue
    } = props
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid
                container
                direction="row"
                alignItems="center"
            >
                <Grid item xs={6}>
                    <TextField
                        name="id"
                        placeholder="Enter ID"
                        label="ID"
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.id}
                        margin="normal"
                        error={errors.id && (touched.id)}
                        FormHelperTextProps={{
                            error: errors.id && (touched.id)
                        }}
                        helperText={
                            errors.id && (touched.id)
                                ? errors.id
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        name="name"
                        placeholder="Enter Name"
                        label="Name"
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                        margin="normal"
                        error={errors.name && (touched.name)}
                        FormHelperTextProps={{
                            error: errors.name && (touched.name)
                        }}
                        helperText={
                            errors.name && (touched.name)
                                ? errors.name
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="dolstatus-simple">DOL Status</InputLabel>
                        <Select
                            value={values.dolstatus}
                            onChange={handleChange}
                            inputProps={{
                                name: 'dolstatus',
                                id: 'dolstatus-simple',
                            }}
                            error={errors.dolstatus && (touched.dolstatus)}
                            FormHelperTextProps={{
                                error: errors.dolstatus && (touched.dolstatus)
                            }}
                            helperText={
                                errors.dolstatus && (touched.dolstatus)
                                    ? errors.dolstatus
                                    : null
                            }
                        >
                            <MenuItem value={0}>YES</MenuItem>
                            <MenuItem value={1}>NO</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        name="ppm"
                        placeholder="Enter PPM"
                        label="PPM"
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.ppm}
                        margin="normal"
                        error={errors.ppm && (touched.ppm)}
                        FormHelperTextProps={{
                            error: errors.ppm && (touched.ppm)
                        }}
                        helperText={
                            errors.ppm && (touched.ppm)
                                ? errors.ppm
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="supplier1-simple">Supplier 1</InputLabel>
                        <Select
                            value={values.supplier1}
                            onChange={handleChange}
                            inputProps={{
                                name: 'supplier1',
                                id: 'supplier1-simple',
                            }}
                            error={errors.supplier1 && (touched.supplier1)}
                            FormHelperTextProps={{
                                error: errors.supplier1 && (touched.supplier1)
                            }}
                            helperText={
                                errors.supplier1 && (touched.supplier1)
                                    ? errors.supplier1
                                    : null
                            }
                        >
                            <MenuItem value={0}>PUNE AUPAC PVT LTD</MenuItem>
                            <MenuItem value={1}>Supplier 1</MenuItem>
                            <MenuItem value={2}>Supplier 2</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="supplier2-simple">Supplier 2</InputLabel>
                        <Select
                            value={values.supplier2}
                            onChange={handleChange}
                            inputProps={{
                                name: 'supplier2',
                                id: 'supplier2-simple',
                            }}
                            error={errors.supplier2 && (touched.supplier2)}
                            FormHelperTextProps={{
                                error: errors.supplier2 && (touched.supplier2)
                            }}
                            helperText={
                                errors.supplier2 && (touched.supplier2)
                                    ? errors.supplier2
                                    : null
                            }
                        >
                            <MenuItem value={0}>PUNE AUPAC PVT LTD</MenuItem>
                            <MenuItem value={1}>Supplier 1</MenuItem>
                            <MenuItem value={2}>Supplier 2</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="supplier3-simple">Supplier 3</InputLabel>
                        <Select
                            value={values.supplier3}
                            onChange={handleChange}
                            inputProps={{
                                name: 'supplier3',
                                id: 'supplier3-simple',
                            }}
                            error={errors.supplier3 && (touched.supplier3)}
                            FormHelperTextProps={{
                                error: errors.supplier3 && (touched.supplier3)
                            }}
                            helperText={
                                errors.supplier3 && (touched.supplier3)
                                    ? errors.supplier3
                                    : null
                            }
                        >
                            <MenuItem value={0}>PUNE AUPAC PVT LTD</MenuItem>
                            <MenuItem value={1}>Supplier 1</MenuItem>
                            <MenuItem value={2}>Supplier 2</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="supplier4-simple">Supplier 4</InputLabel>
                        <Select
                            value={values.supplier4}
                            onChange={handleChange}
                            inputProps={{
                                name: 'supplier4',
                                id: 'supplier4-simple',
                            }}
                            error={errors.supplier4 && (touched.supplier4)}
                            FormHelperTextProps={{
                                error: errors.supplier4 && (touched.supplier4)
                            }}
                            helperText={
                                errors.supplier4 && (touched.supplier4)
                                    ? errors.supplier4
                                    : null
                            }
                        >
                            <MenuItem value={0}>PUNE AUPAC PVT LTD</MenuItem>
                            <MenuItem value={1}>Supplier 1</MenuItem>
                            <MenuItem value={2}>Supplier 2</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="supplier5-simple">Supplier 5</InputLabel>
                        <Select
                            value={values.supplier5}
                            onChange={handleChange}
                            inputProps={{
                                name: 'supplier5',
                                id: 'supplier5-simple',
                            }}
                            error={errors.supplier5 && (touched.supplier5)}
                            FormHelperTextProps={{
                                error: errors.supplier5 && (touched.supplier5)
                            }}
                            helperText={
                                errors.supplier5 && (touched.supplier5)
                                    ? errors.supplier5
                                    : null
                            }
                        >
                            <MenuItem value={0}>PUNE AUPAC PVT LTD</MenuItem>
                            <MenuItem value={1}>Supplier 1</MenuItem>
                            <MenuItem value={2}>Supplier 2</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="product-simple">Product</InputLabel>
                        <Select
                            value={values.product}
                            onChange={handleChange}
                            inputProps={{
                                name: 'product',
                                id: 'product-simple',
                            }}
                            error={errors.product && (touched.product)}
                            FormHelperTextProps={{
                                error: errors.product && (touched.product)
                            }}
                            helperText={
                                errors.product && (touched.product)
                                    ? errors.product
                                    : null
                            }
                        >
                            <MenuItem value={0}>3 Whlr</MenuItem>
                            <MenuItem value={1}>Welding Assembly</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        name="model"
                        placeholder="Enter Model"
                        label="Model"
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.model}
                        margin="normal"
                        error={errors.model && (touched.model)}
                        FormHelperTextProps={{
                            error: errors.model && (touched.model)
                        }}
                        helperText={
                            errors.model && (touched.model)
                                ? errors.model
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <FormControl component="fieldset" className={classes.formControl}>
                        <FormLabel component="partassembly">Part/Assembly</FormLabel>
                        <RadioGroup
                            aria-label="Part/Assembly"
                            name="partassembly"
                            className={classes.group}
                            value={values.partassembly}
                            onChange={this.handleChange}
                        >
                            <FormControlLabel value="part" control={<Radio />} label="Part" />
                            <FormControlLabel value="assembly" control={<Radio />} label="Assembly" />
                        </RadioGroup>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        name="handlingunit"
                        placeholder="Enter Handling Unit"
                        label="Handling Unit"
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.handlingunit}
                        margin="normal"
                        error={errors.handlingunit && (touched.handlingunit)}
                        FormHelperTextProps={{
                            error: errors.handlingunit && (touched.handlingunit)
                        }}
                        helperText={
                            errors.handlingunit && (touched.handlingunit)
                                ? errors.handlingunit
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <FormControl component="fieldset" className={classes.formControl}>
                        <FormLabel component="inspectiontype">Inspection Type</FormLabel>
                        <RadioGroup
                            aria-label="Inspection Type"
                            name="inspectiontype"
                            className={classes.group}
                            value={values.inspectiontype}
                            onChange={this.handleChange}
                        >
                            <FormControlLabel value="partlevel" control={<Radio />} label="Part Level" />
                            <FormControlLabel value="parameterlevel" control={<Radio />} label="Parameter Level" />
                        </RadioGroup>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="rm-simple">R/M</InputLabel>
                        <Select
                            value={values.rm}
                            onChange={handleChange}
                            inputProps={{
                                name: 'rm',
                                id: 'rm-simple',
                            }}
                            error={errors.rm && (touched.rm)}
                            FormHelperTextProps={{
                                error: errors.rm && (touched.rm)
                            }}
                            helperText={
                                errors.rm && (touched.rm)
                                    ? errors.rm
                                    : null
                            }
                        >
                            <MenuItem value={0}>PUNE AUPAC PVT LTD</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        name="netweight"
                        placeholder="Enter Net Weight"
                        label="Net Weight"
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.netweight}
                        margin="normal"
                        error={errors.netweight && (touched.netweight)}
                        FormHelperTextProps={{
                            error: errors.netweight && (touched.netweight)
                        }}
                        helperText={
                            errors.netweight && (touched.netweight)
                                ? errors.netweight
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <DatePicker
                        className={classes.datepicker}
                        keyboard
                        name="samplinginspectionduedate"
                        label="Sampling Inspection Due Date"
                        placeholder="10/10/2018"
                        value={values.samplinginspectionduedate}
                        onChange={(date) => {
                            setFieldValue("samplinginspectionduedate", date)
                        }}
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                </Grid>

                <Grid item xs={6}>
                    <DatePicker
                        className={classes.datepicker}
                        keyboard
                        name="rawmaterialdate"
                        label="Raw Material Insp. Due Date"
                        placeholder="10/10/2018"
                        value={values.rawmaterialdate}
                        onChange={(date) => {
                            setFieldValue("rawmaterialdate", date)
                        }}
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                </Grid>

                <Grid item xs={6}>
                    <DatePicker
                        className={classes.datepicker}
                        keyboard
                        name="periodicaltestingduedate"
                        label="Periodical Testing Due Date"
                        placeholder="10/10/2018"
                        value={values.periodicaltestingduedate}
                        onChange={(date) => {
                            setFieldValue("periodicaltestingduedate", date)
                        }}
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="inspectionforallparts-simple">Inspection For All Parts</InputLabel>
                        <Select
                            value={values.inspectionforallparts}
                            onChange={handleChange}
                            inputProps={{
                                name: 'inspectionforallparts',
                                id: 'inspectionforallparts-simple',
                            }}
                            error={errors.inspectionforallparts && (touched.inspectionforallparts)}
                            FormHelperTextProps={{
                                error: errors.inspectionforallparts && (touched.inspectionforallparts)
                            }}
                            helperText={
                                errors.inspectionforallparts && (touched.inspectionforallparts)
                                    ? errors.inspectionforallparts
                                    : null
                            }
                        >
                            <MenuItem value={0}>YES</MenuItem>
                            <MenuItem value={1}>NO</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="defaultinspector-simple">Default Inspector</InputLabel>
                        <Select
                            value={values.defaultinspector}
                            onChange={handleChange}
                            inputProps={{
                                name: 'defaultinspector',
                                id: 'defaultinspector-simple',
                            }}
                            error={errors.defaultinspector && (touched.defaultinspector)}
                            FormHelperTextProps={{
                                error: errors.defaultinspector && (touched.defaultinspector)
                            }}
                            helperText={
                                errors.defaultinspector && (touched.defaultinspector)
                                    ? errors.defaultinspector
                                    : null
                            }
                        >
                            <MenuItem value={0}>34</MenuItem>
                            <MenuItem value={1}>Demo</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        name="drawingno"
                        placeholder="Enter Drawing No"
                        label="Drawing No"
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.drawingno}
                        margin="normal"
                        error={errors.drawingno && (touched.drawingno)}
                        FormHelperTextProps={{
                            error: errors.drawingno && (touched.drawingno)
                        }}
                        helperText={
                            errors.drawingno && (touched.drawingno)
                                ? errors.drawingno
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        name="drawingrevisionno"
                        placeholder="Enter Drawing Revision No"
                        label="Drawing Revision No"
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.drawingrevisionno}
                        margin="normal"
                        error={errors.drawingrevisionno && (touched.drawingrevisionno)}
                        FormHelperTextProps={{
                            error: errors.drawingrevisionno && (touched.drawingrevisionno)
                        }}
                        helperText={
                            errors.drawingrevisionno && (touched.drawingrevisionno)
                                ? errors.drawingrevisionno
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="partstatus-simple">Part Status</InputLabel>
                        <Select
                            value={values.partstatus}
                            onChange={handleChange}
                            inputProps={{
                                name: 'partstatus',
                                id: 'partstatus-simple',
                            }}
                            error={errors.partstatus && (touched.partstatus)}
                            FormHelperTextProps={{
                                error: errors.partstatus && (touched.partstatus)
                            }}
                            helperText={
                                errors.partstatus && (touched.partstatus)
                                    ? errors.partstatus
                                    : null
                            }
                        >
                            <MenuItem value={0}>3 Whlr</MenuItem>
                            <MenuItem value={1}>Welding Assembly</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <Typography variant="subtitle1">PPAP Document : </Typography>
                        <input type="file" onChange={this.fileChangedHandler} />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        name="documentname"
                        placeholder="Enter Document Name"
                        label="Document Name"
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.documentname}
                        margin="normal"
                        error={errors.documentname && (touched.documentname)}
                        FormHelperTextProps={{
                            error: errors.documentname && (touched.documentname)
                        }}
                        helperText={
                            errors.documentname && (touched.documentname)
                                ? errors.documentname
                                : null
                        }
                    />
                </Grid>
            </Grid>

            <Grid item xs={4}>
                <Button variant="contained" type="submit" color="primary" className={classes.button}>
                    Save
  </Button>
            </Grid>
            <Grid item xs={4}>
                <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                    Cancel
                </Button>
            </Grid>
            <Grid item xs={4}>
                <Button type="submit" variant="contained" color="default" className={classes.button}>
                    View PPAP Document
                </Button>
            </Grid>

        </form >
    );
})

const EnhancedRegisterForm = withFormik({
    mapPropsToValues: () => ({ id: '', name: '', dolstatus: '', ppm: '', supplier1: '', supplier2: '', supplier3: '', supplier4: '', supplier5: '', product: '', model: '', handlingunit: '', inspectiontype: '', rm: '', netweight: '', samplinginspectionduedate: new Date(), rawmaterialdate: new Date(), periodicaltestingduedate: new Date(), inspectionforallparts: '', defaultinspector: '', drawingno: '', drawingrevisionno: '', partstatus: '', documentname: '' }),
    validationSchema: Yup.object().shape({
        id: Yup.string("ID is required and should be a String").required("ID Required"),
        name: Yup.string("Name is required and should be a String").required("Name is required"),
        supplier1: Yup.string("Supplier 1 is required and should be a String").required("Supplier 1 is required"),
        product: Yup.string("Product is required and should be a String").required("Product is required")
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'BasicForm',
})(RegisterForm)

// Process Document

const ProcessDocumentForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue
    } = props
    return (

        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">

            <Typography variant="h6">Process Flow :</Typography>
            <Fragment>
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <TextField
                                name="flowno"
                                placeholder="Enter No"
                                label="No"
                                className={classes.textField}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.flowno}
                                error={errors.flowno && (touched.flowno)}
                                FormHelperTextProps={{
                                    error: errors.flowno && (touched.flowno)
                                }}
                                helperText={
                                    errors.flowno && (touched.flowno)
                                        ? errors.flowno
                                        : null
                                }
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <DatePicker
                                className={classes.datepicker}
                                keyboard
                                name="flowcreatedondate"
                                label="Created On"
                                placeholder="10/10/2018"
                                value={values.flowcreatedondate}
                                onChange={(date) => {
                                    setFieldValue("flowcreatedondate", date)
                                }}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <TextField
                                name="flowrevisionno"
                                placeholder="Enter Revision No"
                                label="Revision No"
                                className={classes.textField}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.flowrevisionno}
                                error={errors.flowrevisionno && (touched.flowrevisionno)}
                                FormHelperTextProps={{
                                    error: errors.flowrevisionno && (touched.flowrevisionno)
                                }}
                                helperText={
                                    errors.flowrevisionno && (touched.flowrevisionno)
                                        ? errors.flowrevisionno
                                        : null
                                }
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <DatePicker
                                className={classes.datepicker}
                                keyboard
                                name="flowrevisiondate"
                                label="Revision Date"
                                placeholder="10/10/2018"
                                value={values.flowrevisiondate}
                                onChange={(date) => {
                                    setFieldValue("flowrevisiondate", date)
                                }}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </Grid>
                    </Grid>
                </Grid>
            </Fragment>

            <Typography variant="h6">Process FMEA :</Typography>
            <Fragment>
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <TextField
                                name="fmeano"
                                placeholder="Enter No"
                                label="No"
                                className={classes.textField}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.fmeano}
                                error={errors.fmeano && (touched.fmeano)}
                                FormHelperTextProps={{
                                    error: errors.fmeano && (touched.fmeano)
                                }}
                                helperText={
                                    errors.fmeano && (touched.fmeano)
                                        ? errors.fmeano
                                        : null
                                }
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <DatePicker
                                className={classes.datepicker}
                                keyboard
                                name="fmeacreatedondate"
                                label="Created On"
                                placeholder="10/10/2018"
                                value={values.fmeacreatedondate}
                                onChange={(date) => {
                                    setFieldValue("fmeacreatedondate", date)
                                }}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <TextField
                                name="fmearevisionno"
                                placeholder="Enter Revision No"
                                label="Revision No"
                                className={classes.textField}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.fmearevisionno}
                                margin="normal"
                                error={errors.fmearevisionno && (touched.fmearevisionno)}
                                FormHelperTextProps={{
                                    error: errors.fmearevisionno && (touched.fmearevisionno)
                                }}
                                helperText={
                                    errors.fmearevisionno && (touched.fmearevisionno)
                                        ? errors.fmearevisionno
                                        : null
                                }
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <DatePicker
                                className={classes.datepicker}
                                keyboard
                                name="fmearevisiondate"
                                label="Revision Date"
                                placeholder="10/10/2018"
                                value={values.fmearevisiondate}
                                onChange={(date) => {
                                    setFieldValue("fmearevisiondate", date)
                                }}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </Grid>
                    </Grid>
                </Grid>
            </Fragment>

            <Typography variant="h6">Process Control Plan - Prototype :</Typography>
            <Fragment>
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <TextField
                                name="prototypeno"
                                placeholder="Enter No"
                                label="No"
                                className={classes.textField}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.prototypeno}
                                error={errors.prototypeno && (touched.prototypeno)}
                                FormHelperTextProps={{
                                    error: errors.prototypeno && (touched.prototypeno)
                                }}
                                helperText={
                                    errors.prototypeno && (touched.prototypeno)
                                        ? errors.prototypeno
                                        : null
                                }
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <DatePicker
                                className={classes.datepicker}
                                keyboard
                                name="prototypecreatedondate"
                                label="Created On"
                                placeholder="10/10/2018"
                                value={values.prototypecreatedondate}
                                onChange={(date) => {
                                    setFieldValue("prototypecreatedondate", date)
                                }}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <TextField
                                name="prototyperevisionno"
                                placeholder="Enter Revision No"
                                label="Revision No"
                                className={classes.textField}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.prototyperevisionno}
                                margin="normal"
                                error={errors.prototyperevisionno && (touched.prototyperevisionno)}
                                FormHelperTextProps={{
                                    error: errors.prototyperevisionno && (touched.prototyperevisionno)
                                }}
                                helperText={
                                    errors.prototyperevisionno && (touched.prototyperevisionno)
                                        ? errors.prototyperevisionno
                                        : null
                                }
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <DatePicker
                                className={classes.datepicker}
                                keyboard
                                name="prototyperevisiondate"
                                label="Revision Date"
                                placeholder="10/10/2018"
                                value={values.prototyperevisiondate}
                                onChange={(date) => {
                                    setFieldValue("prototyperevisiondate", date)
                                }}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </Grid>
                    </Grid>
                </Grid>
            </Fragment>

            <Typography variant="h6">Process Control Plan - Production :</Typography>
            <Fragment>
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <TextField
                                name="productionno"
                                placeholder="Enter No"
                                label="No"
                                className={classes.textField}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.productionno}
                                error={errors.productionno && (touched.productionno)}
                                FormHelperTextProps={{
                                    error: errors.productionno && (touched.productionno)
                                }}
                                helperText={
                                    errors.productionno && (touched.productionno)
                                        ? errors.productionno
                                        : null
                                }
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <DatePicker
                                className={classes.datepicker}
                                keyboard
                                name="productioncreatedondate"
                                label="Created On"
                                placeholder="10/10/2018"
                                value={values.productioncreatedondate}
                                onChange={(date) => {
                                    setFieldValue("productioncreatedondate", date)
                                }}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <TextField
                                name="productionrevisionno"
                                placeholder="Enter Revision No"
                                label="Revision No"
                                className={classes.textField}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.productionrevisionno}
                                error={errors.productionrevisionno && (touched.productionrevisionno)}
                                FormHelperTextProps={{
                                    error: errors.productionrevisionno && (touched.productionrevisionno)
                                }}
                                helperText={
                                    errors.productionrevisionno && (touched.productionrevisionno)
                                        ? errors.productionrevisionno
                                        : null
                                }
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <DatePicker
                                className={classes.datepicker}
                                keyboard
                                name="productionrevisiondate"
                                label="Revision Date"
                                placeholder="10/10/2018"
                                value={values.productionrevisiondate}
                                onChange={(date) => {
                                    setFieldValue("productionrevisiondate", date)
                                }}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </Grid>
                    </Grid>
                </Grid>
            </Fragment>

            <Typography variant="h6">Process Control Plan - Pre Launch :</Typography>
            <Fragment>
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <TextField
                                name="prelaunchno"
                                placeholder="Enter No"
                                label="No"
                                className={classes.textField}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.prelaunchno}
                                error={errors.prelaunchno && (touched.prelaunchno)}
                                FormHelperTextProps={{
                                    error: errors.prelaunchno && (touched.prelaunchno)
                                }}
                                helperText={
                                    errors.prelaunchno && (touched.prelaunchno)
                                        ? errors.prelaunchno
                                        : null
                                }
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <DatePicker
                                className={classes.datepicker}
                                keyboard
                                name="prelaunchcreatedondate"
                                label="Created On"
                                placeholder="10/10/2018"
                                value={values.prelaunchcreatedondate}
                                onChange={(date) => {
                                    setFieldValue("prelaunchcreatedondate", date)
                                }}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <TextField
                                name="prelaunchrevisionno"
                                placeholder="Enter Revision No"
                                label="Revision No"
                                className={classes.textField}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.prelaunchrevisionno}
                                margin="normal"
                                error={errors.prelaunchrevisionno && (touched.prelaunchrevisionno)}
                                FormHelperTextProps={{
                                    error: errors.prelaunchrevisionno && (touched.prelaunchrevisionno)
                                }}
                                helperText={
                                    errors.prelaunchrevisionno && (touched.prelaunchrevisionno)
                                        ? errors.prelaunchrevisionno
                                        : null
                                }
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={6}>
                        <Grid container className={classes.grid} justify="space-around">
                            <DatePicker
                                className={classes.datepicker}
                                keyboard
                                name="prelaunchrevisiondate"
                                label="Revision Date"
                                placeholder="10/10/2018"
                                value={values.prelaunchrevisiondate}
                                onChange={(date) => {
                                    setFieldValue("prelaunchrevisiondate", date)
                                }}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </Grid>
                    </Grid>
                </Grid>
            </Fragment>

            <Button variant="contained" type="submit" color="primary" className={classes.button}>
                Save
            </Button>
            <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                Cancel
            </Button>
        </form>
    );
})

const EnhancedProcessDocumentForm = withFormik({
    mapPropsToValues: () => ({ flowno: '', flowcreatedondate: new Date(), flowrevisionno: '', flowrevisiondate: new Date(), fmeano: '', fmeacreatedondate: new Date(), fmearevisionno: '', fmearevisiondate: new Date(), prototypeno: '', prototypecreatedondate: new Date(), prototyperevisionno: '', prototyperevisiondate: new Date(), productionno: '', productioncreatedondate: new Date(), productionrevisionno: '', productionrevisiondate: new Date(), prelaunchno: '', prelaunchcreatedondate: new Date(), prelaunchrevisionno: '', prelaunchrevisiondate: new Date() }),
    validationSchema: Yup.object().shape({
        flowno: Yup.string("Flow no is required and should be a String").required("Flow no Required"),
        flowrevisionno: Yup.string("Flow Revision no is required and should be a String").required("Flow Revision no Required"),
        fmeano: Yup.string("FMEA no is required and should be a String").required("FMEA no Required"),
        fmearevisionno: Yup.string("FMEA Revision no is required and should be a String").required("FMEA Revision no Required"),
        prototypeno: Yup.string("Prototype no is required and should be a String").required("Prototype no Required"),
        prototyperevisionno: Yup.string("Prototype Revision no is required and should be a String").required("Prototype Revision no Required"),
        productionno: Yup.string("Production no is required and should be a String").required("Production no Required"),
        productionrevisionno: Yup.string("Production Revision no is required and should be a String").required("Production Revision no Required"),
        prelaunchno: Yup.string("Pre Launch no is required and should be a String").required("Pre Launch no Required"),
        prelaunchrevisionno: Yup.string("Pre Launch Revision no is required and should be a String").required("Pre Launch Revision no Required"),
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'BasicForm',
})(ProcessDocumentForm)

// Part Document

const PartDocumentForm = withStyles(styles)((props) => {
    const {
        handleSubmit,
        classes,
        handleReset
    } = props
    return (

        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">

            <Typography variant="h6" className={classes.typoGraphy}>Relation Gauge :</Typography>
            <Fragment>
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid className={classes.file}>
                        <input type="file" onChange={this.fileChangedHandler} />
                    </Grid>
                </Grid>
            </Fragment>

            <Typography variant="h6" className={classes.typoGraphy}>Drawing :</Typography>
            <Fragment>
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid className={classes.file}>
                        <input type="file" onChange={this.fileChangedHandler} />
                    </Grid>
                </Grid>
            </Fragment>

            <Typography variant="h6" className={classes.typoGraphy}>Checking Sequence :</Typography>
            <Fragment>
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid className={classes.file}>
                        <input type="file" onChange={this.fileChangedHandler} />
                    </Grid>
                </Grid>
            </Fragment>

            <Typography variant="h6" className={classes.typoGraphy}>Part Picture :</Typography>
            <Fragment>
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid className={classes.file}>
                        <input type="file" onChange={this.fileChangedHandler} />
                    </Grid>
                </Grid>
            </Fragment>

            <Typography variant="h6" className={classes.typoGraphy}>Checking Method Animation :</Typography>
            <Fragment>
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid className={classes.file}>
                        <input type="file" onChange={this.fileChangedHandler} />
                    </Grid>
                </Grid>
            </Fragment>

            <Typography variant="h6" className={classes.typoGraphy}>Inspection Agreement for Part :</Typography>
            <Fragment>
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid className={classes.file}>
                        <input type="file" onChange={this.fileChangedHandler} />
                    </Grid>
                </Grid>
            </Fragment>

            <Typography variant="h6" className={classes.typoGraphy}>CTQ Parameter Document :</Typography>
            <Fragment>
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid className={classes.file}>
                        <input type="file" onChange={this.fileChangedHandler} />
                    </Grid>
                </Grid>
            </Fragment>

            <Button variant="contained" type="submit" color="primary" className={classes.button}>
                Save
            </Button>
            <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                Cancel
            </Button>
        </form>
    );
})

const EnhancedPartDocumentForm = withFormik({
    mapPropsToValues: () => ({ designation: '' }),
    validationSchema: Yup.object().shape({
        description: Yup.string("Description is required and should be a String").required("Description Required")
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'BasicForm',
})(PartDocumentForm)

// Inspection Parameter

const InspectionParameterForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset
    } = props
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    name="partid"
                    placeholder="Enter Part ID"
                    label="Part ID"
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.partid}
                    margin="normal"
                    error={errors.partid && (touched.partid)}
                    FormHelperTextProps={{
                        error: errors.partid && (touched.partid)
                    }}
                    helperText={
                        errors.partid && (touched.partid)
                            ? errors.partid
                            : null
                    }
                />
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    name="description"
                    placeholder="Enter Description"
                    label="Description"
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.description}
                    margin="normal"
                    error={errors.description && (touched.description)}
                    FormHelperTextProps={{
                        error: errors.description && (touched.description)
                    }}
                    helperText={
                        errors.description && (touched.description)
                            ? errors.description
                            : null
                    }
                />
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <FormControl component="fieldset" className={classes.formControl}>
                    <FormLabel component="paramtertype" style={{ marginTop: "20px" }}>Paramter Type</FormLabel>
                    <RadioGroup
                        aria-label="Paramter Type"
                        name="paramtertype"
                        className={classes.group}
                        value={values.paramtertype}
                        onChange={this.handleChange}
                    >
                        <FormControlLabel value="visual" control={<Radio />} label="Visual" />
                        <FormControlLabel value="dimensional" control={<Radio />} label="Dimensional" />
                        <FormControlLabel value="testing" control={<Radio />} label="Testing" />
                    </RadioGroup>
                </FormControl>
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    style={{ marginTop: "-10px" }}
                    name="parameter"
                    placeholder="Enter Parameter"
                    label="Parameter"
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.parameter}
                    error={errors.parameter && (touched.parameter)}
                    FormHelperTextProps={{
                        error: errors.parameter && (touched.parameter)
                    }}
                    helperText={
                        errors.parameter && (touched.parameter)
                            ? errors.parameter
                            : null
                    }
                />
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    name="specification"
                    placeholder="Enter Specification"
                    label="Specification"
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.specification}
                    error={errors.specification && (touched.specification)}
                    FormHelperTextProps={{
                        error: errors.specification && (touched.specification)
                    }}
                    helperText={
                        errors.specification && (touched.specification)
                            ? errors.specification
                            : null
                    }
                />
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    name="inspectionmethod"
                    placeholder="Enter Inspection Method"
                    label="Inspection Method"
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.inspectionmethod}
                    error={errors.inspectionmethod && (touched.inspectionmethod)}
                    FormHelperTextProps={{
                        error: errors.inspectionmethod && (touched.inspectionmethod)
                    }}
                    helperText={
                        errors.inspectionmethod && (touched.inspectionmethod)
                            ? errors.inspectionmethod
                            : null
                    }
                />
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <FormControl component="fieldset" className={classes.formControl}>
                    <FormLabel component="datatype" style={{ marginTop: "20px" }}>Data Type</FormLabel>
                    <RadioGroup
                        aria-label="Data Type"
                        name="datatype"
                        className={classes.group}
                        value={values.datatype}
                        onChange={this.handleChange}
                    >
                        <FormControlLabel value="attribute" control={<Radio />} label="Attribute" />
                        <FormControlLabel value="variable" control={<Radio />} label="Variable" />
                    </RadioGroup>
                </FormControl>
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="inspectionfrequency-simple" style={{ marginTop: "-10px" }}>Inspection Frequency</InputLabel>
                    <Select
                        value={values.inspectionfrequency}
                        onChange={handleChange}
                        inputProps={{
                            name: 'inspectionfrequency',
                            id: 'inspectionfrequency-simple',
                        }}
                        error={errors.inspectionfrequency && (touched.inspectionfrequency)}
                        FormHelperTextProps={{
                            error: errors.inspectionfrequency && (touched.inspectionfrequency)
                        }}
                        helperText={
                            errors.inspectionfrequency && (touched.inspectionfrequency)
                                ? errors.inspectionfrequency
                                : null
                        }
                    >
                        <MenuItem value={0}>Testing</MenuItem>
                    </Select>
                </FormControl>
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    name="lowertolerancelimit"
                    placeholder="Enter Lower Tolerance Limit (LTL)"
                    label="Lower Tolerance Limit (LTL)"
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.lowertolerancelimit}
                    error={errors.lowertolerancelimit && (touched.lowertolerancelimit)}
                    FormHelperTextProps={{
                        error: errors.lowertolerancelimit && (touched.lowertolerancelimit)
                    }}
                    helperText={
                        errors.lowertolerancelimit && (touched.lowertolerancelimit)
                            ? errors.lowertolerancelimit
                            : null
                    }
                />
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    name="uppertolerancelimit"
                    placeholder="Enter Upper Tolerance Limit (UTL)"
                    label="Upper Tolerance Limit (UTL)"
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.uppertolerancelimit}
                    error={errors.uppertolerancelimit && (touched.uppertolerancelimit)}
                    FormHelperTextProps={{
                        error: errors.uppertolerancelimit && (touched.uppertolerancelimit)
                    }}
                    helperText={
                        errors.uppertolerancelimit && (touched.uppertolerancelimit)
                            ? errors.uppertolerancelimit
                            : null
                    }
                />
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    name="pdiparamid"
                    placeholder="Enter PDI Param ID"
                    label="PDI Param ID"
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.pdiparamid}
                    error={errors.pdiparamid && (touched.pdiparamid)}
                    FormHelperTextProps={{
                        error: errors.pdiparamid && (touched.pdiparamid)
                    }}
                    helperText={
                        errors.pdiparamid && (touched.pdiparamid)
                            ? errors.pdiparamid
                            : null
                    }
                />
            </Grid>

            <Button variant="contained" type="submit" color="primary" className={classes.button}>
                Save
            </Button>
            <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                Cancel
            </Button>
        </form>
    );
})

const EnhancedInspectionParameterForm = withFormik({
    mapPropsToValues: () => ({ partid: '', description: '', parameter: '', specification: '', inspectionmethod: '', inspectionfrequency: '', lowertolerancelimit: '', uppertolerancelimit: '', pdiparamid: '' }),
    validationSchema: Yup.object().shape({
        parameter: Yup.string("Paramter is required and should be a String").required("Paramter Required"),
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'BasicForm',
})(InspectionParameterForm)

// Checking Sequence

const CheckingSequenceForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset
    } = props
    return (

        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    name="partid"
                    placeholder="Enter Part ID"
                    label="Part ID"
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.partid}
                    error={errors.partid && (touched.partid)}
                    FormHelperTextProps={{
                        error: errors.partid && (touched.partid)
                    }}
                    helperText={
                        errors.partid && (touched.partid)
                            ? errors.partid
                            : null
                    }
                />
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    name="description"
                    placeholder="Enter Description"
                    label="Description"
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.description}
                    error={errors.description && (touched.description)}
                    FormHelperTextProps={{
                        error: errors.description && (touched.description)
                    }}
                    helperText={
                        errors.description && (touched.description)
                            ? errors.description
                            : null
                    }
                />
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    id="checkingsequence"
                    label="Checking Sequence"
                    multiline
                    rows="1"
                    value={values.checkingsequence}
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.checkingsequence && (touched.checkingsequence)}
                    FormHelperTextProps={{
                        error: errors.checkingsequence && (touched.checkingsequence)
                    }}
                    helperText={
                        errors.checkingsequence && (touched.checkingsequence)
                            ? errors.checkingsequence
                            : null
                    }
                />
            </Grid>

            <Button variant="contained" type="submit" color="primary" className={classes.button}>
                Save
            </Button>
            <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                Cancel
            </Button>
        </form>
    );
})

const EnhancedCheckingSequenceForm = withFormik({
    mapPropsToValues: () => ({ partid: '', description: '', checkingsequence: '' }),
    validationSchema: Yup.object().shape({
        checkingsequence: Yup.string("Checking Sequence is required and should be a String").required("Checking Sequence Required"),
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'BasicForm',
})(CheckingSequenceForm)

// Deviation

const DeviationForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue
    } = props
    return (

        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">

            <Grid
                container
                direction="row"
                alignItems="center"
            >
                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="supplier-simple">Supplier</InputLabel>
                        <Select
                            value={values.supplier}
                            onChange={handleChange}
                            inputProps={{
                                name: 'supplier',
                                id: 'supplier-simple',
                            }}
                            error={errors.supplier && (touched.supplier)}
                            FormHelperTextProps={{
                                error: errors.supplier && (touched.supplier)
                            }}
                            helperText={
                                errors.supplier && (touched.supplier)
                                    ? errors.supplier
                                    : null
                            }
                        >
                            <MenuItem value={0}>Supplier 1</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <TextField
                            name="lotquantity"
                            placeholder="Enter Lot Quantity"
                            label="Lot Quantity"
                            className={classes.textField}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.lotquantity}
                            margin="normal"
                            error={errors.lotquantity && (touched.lotquantity)}
                            FormHelperTextProps={{
                                error: errors.lotquantity && (touched.lotquantity)
                            }}
                            helperText={
                                errors.lotquantity && (touched.lotquantity)
                                    ? errors.lotquantity
                                    : null
                            }
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <TextField
                            name="quantityrejected"
                            placeholder="Enter Quantity Rejected"
                            label="Quantity Rejected"
                            classquantityrejected={classes.textField}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.quantityrejected}
                            margin="normal"
                            error={errors.quantityrejected && (touched.quantityrejected)}
                            FormHelperTextProps={{
                                error: errors.quantityrejected && (touched.quantityrejected)
                            }}
                            helperText={
                                errors.quantityrejected && (touched.quantityrejected)
                                    ? errors.quantityrejected
                                    : null
                            }
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <DatePicker
                            className={classes.datepicker}
                            keyboard
                            name="date"
                            label="Date Of Deviation"
                            placeholder="10/10/2018"
                            value={values.date}
                            onChange={(date) => {
                                setFieldValue("date", date)
                            }}
                            disableOpenOnEnter
                            animateYearScrolling={false}
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <TextField
                            name="deviationno"
                            placeholder="Enter Deviation No"
                            label="Deviation No"
                            classdeviationno={classes.textField}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.deviationno}
                            margin="normal"
                            error={errors.deviationno && (touched.deviationno)}
                            FormHelperTextProps={{
                                error: errors.deviationno && (touched.deviationno)
                            }}
                            helperText={
                                errors.deviationno && (touched.deviationno)
                                    ? errors.deviationno
                                    : null
                            }
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <DatePicker
                            className={classes.datepicker}
                            keyboard
                            name="date"
                            label="Lot Receipt Date"
                            placeholder="10/10/2018"
                            value={values.date}
                            onChange={(date) => {
                                setFieldValue("date", date)
                            }}
                            disableOpenOnEnter
                            animateYearScrolling={false}
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <TextField
                            name="invoiceno"
                            placeholder="Enter Invoice No"
                            label="Invoice No"
                            classinvoiceno={classes.textField}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.invoiceno}
                            margin="normal"
                            error={errors.invoiceno && (touched.invoiceno)}
                            FormHelperTextProps={{
                                error: errors.invoiceno && (touched.invoiceno)
                            }}
                            helperText={
                                errors.invoiceno && (touched.invoiceno)
                                    ? errors.invoiceno
                                    : null
                            }
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <DatePicker
                            className={classes.datepicker}
                            keyboard
                            name="date"
                            label="Close Date"
                            placeholder="10/10/2018"
                            value={values.date}
                            onChange={(date) => {
                                setFieldValue("date", date)
                            }}
                            disableOpenOnEnter
                            animateYearScrolling={false}
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={12}>
                    <Grid container className={classes.grid} justify="left">
                        <FormControl className={classes.formControl}>
                            <TextField
                                name="requiredqty"
                                placeholder="Enter Required Quality"
                                label="Required Quality"
                                classrequiredqty={classes.textField}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.requiredqty}
                                margin="normal"
                                error={errors.requiredqty && (touched.requiredqty)}
                                FormHelperTextProps={{
                                    error: errors.requiredqty && (touched.requiredqty)
                                }}
                                helperText={
                                    errors.requiredqty && (touched.requiredqty)
                                        ? errors.requiredqty
                                        : null
                                }
                            />
                        </FormControl>
                    </Grid>
                </Grid>

                <Grid item xs={2}>
                    <TextField
                        style={{ margin: "2px" }}
                        id="parameter1"
                        label="Paramter1"
                        multiline
                        rows="1"
                        value={values.parameter1}
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={errors.parameter1 && (touched.parameter1)}
                        FormHelperTextProps={{
                            error: errors.parameter1 && (touched.parameter1)
                        }}
                        helperText={
                            errors.parameter1 && (touched.parameter1)
                                ? errors.parameter1
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={2}>
                    <TextField
                        style={{ margin: "2px" }}
                        id="specification1"
                        label="Specification1"
                        multiline
                        rows="1"
                        value={values.specification1}
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={errors.specification1 && (touched.specification1)}
                        FormHelperTextProps={{
                            error: errors.specification1 && (touched.specification1)
                        }}
                        helperText={
                            errors.specification1 && (touched.specification1)
                                ? errors.specification1
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={2}>
                    <TextField
                        style={{ margin: "2px" }}
                        id="reason1"
                        label="Reason1"
                        multiline
                        rows="1"
                        value={values.reason1}
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        // variant="outlined"
                        error={errors.reason1 && (touched.reason1)}
                        FormHelperTextProps={{
                            error: errors.reason1 && (touched.reason1)
                        }}
                        helperText={
                            errors.reason1 && (touched.reason1)
                                ? errors.reason1
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={2}>
                    <Grid container className={classes.grid} justify="space-around">
                        <TextField
                            style={{ margin: "2px" }}
                            id="impact1"
                            label="Impact1"
                            multiline
                            rows="1"
                            value={values.impact1}
                            className={classes.textField}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            margin="normal"
                            // variant="outlined"
                            error={errors.impact1 && (touched.impact1)}
                            FormHelperTextProps={{
                                error: errors.impact1 && (touched.impact1)
                            }}
                            helperText={
                                errors.impact1 && (touched.impact1)
                                    ? errors.impact1
                                    : null
                            }
                        />
                    </Grid>
                </Grid>

                <Grid item xs={4}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="customerapproval-simple">Customer Approval</InputLabel>
                        <Select
                            value={values.customerapproval}
                            onChange={handleChange}
                            inputProps={{
                                name: 'customerapproval',
                                id: 'customerapproval-simple',
                            }}
                            error={errors.customerapproval && (touched.customerapproval)}
                            FormHelperTextProps={{
                                error: errors.customerapproval && (touched.customerapproval)
                            }}
                            helperText={
                                errors.customerapproval && (touched.customerapproval)
                                    ? errors.customerapproval
                                    : null
                            }
                        >
                            <MenuItem value={0}>YES</MenuItem>
                            <MenuItem value={1}>NO</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        id="rootcause"
                        label="Root Cause"
                        multiline
                        rows="1"
                        value={values.rootcause}
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        variant="outlined"
                        error={errors.rootcause && (touched.rootcause)}
                        FormHelperTextProps={{
                            error: errors.rootcause && (touched.rootcause)
                        }}
                        helperText={
                            errors.rootcause && (touched.rootcause)
                                ? errors.rootcause
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        id="correctiveaction"
                        label="Corrective Action"
                        multiline
                        rows="1"
                        value={values.correctiveaction}
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        variant="outlined"
                        error={errors.correctiveaction && (touched.correctiveaction)}
                        FormHelperTextProps={{
                            error: errors.correctiveaction && (touched.correctiveaction)
                        }}
                        helperText={
                            errors.correctiveaction && (touched.correctiveaction)
                                ? errors.correctiveaction
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        id="responsibility"
                        label="Responsibility"
                        multiline
                        rows="1"
                        value={values.responsibility}
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        variant="outlined"
                        error={errors.responsibility && (touched.responsibility)}
                        FormHelperTextProps={{
                            error: errors.responsibility && (touched.responsibility)
                        }}
                        helperText={
                            errors.responsibility && (touched.responsibility)
                                ? errors.responsibility
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        id="remark"
                        label="Remark"
                        multiline
                        rows="1"
                        value={values.remark}
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        variant="outlined"
                        error={errors.remark && (touched.remark)}
                        FormHelperTextProps={{
                            error: errors.remark && (touched.remark)
                        }}
                        helperText={
                            errors.remark && (touched.remark)
                                ? errors.remark
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <DatePicker
                            className={classes.datepicker}
                            keyboard
                            name="date"
                            label="Approval Date"
                            placeholder="10/10/2018"
                            value={values.date}
                            onChange={(date) => {
                                setFieldValue("date", date)
                            }}
                            disableOpenOnEnter
                            animateYearScrolling={false}
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <FormGroup row>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={values.checkedA}
                                        onChange={handleChange('checkedA')}
                                        value="checkedA"
                                        color="primary"
                                    />
                                }
                                label="Kaizen Done"
                            />
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={values.checkedB}
                                        onChange={handleChange('checkedB')}
                                        value="checkedB"
                                        color="primary"
                                    />
                                }
                                label="Document Change"
                            />
                        </FormGroup>
                    </FormControl>
                </Grid>

            </Grid>

            <Button variant="contained" type="submit" color="primary" className={classes.button}>
                Save
            </Button>
            <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                Cancel
            </Button>
        </form>
    );
})

const EnhancedDeviationForm = withFormik({
    mapPropsToValues: () => ({ supplier: '', lotquantity: '', quantityrejected: '', invoiceno: '', requiredqty: '' }),
    validationSchema: Yup.object().shape({
        supplier: Yup.string("Supplier is required and should be a String").required("Supplier Required"),
        lotquantity: Yup.string("Lot Quantity is required and should be a String").required("Lot Quantity Required"),
        quantityrejected: Yup.string("Quantity Rejected is required and should be a String").required("Quantity Rejected Required"),
        invoiceno: Yup.string("Invoice No is required and should be a String").required("Invoice No Required"),
        requiredqty: Yup.string("Required Qty is required and should be a String").required("Required Qty Required"),
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'BasicForm',
})(DeviationForm)

// Raw Material

const RawMaterialForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue
    } = props
    return (

        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid container className={classes.grid} justify="space-around">
                <DatePicker
                    className={classes.datepicker}
                    keyboard
                    name="date"
                    label="Date"
                    placeholder="10/10/2018"
                    value={values.date}
                    onChange={(date) => {
                        setFieldValue("date", date)
                    }}
                    disableOpenOnEnter
                    animateYearScrolling={false}
                />
            </Grid>

            <FormControl className={classes.formControl}>
                <Typography variant="subtitle1">Certificate File : </Typography>
                <input type="file" onChange={this.fileChangedHandler} />
            </FormControl>

            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    id="description"
                    label="Description"
                    multiline
                    rows="1"
                    value={values.description}
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.description && (touched.description)}
                    FormHelperTextProps={{
                        error: errors.description && (touched.description)
                    }}
                    helperText={
                        errors.description && (touched.description)
                            ? errors.description
                            : null
                    }
                />
            </Grid>

            <Button variant="contained" type="submit" color="primary" className={classes.button}>
                Save
            </Button>
            <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                Cancel
            </Button>
        </form>
    );
})

const EnhancedRawMaterialForm = withFormik({
    mapPropsToValues: () => ({ date: new Date(), description: '' }),
    validationSchema: Yup.object().shape({
        description: Yup.string("Description is required and should be a String").required("Description Required")
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'BasicForm',
})(RawMaterialForm)

// Sampling Inspection

const SamplingInspectionForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue
    } = props
    return (

        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid container className={classes.grid} justify="space-around">
                <DatePicker
                    className={classes.datepicker}
                    keyboard
                    name="date"
                    label="Date"
                    placeholder="10/10/2018"
                    value={values.date}
                    onChange={(date) => {
                        setFieldValue("date", date)
                    }}
                    disableOpenOnEnter
                    animateYearScrolling={false}
                />
            </Grid>

            <FormControl className={classes.formControl}>
                <Typography variant="subtitle1">Certificate File : </Typography>
                <input type="file" onChange={this.fileChangedHandler} />
            </FormControl>

            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    id="description"
                    label="Description"
                    multiline
                    rows="1"
                    value={values.description}
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.description && (touched.description)}
                    FormHelperTextProps={{
                        error: errors.description && (touched.description)
                    }}
                    helperText={
                        errors.description && (touched.description)
                            ? errors.description
                            : null
                    }
                />
            </Grid>

            <Button variant="contained" type="submit" color="primary" className={classes.button}>
                Save
            </Button>
            <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                Cancel
            </Button>
        </form>
    );
})

const EnhancedSamplingInspectionForm = withFormik({
    mapPropsToValues: () => ({ date: new Date(), description: '' }),
    validationSchema: Yup.object().shape({
        description: Yup.string("Description is required and should be a String").required("Description Required")
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'BasicForm',
})(SamplingInspectionForm)

// Periodical Testing

const PeriodicalTestingForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue
    } = props
    return (

        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid container className={classes.grid} justify="space-around">
                <DatePicker
                    className={classes.datepicker}
                    keyboard
                    name="date"
                    label="Date"
                    placeholder="10/10/2018"
                    value={values.date}
                    onChange={(date) => {
                        setFieldValue("date", date)
                    }}
                    disableOpenOnEnter
                    animateYearScrolling={false}
                />
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="testingparameter-simple">Testing Parameter</InputLabel>
                    <Select
                        value={values.testingparameter}
                        onChange={handleChange}
                        inputProps={{
                            name: 'testingparameter',
                            id: 'testingparameter-simple',
                        }}
                        error={errors.testingparameter && (touched.testingparameter)}
                        FormHelperTextProps={{
                            error: errors.testingparameter && (touched.testingparameter)
                        }}
                        helperText={
                            errors.testingparameter && (touched.testingparameter)
                                ? errors.testingparameter
                                : null
                        }
                    >
                        <MenuItem value={0}>YES</MenuItem>
                        <MenuItem value={1}>NO</MenuItem>
                    </Select>
                </FormControl>
            </Grid>

            <FormControl className={classes.formControl}>
                <Typography variant="subtitle1">Certificate File : </Typography>
                <input type="file" onChange={this.fileChangedHandler} />
            </FormControl>

            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    id="note"
                    label="Note"
                    multiline
                    rows="1"
                    value={values.note}
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.note && (touched.note)}
                    FormHelperTextProps={{
                        error: errors.note && (touched.note)
                    }}
                    helperText={
                        errors.note && (touched.note)
                            ? errors.note
                            : null
                    }
                />
            </Grid>

            <Button variant="contained" type="submit" color="primary" className={classes.button}>
                Save
            </Button>
            <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                Cancel
            </Button>
        </form>
    );
})

const EnhancedPeriodicalTestingForm = withFormik({
    mapPropsToValues: () => ({ date: new Date(), testingparameter: '', note: '' }),
    validationSchema: Yup.object().shape({
        note: Yup.string("Note is required and should be a String").required("Note Required")
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'BasicForm',
})(PeriodicalTestingForm)

// Engineering Change Note

const EngineeringChangeNoteForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue
    } = props
    return (

        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    name="revisionno"
                    placeholder="Enter Revision No"
                    label="Revision No"
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.revisionno}
                    error={errors.revisionno && (touched.revisionno)}
                    FormHelperTextProps={{
                        error: errors.revisionno && (touched.revisionno)
                    }}
                    helperText={
                        errors.revisionno && (touched.revisionno)
                            ? errors.revisionno
                            : null
                    }
                />
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <DatePicker
                    className={classes.datepicker}
                    keyboard
                    name="revisiondate"
                    label="Date"
                    placeholder="10/10/2018"
                    value={values.revisiondate}
                    onChange={(date) => {
                        setFieldValue("revisiondate", date)
                    }}
                    disableOpenOnEnter
                    animateYearScrolling={false}
                />
            </Grid>

            <FormControl className={classes.formControl}>
                <Typography variant="subtitle1">Certificate File : </Typography>
                <input type="file" onChange={this.fileChangedHandler} />
            </FormControl>

            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    id="revisionreason"
                    label="Revision Reason"
                    multiline
                    rows="1"
                    value={values.revisionreason}
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.revisionreason && (touched.revisionreason)}
                    FormHelperTextProps={{
                        error: errors.revisionreason && (touched.revisionreason)
                    }}
                    helperText={
                        errors.revisionreason && (touched.revisionreason)
                            ? errors.revisionreason
                            : null
                    }
                />
            </Grid>

            <Button variant="contained" type="submit" color="primary" className={classes.button}>
                Save
            </Button>
            <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                Cancel
            </Button>
        </form>
    );
})

const EnhancedEngineeringChangeNoteForm = withFormik({
    mapPropsToValues: () => ({ revisionno: '', revisiondate: new Date(), revisionreason: '' }),
    validationSchema: Yup.object().shape({
        revisionno: Yup.string("Revision no is required and should be a String").required("Revision no Required"),
        revisionreason: Yup.string("Revision reason is required and should be a String").required("Revision reason Required")
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'BasicForm',
})(EngineeringChangeNoteForm)

// Gauge Calibration

const GaugeCalibrationForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue
    } = props
    return (

        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    name="gaugeid"
                    placeholder="Enter Gauge ID"
                    label="Gauge ID"
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.gaugeid}
                    margin="normal"
                    error={errors.gaugeid && (touched.gaugeid)}
                    FormHelperTextProps={{
                        error: errors.gaugeid && (touched.gaugeid)
                    }}
                    helperText={
                        errors.gaugeid && (touched.gaugeid)
                            ? errors.gaugeid
                            : null
                    }
                />
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <TextField
                    name="gaugename"
                    placeholder="Enter Gauge Name"
                    label="Gauge Name"
                    className={classes.textField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.gaugename}
                    margin="normal"
                    error={errors.gaugename && (touched.gaugename)}
                    FormHelperTextProps={{
                        error: errors.gaugename && (touched.gaugename)
                    }}
                    helperText={
                        errors.gaugename && (touched.gaugename)
                            ? errors.gaugename
                            : null
                    }
                />
            </Grid>

            <Grid container className={classes.grid} justify="space-around">
                <DatePicker
                    className={classes.datepicker}
                    keyboard
                    name="calibrationduedate"
                    label="Calibration Due Date"
                    placeholder="10/10/2018"
                    value={values.calibrationduedate}
                    onChange={(date) => {
                        setFieldValue("calibrationduedate", date)
                    }}
                    disableOpenOnEnter
                    animateYearScrolling={false}
                />
            </Grid>

            <Button variant="contained" type="submit" color="primary" className={classes.button}>
                Save
            </Button>
            <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                Cancel
            </Button>
        </form>
    );
})

const EnhancedGaugeCalibrationForm = withFormik({
    mapPropsToValues: () => ({ gaugeid: '', gaugename: '', calibrationduedate: new Date() }),
    validationSchema: Yup.object().shape({
        gaugeid: Yup.string("Gauge Id is required and should be a String").required("Gauge Id Required"),
        gaugename: Yup.string("Gauge Name is required and should be a String").required("Gauge Name Required")
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'BasicForm',
})(GaugeCalibrationForm)

const IncomingPartsPage = withStyles(styles)((props) => {
    const { classes } = props
    return (
        <Fragment>
            <Card className={classes.card1}>
                <Typography variant="h4">Registration</Typography>
                <EnhancedRegisterForm {...props} />
            </Card>

            <Card className={classes.card1} >
                <Typography variant="h4">Process Document</Typography>
                <EnhancedProcessDocumentForm {...props} />
            </Card>

            <Card className={classes.card2} >
                <Typography variant="h4">Part Document</Typography>
                <EnhancedPartDocumentForm {...props} />
            </Card>

            <Card className={classes.card2} >
                <Typography variant="h4">Inspection Parameter</Typography>
                <EnhancedInspectionParameterForm {...props} />
            </Card>

            <Card className={classes.card2} >
                <Typography variant="h4">Checking Sequence</Typography>
                <EnhancedCheckingSequenceForm {...props} />
            </Card>

            <Card className={classes.card1} >
                <Typography variant="h4">Deviation</Typography>
                <EnhancedDeviationForm {...props} />
            </Card>

            <Card className={classes.card2} >
                <Typography variant="h4">Raw Material Inspection</Typography>
                <EnhancedRawMaterialForm {...props} />
            </Card>

            <Card className={classes.card2} >
                <Typography variant="h4">Sampling Inspection</Typography>
                <EnhancedSamplingInspectionForm {...props} />
            </Card>

            <Card className={classes.card2} >
                <Typography variant="h4">Periodical Testing</Typography>
                <EnhancedPeriodicalTestingForm {...props} />
            </Card>

            <Card className={classes.card2} >
                <Typography variant="h4">Engineering Change Note</Typography>
                <EnhancedEngineeringChangeNoteForm {...props} />
            </Card>

            <Card className={classes.card2} >
                <Typography variant="h4">Gauge Calibration</Typography>
                <EnhancedGaugeCalibrationForm {...props} />
            </Card>
        </Fragment >)
})

export default IncomingPartsPage;