import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { withFormik } from "formik";
import * as Yup from "yup";
import AgTable from "./../../components/AgTable";
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Export from "@material-ui/icons/CloudUploadOutlined";
import Import from "@material-ui/icons/CloudDownloadOutlined";
import Tooltip from '@material-ui/core/Tooltip';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from "@material-ui/core/Grid";
import RefreshIcon from "@material-ui/icons/RefreshOutlined";
import { compose } from "recompose";
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import Header1 from './../../components/Header1/Header1';

//your styles here
const styles = theme => ({
    //header styles
    breakCrumbIcon: {
        marginTop: '3px',
        fontSize: "20px"
    },
    breadCrumbText: {
        marginTop: '4px',
        color: "white",
        fontSize: "13px",
        "font-weight": "normal"
    },
    headerText: {
        color: "white",
        paddingBottom: "4px",
        fontSize: "18px",
    },
    header: {
        "background-color": "#3f51b5",
        color: "white",
    },

    //action style
    dialogStyles: {
        paperWidthSm: '400px',
        textAlign: 'center'
    },
    toolbarIcon: {
        margin: '10px'
    },
    toolbar: {
        margin: '5px',
    },

    //page styles
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    button: {
        padding: theme.spacing.unit * 2
    },
    formControl: {
        margin: theme.spacing.unit,
        maxWidth: 400,
        width: "100%"
    },
});

const Form = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        handleClose,
        onCancel
    } = props
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} onClick={handleClose} className={classes.container} autoComplete="off">
            <Grid
                item xs={12}
                justify="space-around"
            >
                    <FormControl className={classes.formControl}>
                        <TextField
                            name="id"
                            placeholder="Enter ID"
                            label="ID"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.id}
                            error={errors.id && (touched.id)}
                            FormHelperTextProps={{
                                error: errors.id && (touched.id)
                            }}
                            helperText={
                                errors.id && (touched.id)
                                    ? errors.id
                                    : null
                            }
                        />
                    </FormControl>

                    <FormControl className={classes.formControl}>
                        <TextField
                            name="name"
                            placeholder="Enter Name"
                            label="Name"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.name}
                            error={errors.name && (touched.name)}
                            FormHelperTextProps={{
                                error: errors.name && (touched.name)
                            }}
                            helperText={
                                errors.name && (touched.name)
                                    ? errors.name
                                    : null
                            }
                        />
                    </FormControl>
            </Grid>

            <Grid
                item xs={12}
                justify="center"
                className={classes.button}
            >
                <Button type="submit" variant="contained" color="primary" style={{ marginLeft: "10px" }}>
                    Save
  </Button>
                <Button variant="contained" color="secondary" onClick={onCancel} style={{ marginLeft: "10px" }}>
                    Cancel
  </Button>
            </Grid>
        </form>
    );
})

const DepartmentsForm = withFormik({
    mapPropsToValues: () => ({ id: '', name: '' }),
    validationSchema: Yup.object().shape({
        id: Yup.string("ID is required and should be a String").required("ID Required"),
        name: Yup.string("Name is required and should be a String").required("Name is required")
    }),

    handleSubmit: (values, { setSubmitting, se }) => {
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'DepartmentForm',
})(Form)

class DepartmentsPage extends React.PureComponent {
    state = {
        dialogOpen: false
    }

    handleClickOpen = () => {
        this.setState({ dialogOpen: true });
    };

    handleClose = () => {
        this.setState({ dialogOpen: false });
    };

    componentDidMount() {
        console.log("gridapi", this.gridApi)
    }

    onGridReady = (params) => {
        params.api.sizeColumnsToFit()
        this.gridParams = params
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

    exportData = () => {
        this.gridApi.exportDataAsCsv(this.gridParams);
    }

    render() {
        const columnDefs = [{
            headerName: "Sr.No.", field: "srno", sortable: true, filter: true, width: 50
        }, {
            headerName: "Id", field: "id", sortable: true, filter: true, width: 100
        }, {
            headerName: "Description", field: "description", sortable: true, filter: true
        }]
        const rowData = [{
            srno: 1, id: "Main", description: "opexworks"
        }, {
            srno: 2, id: "Main", description: "opexworks"
        }, {
            srno: 3, id: "Main", description: "opexworks"
        }]
        const { classes } = this.props
        return (
            <Fragment>
                <Dialog
                    className={classes.dialogStyles}
                    open={this.state.dialogOpen}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                    maxWidth="sm"
                >
                    <DialogTitle id="form-dialog-title">Add Department</DialogTitle>
                    <DialogContent>
                        <DepartmentsForm {...this.props} onCancel={this.handleClose} />
                    </DialogContent>
                </Dialog>

                <Header1 breadCrumbText="Home / Departments" breadCrumbHeader="DEPARTMENTS" />

                <Grid
                    container
                    direction="row"
                    justify="flex-end"
                    alignItems="right"
                >
                    <Grid item key={4}>
                        <div className={classes.toolbar}>
                            <Tooltip title="Add Department">
                                <Fab size={"small"} color="primary" variant="extended" onClick={this.handleClickOpen} className={classes.toolbarIcon}>
                                    <AddIcon />&nbsp;
                        </Fab>
                            </Tooltip>
                            <Tooltip title="Import">
                                <Fab onClick={this.exportData} size={"small"} variant="extended" color="primary" className={classes.toolbarIcon}>
                                    <Import />&nbsp;
                        </Fab>
                            </Tooltip>
                            <Tooltip title="Export">
                                <Fab size={"small"} color="primary" variant="extended" className={classes.toolbarIcon}>
                                    <Export /> &nbsp;
                        </Fab>
                            </Tooltip>
                            <Tooltip title="Refresh">
                                <Fab size={"small"} color="primary" variant="extended" onClick={this.handleClickOpen} className={classes.toolbarIcon}>
                                    <RefreshIcon />&nbsp;
                        </Fab>
                            </Tooltip>
                        </div>
                    </Grid>
                </Grid>

                <Paper>
                    <AgTable
                        onGridReady={this.onGridReady}
                        columnDefs={columnDefs}
                        rowData={rowData}
                    />
                </Paper>
            </Fragment >)
    }
}

export default compose(withStyles(styles))(DepartmentsPage);