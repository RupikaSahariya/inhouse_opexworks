
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import { withFormik } from "formik"
import * as Yup from "yup";
import Typography from '@material-ui/core/Typography';
import AgTable from "./../../components/AgTable";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from "@material-ui/core/Grid";
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Export from "@material-ui/icons/CloudUploadOutlined";
import Import from "@material-ui/icons/CloudDownloadOutlined";

//your styles here
const styles = theme => ({
    //action style
    dialogStyles: {
        paperWidthSm: '300px',
        textAlign: 'center'
    },
    toolbarIcon: {
        margin: '10px'
    },
    toolbar: {
        margin: '5px'
    },
    //page styles
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        marginLeft: "20px"
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
    card: {
        minWidth: 320,
        "width": "15px",
        "padding": "50px"
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        maxWidth: 400,
        width: "100%"
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
});

const Form = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset
    } = props
    return (

        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <FormControl className={classes.formControl}>
                <TextField
                    id="description"
                    label="Description"
                    multiline
                    rows="1"
                    value={values.description}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.description && (touched.description)}
                    FormHelperTextProps={{
                        error: errors.description && (touched.description)
                    }}
                    helperText={
                        errors.description && (touched.description)
                            ? errors.description
                            : null
                    }
                />
            </FormControl>

            <FormControl className={classes.formControl}>
                <InputLabel htmlFor="customer-simple" error={errors.customer && (touched.customer)}>Customer</InputLabel>
                <Select
                    value={values.customer}
                    onChange={handleChange}
                    inputProps={{
                        name: 'customer',
                        id: 'customer-simple',
                    }}
                    error={errors.customer && (touched.customer)}
                    FormHelperTextProps={{
                        error: errors.customer && (touched.customer)
                    }}
                    helperText={
                        errors.customer && (touched.customer)
                            ? errors.customer
                            : null
                    }
                >
                    <MenuItem value={0}>ABC Pvt LTd</MenuItem>
                    <MenuItem value={1}>fdg</MenuItem>
                    <MenuItem value={2}>JCB INDIA PVT LTD</MenuItem>
                </Select>
            </FormControl>

            <DialogActions>
                <Button variant="contained" type="submit" color="primary" className={classes.button}>
                    Save
            </Button>
                <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                    Cancel
            </Button>
            </DialogActions>
        </form>
    );
})

const ProductsForm = withFormik({
    mapPropsToValues: () => ({ description: '', customer: '' }),
    validationSchema: Yup.object().shape({
        description: Yup.string("Description is required and should be a String").required("Description Required"),
        customer: Yup.string("Customer is required and should be a String").required("Customer Required")
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'ProductsForm',
})(Form)

class ProductsPage extends React.PureComponent {
    state = {
        dialogOpen: false
    }

    handleClickOpen = () => {
        this.setState({ dialogOpen: true });
    };

    handleClose = () => {
        this.setState({ dialogOpen: false });
    };

    componentDidMount() {
        console.log("gridapi", this.gridApi)
    }

    onGridReady = (params) => {
        params.api.sizeColumnsToFit()
        this.gridParams = params
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

    exportData = () => {
        this.gridApi.exportDataAsCsv(this.gridParams);
    }

    render() {
        const columnDefs = [{
            headerName: "Sr.No.", field: "srno", sortable: true, filter: true, width: 50
        }, {
            headerName: "Id", field: "id", sortable: true, filter: true, width: 100
        }, {
            headerName: "Description", field: "description", sortable: true, filter: true
        }, {
            headerName: "Customer", field: "customer", sortable: true, filter: true
        }]
        const rowData = [{
            srno: 1, id: "Main", description: "opexworks", customer: "customer1"
        }, {
            srno: 2, id: "Main", description: "opexworks", customer: "customer2"
        }, {
            srno: 3, id: "Main", description: "opexworks", customer: "customer3"
        }]
        const { classes } = this.props
        return (
            <Fragment>
                <Dialog
                    className={classes.dialogStyles}
                    open={this.state.dialogOpen}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                    maxWidth="sm"
                >
                    <DialogTitle id="form-dialog-title">Add a Product</DialogTitle>
                    <DialogContent className={classes.dialogStyles}>
                        <ProductsForm {...this.props} />
                    </DialogContent>
                </Dialog>
                <Grid
                    container
                    direction="row"
                    justify="space-between"
                    flx
                >
                    <Grid item xs>
                        <div className={classes.toolbar}>
                            <Fab size={"small"} variant="extended" color="primary" onClick={this.handleClickOpen} className={classes.toolbarIcon}>
                                <AddIcon />&nbsp;
                                Add a Product
                        </Fab>

                            <Fab onClick={this.exportData} size={"small"} variant="extended" color="primary" className={classes.toolbarIcon}>
                                <Import />&nbsp;
                                Export
                        </Fab>

                            <Fab size={"small"} variant="extended" color="primary" className={classes.toolbarIcon}>
                                <Export /> &nbsp;
                                Import
                        </Fab>
                        </div>
                    </Grid>
                </Grid>

                <Paper>
                    <AgTable onGridReady={this.onGridReady} columnDefs={columnDefs} rowData={rowData} />
                    `                </Paper>
            </Fragment >)
    }
}

export default withStyles(styles)(ProductsPage);