
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import { withFormik } from "formik"
import * as Yup from "yup";
import AgTable from "./../../components/AgTable";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from "@material-ui/core/Grid";
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Export from "@material-ui/icons/CloudUploadOutlined";
import Import from "@material-ui/icons/CloudDownloadOutlined";

//your styles here
const styles = theme => ({
    //action style
    dialogStyles: {
        paperWidthSm: '300px',
        textAlign: 'center'
    },
    toolbarIcon: {
        margin: '10px'
    },
    toolbar: {
        margin: '5px'
    },
    //page styles
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        margin: theme.spacing.unit,
        width: "100%",
        maxWidth: 400
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
    card: {
        minWidth: 275,
        maxWidth: "850px",
        width: "15px",
        padding: 30,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    formControl: {
        margin: theme.spacing.unit,
        maxWidth: 400,
        width: "100%"
    },
});

const Form = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset
    } = props
    return (

        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <FormControl className={classes.formControl}>
                <TextField
                    name="designation"
                    placeholder="Enter Designation"
                    label="Designation"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.designation}
                    error={errors.designation && (touched.designation)}
                    FormHelperTextProps={{
                        error: errors.designation && (touched.designation)
                    }}
                    helperText={
                        errors.designation && (touched.designation)
                            ? errors.designation
                            : null
                    }
                />
            </FormControl>

            <DialogActions>
                <Button variant="contained" type="submit" color="primary" className={classes.button}>
                    Save
            </Button>
                <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                    Cancel
            </Button>
            </DialogActions>
        </form>
    );
})

const EmpolyeeDesignationForm = withFormik({
    mapPropsToValues: () => ({ designation: '' }),
    validationSchema: Yup.object().shape({
        designation: Yup.string("Designation is required and should be a String").required("Designation Required")
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'BasicForm',
})(Form)

class EmployeeDesignationPage extends React.PureComponent {
    state = {
        dialogOpen: false
    }

    handleClickOpen = () => {
        this.setState({ dialogOpen: true });
    };

    handleClose = () => {
        this.setState({ dialogOpen: false });
    };

    componentDidMount() {
        console.log("gridapi", this.gridApi)
    }

    onGridReady = (params) => {
        params.api.sizeColumnsToFit()
        this.gridParams = params
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

    exportData = () => {
        this.gridApi.exportDataAsCsv(this.gridParams);
    }

    render() {
        const columnDefs = [{
            headerName: "Sr.No.", field: "srno", sortable: true, filter: true, width: 50
        }, {
            headerName: "Designation", field: "designation", sortable: true, filter: true
        }]
        const rowData = [{
            srno: 1, designation: "opexworks"
        }, {
            srno: 2, designation: "opexworks"
        }, {
            srno: 3, designation: "opexworks"
        }]
        const { classes } = this.props
        return (
            <Fragment>
                <Dialog
                    className={classes.dialogStyles}
                    open={this.state.dialogOpen}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                    maxWidth="sm"
                >
                    <DialogTitle id="form-dialog-title">Add a Empolyee Designation</DialogTitle>
                    <DialogContent className={classes.dialogStyles}>
                        <EmpolyeeDesignationForm {...this.props} />
                    </DialogContent>
                </Dialog>
                <Grid
                    container
                    direction="row"
                    justify="space-between"
                    flx
                >
                    <Grid item xs>
                        <div className={classes.toolbar}>
                            <Fab size={"small"} variant="extended" color="primary" onClick={this.handleClickOpen} className={classes.toolbarIcon}>
                                <AddIcon />&nbsp;
                                Add a Empolyee Designation
                        </Fab>

                            <Fab onClick={this.exportData} size={"small"} variant="extended" color="primary" className={classes.toolbarIcon}>
                                <Import />&nbsp;
                                Export
                        </Fab>

                            <Fab size={"small"} variant="extended" color="primary" className={classes.toolbarIcon}>
                                <Export /> &nbsp;
                                Import
                        </Fab>
                        </div>
                    </Grid>
                </Grid>

                <Paper>
                    <AgTable onGridReady={this.onGridReady} columnDefs={columnDefs} rowData={rowData} />
                    `                </Paper>
            </Fragment >)
    }
}

export default withStyles(styles)(EmployeeDesignationPage);