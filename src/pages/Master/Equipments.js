import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withFormik, Field } from "formik"
import * as Yup from "yup";
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import { DatePicker } from 'material-ui-pickers';

//your styles here
const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        margin: theme.spacing.unit,
        width: "100%",
        maxWidth: 400
    },
    textField2: {
        margin: theme.spacing.unit,
        width: "100%"
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
    card: {
        maxWidth: "850px",
        width: "100%",
        padding: 30,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        maxWidth: 400,
        width: "100%"
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
    datepicker: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: "100%",
        maxWidth: 400
    },
});

const Form = withStyles(styles)((props) => {
    console.log("props", props) //keep this line while working on form then comment it.
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue
    } = props
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid
                container
                direction="row"
                alignItems="center"
            >
                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <TextField
                            name="id"
                            placeholder="Enter ID"
                            label="ID"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.id}
                            error={errors.id && (touched.id)}
                            FormHelperTextProps={{
                                error: errors.id && (touched.id)
                            }}
                            helperText={
                                errors.id && (touched.id)
                                    ? errors.id
                                    : null
                            }
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <TextField
                            id="description"
                            label="Description"
                            multiline
                            rows="1"
                            value={values.description}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            error={errors.description && (touched.description)}
                            FormHelperTextProps={{
                                error: errors.description && (touched.description)
                            }}
                            helperText={
                                errors.description && (touched.description)
                                    ? errors.description
                                    : null
                            }
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="department-simple">Department</InputLabel>
                        <Select
                            value={values.department}
                            onChange={handleChange}
                            inputProps={{
                                name: 'department',
                                id: 'department-simple',
                            }}
                            error={errors.department && (touched.department)}
                            FormHelperTextProps={{
                                error: errors.department && (touched.department)
                            }}
                            helperText={
                                errors.department && (touched.department)
                                    ? errors.department
                                    : null
                            }
                        >
                            <MenuItem value={0}>Maintenance</MenuItem>
                            <MenuItem value={1}>New Product Development</MenuItem>
                            <MenuItem value={2}>Production</MenuItem>
                            <MenuItem value={3}>Quality</MenuItem>
                            <MenuItem value={4}>Supplier Quality Assurance</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="cell-simple">Cell</InputLabel>
                        <Select
                            value={values.cell}
                            onChange={handleChange}
                            inputProps={{
                                name: 'cell',
                                id: 'cell-simple',
                            }}
                            error={errors.cell && (touched.cell)}
                            FormHelperTextProps={{
                                error: errors.cell && (touched.cell)
                            }}
                            helperText={
                                errors.cell && (touched.cell)
                                    ? errors.cell
                                    : null
                            }
                        >
                            <MenuItem value={0}>Select</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        id="make"
                        label="Make"
                        multiline
                        rows="1"
                        value={values.make}
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={errors.make && (touched.make)}
                        FormHelperTextProps={{
                            error: errors.make && (touched.make)
                        }}
                        helperText={
                            errors.make && (touched.make)
                                ? errors.make
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        id="model"
                        label="Model"
                        multiline
                        rows="1"
                        value={values.model}
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={errors.model && (touched.model)}
                        FormHelperTextProps={{
                            error: errors.model && (touched.model)
                        }}
                        helperText={
                            errors.model && (touched.model)
                                ? errors.model
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        name="serialno"
                        placeholder="Enter Serial No"
                        label="Serial No"
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.serialno}
                        margin="normal"
                        error={errors.serialno && (touched.serialno)}
                        FormHelperTextProps={{
                            error: errors.serialno && (touched.serialno)
                        }}
                        helperText={
                            errors.serialno && (touched.serialno)
                                ? errors.serialno
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        name="assetcode"
                        placeholder="Enter Asset Code"
                        label="Asset Code"
                        className={classes.textField}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.assetcode}
                        error={errors.assetcode && (touched.assetcode)}
                        FormHelperTextProps={{
                            error: errors.assetcode && (touched.assetcode)
                        }}
                        helperText={
                            errors.assetcode && (touched.assetcode)
                                ? errors.assetcode
                                : null
                        }
                    />
                </Grid>

                <Grid item xs={6}>
                    <DatePicker
                        className={classes.datepicker}
                        keyboard
                        name="purchasedate"
                        label="Purchase Date"
                        placeholder="10/10/2018"
                        value={values.purchasedate}
                        onChange={(date) => {
                            setFieldValue("purchasedate", date)
                        }}
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                </Grid>

                <Grid item xs={6}>
                    <DatePicker
                        className={classes.datepicker}
                        keyboard
                        name="commisiondate"
                        label="Commission Date"
                        placeholder="10/10/2018"
                        value={values.commisiondate}
                        onChange={(date) => {
                            setFieldValue("commisiondate", date)
                        }}
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                </Grid>

                <Grid item xs={6}>
                    <DatePicker
                        className={classes.datepicker}
                        keyboard
                        name="warrantyexpirydate"
                        label="Warranty Expiry Date"
                        placeholder="10/10/2018"
                        value={values.warrantyexpirydatedate}
                        onChange={(date) => {
                            setFieldValue("warrantyexpirydatedate", date)
                        }}
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                </Grid>

                <Grid item xs={12}>
                    <Grid container className={classes.grid} justify="space-around">
                        <TextField
                            id="note"
                            label="Note"
                            multiline
                            rows="2"
                            value={values.note}
                            className={classes.textField2}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            margin="normal"
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
            </Grid>

            <Grid item xs={6}>
                <Button variant="contained" type="submit" color="primary" className={classes.button}>
                    Save
  </Button>
            </Grid>
            <Grid item xs={6}>
                <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                    Cancel
                </Button>
            </Grid>

        </form >
    );
})

const EquipmentsForm = withFormik({
    mapPropsToValues: () => ({ id: '', description: '', department: '', cell: '', make: '', model: '', serialno: '', assetcode: '', purchasedate: new Date(), commisiondate: new Date(), warrantyexpirydate: new Date() }),
    validationSchema: Yup.object().shape({
        id: Yup.string("ID is required and should be a String").required("ID Required"),
        description: Yup.string("Description is required and should be a String").required("Description is required"),
        department: Yup.string("Department is required and should be a String").required("Department is required"),
        cell: Yup.string("Cell is required and should be a String").required("Cell is required"),
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },

    displayName: 'BasicForm',
})(Form)

const EquipmentsPage = withStyles(styles)((props) => {
    const { classes } = props
    return (
        <Fragment>
            <Card className={classes.card}>
                <Typography variant="h4">Equipments</Typography>
                <EquipmentsForm {...props} />
            </Card>
        </Fragment >)
})

export default EquipmentsPage;