import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withFormik } from "formik"
import * as Yup from "yup";
import Typography from '@material-ui/core/Typography';

//your styles here
const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        margin: theme.spacing.unit,
        width: "100%",
        maxWidth: 400
    },
    textField2: {
        margin: theme.spacing.unit,
        width: "100%"
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
    card: {
        maxWidth: "850px",
        width: "100%",
        padding: 30,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        maxWidth: 400,
        width: "100%"
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
    datepicker: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: "100%",
        maxWidth: 400
    },
    file: {
        marginTop: 20,
        marginBottom: 20
    }
});

const Form = withStyles(styles)((props) => {
    console.log("props", props) //keep this line while working on form then comment it.
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue
    } = props
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid
                container
                direction="row"
                alignItems="center"
            >
                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <TextField
                            name="id"
                            placeholder="Enter ID"
                            label="ID"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.id}
                            error={errors.id && (touched.id)}
                            FormHelperTextProps={{
                                error: errors.id && (touched.id)
                            }}
                            helperText={
                                errors.id && (touched.id)
                                    ? errors.id
                                    : null
                            }
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <TextField
                            name="name"
                            placeholder="Enter Name"
                            label="Name"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.name}
                            error={errors.name && (touched.name)}
                            FormHelperTextProps={{
                                error: errors.name && (touched.name)
                            }}
                            helperText={
                                errors.name && (touched.name)
                                    ? errors.name
                                    : null
                            }
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <TextField
                            name="mobileno"
                            placeholder="Enter Mobile No"
                            label="Mobile No"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.mobileno}
                            error={errors.mobileno && (touched.mobileno)}
                            FormHelperTextProps={{
                                error: errors.mobileno && (touched.mobileno)
                            }}
                            helperText={
                                errors.mobileno && (touched.mobileno)
                                    ? errors.mobileno
                                    : null
                            }
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <TextField
                            name="emailid"
                            placeholder="Enter Email Id"
                            label="Email Id"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.emailid}
                            error={errors.emailid && (touched.emailid)}
                            FormHelperTextProps={{
                                error: errors.emailid && (touched.emailid)
                            }}
                            helperText={
                                errors.emailid && (touched.emailid)
                                    ? errors.emailid
                                    : null
                            }
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <TextField
                            name="password"
                            placeholder="Enter Password"
                            label="Password"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.password}
                            error={errors.password && (touched.password)}
                            FormHelperTextProps={{
                                error: errors.password && (touched.password)
                            }}
                            helperText={
                                errors.password && (touched.password)
                                    ? errors.password
                                    : null
                            }
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="designation-simple">Designation</InputLabel>
                        <Select
                            value={values.designation}
                            onChange={handleChange}
                            inputProps={{
                                name: 'designation',
                                id: 'designation-simple',
                            }}
                            error={errors.designation && (touched.designation)}
                            FormHelperTextProps={{
                                error: errors.designation && (touched.designation)
                            }}
                            helperText={
                                errors.designation && (touched.designation)
                                    ? errors.designation
                                    : null
                            }
                        >
                            <MenuItem value={0}>Select</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="department-simple">Department</InputLabel>
                        <Select
                            value={values.department}
                            onChange={handleChange}
                            inputProps={{
                                name: 'department',
                                id: 'department-simple',
                            }}
                            error={errors.department && (touched.department)}
                            FormHelperTextProps={{
                                error: errors.department && (touched.department)
                            }}
                            helperText={
                                errors.department && (touched.department)
                                    ? errors.department
                                    : null
                            }
                        >
                            <MenuItem value={0}>Maintenance</MenuItem>
                            <MenuItem value={1}>New Product Development</MenuItem>
                            <MenuItem value={2}>Production</MenuItem>
                            <MenuItem value={3}>Quality</MenuItem>
                            <MenuItem value={4}>Supplier Quality Assurance</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="cell-simple">Cell</InputLabel>
                        <Select
                            value={values.cell}
                            onChange={handleChange}
                            inputProps={{
                                name: 'cell',
                                id: 'cell-simple',
                            }}
                            error={errors.cell && (touched.cell)}
                            FormHelperTextProps={{
                                error: errors.cell && (touched.cell)
                            }}
                            helperText={
                                errors.cell && (touched.cell)
                                    ? errors.cell
                                    : null
                            }
                        >
                            <MenuItem value={0}>Select</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="category-simple">Category</InputLabel>
                        <Select
                            value={values.category}
                            onChange={handleChange}
                            inputProps={{
                                name: 'category',
                                id: 'category-simple',
                            }}
                            error={errors.category && (touched.category)}
                            FormHelperTextProps={{
                                error: errors.category && (touched.category)
                            }}
                            helperText={
                                errors.category && (touched.category)
                                    ? errors.category
                                    : null
                            }
                        >
                            <MenuItem value={0}>Select</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="grade-simple">Grade</InputLabel>
                        <Select
                            value={values.grade}
                            onChange={handleChange}
                            inputProps={{
                                name: 'grade',
                                id: 'grade-simple',
                            }}
                            error={errors.grade && (touched.grade)}
                            FormHelperTextProps={{
                                error: errors.grade && (touched.grade)
                            }}
                            helperText={
                                errors.grade && (touched.grade)
                                    ? errors.grade
                                    : null
                            }
                        >
                            <MenuItem value={0}>Select</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={12}>
                    <FormControl className={classes.formControl}>
                        <TextField
                            name="ipaddress"
                            placeholder="Enter IP Address"
                            label="IP Address"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.ipaddress}
                            error={errors.ipaddress && (touched.ipaddress)}
                            FormHelperTextProps={{
                                error: errors.ipaddress && (touched.ipaddress)
                            }}
                            helperText={
                                errors.ipaddress && (touched.ipaddress)
                                    ? errors.ipaddress
                                    : null
                            }
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl className={classes.formControl}>
                        <Typography variant="subtitle1">Photo : </Typography>
                        <input type="file" onChange={this.fileChangedHandler} />
                    </FormControl>
                </Grid>
            </Grid>

            <Grid item xs={6}>
                <Button variant="contained" type="submit" color="primary" className={classes.button}>
                    Save
  </Button>
            </Grid>
            <Grid item xs={6}>
                <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                    Cancel
                </Button>
            </Grid>

        </form >
    );
})


const EmployeesForm = withFormik({
    mapPropsToValues: () => ({ id: '', name: '', mobileno: '', emailid: '', password: '', designation: '', department: '', cell: '', category: '', grade: '', ipaddress: '', }),
    validationSchema: Yup.object().shape({
        id: Yup.string("ID is required and should be a String").required("ID Required"),
        name: Yup.string("Password is required and should be a String").required("Password is required"),
        password: Yup.string("Password is required and should be a String").required("Password Required"),
        designation: Yup.string("Designation is required and should be a String").required("Designation Required"),
        department: Yup.string("Department is required and should be a String").required("Department Required"),
        cell: Yup.string("Cell is required and should be a String").required("Cell Required")
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);
    },
    fileChangedHandler: (event) => {
        const file = event.target.files[0]
    },

    uploadHandler: () => { },

    displayName: 'BasicForm',
})(Form)

const EmployeesPage = withStyles(styles)((props) => {
    const { classes } = props
    return (
        <Fragment>
            <Card className={classes.card}>
                <Typography variant="h4">Employees</Typography>
                <EmployeesForm {...props} />
            </Card>
        </Fragment >)
})

export default EmployeesPage;