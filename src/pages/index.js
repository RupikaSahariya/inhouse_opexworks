import loadComponent from "./../utils/loadComponent";
// Home
import Home from "./Home/Home";
import Signin from "./Authentication/Signin";

// import Departments from './Master/Departments';
// import Cells from './Master/Cells';
// import Equipments from './Master/Equipments';
// import EmployeeDesignation from './Master/EmployeeDesignation';
// import Employees from './Master/Employees';
// import LossesInOperation from './Master/LossesInOperation';
// import Suppliers from './Master/Suppliers';
// import Customers from './Master/Customers';
// import Products from './Master/Products';

// import Registration from './IncomParts/Registration';
// import ProcessDocument from './IncomParts/ProcessDocument';
// import PartDocument from './IncomParts/PartDocument';
// import InspectionParameters from './IncomParts/InspectionParameters';
// // import CheckingSequence from './IncomParts/CheckingSequence';
// import Deviation from './IncomParts/Deviation';
// import RawMaterial from './IncomParts/RawMaterial';
// import SamplingInspection from './IncomParts/SamplingInspection';
// import PeriodicalTesting from './IncomParts/PeriodicalTesting';
// import EngineeringChangeNote from './IncomParts/EngineeringChangeNote';
// import GaugeCalibration from './IncomParts/GaugeCalibration';

// import IncomingParts from './Master/IncomingParts';
// import EventsForAlerts from './Master/EventsForAlerts';
// import DeleteInspectionData from './Master/Utility/DeleteInspectionData';
// import DeleteParts from './Master/Utility/DeleteParts';
// import DeleteSuppliers from './Master/Utility/DeleteSuppliers';
// import Configuration from './Master/Configuration';

// // Apps
// import Calendar from './Apps/Calendar';
// import Chat from './Apps/Chat';
// import Media from './Apps/Media';
// import Messages from './Apps/Messages';
// import Social from './Apps/Social';

// // Charts
// import Charts from './Charts/Charts';

// // Maps
// import Google from './Maps/Google';
// import Leaflet from './Maps/Leaflet';

// Pages

// import Invoice from './Pages/Invoice';
// import PricingPage from './Pages/Pricing';
// import TimelinePage from './Pages/Timeline';

// // Taskboard
// import Taskboard from './Taskboard/Taskboard';

// // Widgets
// import Widgets from './Widgets/Widgets';

// // Form
// import Editor from './Forms/Editor';

// Authentication
const Lockscreen = loadComponent({
  loader: () => import("./Authentication/Lockscreen")
});
const PasswordReset = loadComponent({
  loader: () => import("./Authentication/PasswordReset")
});
const Signup = loadComponent({
  loader: () => import("./Authentication/Signup")
});

// Error
const NotFound = loadComponent({ loader: () => import("./Errors/NotFound") });
const BackendError = loadComponent({
  loader: () => import("./Errors/BackendError")
});
const PendingBreakdown = loadComponent({
  loader: () => import("./BreakdownMaintenance/PendingBreakdown")
});

const EquipmentwiseBreakdown = loadComponent({
  loader: () => import("./BreakdownMaintenance/EquipmentwiseBreakdown")
});
const RepetitiveBreakdown = loadComponent({
  loader: () => import("./BreakdownMaintenance/RepetitiveBreakdown")
});
const BreakdownReporting = loadComponent({
  loader: () => import("./BreakdownMaintenance/BreakdownReporting")
});
const Documents = loadComponent({
  loader: () => import("./BreakdownMaintenance/Documents")
});
const ManPower = loadComponent({
  loader: () => import("./BreakdownMaintenance/ManPower")
});
const SparesConsumed = loadComponent({
  loader: () => import("./BreakdownMaintenance/SparesConsumed")
});
const WhyWhyAnalysis = loadComponent({
  loader: () => import("./BreakdownMaintenance/WhyWhyAnalysis")
});

const MtbfAndMttrAnalysis = loadComponent({
  loader: () => import("./MTBFAndMTTRAnalysis/MtbfAndMttrAnalysis")
});
const ReportedBreakdowns = loadComponent({
  loader: () => import("./Reports/ReportedBreakdowns")
});
const PendingBreakdowns = loadComponent({
  loader: () => import("./Reports/PendingBreakdowns")
});
const MTBFAndMTTR = loadComponent({
  loader: () => import("./Reports/MTBFAndMTTR")
});
const BreakdownHistory = loadComponent({
  loader: () => import("./Reports/BreakdownHistory")
});
const PhenomenonWiseBreakdown = loadComponent({
  loader: () => import("./Reports/PhenomenonWiseBreakdown")
});
const BreakdownRootCauseAnalysisPending = loadComponent({
  loader: () => import("./Reports/BreakdownRootCauseAnalysisPending")
});
const WaterCostvsSales = loadComponent({
  loader: () => import("./Graph/WaterCostvsSales")
});
const PowerCostvsSales = loadComponent({
  loader: () => import("./Graph/PowerCostvsSales")
});
const TotalMaintenanceCostvsSales = loadComponent({
  loader: () => import("./Graph/TotalMaintenanceCostvsSales")
});
const TotalBreakdownCostvsSales = loadComponent({
  loader: () => import("./Graph/TotalBreakdownCostvsSales")
});
const Downtime = loadComponent({ loader: () => import("./Graph/Downtime") });
const MTBF = loadComponent({ loader: () => import("./Graph/MTBF") });
const MTTR = loadComponent({ loader: () => import("./Graph/MTTR") });
const FailureAreaWise = loadComponent({
  loader: () => import("./Graph/FailureAreaWise")
});
const PhysicalPhenomenonWise = loadComponent({
  loader: () => import("./Graph/PhysicalPhenomenonWise")
});
const EquipmentWise = loadComponent({
  loader: () => import("./Graph/EquipmentWise")
});
const TokenLogin = loadComponent({
  loader: () => import("./TokenLogin/tokenlogin")
});

// // Ecommerce
// const Products = loadComponent({loader: () => import('./Ecommerce/Products') })
// const Detail = loadComponent({loader: () => import('./Ecommerce/Detail') })

// // Material Examples
// const AppBar = loadComponent({loader: () => import('./Material/app-bar') })
// const Autocomplete = loadComponent({loader: () => import('./Material/autocomplete') })
// const Avatars = loadComponent({loader: () => import('./Material/avatars') })
// const Badges = loadComponent({loader: () => import('./Material/badges') })
// const ButtonNavigation = loadComponent({loader: () => import('./Material/bottom-navigation') })
// const Buttons = loadComponent({loader: () => import('./Material/buttons') })
// const Cards = loadComponent({loader: () => import('./Material/cards') })
// const Chips = loadComponent({loader: () => import('./Material/chips') })
// const Dialogs = loadComponent({loader: () => import('./Material/dialogs') })
// const Dividers = loadComponent({loader: () => import('./Material/dividers') })
// const Drawers = loadComponent({loader: () => import('./Material/drawers') })
// const ExpansionPanels = loadComponent({loader: () => import('./Material/expansion-panels') })
// const GridList = loadComponent({loader: () => import('./Material/grid-list') })
// const Lists = loadComponent({loader: () => import('./Material/lists') })
// const Menus = loadComponent({loader: () => import('./Material/menus') })
// const Paper = loadComponent({loader: () => import('./Material/paper') })
// const Pickers = loadComponent({loader: () => import('./Material/pickers') })
// const Progress = loadComponent({loader: () => import('./Material/progress') })
// const SelectionControls = loadComponent({loader: () => import('./Material/selection-controls') })
// const Selects = loadComponent({loader: () => import('./Material/selects') })
// const Snackbars = loadComponent({loader: () => import('./Material/snackbars') })
// const Steppers = loadComponent({loader: () => import('./Material/steppers') })
// const Tables = loadComponent({loader: () => import('./Material/tables') })
// const Tabs = loadComponent({loader: () => import('./Material/tabs') })
// const TextFields = loadComponent({loader: () => import('./Material/text-fields') })
// const Tooltips = loadComponent({loader: () => import('./Material/tooltips') })

export {
  Home,
  // Departments,
  // Cells,
  // Equipments,
  // EmployeeDesignation,
  // Employees,
  // LossesInOperation,
  // Suppliers,
  // Customers,
  // Products,
  // Registration,
  // ProcessDocument,
  // PartDocument,
  // InspectionParameters,
  // CheckingSequence,
  // Deviation,
  // RawMaterial,
  // SamplingInspection,
  // PeriodicalTesting,
  // EngineeringChangeNote,
  // GaugeCalibration,

  // IncomingParts,
  // EventsForAlerts,
  // DeleteInspectionData,
  // DeleteParts,
  // DeleteSuppliers,
  // Configuration,
  PendingBreakdown,
  EquipmentwiseBreakdown,
  RepetitiveBreakdown,
  BreakdownReporting,
  Documents,
  ManPower,
  SparesConsumed,
  WhyWhyAnalysis,
  MtbfAndMttrAnalysis,
  ReportedBreakdowns,
  PendingBreakdowns,
  MTBFAndMTTR,
  BreakdownHistory,
  PhenomenonWiseBreakdown,
  BreakdownRootCauseAnalysisPending,
  WaterCostvsSales,
  PowerCostvsSales,
  TotalMaintenanceCostvsSales,
  TotalBreakdownCostvsSales,
  Downtime,
  MTBF,
  MTTR,
  FailureAreaWise,
  PhysicalPhenomenonWise,
  EquipmentWise,
  // Calendar,
  // Chat,
  // Media,
  // Messages,
  // Social,
  // Google,
  // Leaflet,
  // Blank,
  // Invoice,
  // PricingPage,
  // TimelinePage,
  // Taskboard,
  // Widgets,
  // Charts,
  // Editor,
  Lockscreen,
  PasswordReset,
  Signin,
  Signup,
  NotFound,
  BackendError
  // Products,
  // Detail,

  // AppBar,
  // Autocomplete,
  // Avatars,
  // Badges,
  // ButtonNavigation,
  // Buttons,
  // Cards,
  // Chips,
  // Dialogs,
  // Dividers,
  // Drawers,
  // ExpansionPanels,
  // GridList,
  // Lists,
  // Menus,
  // Paper,
  // Pickers,
  // Progress,
  // SelectionControls,
  // Selects,
  // Snackbars,
  // Steppers,
  // Tables,
  // Tabs,
  // TextFields,
  // Tooltips
};
