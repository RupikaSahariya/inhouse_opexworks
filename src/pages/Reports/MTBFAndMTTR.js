import React, { Fragment, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withFormik } from "formik"
import * as Yup from "yup";
import Grid from '@material-ui/core/Grid';
import TextData from './textData';
import styles from './styles';
import { actions, Consumer } from "../../actions";
import Header1 from './../../components/Header1/Header1';
import {VALID_YEAR} from "./../../utils/valdations";

const MTBFAndMTTRForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
    } = props
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid
                container
                direction="row"
            >
                <Grid item xs={12} md={4}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="month-simple">Select Month</InputLabel>
                        <Select
                            value={values.month}
                            onChange={handleChange}
                            inputProps={{
                                name: 'month',
                                id: 'month-simple',
                            }}
                            error={errors.month && (touched.month)}
                            FormHelperTextProps={{
                                error: errors.month && (touched.month)
                            }}
                            helperText={
                                errors.month && (touched.month)
                                    ? errors.month
                                    : null
                            }
                        >
                            <MenuItem value="January">January</MenuItem>
                            <MenuItem value="February">February</MenuItem>
                            <MenuItem value='March'>March</MenuItem>
                            <MenuItem value='April'>April</MenuItem>
                            <MenuItem value='May'>May</MenuItem>
                            <MenuItem value='June'>June</MenuItem>
                            <MenuItem value='July'>July</MenuItem>
                            <MenuItem value='August'>August</MenuItem>
                            <MenuItem value='September'>September</MenuItem>
                            <MenuItem value='October'>October</MenuItem>
                            <MenuItem value='November'>November</MenuItem>
                            <MenuItem value='December'>December</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={12} md={4}>
                    <FormControl className={classes.formControl}>
                        <TextField
                            name="year"
                            placeholder="Select Year"
                            label="Year"
                            className={classes.textField}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.year}
                            error={errors.year && (touched.year)}
                            FormHelperTextProps={{
                                error: errors.year && (touched.year)
                            }}
                            helperText={
                                errors.year && (touched.year)
                                    ? errors.year
                                    : null
                            }
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={12} md={4}>
                    <Button variant="contained" type="submit" color="primary" className={classes.button}>
                        {TextData.report}
                    </Button>

                    <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                        {TextData.cancel}
                    </Button>
                </Grid>
            </Grid>

        </form >
    );
})

const EnhancedMTBFAndMTTRForm = withFormik({
    mapPropsToValues: () => ({ month: '', year: '' }),
    validationSchema: Yup.object().shape({
        month: Yup.string("Month is required and should be valid").required("Month Required"),
        year: VALID_YEAR,
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess", values)
        actions.getReport({
            spName: "SP_rpt_BM_MTBF_And_MTTR",
            payload: {
                oee_month: values.month,
                oee_year: values.year
            },
            key: "mtbfAndMttr_Breakdown"
        })
    },

    displayName: 'EnhancedMTBFAndMTTRForm',
})(MTBFAndMTTRForm)

const EnhancedMTBFAndMTTRpage = withStyles(styles)((props) => {
    useEffect(() => {
        return () => {
            actions.setState({
                mtbfAndMttr_Breakdown: null
            })
        }
    })
    const { classes } = props
    const breadcrumb = [
        { name: "Home", path: "/" },
        { name: TextData.mtbfandmttr, path: "/reports/mtbfandmttr" }]

    return (
        <Fragment>
            <Header1 breadCrumbText={breadcrumb} breadCrumbHeader={TextData.mtbfandmttr} />
            <Card className={classes.card}>

                <EnhancedMTBFAndMTTRForm {...props} />
            
            <Consumer>{({ mtbfAndMttr_Breakdown }) => {
                if (!mtbfAndMttr_Breakdown) {
                    return null
                }
                console.log(window.URL.createObjectURL(mtbfAndMttr_Breakdown))
                return <div>
                    <iframe height="600px" width="100%" src={window.URL.createObjectURL(mtbfAndMttr_Breakdown)}>
                    </iframe>
                </div>

            }}</Consumer>
            </Card>
        </Fragment >)
})

export default EnhancedMTBFAndMTTRpage;