import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withFormik, Field } from "formik"
import * as Yup from "yup";
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import { DatePicker } from 'material-ui-pickers';
import { actions, Consumer } from "./../../actions";
import Fab from "@material-ui/core/Fab";
import DownloadIcon from "@material-ui/icons/ArrowDownward";
import Header1 from './../../components/Header1/Header1';
import TextData from './textdata';
import styles from './style';

const PendingBreakdownReportsForm = withStyles(styles)((props) => {
    console.log("props", props)
    const {
        values,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue
    } = props
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid
                container
                direction="row"
                alignItems="center"
            >
            <Grid item xs={12} md={4}>
                <Button variant="contained" type="submit" color="primary" className={classes.button}>
                    {TextData.report}
                </Button>
                <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                    {TextData.cancel}
                </Button>
            </Grid>
        </Grid>
        </form >
    );
})

const EnhancedPendingBreakdownReportsForm = withFormik({

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        actions.getReport({
            spName: "Sp_Select_All_From_Pending_Breakdown_By_Plant_ID",
            payload: {},
            key: "Pending_Breakdown"
        })
    },

    displayName: 'PendingBreakdownReportsForm',
})(PendingBreakdownReportsForm)

const PendingBreakdownPage = withStyles(styles)((props) => {
    const { classes } = props
    const breadcrumb = [
        { name: "Home", path: "/" },
        { name: TextData.pendingBreakdown, path: "/reports/reportspendingbreakdown" }]
    return (
        <Fragment>
            <Header1 breadCrumbText={breadcrumb} breadCrumbHeader={TextData.pendingBreakdown} />
            <Card className={classes.card}>
            <EnhancedPendingBreakdownReportsForm style={{ "textAlign":"center"}}/>
                <Consumer>{({ Pending_Breakdown }) => {
                    if (!Pending_Breakdown) {
                        return null
                    }
                    console.log(window.URL.createObjectURL(Pending_Breakdown))
                    return <div>
                        <iframe height="600px" width="100%" src={window.URL.createObjectURL(Pending_Breakdown)}>
                        </iframe>
                    </div>
                    // return <object src="http://www.africau.edu/images/default/sample.pdf" width="100%" height="100%"></object>
                    // return Pending_Breakdown ? React.createElement("div", { dangerouslySetInnerHTML: { __html: Pending_Breakdown } }) : null

                }}</Consumer>
            </Card>
        </Fragment >)
})

export default PendingBreakdownPage;