import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withFormik, Field } from "formik"
import * as Yup from "yup";
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import { DatePicker } from 'material-ui-pickers';
import TextData from './textData';
import styles from './styles';
import { actions, Consumer } from "../../actions";
import Header1 from './../../components/Header1/Header1';

const PhenonmenonWiseBreakdownForm = withStyles(styles)((props) => {
    console.log("props", props)
    const {
        values,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue,
        errors,
        touched
    } = props
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid
                container
                direction="row"
                alignItems="center"
            >
                <Grid item xs={12} md={4}>
                    <DatePicker
                        className={classes.datepicker}
                        keyboard
                        name="fromDate"
                        label="From Date"
                        format={'DD/MM/YYYY'}
                        value={values.fromDate ? values.fromDate : null}
                        onChange={(date) => {
                            setFieldValue("fromDate", date)
                        }}
                        error={errors.fromDate && (touched.fromDate)}
                        FormHelperTextProps={{
                            error: errors.fromDate
                        }}
                        helperText={
                            errors.fromDate 
                        }
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                </Grid>

                <Grid item xs={12} md={4}>
                    <DatePicker
                        className={classes.datepicker}
                        keyboard
                        name="toDate"
                        label="To Date"
                        format={'DD/MM/YYYY'}
                        value={values.toDate ? values.toDate : null}
                        onChange={(date) => {
                            setFieldValue("toDate", date)
                        }}
                        error={errors.toDate && (touched.toDate)}
                        FormHelperTextProps={{
                            error: errors.toDate && (touched.toDate)
                        }}
                        helperText={
                            errors.toDate && (touched.toDate)
                                ? errors.toDate
                                : null
                        }
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                </Grid>
                <Grid item xs={12} md={4}>
                    <Button variant="contained" type="submit" color="primary" className={classes.button}>
                        {TextData.report}
                    </Button>
                    <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                        {TextData.cancel}
                    </Button>
                </Grid>
            </Grid>
        </form >
    );
})

const EnhancedPhenonmenonWiseBreakdownForm = withFormik({
    mapPropsToValues: () => ({ fromDate: '', toDate: '' }),
    validationSchema: Yup.object().shape({
        fromDate: Yup.date("From Date is required and should be valid").required("From Date Required"),
        toDate: Yup.date("To Date is required and should be a String").required("To Date required"),
    }),
    // Custom sync validation

    handleSubmit: (values, { setSubmitting, se, setFieldError, resetForm }) => {
        console.log("hess")
        if(values.fromDate>=values.toDate){
            setFieldError('toDate',"Should be after from date")
            return;
        }
        actions.getReport({
            spName: "SP_rpt_BM_Phenomenon_wise_breakdowns",
            payload: {
                from_date: values.fromDate,
                to_date: values.toDate
            },
            key: "Phenomenonwise_Breakdown"
        })
    },

    displayName: 'BasicForm',
})(PhenonmenonWiseBreakdownForm)

const PhenonmenonWiseBreakdownPage = withStyles(styles)((props) => {
    useEffect(() => {
        return () => {
            actions.setState({
                Phenomenonwise_Breakdown: null
            })
        }
    })
    const { classes } = props
    const breadcrumb = [
        { name: "Home", path: "/" },
        { name: TextData.phenomenonwiseBreakdown, path: "/reports/PhenomenonWiseBreakdown" }]
    return (
        <Fragment>
            <Header1 breadCrumbText={breadcrumb} breadCrumbHeader={TextData.phenomenonwiseBreakdown} />
            <Card className={classes.card}>

                <EnhancedPhenonmenonWiseBreakdownForm {...props} />
            
            <Consumer>{({ Phenomenonwise_Breakdown }) => {

                if (!Phenomenonwise_Breakdown) {
                    return null
                }
                console.log(window.URL.createObjectURL(Phenomenonwise_Breakdown))
                return <div>
                    <iframe height="600px" width="100%" src={window.URL.createObjectURL(Phenomenonwise_Breakdown)}>
                    </iframe>
                </div>

            }}</Consumer>
            </Card>
        </Fragment >)
})

export default PhenonmenonWiseBreakdownPage;