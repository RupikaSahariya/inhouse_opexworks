import React, { Fragment, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import { withFormik } from "formik"
import * as Yup from "yup";
import Grid from '@material-ui/core/Grid';
import { DatePicker } from 'material-ui-pickers';
import TextData from './textData';
import styles from './styles';
import { compose } from "recompose";
import { dataProvider } from "./../../utils/AppContext";
import { actions, Consumer } from "../../actions";
import Header1 from './../../components/Header1/Header1';
import AutoSuggest from "./../../components/Select";
import moment from "moment";

const Equipment = ((props) => {
    const {
        values,
        classes,
        errors,
        touched,
        setFieldValue
    } = props
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest2}
                name="equipment"
                id="equipment"
                label="Equipment"
                textFieldProps={{
                    helperText: errors.equipment && (touched.equipment)
                        ? errors.equipment
                        : null,
                    error: errors.equipment && (touched.equipment),
                }}
                value={values.equipment}
                placeholder={TextData.equipment}
                options={props.data ? props.data.map((item, i) => {
                    return ({
                        value: item.Eqp_ID,
                        label: item.eqp_desc,
                    })
                }) : []}
                onChange={(value) => {
                    setFieldValue("equipment", value)
                }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})

const BreakdownHistoryForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue
    } = props
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid
                container
                direction="row"
                alignItems="center"
            >
                <Grid item xs={12} md={4}>
                    <Equipment {...props} />
                </Grid>

                <Grid item xs={12} md={4}>
                    <DatePicker
                        className={classes.autoSuggest}
                        keyboard
                        name="fromDate"
                        label="From Date"
                        format={'DD/MM/YYYY'}
                        value={values.fromDate ? values.fromDate : null}
                        onChange={(date) => {
                            setFieldValue("fromDate", date)
                        }}
                        error={errors.fromDate && (touched.fromDate)}
                        FormHelperTextProps={{
                            error: errors.fromDate && (touched.fromDate)
                        }}
                        helperText={
                            errors.fromDate && (touched.fromDate)
                                ? errors.fromDate
                                : null
                        }
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                </Grid>

                <Grid item xs={12} md={4}>
                    <DatePicker
                        className={classes.autoSuggest}
                        keyboard
                        name="toDate"
                        label="To Date"
                        format={'DD/MM/YYYY'}
                        value={values.toDate ? values.toDate : null}
                        onChange={(date) => {
                            setFieldValue("toDate", date)
                        }}
                        error={errors.toDate && (touched.toDate)}
                        FormHelperTextProps={{
                            error: errors.toDate && (touched.toDate)
                        }}
                        helperText={
                            errors.toDate && (touched.toDate)
                                ? errors.toDate
                                : null
                        }
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                </Grid>
            </Grid>
            <Grid
                container
                direction="row"
                alignItems="center"
            >
                <Grid item xs={12} md={6} style={{ 'marginLeft': '20%' }}>
                    <Button variant="contained" type="submit" color="primary" className={classes.button}>
                        {TextData.report}
                    </Button>
                    <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                        {TextData.cancel}
                    </Button>
                </Grid>
            </Grid>
        </form >
    );
})

const EnhancedBreakdownHistoryForm = withFormik({
    mapPropsToValues: () => ({ equipment: '', fromDate: '', toDate: '' }),
    validationSchema: Yup.object().shape({
        equipment: Yup.string("Equipment is required and should be a String").required("Equipment Required"),
        fromDate: Yup.date("fromDate is required and should be a valid Date").required("fromDate is required"),
        toDate: Yup.date("To Date is required and should be a String").required("To Date is required"),
    }),

    handleSubmit: (values, { props, setSubmitting, se, setFieldError }) => {
        if (values.fromDate >= values.toDate) {
            setFieldError('toDate', "Should be after from date")
            return;
        }
        actions.getReport({
            spName: "SP_rpt_BM_break_down_history",
            payload: {
                from_date: moment(values.fromDate).toISOString(),
                to_date: values.toDate,
                bdm_cbm_flag: 'BDM',
                nature: '',
                eqp_id: ''
            },
            key: "Breakdown_History"
        })
    },
    displayName: 'BreakdownHistoryForm',
})(BreakdownHistoryForm)

const BreakdownHistoryPage = withStyles(styles)((props) => {
    const { classes } = props
    const breadcrumb = [
        { name: "Home", path: "/" },
        { name: TextData.breakdownHistory, path: "/reports/breakdownhistory" }]

    useEffect(() => {
        actions.getData({
            spName: "SP_Fun_Select_All_From_Equipment_By_Plant_ID",
            payload: {},
            key: "ALL_EQUIPMENTS"
        })
        return () => {
            actions.setState({
                Breakdown_History: null
            })
        }
    })
    return (
        <Fragment>
            <Header1 breadCrumbText={breadcrumb} breadCrumbHeader={TextData.breakdownHistory} />
            <Card className={classes.card}>
                <EnhancedBreakdownHistoryForm {...props} />
                <Consumer>{({ Breakdown_History }) => {
                    if (!Breakdown_History) {
                        return null
                    }
                    return <div>
                        <iframe height="600px" width="100%" src={window.URL.createObjectURL(Breakdown_History)}>
                        </iframe>
                    </div>
                }}</Consumer>
            </Card>
        </Fragment >)
})

export default compose(withStyles(styles), dataProvider({
    query: async (props) => {
        return ({
            spName: "SP_Fun_Select_All_From_Equipment_By_Plant_ID",
            payload: {}
        })
    }
}))(BreakdownHistoryPage);