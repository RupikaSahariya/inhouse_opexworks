import React, { Fragment, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import { withFormik } from "formik"
import * as Yup from "yup";
import Grid from '@material-ui/core/Grid';
import { DatePicker } from 'material-ui-pickers';
import TextData from './textData';
import styles from './styles';
import { actions, Consumer } from "../../actions";
import Header1 from './../../components/Header1/Header1';
import moment from "moment";

const ReportedBreakdownForm = withStyles(styles)((props) => {
    useEffect(() => {

    }, [])
    const {
        values,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue,
        errors,
        touched
    } = props
    console.log("props", props)
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid
                container
                direction="row"
                alignItems="center"
            >

                <Grid item xs={12} md={4}>
                    <DatePicker
                        className={classes.datepicker}
                        keyboard
                        name="fromDate"
                        id="fronDate"
                        label="From Date"
                        format={'DD/MM/YYYY'}
                        value={values.fromDate ? values.fromDate : null}
                        error={errors.fromDate }
                        FormHelperTextProps={{
                            error: errors.fromDate
                        }}
                        helperText={
                            errors.fromDate 
                        }
                        onChange={(date) => {
                            setFieldValue("fromDate", date)
                        }}
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                </Grid>
                        {console.log( touched)}
                <Grid item xs={12} md={4}>
                    <DatePicker
                        className={classes.datepicker}
                        keyboard
                        name="toDate"
                        id="toDate"
                        label="To Date"
                        format={'DD/MM/YYYY'}
                        value={values.toDate ? values.toDate : null}
                        onChange={(date) => {
                            setFieldValue("toDate", date)
                        }}
                        error={errors.toDate}
                        FormHelperTextProps={{
                            error: errors.toDate && (touched.toDate)
                        }}
                        helperText={
                            errors.toDate
                        }
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                </Grid>
                <Grid item xs={12} md={4}>
                    <Button variant="contained" type="submit" color="primary" className={classes.button}>
                        {TextData.report}
                    </Button>
                    <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                        {TextData.cancel}
                    </Button>
                </Grid>
            </Grid>
        </form >
    );
})

const EnhancedReportedBreakdownForm = withFormik({
    // mapPropsToValues: () => ({ fromDate: new Date(), toDate:new Date() }),
    validationSchema: Yup.object().shape({
        fromDate: Yup.date("From Date is required and should be valid").required("From Date Required"),
        toDate: Yup.date("To Date is required and should be a String").required("To Date required"),
    }),
    // Custom sync validation
    handleSubmit: (values, { setSubmitting, setFieldError,setFieldValue,props }) => {
        if(values.fromDate>=values.toDate){
            setFieldError('toDate',"Should be after from date")
            return;
        }
        console.log("submitted values:", values)
        actions.getReport({
            spName: "SP_rpt_BM_reported_breakdowns",
            payload: {
                from_date: moment(values.fromDate).toISOString(),
                to_date: moment(values.toDate).toISOString(),
                bdm_cbm_flag: 'BDM'
            },
            key: "Reported_Breakdown"
        })
    },

    displayName: 'BasicForm',
})(ReportedBreakdownForm)

const ReportedBreakdownPage = withStyles(styles)((props) => {
    useEffect(() => {
        return () => {
            actions.setState({
                Reported_Breakdown: null
            })
        }
    })
    const { classes } = props
    const breadcrumb = [
        { name: "Home", path: "/" },
        { name: TextData.reportedBreakdown, path: "/reports/reportedbreakdowns" }]
    console.log("Props: ", props)
    return (
        <Fragment>
            <Header1 breadCrumbText={breadcrumb} breadCrumbHeader={TextData.reportedBreakdown} />
            <Card className={classes.card}>

                <EnhancedReportedBreakdownForm {...props} />

            
            <Consumer>{({ Reported_Breakdown }) => {
                if (!Reported_Breakdown) {
                    return null
                }
                console.log(window.URL.createObjectURL(Reported_Breakdown))
                return <div>
                    <iframe height="600px" width="100%" src={window.URL.createObjectURL(Reported_Breakdown)}>
                    </iframe>
                </div>

            }}</Consumer>
            </Card>
        </Fragment >)
})

export default ReportedBreakdownPage;