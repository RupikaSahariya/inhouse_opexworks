import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withFormik, Field } from "formik"
import * as Yup from "yup";
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import { DatePicker } from 'material-ui-pickers';
import { actions, Consumer } from "../../actions";
import Fab from "@material-ui/core/Fab";
import DownloadIcon from "@material-ui/icons/ArrowDownward";
import TextData from './textData';
import styles from './styles';
import Header1 from './../../components/Header1/Header1';

const BreakdownRootCauseAnalysisForm = withStyles(styles)((props) => {
    console.log("props", props)
    const {
        values,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue
    } = props
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid
                container
                direction="row"
                alignItems="center"
            >
                <Grid item xs={12} md={6} style={{ 'marginLeft': '24%' }}>
                    <Button variant="contained" type="submit" color="primary" className={classes.button}>
                        {TextData.report}
                    </Button>
                    <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                        {TextData.cancel}
                    </Button>
                </Grid>
            </Grid>
        </form >
    );
})

const EnhancedBreakdownRootCauseAnalysisForm = withFormik({

    handleSubmit: (values, { setSubmitting, se }) => {
        console.log("hess")
        actions.getReport({
            spName: "SP_rpt_BM_RCA_Pending",
            payload: {
                bdm_cbm_flag: 'BDM'
            },
            key: "BreakdownRCAPending"
        })
    },

    displayName: 'BreakdownRootCauseAnalysisForm',
})(BreakdownRootCauseAnalysisForm)

const BreakdownRootCauseAnalysisPendingPage = withStyles(styles)((props) => {
    useEffect(() => {
        return () => {
            actions.setState({
                BreakdownRCAPending: null
            })
        }
    })
    const { classes } = props
    const breadcrumb = [
        { name: "Home", path: "/" },
        { name: TextData.breakdownRootCauseAnalysis, path: "/reports/breakdownrootcauseanalysispending" }]
    return (
        <Fragment>
            <Header1 breadCrumbText={breadcrumb} breadCrumbHeader={TextData.breakdownRootCauseAnalysis} />
            <Card className={classes.card}>
                <EnhancedBreakdownRootCauseAnalysisForm />
                {/* <Consumer>{({ BreakdownRCAPending }) => { return BreakdownRCAPending ? React.createElement("div", { dangerouslySetInnerHTML: { __html: BreakdownRCAPending } }) : null }}</Consumer> */}

                <Consumer>{({ BreakdownRCAPending }) => {
                    if (!BreakdownRCAPending) {
                        return null
                    }
                    console.log(window.URL.createObjectURL(BreakdownRCAPending))
                    return <div>
                        <iframe height="600px" width="100%" src={window.URL.createObjectURL(BreakdownRCAPending)}>
                        </iframe>
                    </div>
                    // return <object src="http://www.africau.edu/images/default/sample.pdf" width="100%" height="100%"></object>
                    // return BreakdownRCAPending ? React.createElement("div", { dangerouslySetInnerHTML: { __html: BreakdownRCAPending } }) : null

                }}</Consumer>
            </Card>
        </Fragment >)
})

export default BreakdownRootCauseAnalysisPendingPage;