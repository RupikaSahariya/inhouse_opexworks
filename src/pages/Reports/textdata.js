const TextData = {
    reportedBreakdown: 'Reported Breakdown',
    report: 'Report',
    cancel: 'Cancel',
    download: 'Download',
    phenomenonwiseBreakdown: 'Phenomenon wise Breakdown',
    mtbfandmttr: 'MTBF And MTTR',
    equipment:'Equipment',
    breakdownHistory: 'Breakdown History',
    breakdownRootCauseAnalysis: 'Breakdown Root Cause Analysis',
    pendingBreakdown: 'Pending Breakdown'
};

export default TextData;