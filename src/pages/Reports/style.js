const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    card: {
        margin: '15px',
        padding: 30,
        margin: 40,
        textAlign:"center"
    },
    button: {
        margin: theme.spacing.unit*2,
    },
    datepicker: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2,
        width: "90%",
        padding:5
    },
    formControl: {
        margin: theme.spacing.unit,
        maxWidth: 400,
        width: "90%",
        'textAlign':'left'
    },
});

export default styles;