const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    card: {
        padding: 30,
        margin: 40,
        textAlign:"center",
        overflow:"visible"
    },
    button: {
        margin: theme.spacing.unit*2,
    },
    datepicker: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2,
        width: "90%",
        padding:5
    },
    formControl: {
        margin: theme.spacing.unit,
        maxWidth: 400,
        width: "90%",
        textAlign:'left'
    },
    autoSuggest: {
        marginTop: theme.spacing.unit * 1.5,
        marginBottom: theme.spacing.unit,
        marginLeft: theme.spacing.unit * 3,
        minWidth: 100,
        width: "90%",
        textAlign: 'left',
        display: "flex"
    },
    autoSuggest2: {
        marginTop: theme.spacing.unit * 3,
        marginBottom: theme.spacing.unit,
        marginLeft: theme.spacing.unit * 3,
        minWidth: 100,
        width: "90%",
        textAlign: 'left',
        display: "flex"
    },
});

export default styles;