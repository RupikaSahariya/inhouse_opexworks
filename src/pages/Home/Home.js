import React, { useEffect, Fragment } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Grid from '@material-ui/core/Grid';
import { Bar, HorizontalBar, Line, Bubble, Doughnut, Pie, Polar, Radar } from 'react-chartjs-2';
import { Wrapper } from '../../components';
import { mockChart } from '../../utils/mock';
import { actions, connect } from './../../actions';
import moment from 'moment';
import Typography from '@material-ui/core/Typography';
import Chart from "./../Graph/Charts";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
// import { Button } from '@material-ui/core';
import StatCard from '../../components/Cards/StatCard';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import DoneIcon from "@material-ui/icons/Done";
import TimerIcon from "@material-ui/icons/Timer";
import TrendingUpIcon from "@material-ui/icons/TrendingUp";
import ExposureIcon from "@material-ui/icons/Exposure";
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core/styles';
import AutoSuggest from "./../../components/Select";
import { VALID_YEAR } from "./../../utils/valdations";
import { withFormik } from "formik"
import * as Yup from "yup";
import { DatePicker } from 'material-ui-pickers';
import Button from '@material-ui/core/Button';
import { Fab } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  datepicker: {
    marginBottom: theme.spacing.unit,
    // marginLeft: theme.spacing.unit * 1.5,
    marginTop: theme.spacing.unit * 3,
    minWidth: 150,
    width: "95%",
    textAlign: 'left',
    display: "flex"
  },
})

const height = 200;
var monthlist1 = [];
var monthlist2 = [];
var monthlist3 = [];
var srno = [];
var percentsalelist1 = [];
var percentsalelist2 = [];
var percentsalelist3 = [];
var totalcountlist = [];

const allYears = [2015, 2016, 2017, 2018, 2019];

const dashboardForm = withStyles(styles)((props) => {
  const {
    values,
    touched,
    errors,
    classes,
    setFieldValue,
    handleChange,
    handleReset,
    handleSubmit
  } = props;
  return (
    <React.Fragment>
      <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
        <Grid container spacing={8} direction="row" justify="space-around">

          <Grid item xs={12} md={4}>
            <DatePicker
              className={classes.datepicker}
              keyboard
              name="fromDate"
              id="fronDate"
              label="From Date"
              format={'DD/MM/YYYY'}
              value={values.fromDate ? values.fromDate : null}
              error={errors.fromDate}
              FormHelperTextProps={{
                error: errors.fromDate
              }}
              helperText={
                errors.fromDate
              }
              onChange={(date) => {
                setFieldValue("fromDate", date)
              }}
              disableOpenOnEnter
              animateYearScrolling={false}
            />
          </Grid>
          {console.log(touched)}
          <Grid item xs={12} md={4}>
            <DatePicker
              className={classes.datepicker}
              keyboard
              name="toDate"
              label="To Date"
              format={'DD/MM/YYYY'}
              // helperText="With min and max"
              value={values.toDate ? values.toDate : null}
              onChange={(date) => {
                setFieldValue("toDate", date)
              }}
              error={errors.toDate && (touched.toDate)}
              FormHelperTextProps={{
                error: errors.toDate && (touched.toDate)
              }}
              helperText={
                errors.toDate && (touched.toDate)
                  ? errors.toDate
                  : null
              }
              disableOpenOnEnter
              animateYearScrolling={false}
            />
          </Grid>
          <Grid item xs={6} sm={3} md={2} className={classes.datepicker}>
            <Fab type="submit" variant="extended" color="primary" style={{ color: "white", fontWeight: "900", borderRadius: "15px", maxHeight: "35px", marginTop: "12px" }}> Analytics </Fab>
          </Grid>
        </Grid>
      </form>
    </React.Fragment>)
})

const EnhancedDashboardForm = withFormik({
  mapPropsToValues: () => ({ fromDate: moment(new Date()).subtract('6', 'months').toISOString(), toDate: moment(new Date()).toISOString() }),
  validationSchema: Yup.object().shape({
    // frommonth: Yup.string("From Month is required and should be valid").required("From Month Required"),
    // tomonth: Yup.string("To Month is required and should be valid").required("To Month Required"),
    // fromyear: Yup.string("From Year is required and should be valid").required("From Year Required"),
    // toyear: Yup.string("To Year is required and should be valid").required("To Year Required"),
    // fromyear: VALID_YEAR,
  }),
  // Custom sync validation

  handleSubmit: (values, { setSubmitting, se }) => {
    actions.percentagedowntimedata({ from_date: moment(values.fromDate).toISOString(), to_date: moment(values.toDate).toISOString() })
    actions.mtbftrenddata({ from_date: moment(values.fromDate).toISOString(), to_date: moment(values.toDate).toISOString() })
    actions.mttrtrenddata({ from_date: moment(values.fromDate).toISOString(), to_date: moment(values.toDate).toISOString() })
    actions.highdowntimedata({ from_date: moment(values.fromDate).toISOString(), to_date: moment(values.toDate).toISOString() })
    actions.highdowntimefailureareadata({ from_date: moment(values.fromDate).toISOString(), to_date: moment(values.toDate).toISOString() })
    actions.toptendowntimedata({ from_date: moment(values.fromDate).toISOString(), to_date: moment(values.toDate).toISOString() })
    actions.numberofbreakdownsdata({ from_date: moment(values.fromDate).toISOString(), to_date: moment(values.toDate).toISOString() })
    actions.totaldowntimedata({ from_date: moment(values.fromDate).toISOString(), to_date: moment(values.toDate).toISOString() })
    actions.percentofpowercostdata({ from_date: moment(values.fromDate).toISOString(), to_date: moment(values.toDate).toISOString() })
    actions.totalmaintenancecostdata({ from_date: moment(values.fromDate).toISOString(), to_date: moment(values.toDate).toISOString() })
    actions.RCADonedata({})
    actions.maintenancekaizendata({ from_date: moment(values.fromDate).toISOString(), to_date: moment(values.toDate).toISOString() })
  },

  displayName: 'EnhancedMTBFAndMTTRForm',
})(dashboardForm)

const Charts = (props) => {
  useEffect(() => {
    actions.percentagedowntimedata({ from_date: moment().subtract('1', 'years').toISOString(), to_date: moment().toISOString() })
    actions.mtbftrenddata({ from_date: moment().subtract('6', 'months').toISOString(), to_date: moment().toISOString() })
    actions.mttrtrenddata({ from_date: moment().subtract('6', 'months').toISOString(), to_date: moment().toISOString() })
    actions.highdowntimedata({ from_date: moment().subtract('6', 'months').toISOString(), to_date: moment().toISOString() })
    actions.highdowntimefailureareadata({ from_date: moment().subtract('6', 'months').toISOString(), to_date: moment().toISOString() })
    actions.toptendowntimedata({ from_date: moment().subtract('6', 'months').toISOString(), to_date: moment().toISOString() })
    actions.numberofbreakdownsdata({ from_date: moment().subtract('6', 'months').toISOString(), to_date: moment().toISOString() })
    actions.totaldowntimedata({ from_date: moment().subtract('6', 'months').toISOString(), to_date: moment().toISOString() })
    actions.percentofpowercostdata({ from_date: moment().subtract('6', 'months').toISOString(), to_date: moment().toISOString() })
    actions.totalmaintenancecostdata({ from_date: moment().subtract('6', 'months').toISOString(), to_date: moment().toISOString() })
    actions.RCADonedata({})
    actions.maintenancekaizendata({ from_date: moment().subtract('6', 'months').toISOString(), to_date: moment().toISOString() })
  }, [])
  const numberofbreakdowns = props.numberofbreakdownsdata ? props.numberofbreakdownsdata.length : 0;
  const totaldowntime = props.totaldowntimedata ? props.totaldowntimedata.length : 0;
  const percentofpowercost = props.percentofpowercostdata ? props.percentofpowercostdata.length : 0;
  const totalmaintenancecost = props.totalmaintenancecostdata ? props.totalmaintenancecostdata.length : 0;
  const RCADone = props.RCADonedata ? props.RCADonedata.length : 0;
  const maintenancekaizen = props.maintenancekaizendata ? props.maintenancekaizendata.length : 0;
  console.log(props)
  props.percentagedowntimedata ? props.percentagedowntimedata.map((record, i) => {
    monthlist1[i] = record.Months;
    percentsalelist1[i] = record.percent_sale    //record.percent_sale
  }) : null

  props.mtbftrenddata ? props.mtbftrenddata.map((record, i) => {
    monthlist2[i] = record.Months;
    percentsalelist2[i] = record.percent_sale    //record.percent_sale
  }) : null

  props.mttrtrenddata ? props.mttrtrenddata.map((record, i) => {
    monthlist3[i] = record.Months;
    percentsalelist3[i] = record.percent_sale    //record.percent_sale
  }) : null

  props.highdowntimedata ? props.highdowntimedata.map((record, i) => {
    srno[i] = i;
    totalcountlist[i] = record.Total_cnt
  }) : null
  console.log(props)

  return (
    <Wrapper>

      <Grid container spacing={8} style={{ marginBottom: "8px" }}>

        <Grid item xs={12}>

          <Grid item style={{ padding: "5px" }}>
            <Card>
              <CardContent>
                <Typography variant="h5"
                  style={{ textAlign: "center", marginTop: "5px", marginBottom: "15px", fontFamily: "Times", fontSize: "200%", color: "#3f51b5" }}>
                  <strong> Breakdown Maintenance Analytics
                  </strong>
                </Typography>
                <EnhancedDashboardForm />

              </CardContent>
            </Card>
          </Grid>

          <Grid container>
            {<Grid item xs={12} sm={6} md={4} style={{ padding: "5px" }}>
              <Card>
                <CardContent>
                  <Typography variant="h6" style={{ textAlign: "center", marginTop: "20px" }}> % Down Time Trend </Typography>
                  {percentsalelist1.length > 0 ?
                    <Bar
                      yAxislabel="Downtime %"
                      xAxislabel="Month"
                      legend={{ display: false }}
                      data={{
                        datasets: [{
                          label: 'Series 1',
                          backgroundColor: "#7986cb",
                          borderColor: "#3f51b5",
                          borderWidth: 0,
                          data: percentsalelist1 ? percentsalelist1.map(item => parseFloat(item)) : null,
                          pointBackgroundColor: "#3f51b5",
                          pointBorderColor: "#fff"
                        }],
                        labels: monthlist1 ? monthlist1.map(item => item) : null
                      }}
                      options={{
                        scales: {
                          yAxes: [{
                            ticks: {
                              beginAtZero: true
                            },
                            scaleLabel: {
                              display: true,
                              fontStyle: "bold",
                              labelString: "% Down Time",
                              fontSize: 14
                            }
                          }],
                          xAxes: [{
                            scaleLabel: {
                              display: true,
                              fontStyle: "bold",
                              labelString: "Month",
                              fontSize: 14
                            }
                          }]
                        }
                      }}
                    /> : <div style={{ height: 110, width: '100%', paddingTop: '20%', paddingLeft: '40%' }} ><CircularProgress /></div>
                  }
                </CardContent>
              </Card>
            </Grid>
            }

            {<Grid item xs={12} sm={6} md={4} style={{ padding: "5px" }}>
              <Card>
                <CardContent>
                  <Typography variant="h6" style={{ textAlign: "center", marginTop: "20px" }}> MTBF Trend </Typography>
                  {percentsalelist2.length > 0 ?
                    <Line
                      legend={{ display: false }}
                      data={{
                        datasets: [{
                          label: 'Series 1',
                          backgroundColor: "#7986cb",
                          borderColor: "#3f51b5",
                          borderWidth: 0,
                          tension: 0,
                          fill: false,
                          data: percentsalelist2 ? percentsalelist2.map(item => parseFloat(item)) : null,
                          pointBackgroundColor: "#3f51b5",
                          pointBorderColor: "#fff"
                        }],
                        labels: monthlist2 ? monthlist2.map(item => item) : null
                      }}
                      options={{
                        scales: {
                          yAxes: [{
                            ticks: {
                              beginAtZero: true
                            },
                            scaleLabel: {
                              display: true,
                              fontStyle: "bold",
                              labelString: "Days",
                              fontSize: 14
                            }
                          }],
                          xAxes: [{
                            scaleLabel: {
                              display: true,
                              fontStyle: "bold",
                              labelString: "Month",
                              fontSize: 14
                            }
                          }]
                        }
                      }}
                    /> : <div style={{ height: 110, width: '100%', paddingTop: '20%', paddingLeft: '40%' }} ><CircularProgress /></div>
                  }
                </CardContent>
              </Card>
            </Grid>
            }

            {
              <Grid item xs={12} sm={6} md={4} style={{ padding: "5px" }}>
                <Card>
                  <CardContent>
                    <Typography variant="h6" style={{ textAlign: "center", marginTop: "20px" }}> MTTR Trend </Typography>
                    {percentsalelist3.length > 0 ?
                      <Line
                        legend={{ display: false }}
                        data={{
                          datasets: [{
                            label: 'Series 1',
                            backgroundColor: "#71106cb",
                            borderColor: "#3f51b5",
                            borderWidth: 0,
                            tension: 0,
                            data: percentsalelist3 ? percentsalelist3.map(item => parseFloat(item)) : null,
                            pointBackgroundColor: "#3f51b5",
                            pointBorderColor: "#fff"
                          }],
                          labels: monthlist3 ? monthlist3.map(item => item) : null
                        }}
                        options={{
                          scales: {
                            yAxes: [{
                              ticks: {
                                beginAtZero: true
                              },
                              scaleLabel: {
                                display: true,
                                fontStyle: "bold",
                                labelString: "Hours",
                                fontSize: 14
                              }
                            }],
                            xAxes: [{
                              scaleLabel: {
                                display: true,
                                fontStyle: "bold",
                                labelString: "Month",
                                fontSize: 14
                              }
                            }]
                          }
                        }}
                      /> : <div style={{ height: 110, width: '100%', paddingTop: '20%', paddingLeft: '40%' }} ><CircularProgress /></div>
                    }
                  </CardContent>
                </Card>
              </Grid>
            }
          </Grid>

          <Grid container>
            {
              <Grid item style={{ textAlign: "center", padding: "5px", minWidth: "100%" }} direction="row">
                <Grid container direction="row">
                  <Grid item xs={12} sm={4} md={3}>
                    <Grid item xs={12} style={{ padding: "2px" }}>
                      <StatCard
                        type="fill"
                        title={numberofbreakdowns}
                        value="Number of Breakdowns"
                        icon={<ExposureIcon style={{ color: 'white' }} />}
                        color="#5469BF"
                      />
                    </Grid>
                  </Grid>

                  <Grid item xs={12} sm={4} md={3}>
                    <Grid item xs={12} style={{ padding: "2px" }}>
                      <StatCard
                        type="fill"
                        title={totaldowntime}
                        value="Total Down Time ( Hrs )"
                        icon={<TimerIcon />}
                        color="#5469BF"
                      />
                    </Grid>
                  </Grid>

                  <Grid item xs={12} sm={4} md={3}>
                    <Grid item xs={12} style={{ padding: "2px" }}>
                      <StatCard
                        type="fill"
                        title={percentofpowercost}
                        value="% of power cost / sales"
                        icon={<TrendingUpIcon />}
                        color="#5469BF"
                      />
                    </Grid>
                  </Grid>

                  <Grid item xs={12} sm={4} md={3}>
                    <Grid item xs={12} style={{ padding: "2px" }}>
                      <StatCard
                        type="fill"
                        title={totalmaintenancecost}
                        value="Total Maintenance Cost"
                        icon={<AttachMoneyIcon />}
                        color="#5469BF"
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            }
          </Grid>

          <Grid container>
            {
              <Grid item xs={12} sm={6} md={4} style={{ padding: "5px" }}  >
                <Card style={{ minHeight: "250px", maxHeight: "35px" }} >
                  <CardContent>
                    <Typography variant="h6" style={{ textAlign: "center" }}> High Down Time Machines </Typography>
                    {totalcountlist.length > 0 ?
                      <HorizontalBar
                        legend={{ display: false }}
                        height="140px"
                        data={{
                          datasets: [{
                            label: 'Series 1',
                            backgroundColor: "#7986cb",
                            borderColor: "#3f51b5",
                            borderWidth: 0,
                            data: totalcountlist ? totalcountlist.map(item => parseFloat(item)) : null,
                            pointBackgroundColor: "#3f51b5",
                            pointBorderColor: "#fff"
                          }],
                          labels: srno ? srno.map(item => item) : null
                        }}
                        options={{
                          scales: {
                            yAxes: [{
                              ticks: {
                                beginAtZero: true
                              },
                              scaleLabel: {
                                display: true,
                                fontStyle: "bold",
                                // labelString: props.yAxislabel,
                                fontSize: 16
                              }
                            }],
                            xAxes: [{
                              scaleLabel: {
                                display: true,
                                fontStyle: "bold",
                                labelString: "Hours",
                                fontSize: 16
                              }
                            }]
                          }
                        }}
                      /> : <div style={{ height: 110, width: '100%', paddingTop: '20%', paddingLeft: '40%' }} ><CircularProgress /></div>
                    }
                  </CardContent>
                </Card>
              </Grid>
            }
            {
              <Grid item xs={12} sm={6} md={5} style={{ padding: "5px" }} >
                <Card>
                  <CardContent>
                    <Typography variant="h6" style={{ color: "red", textAlign: "center" }}> High Down Time Failure Areas </Typography>

                    <Table size="small">
                      <TableBody>
                        {props.highdowntimefailureareadata ? props.highdowntimefailureareadata.map((row, i) => (
                          <TableRow key={row.name} style={{ height: "35px" }}>
                            <TableCell component="th" scope="row"> {i + 1} </TableCell>
                            <TableCell align="left"> {row.area_desc} </TableCell>
                            <TableCell align="right" style={{ paddingRight: "5%" }}> {row.Total_cnt} </TableCell>
                          </TableRow>
                        )) : <div style={{ height: 110, width: '100%', paddingTop: '20%', paddingLeft: '40%' }} ><CircularProgress /></div>
                        }
                      </TableBody>
                    </Table>
                  </CardContent>
                </Card>
              </Grid>
            }

            {
              <Grid item xs={12} sm={6} md={3} style={{ paddingTop: "5px" }}>
                <Card style={{ minHeight: "250px", maxHeight: "35px" }}>
                  <CardContent>
                    <Grid item style={{ textAlign: "center" }}>
                      <Grid item xs={12} style={{ color: "red", marginTop: "-5px" }}>
                        <StatCard
                          type="fill"
                          title={RCADone}
                          value="Root Cause Analysis Done"
                          // icon={<DoneIcon />}
                          color="#0097A7"
                        />
                      </Grid>
                      <Grid item xs={12} style={{ marginTop: "5px" }}>
                        <StatCard
                          type="fill"
                          title={maintenancekaizen}
                          value="Maintenance Kaizen Done"
                          // icon={<DoneIcon />}
                          color="#0097A7"
                          style={{ textAlign: "center" }}
                        />
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>
            }
          </Grid>
        </Grid>
      </Grid>
    </Wrapper>
  )
};

const mapper = state => {
  console.log(state)
  return state

}

export default connect(mapper)(Charts);