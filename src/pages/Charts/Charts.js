import React from 'react';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import { Bar } from 'react-chartjs-2';
import { Wrapper } from '../../components';
import { mockChart } from '../../utils/mock';

const Charts = () => (
  <Wrapper>
    <Grid container spacing={8}>
      { mockChart.map((chart, index) => (
        <Grid item xs={12} sm={6} md={6} lg={12} key={index}>
            <CardContent>
              { chart.type === 'bar' &&
                <Bar
                  data={chart.data}
                  height={chart.height}
                  options={chart.options}
                />
              }
            </CardContent>
        </Grid>
      ))}
    </Grid>
  </Wrapper>
);

export default Charts;