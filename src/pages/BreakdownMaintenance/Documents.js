import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withFormik } from "formik";
import * as Yup from "yup";
import { DatePicker } from 'material-ui-pickers';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import AgTable from "./../../components/AgTable";
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import RefreshIcon from "@material-ui/icons/RefreshOutlined";
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { compose } from "recompose";
import moment from "moment";
import { Consumer, actions } from "./../../actions";
import styles from "./styles1";
import TextData from "./textData1";
import AutoSuggest from "./../../components/Select";
import Typography from '@material-ui/core/Typography';
import FormLabel from '@material-ui/core/FormLabel';

const UploadedBy = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props;
    return (
        <React.Fragment>
            {/* <FormLabel style={{  textAlign:"left", marginTop: "25px", fontSize: "12px"}} className={classes.formlabel}>Uploaded By</FormLabel> */}
            <AutoSuggest className={classes.autoSuggest2}
                name="uploadedBy"
                id="uploadedBy"
                label={TextData.uploadedBy}
                error={errors.uploadedBy && (touched.uploadedBy)} FormHelperTextProps={{
                    error: errors.uploadedBy && (touched.uploadedBy)
                }}
                helperText={
                    errors.uploadedBy && (touched.uploadedBy)
                        ? errors.uploadedBy
                        : null
                }
                value={values.uploadedBy}
                placeholder={TextData.uploadedBy}
                options={props.ALL_EMPLOYEE_MASTER ? props.ALL_EMPLOYEE_MASTER.data.map((item, i) => {
                    return ({
                        value: item.emp_id,
                        label: item.emp_name,
                    })
                }) : []}
                onChange={(value) => {
                    setFieldValue("uploadedBy", value)
                }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})

const DocumentsForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        handleClose,
        setFieldValue
    } = props
    console.log(props)
    return (
        <Fragment>
            <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid item xs={12}>
                        <FormControl className={classes.autoSuggest} style={{ marginTop: "-25px" }}>
                            <Typography variant="subheading" style={{ color: "#888", marginBottom: "5px" }}>Document Name: {props.payload ? props.payload.name : ""} </Typography>
                            <input
                                id="file"
                                name="file"
                                type="file"
                                onChange={(event) => {
                                    setFieldValue("file", event.currentTarget.files[0]);
                                }}
                            />
                            {/* </Typography> */}
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} className={classes.Grid}>
                        <FormControl className={classes.autoSuggest}>
                            <TextField fullWidth
                                name="description"
                                label="Description"
                                multiline
                                rows='1'
                                value={values.description}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                error={errors.description && (touched.description)}
                                FormHelperTextProps={{
                                    error: errors.description && (touched.description)
                                }}
                                helperText={
                                    errors.description && (touched.description)
                                        ? errors.description
                                        : null
                                }
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} className={classes.Grid}>
                        <UploadedBy {...props} />
                    </Grid>

                    <Grid item xs={12} className={classes.Grid}>
                        <FormControl className={classes.autoSuggest}>
                            <DatePicker
                                keyboard
                                name="uploadedDate"
                                id="uploadedDate"
                                label="Uploaded Date"
                                format={'DD/MM/YYYY'}
                                value={values.uploadedDate}
                                onChange={(date) => {
                                    setFieldValue("uploadedDate", date)
                                }}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </FormControl>
                    </Grid>

                    <Grid
                        item xs={12}
                        justify="center"
                        className={classes.button}
                    >
                        <Button variant="contained" type="submit" color="primary" style={{ marginLeft: "10px" }}>
                            {TextData.save}
                        </Button>
                        <Button type="button" variant="contained" color="secondary" onClick={handleClose} style={{ marginLeft: "10px" }}>
                            {TextData.cancel}
                        </Button>
                    </Grid>

                </Grid>
            </form>
        </Fragment>
    )
});

const EnhancedDocumentsForm = withFormik({
    mapPropsToValues: (props) => {
        if (!props.payload) {
            return ({ description: '', uploadedBy: '', uploadedDate: new Date() })
        }
        return ({
            description: props.payload.data.Document_Description, uploadedBy: props.payload.data.Document_Uploaded_by, uploadedDate: moment(props.payload.data.Uploaded_Date)
        })
    },
    validationSchema: Yup.object().shape({
        description: Yup.string("Description is required and should be a String").required("Description Required"),
        uploadedBy: Yup.string("Uploaded By is required and should be a String").required("Uploaded By Required"),
        uploadedDate: Yup.date("Uploaded Date is required and should be a valid Date").required("Uploaded Date Required"),
    }),

    handleSubmit: async (values, formikBag) => actions.documentInsert({ values, formikBag }),
    displayName: 'DocumentsForm',
})(DocumentsForm)

class DocumentsPage extends React.PureComponent {
    state = {
        // dialogOpen: false
        selectedData: null
    }

    handleClickOpen = (selectedData) => {
        this.setState({ dialogOpen: true, selectedData: selectedData });
    };

    handleClose = () => {
        actions.closeDocument({ page: 'DOCUMENTS_PAGE_DIALOG' })
    };

    componentDidMount() {
        actions.getDatas([{
            spName: "sp_select_all_from_Employee_master",
            payload: {},
            key: "ALL_EMPLOYEE_MASTER"
        }, {
            spName: "SP_Select_All_From_Break_Down_Maint_Documents_By_Eqp_ID_And_BD_SrNo",
            payload: {
                Eqp_ID: this.props.documentData[4][0].Eqp_ID ? this.props.documentData[4][0].Eqp_ID : null,
                BD_SrNo: this.props.documentData[4][0].bd_srno ? this.props.documentData[4][0].bd_srno : null
            },
            key: "PENDING_BREAKDOWN_DOCUMENT_DATA"
        }]);
    }

    onGridReady = (params) => {
        params.api.sizeColumnsToFit()
        this.gridParams = params
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

    exportData = () => {
        this.gridApi.exportDataAsCsv(this.gridParams);
    }

    onDelete = async (selectedData) => {
        await actions.getData({
            spName: "SP_Insert_Update_Delete_Break_Down_Maint_Documents",
            payload: {
                Document_Srno: selectedData.data.Document_srno,
                Eqp_ID: selectedData.data.Eqp_ID,
                BD_SrNo: selectedData.data.bd_srno,
                Document_Name: selectedData.data.Document_Name,
                Document_Description: selectedData.data.Document_Description,
                Document_Uploaded_by: selectedData.data.Document_Uploaded_by,
                Uploaded_Date: selectedData.data.Uploaded_Date,
                Document_Data: selectedData.data.Document_Data,
                Flag: "D"
            },
            key: "SP_Insert_Update_Delete_Break_Down_Maint_Documents" + new Date().toISOString()
        })
        actions.snackBarHandleOpen({
            message: "Document deleted"
        })
        await actions.getData({
            spName: "SP_Select_All_From_Break_Down_Maint_Documents_By_Eqp_ID_And_BD_SrNo",
            payload: {
                Eqp_ID: selectedData.data.Eqp_ID,
                BD_SrNo: selectedData.data.bd_srno
            },
            key: "PENDING_BREAKDOWN_DOCUMENT_DATA"
        })
    }

    onEdit = (selectedData) => {
        actions.handleDocumentDialog({ page: "DOCUMENTS_PAGE_DIALOG", payload: selectedData })
    }

    onViewDocument = (selectedData) => {
        window.open(`${process.env.REACT_APP_API_URL}/get-file/${selectedData.data.Document_Data}`)
    }

    handleRefresh = async () => {
        await actions.getData({
            spName: "SP_Select_All_From_Break_Down_Maint_Documents_By_Eqp_ID_And_BD_SrNo",
            payload: {
                Eqp_ID: this.props.documentData.Eqp_ID,
                BD_SrNo: this.props.documentData.bd_srno
            },
            key: "PENDING_BREAKDOWN_DOCUMENT_DATA"
        })
    }

    render() {
        const columnDefs = [{
            headerName: "Sr.No.", field: "srno", sortable: true, filter: true, width: 40,
            cellRenderer: "toolTipRenderer", toolTipText: "srno", cellClass: "centered-content"
        },
        {
            headerName: "Name", field: "name", sortable: true, filter: true, width: 90,
            cellRenderer: "toolTipRenderer", toolTipText: "name", cellClass: "left-aligned-content"
        },
        {
            headerName: "Description", field: "description", sortable: true, filter: true, width: 90,
            cellRenderer: "toolTipRenderer", toolTipText: "description", cellClass: "left-aligned-content"
        },
        {
            headerName: "Uploaded Date", field: "uploadedDate", sortable: true, filter: true, width: 40,
            cellRenderer: "toolTipRenderer", toolTipText: "uploadedDate"
        },
        {
            headerName: "", width: 25, cellRenderer: "viewDocumentRenderer"
        },
        {
            headerName: "", width: 50, cellRenderer: "editRenderer"
        },
        ]

        const { classes } = this.props
        console.log(this.props)
        return (
            <Fragment>
                <Consumer>
                    {React.memo(({ ALL_EMPLOYEE_MASTER, DOCUMENTS_PAGE_DIALOG }) => (<Dialog
                        disableBackdropClick
                        className={classes.dialogStyles}
                        open={DOCUMENTS_PAGE_DIALOG.isOpen}
                        handleClose={this.handleClose}
                        aria-labelledby="form-dialog-title"
                        maxWidth="sm"
                    >
                        <DialogTitle id="form-dialog-title">{TextData.addDocument}</DialogTitle>
                        <DialogContent>
                            <EnhancedDocumentsForm {...this.props} handleClose={this.handleClose} payload={DOCUMENTS_PAGE_DIALOG.payload} ALL_EMPLOYEE_MASTER={ALL_EMPLOYEE_MASTER} selectedData={this.state.selectedData} />
                        </DialogContent>
                    </Dialog>))}
                </Consumer>
                <Grid
                    container
                    direction="row"
                    justify="flex-end" engine
                >
                    <div className={classes.toolbar}>
                        <Grid item xs={12} direction="row" className={classes.gridLabel}>
                            <Typography variant="subtitle2">
                                Equipment : {this.props.documentData.eqp_desc}
                                {/* </Typography>
                            <Typography variant="subtitle2"> */}
                                &nbsp; &nbsp;   |   &nbsp;&nbsp;
                                Nature of Breakdown : {this.props.documentData.breakdownData[0].bd_det}
                            </Typography>
                        </Grid>
                        <Tooltip title="Add New Breakdown Documents">
                            <Fab size={"small"} color="primary" variant="extended" onClick={() => actions.handleDocumentDialog({ page: "DOCUMENTS_PAGE_DIALOG" })} className={classes.toolbarIcon1}>
                                <AddIcon />&nbsp;
                        </Fab>
                        </Tooltip>
                        <Tooltip title="Refresh">
                            <Fab size={"small"} color="primary" variant="extended" onClick={this.handleRefresh} className={classes.toolbarIcon2}>
                                <RefreshIcon />&nbsp;
                        </Fab>
                        </Tooltip>
                    </div>
                </Grid>
                <Paper style={{ height: window.innerHeight - 160, paddingTop: '10px' }}>
                    <Consumer>
                        {({ PENDING_BREAKDOWN_DOCUMENT_DATA }) => {
                            if (!PENDING_BREAKDOWN_DOCUMENT_DATA) {
                                return <div style={{ height: window.innerHeight - 250, width: '100%', paddingTop: '15%' }} ><CircularProgress /></div>
                            }
                            const rowData = PENDING_BREAKDOWN_DOCUMENT_DATA.data.map((item, i) => ({
                                data: item,
                                srno: item.Document_srno, name: item.Document_Name, description: item.Document_Description, uploadedDate: moment(item.Uploaded_Date).format("DD/MM/YY")
                            }))
                            return (
                                <AgTable
                                    loading={this.props.loading}
                                    onGridReady={this.onGridReady}
                                    columnDefs={columnDefs}
                                    rowData={rowData}
                                    onEdit={this.onEdit}
                                    onDelete={this.onDelete}
                                    onViewDocument={this.onViewDocument}
                                />
                            )
                        }}
                    </Consumer>
                </Paper>
            </Fragment >)
    }
}

export default compose(withStyles(styles))(DocumentsPage);