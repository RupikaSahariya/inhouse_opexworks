import React, { Fragment, useState, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import { withFormik } from "formik";
import * as Yup from "yup";
import { DatePicker, TimePicker } from 'material-ui-pickers';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import TextData from './textData';
import Scroll from '../Scroll';
import styles from "./styles";
import CircularProgress from '@material-ui/core/CircularProgress';
import moment from "moment";
import { actions } from "./../../../actions";
import AutoSuggest from "./../../../components/Select";

const Equipments = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props
    return (
    <React.Fragment>
        <AutoSuggest className={classes.autoSuggest}
                name="equipment"
                label="Equipment"
                error={errors.equipment && (touched.equipment)} FormHelperTextProps={{
                    error: errors.equipment && (touched.equipment)
                }}                
                helperText={
                    errors.equipment && (touched.equipment)
                        ? errors.equipment
                        : null
                }
                value={values.equipment}
                placeholder={TextData.equipment}
                options={props.loading || props.data[0].map((item, i) => ({
                    value: item.Eqp_ID,
                    label: item.eqp_desc ,
                }))}
                onChange={(value) => {
                    setFieldValue("equipment", value)
                    setFieldValue("breakdownPhenomenon", null)
                }}
            >
            </AutoSuggest>
    </React.Fragment>
    )
})

const ReportedBy = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props;
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest}
                name="reportedby"
                // FormHelperTextProps={{
                //     error: errors.reportedby && (touched.reportedby)
                // }}
                label="Reported By"
                error={errors.reportedby && (touched.reportedby)} FormHelperTextProps={{
                    error: errors.reportedby && (touched.reportedby)
                }}                
                helperText={
                    errors.reportedby && (touched.reportedby)
                        ? errors.reportedby
                        : null
                }
                value={values.reportedby}
                placeholder={TextData.reportedBy}
                options={props.loading || props.data[1].map((item, i) => ({
                    value: item.emp_id,
                    label: item.emp_name,
                }))}
                onChange={(value) => {
                    setFieldValue("reportedby", value)
                }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})

const BreakdownPhenomenon = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue,
        breakdownPhenomenonMaster
    } = props
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest}
                name="breakdownPhenomenon"
                error={errors.breakdownPhenomenon && (touched.breakdownPhenomenon)} FormHelperTextProps={{
                    error: errors.breakdownPhenomenon && (touched.breakdownPhenomenon)
                }}
                label="breakdown Phenomenon"
                helperText={
                    errors.breakdownPhenomenon && (touched.breakdownPhenomenon)
                        ? errors.breakdownPhenomenon
                        : null
                }
                value={values.breakdownPhenomenon}
                placeholder={TextData.breakdownPhenomenon}
                isDisabled = {breakdownPhenomenonMaster === undefined || breakdownPhenomenonMaster.length==0}
                options={breakdownPhenomenonMaster ? breakdownPhenomenonMaster.map((item, i) => ({
                    value: i,
                    label: item.Phy_Pheno_Desc,
                })) : []}
                onChange={(value) => {
                    setFieldValue("breakdownPhenomenon", value)
                }}
            >
            </AutoSuggest>

        </React.Fragment>
    )
})

const EnteredBy = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest}
                name="enteredBy"
                error={errors.enteredBy && (touched.enteredBy)} FormHelperTextProps={{
                    error: errors.enteredBy && (touched.enteredBy)
                }}
                label="Entered By"
                helperText={
                    errors.enteredBy && (touched.enteredBy)
                        ? errors.enteredBy
                        : null
                }
                value={values.enteredBy}
                placeholder={TextData.enteredBy}
                options={props.loading || props.data[1].map((item, i) => ({
                    value: item.emp_id,
                    label: item.emp_name,
                }))}
                onChange={(value) => {
                    setFieldValue("enteredBy", value)
                }}
            >
            </AutoSuggest>

        </React.Fragment>
        )
})

const AssignedTo = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props
    return (
        <React.Fragment>
        <AutoSuggest className={classes.autoSuggest}
            name="assignedTo"
            error={errors.assignedTo && (touched.assignedTo)} FormHelperTextProps={{
                error: errors.assignedTo && (touched.assignedTo)
            }}
            label="Assigned to"
            helperText={
                errors.assignedTo && (touched.assignedTo)
                    ? errors.assignedTo
                    : null
            }
            value={values.assignedTo}
            placeholder={TextData.assignedTo}
            options={props.loading || props.data[1].map((item, i) => ({
                value: item.emp_id,
                label: item.emp_name,
            }))}
            onChange={(value) => {
                setFieldValue("assignedTo", value)
            }}
        >
        </AutoSuggest>

    </React.Fragment>
    )
})

const ReportingForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue,
        getData,
        status
    } = props
    const [breakdownPhenomenonMaster, setbreakdownPhenomenonMaster] = useState([]);
    useEffect(() => {
        async function setbreakdownPhenomenonMasterData() {
            const data = await Promise.all([
                getData({
                    spName: "SP_Select_All_From_Bd_Physical_Pheno_By_Eqp_ID",
                    payload: {
                        Eqp_id: values.equipment
                    }
                })
            ])
            if (data && data.length > 0) {
                setbreakdownPhenomenonMaster(data[0].data);
            }
        }
        setbreakdownPhenomenonMasterData();
    }, [props.values.equipment]);

    if (props.loading) {
        return (<div
            style={{
                height: window.innerHeight - 250,
                width: '100%',
                paddingTop: '15%'
            }}>
            <CircularProgress />;
        </div>)
    }
    return (
        <Scroll>
            <Fragment>
                <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
                    <Grid
                        container
                        item xs={12}
                        direction="row"
                        justify="space-around"
                    >
                        <Grid
                            container
                            direction="row"
                            alignItems="center"
                        >
                            <Grid item xs={12} md={6}>

                                <FormControl className={classes.autoSuggest2}>
                                    <TextField fullWidth
                                        name="breakdownNo"
                                        placeholder="Enter Breakdown No"
                                        label="Breakdown Number"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.breakdownNo}
                                        disabled={status.isDisabled.breakdownNo}
                                        inputProps={{ style: { "color": "black" } }}
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <Equipments {...props} />
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <ReportedBy {...props} />
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest2}>
                                    <DatePicker
                                        keyboard
                                        name="breakdownDate"
                                        id="breakdowndate"
                                        label="Breakdown Date"
                                        format={'DD/MM/YYYY'}
                                        value={values.breakdownDate}
                                        onChange={(date) => {
                                            setFieldValue("breakdownDate", date)
                                        }}
                                        disableOpenOnEnter
                                        animateYearScrolling={false}
                                        error={errors.breakdownDate && (touched.breakdownDate)}
                                        FormHelperTextProps={{
                                            error: errors.breakdownDate && (touched.breakdownDate)
                                        }}
                                        helperText={
                                            errors.breakdownDate && (touched.breakdownDate)
                                                ? errors.breakdownDate
                                                : null
                                        }
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest2}>
                                    <TimePicker fullWidth
                                        keyboard
                                        label="Breakdown Time"
                                        id="breakdownTime"
                                        name="breakdownTime"
                                        value={values.breakdownTime}
                                        onChange={(date) => {
                                            setFieldValue("breakdownTime", date)
                                        }}
                                        mask={[/\d/, /\d/, ":", /\d/, /\d/, " ", /a|p/i, "M"]}
                                        error={errors.breakdownTime && (touched.breakdownTime)}
                                        FormHelperTextProps={{
                                            error: errors.breakdownTime && (touched.breakdownTime)
                                        }}
                                        helperText={
                                            errors.breakdownTime && (touched.breakdownTime)
                                                ? errors.breakdownTime
                                                : null
                                        }
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest2}>

                                    <InputLabel htmlFor="productionStatus-simple">{TextData.productionStatus}</InputLabel>
                                    <Select fullWidth
                                        value={values.productionStatus}
                                        onChange={handleChange}
                                        inputProps={{
                                            name: 'productionStatus',
                                            id: 'productionStatus-simple',
                                        }}
                                        error={errors.productionStatus && (touched.productionStatus)}
                                        FormHelperTextProps={{
                                            error: errors.productionStatus && (touched.productionStatus)
                                        }}
                                        helperText={
                                            errors.productionStatus && (touched.productionStatus)
                                                ? errors.productionStatus
                                                : null
                                        }>
                                        <MenuItem value={0}>Select</MenuItem>
                                        <MenuItem value={1}>Stopped</MenuItem>
                                        <MenuItem value={2}>Not stopped</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest2}>
                                    <TextField fullWidth
                                        id="breakdownDetails"
                                        label="Breakdown Details"
                                        multiline
                                        rows='1'
                                        value={values.breakdownDetails}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={errors.breakdownDetails && (touched.breakdownDetails)}
                                        FormHelperTextProps={{
                                            error: errors.breakdownDetails && (touched.breakdownDetails)
                                        }}
                                        helperText={
                                            errors.breakdownDetails && (touched.breakdownDetails)
                                                ? errors.breakdownDetails
                                                : null
                                        }
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <BreakdownPhenomenon breakdownPhenomenonMaster={breakdownPhenomenonMaster} {...props} />
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <EnteredBy {...props} />
                            </Grid>

                            <Grid item xs={12} md={6} style={{ "justify": "flex-start" }}>
                                <AssignedTo {...props} />
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <Button variant="contained" type="submit" color="primary" className={classes.button}>
                                {TextData.save}
                            </Button>
                            <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                                {TextData.reset}
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </Fragment>
        </Scroll>
    );
});

const EnhancedReportingForm = withFormik({
    enableReinitialize: true,
    mapPropsToStatus: (props) => ({
        isDisabled: {
            breakdownNo: true
        }}),
    mapPropsToValues: ({ data, breakdownData }) => {
        console.log("breakdownData",breakdownData)
        if (breakdownData && breakdownData[0]) {
            console.log("breakdownData[0]",breakdownData[0])
            return ({
                breakdownNo: breakdownData[0].bd_srno,
                equipment: breakdownData[0].Eqp_ID,
                reportedby: breakdownData[0].reported_by ? breakdownData[0].reported_by.replace(/ /g, "") : null,
                breakdownDate: moment(breakdownData[0].bd_date), breakdownTime: moment(breakdownData[0].bd_time),
                productionStatus: breakdownData[0].prod_code ? breakdownData[0].prod_code.replace(/ /g, "") : null,
                breakdownDetails: breakdownData[0].bd_det, breakdownPhenomenon: breakdownData[0].Phy_Pheno_Desc,
                enteredBy: breakdownData[0].entered_by ? breakdownData[0].entered_by.replace(/ /g, "") : null,
                assignedTo: breakdownData[0].assign_to_id1
            })
        } else {
            return ({
                breakdownNo: '', equipment: '', reportedby: '', breakdownDate: new Date(), breakdownTime: new Date(),
                productionStatus: '', breakdownDetails: '', breakdownPhenomenon: '', enteredBy: '', assignedTo: ''
            })
        }
    },

    validationSchema: Yup.object().shape({
        equipment: Yup.string("Equipment is required and should be a String").required("Equipment Required"),
        reportedby: Yup.string("Reportedby is required and should be a String").required("Reportedby is required"),
        breakdownDetails: Yup.string("breakdownDetails is required and should be a String").required("BreakdownDetails Required")
    }),

    handleSubmit: async (values, { setSubmitting, props }) => {
        setSubmitting(true);
        if (props.type === "NEW") {
            const r = await props.getData({
                spName: "SP_Insert_Update_Delete_Break_Down_Maint_Output",
                payload: {
                    bd_srno: null,
                    Eqp_ID: values.equipment,
                    bd_det: values.breakdownDetails,
                    bd_date: values.breakdownDate,
                    bd_time: values.breakdownTime,
                    prod_dntime: 0,
                    agency: "",
                    coord_per: "",
                    comp_date: moment().toISOString(),
                    comp_time: moment().toISOString(),
                    notes: "",
                    area_id: "",
                    failure_id: "",
                    opportunity_loss: 2,
                    nature: "",
                    Flag: "I",
                    reported_By: values.reportedby,
                    con_cost: 0.0,
                    Line: "",
                    emp_id: "",
                    eqp_unit: "",
                    entered_by: values.enteredBy,
                    bd_approval_status: "",
                    bd_approval_date: moment().toISOString(),
                    bd_approval_name: "",
                    assign_to_id1: values.assignedTo,
                    assign_to_id2: "",
                    assign_to_id3: "",
                    assign_to_id4: "",
                    assembly_description: "",
                    sub_assembly: "",
                    Phy_Pheno_Desc: values.breakdownPhenomenon,
                    Analysis_rqrd: "",
                    prod_code: values.productionStatus
                }
            })
            props.afterCreate({ ...r.data[0], ...values })

            setSubmitting(false);
            actions.snackBarHandleOpen({ message: "Record created successfully." })
        }
        if (props.type === "EDIT") {
            console.log("props.data[4][0]",props.data[4][0])
            await props.getData({
                spName: "Sp_Insert_Update_Delete_Break_Down_Maint1",
                payload: {
                    bd_srno: values.breakdownNo,
                    Eqp_ID: values.equipment,
                    bd_det: values.breakdownDetails,
                    bd_date: values.breakdownDate,
                    bd_time: values.breakdownTime,
                    prod_dntime: props.data[4][0].prod_dntime,
                    agency: props.data[4][0].agency,
                    coord_per: props.data[4][0].coord_per,
                    comp_date: moment().toISOString(),
                    comp_time: moment().toISOString(),
                    notes: props.data[4][0].notes,
                    area_id: props.data[4][0].area_id,
                    failure_id: props.data[4][0].failure_id,
                    opportunity_loss: props.data[4][0].opportunity_loss,
                    nature: props.data[4][0].nature,
                    Flag: "U",
                    reported_By: values.reportedby,
                    con_cost: props.data[4][0].con_cost,
                    Line: props.data[4][0].Line,
                    emp_id: props.data[4][0].emp_id,
                    eqp_unit: props.data[4][0].eqp_unit,
                    entered_by: values.enteredBy,
                    bd_approval_status: props.data[4][0].bd_approval_status,
                    bd_approval_date: moment().toISOString(),
                    bd_approval_name: props.data[4][0].bd_approval_name,
                    assign_to_id1: values.assignedTo,
                    assign_to_id2: props.data[4][0].assign_to_id2,
                    assign_to_id3: props.data[4][0].assign_to_id3,
                    assign_to_id4: props.data[4][0].assign_to_id4,
                    assembly_description: props.data[4][0].assembly_description,
                    sub_assembly: props.data[4][0].sub_assembly,
                    Phy_Pheno_Desc: values.breakdownPhenomenon,
                    Analysis_rqrd: props.data[4][0].Analysis_rqrd,
                    prod_code: values.productionStatus
                }
            })
            setSubmitting(false);
            actions.snackBarHandleOpen({ message: "Record updated successfully." })
        }
    },
    displayName: 'ReportingForm',
})(ReportingForm)

export default EnhancedReportingForm;