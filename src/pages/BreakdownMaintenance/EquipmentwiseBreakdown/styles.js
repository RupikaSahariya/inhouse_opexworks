const styles = theme => ({
    //fullscreen form
    fullScreenFormView: {
        flexGrow: 1
    },
    autoSuggest: {
        marginTop: theme.spacing.unit * 2,
        maxWidth: 550,
        width: "100%",
        textAlign: 'left',
        display: "flex",
        flexWrap: 'wrap'
    },
    autoSuggest2: {
        marginTop: theme.spacing.unit * 3,
        marginBottom: theme.spacing.unit,
        maxWidth: 550,
        width: "100%",
        textAlign: 'left',
        display: "flex",
        flexWrap: 'wrap'
    },
    autoSuggest3: {
        marginTop: "2px",
        marginBottom: theme.spacing.unit,
        marginLeft: theme.spacing.unit,
        maxWidth: 300,
        width: "100%",
        textAlign: 'left',
        display: "flex",
        flexWrap: 'wrap'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        maxHeight:'75%',
        justify: "space-around",
        paddingLeft: "6%"
    },
    container2: {
        display: 'flex',
        flexWrap: 'wrap',
        maxHeight:'75%',
        justify: "space-around"
    },
    formControl2: {
        margin: theme.spacing.unit,
        maxWidth: "92%",
        width: "100%"
    },
    formControl3: {
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit,
        marginRight: "-20px",
        marginLeft: "-60px",
        width: "80%"
    },
    formControl4: {
        margin: theme.spacing.unit,
        maxWidth: 380,
        width: "100%",
        textAlign: 'left'
    },
    formControl5: {
        margin: theme.spacing.unit,
        marginLeft: "4%",
        maxWidth: "92%",
        width: "100%"
    },
    datepicker: {
        marginRight: theme.spacing.unit,
        width: "100%",
        maxWidth: 400
    },
    button: {
        margin: theme.spacing.unit * 2,
    },

    //header styles
    breakCrumbIcon: {
        marginTop: '3px',
        fontSize: "20px"
    },
    breadCrumbText: {
        marginTop: '4px',
        color: "white",
        fontSize: "13px",
        "font-weight": "normal"
    },
    headerText: {
        color: "white",
        paddingBottom: "4px",
        fontSize: "18px",
    },
    header: {
        "background-color": "#3f51b5",
        color: "white",
    },

    //action style
    dialogStyles: {
        paperWidthSm: '800px',
        textAlign: 'center'
    },
    toolbarIcon: {
        margin: '10px'
    },
    toolbar: {
        margin: '5px',
    },
    tc: {
        marginLeft: '-5px',
        marginTop: '25px',
        marginBottom: '-10px',
        fontWeight: 'bold',
        color: 'black',
        display: 'flex',
        flexWrap:'wrap'
    },
    tc2: {
        marginLeft: "70%",
        marginTop: "28px",
        display: 'flex',
        flexWrap: 'wrap',
    },
    alignHeader: {
        flex: '1 1 0%'
    },
    card: {
        maxWidth: "100%",
        width: "100%",
        padding: 18
    },
});

export default styles;