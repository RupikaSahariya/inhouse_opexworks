import React, { Fragment, useState, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import { withFormik } from "formik";
import * as Yup from "yup";
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import TextData from './textData';
import Scroll from './../Scroll';
import styles from "./styles";
import CircularProgress from '@material-ui/core/CircularProgress';
import moment from "moment";
import { actions } from "./../../../actions";
import AutoSuggest from "./../../../components/Select";

const Coordinator = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props
    return (
    <React.Fragment>
    <AutoSuggest className={classes.autoSuggest}
        name="coordinator"
        error={errors.coordinator && (touched.coordinator)} FormHelperTextProps={{
            error: errors.coordinator && (touched.coordinator)
        }}
        label="Coordinator"
        helperText={
            errors.coordinator && (touched.coordinator)
                ? errors.coordinator
                : null
        }
        value={values.coordinator}
        placeholder={TextData.coordinator}
        options={props.loading || props.data[1].map((item, i) => ({
            value: item.emp_id,
            label: item.emp_name,
        }))}
        onChange={(value) => {
            setFieldValue("coordinator", value)
        }}
    >
    </AutoSuggest>

</React.Fragment>
    )
})

const AssignedTo1 = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props;
    return (
    <React.Fragment>
    <AutoSuggest className={classes.autoSuggest}
        name="assignedTo1"
        label="Assigned to"
        error={errors.assignedTo1 && (touched.assignedTo1)} FormHelperTextProps={{
            error: errors.assignedTo1 && (touched.assignedTo1)
        }}
        helperText={
            errors.assignedTo1 && (touched.assignedTo1)
                ? errors.assignedTo1
                : null
        }
        value={values.assignedTo1}
        placeholder={TextData.assignedTo1}
        options={props.loading || props.data[1].map((item, i) => ({
            value: item.emp_id,
            label: item.emp_name,
        }))}
        onChange={(value) => {
            setFieldValue("assignedTo1", value)
        }}
    >
    </AutoSuggest>

</React.Fragment>
    )
})

const AssignedTo2 = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props
    return (
    <React.Fragment>
    <AutoSuggest className={classes.autoSuggest}
        name="assignedTo2"
        error={errors.assignedTo2 && (touched.assignedTo2)} FormHelperTextProps={{
            error: errors.assignedTo2 && (touched.assignedTo2)
        }}
        label="Assigned to"
        helperText={
            errors.assignedTo2 && (touched.assignedTo2)
                ? errors.assignedTo2
                : null
        }
        value={values.assignedTo2}
        placeholder={TextData.assignedTo2}
        options={props.loading || props.data[1].map((item, i) => ({
            value: item.emp_id,
            label: item.emp_name,
        }))}
        onChange={(value) => {
            setFieldValue("assignedTo2", value)
        }}
    >
    </AutoSuggest>

</React.Fragment>
    )
})

const FailureArea = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props
    return (
    <React.Fragment>
    <AutoSuggest className={classes.autoSuggest}
        name="failureArea"
        error={errors.failureArea && (touched.failureArea)} FormHelperTextProps={{
            error: errors.failureArea && (touched.failureArea)
        }}
        label="Failure Area"
        helperText={
            errors.failureArea && (touched.failureArea)
                ? errors.failureArea
                : null
        }
        value={values.failureArea}
        placeholder={TextData.failureArea}
        options={props.loading || props.data[2].map((item, i) => ({
            value: item.area_id,
            label: item.area_desc,
        }))}
        onChange={(value) => {
            setFieldValue("failureArea", value)
            setFieldValue("failureType", null)
        }}
    >
    </AutoSuggest>

</React.Fragment>
    )
})

const FailureType = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        failureTypeMaster,
        setFieldValue
    } = props
    return (
    <React.Fragment>
    <AutoSuggest className={classes.autoSuggest}
        name="failureType"
        error={errors.failureType && (touched.failureType)} FormHelperTextProps={{
            error: errors.failureType && (touched.failureType)
        }}
        label="Failure Type"
        helperText={
            errors.failureType && (touched.failureType)
                ? errors.failureType
                : null
        }
        value={values.failureType}
        placeholder={TextData.failureType}
        isDisabled= {failureTypeMaster === undefined || failureTypeMaster.length==0}
        options={ failureTypeMaster ? failureTypeMaster.map((item, i) => ({
            value: item.failure_id,
            label: item.failure_desc,
        })): null}
        onChange={(value) => {
            setFieldValue("failureType", value)
        }}
    >
    </AutoSuggest>

</React.Fragment>
    )
})

const AnalysisForm = withStyles(styles)((props) => {
    if (props.loading) {
        return (<div
            style={{
                height: window.innerHeight - 250,
                width: '100%',
                paddingTop: '15%'
            }}>
            <CircularProgress />;
        </div>)
    }
    const {
        values,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        getData
    } = props;

    const [failureTypeMaster, setFailureTypeMaster] = useState([]);
    useEffect(() => {
        async function setFailureTypeData() {
            const data = await Promise.all([
                getData({
                    spName: "SP_Select_All_From_failure_code_master_By_Area_Code",
                    payload: {
                        area_code_id: values.failureArea
                    }
                })
            ])
            if (data && data.length > 0) {
                setFailureTypeMaster(data[0].data);
            }
        }
        setFailureTypeData();
    }, [props.values.failureArea]);

    return (
        <Scroll>
            <Fragment className={classes.fragment}>
                <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
                    <Typography variant="caption" className={classes.tc}>{TextData.allocation}</Typography>
                    <Fragment>
                        <Grid
                            container
                            item xs={12}
                            direction="row"
                        >
                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TextField fullWidth
                                        name="agency"
                                        placeholder="Enter Agency Name"
                                        label="Agency"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.agency}
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Coordinator {...props} />
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <AssignedTo1 {...props} />
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <AssignedTo2 {...props} />
                            </Grid>
                        </Grid>
                    </Fragment>
                    <Typography variant="caption" className={classes.tc}>{TextData.affectedParts}</Typography>
                    <Fragment>
                        <Grid
                            container
                            direction="row"
                            alignItems="center"
                        >
                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TextField fullWidth
                                        name="equipmentUnit"
                                        placeholder="Enter Equipment Unit"
                                        label="Equipment unit"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.equipmentUnit}
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TextField fullWidth
                                        name="assemblyDescription"
                                        placeholder="Enter Assembly Description"
                                        label="Assembly Description"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.assemblyDescription}
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TextField fullWidth
                                        name="subAssembly"
                                        placeholder="Sub Assembly"
                                        label="Sub Assembly"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.subAssembly}
                                    />
                                </FormControl>
                            </Grid>
                        </Grid>
                    </Fragment>

                    <Typography variant="caption" className={classes.tc}>{TextData.breakdownAnalysis}</Typography>
                    <Fragment>
                        <Grid
                            container
                            direction="row"
                            alignItems="center"
                        >
                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <InputLabel htmlFor="nature-simple">{TextData.nature}</InputLabel>
                                    <Select
                                        fullWidth
                                        value={values.nature}
                                        onChange={handleChange}
                                        inputProps={{
                                            name: 'nature',
                                            id: 'nature-simple',
                                        }}>
                                        <MenuItem value="">Select</MenuItem>
                                        <MenuItem value="M">Major</MenuItem>
                                        <MenuItem value="N">Minor</MenuItem>
                                        <MenuItem value="O">Critical</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FailureArea {...props} />
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FailureType failureTypeMaster={failureTypeMaster} {...props} />
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TextField fullWidth
                                        name="breakdownAnalysis"
                                        label="Breakdown Analysis"
                                        multiline
                                        rows='1'
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.breakdownAnalysis}
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TextField fullWidth
                                        name="breakdownPhenomenon"
                                        label="Breakdown Phenomenon"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.breakdownPhenomenon}
                                    />
                                </FormControl>
                            </Grid>

                        </Grid>
                        <Grid item xs={12}>
                            <Button variant="contained" type="submit" color="primary" className={classes.button}>
                                {TextData.save}
                            </Button>
                            <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                                {TextData.reset}
                            </Button>
                        </Grid>
                    </Fragment>

                </form>
            </Fragment>
        </Scroll>
    );
});

const EnhancedAnalysisForm = withFormik({
    enableReinitialize: true,
    mapPropsToValues: ({ data, breakdownData }) => {
        if (breakdownData && breakdownData[0]) {
            return ({
                agency:  breakdownData[0].agency, coordinator: breakdownData[0].coord_per? breakdownData[0].coord_per.replace(/ /g, "") : null,
                assignedTo1: breakdownData[0].assign_to_id1 ? breakdownData[0].assign_to_id1.replace(/ /g, "") : null,
                assignedTo2: breakdownData[0].assign_to_id2 ? breakdownData[0].assign_to_id2.replace(/ /g, "") : null,
                equipmentUnit: breakdownData[0].eqp_unit,
                assemblyDescription: breakdownData[0].assembly_description, subAssembly: breakdownData[0].sub_assembly, 
                nature: breakdownData[0].nature ? breakdownData[0].nature.replace(/ /g, "") : null,
                failureArea: breakdownData[0].area_id,
                failureType: breakdownData[0].failure_id, breakdownAnalysis: breakdownData[0].notes,
                breakdownPhenomenon:breakdownData[0].Phy_Pheno_Desc
            })
        } else {
            return ({
                agency: '', coordinator: '', assignedTo1: '', assignedTo2: '', equipmentUnit: '', assemblyDescription: '', subAssembly: '',
                nature: '', failureArea: '', failureType: '', breakdownAnalysis: '', breakdownPhenomenon: ''
            })
        }
    },

    validationSchema: Yup.object().shape({
        assignedTo1: Yup.string("assignedTo1 is required and should be a String").required("assignedTo1 Required"),
    }),

    handleSubmit: async (values, { setSubmitting, props }) => {
        setSubmitting(true);
    
    if(props.type==="EDIT")
    {
        await props.getData({
            spName: "Sp_Insert_Update_Delete_Break_Down_Maint1",
            payload: {
                bd_srno: props.breakdownData[0].bd_srno,
                Eqp_ID: props.breakdownData[0].Eqp_ID,
                bd_det: props.breakdownData[0].bd_det,
                bd_date: props.breakdownData[0].bd_date,
                bd_time: props.breakdownData[0].bd_time,
                prod_dntime: props.breakdownData[0].prod_dntime,
                agency: values.agency,
                coord_per: values.coordinator,
                comp_date: moment().toISOString(),
                comp_time: moment().toISOString(),
                notes: values.breakdownAnalysis,
                area_id: values.failureArea,
                failure_id: values.failureType,
                opportunity_loss: props.breakdownData[0].opportunity_loss,
                nature: values.nature,
                Flag: "U",
                reported_By: props.breakdownData[0].reported_by,
                con_cost: props.breakdownData[0].con_cost,
                Line: props.breakdownData[0].Line,
                emp_id: props.breakdownData[0].emp_id,
                eqp_unit: values.equipmentUnit,
                entered_by: props.breakdownData[0].entered_by,
                bd_approval_status: props.breakdownData[0].bd_approval_status,
                bd_approval_date:  moment().toISOString(),
                bd_approval_name: props.breakdownData[0].bd_approval_name,
                assign_to_id1: values.assignedTo1,
                assign_to_id2: values.assignedTo2,
                assign_to_id3: props.breakdownData[0].assign_to_id3,
                assign_to_id4: props.breakdownData[0].assign_to_id4,
                assembly_description: values.assemblyDescription,
                sub_assembly: values.subAssembly,
                Phy_Pheno_Desc: values.breakdownPhenomenon,
                Analysis_rqrd: props.breakdownData[0].Analysis_rqrd,
                prod_code: props.breakdownData[0].prod_code
            }
        })
        setSubmitting(false);
        actions.snackBarHandleOpen({ message: "Record updated successfully." })
    }
    },
    displayName: 'AnalysisForm',
})(AnalysisForm);

export default EnhancedAnalysisForm;