import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Dialog from '@material-ui/core/Dialog';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import SearchIcon from '@material-ui/icons/Search';
import Slide from '@material-ui/core/Slide';
import Tooltip from '@material-ui/core/Tooltip';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import AgTable from "./../../../components/AgTable";
import { compose } from "recompose";
import Paper from "@material-ui/core/Paper";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import moment from "moment";

import RefreshIcon from "@material-ui/icons/RefreshOutlined";
import Header1 from './../../../components/Header1/Header1';
import styles from "./styles";
import EnhancedAnalysisForm from "./AnalysisForm";
import EnhancedCompletionForm from "./CompletionForm";
import EnhancedReportingForm from "./ReportingForm";
import TextData from "./textData";
import AppContext from "./../../../utils/AppContext";
import { dataProvider } from "./../../../utils/AppContext";
import DocumentsPage from "../Documents";
import ManpowerPage from "../ManPower";
import SparesConsumedPage from "../SparesConsumed";
import WhyWhyAnalysisPage from "../WhyWhyAnalysis";
import { Consumer, actions } from "../../../actions";
import AutoSuggest from "./../../../components/Select";

const Equipments = ((props) => {
    const {
        classes,
        touched,
        errors,
        selectedEquipment,
        onEquipmentSelect,
        setFieldValue
    } = props
    return (
        // <FormControl className={classes.formControl4}>
        //     <InputLabel shrink={selectedEquipment != null} htmlFor="equipments-simple">{TextData.equipments}</InputLabel>
        //     <Select
        //         id="standard-full-width"
        //         placeholder="Equpiments"
        //         height="5"
        //         autoWidth
        //         value={selectedEquipment}
        //         onChange={onEquipmentSelect}
        //         inputProps={{
        //             name: 'equipments',
        //             id: 'equipments-simple',
        //         }}>
        //         {props.loading || props.data.map((item, i) => <MenuItem key={item.Eqp_ID} value={item.Eqp_ID}>{item.eqp_desc}</MenuItem>)}
        //     </Select>
        // </FormControl>
        <React.Fragment>

            <AutoSuggest className={classes.autoSuggest3}
                name="equipment"
                // error={errors.equipment && (touched.equipment)} FormHelperTextProps={{
                //     error: errors.equipment && (touched.equipment)
                // }}
                label="Equipment"
                // helperText={
                //     errors.equipment && (touched.equipment)
                //         ? errors.equipment
                //         : null
                // }
                value={selectedEquipment}
                onChange={(value) => onEquipmentSelect(value)}
                placeholder={TextData.equipment}
                options={props.data ? props.data.map((item, i) => ({
                    value: item.Eqp_ID,
                    label: item.eqp_desc,
                })) : []}
            // onChange={(value) => {
            //     setFieldValue("equipment", value)
            // }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class EquipmentwiseBreakdown extends React.PureComponent {
    state = {
        value: 0,
        documentOpen: false,
        manpowerOpen: false,
        sparesUsed: false,
        data: false,
        selectData: false
    };

    onDocumentOpen = () => {
        this.setState({ documentOpen: true });
    };
    onDocumentClose = () => {
        this.setState({ documentOpen: false });
    };
    onManpowerOpen = () => {
        this.setState({ manpowerOpen: true });
    };
    onManpowerClosed = () => {
        this.setState({ manpowerOpen: false });
    };
    onSparesUsedOpen = () => {
        this.setState({ sparesUsed: true });
    };
    onSparesUsedClosed = () => {
        this.setState({ sparesUsed: false });
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    afterCreate = async (props) => {
        const data = await this.props.getData({
            spName: "sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID",
            payload: {
                Eqp_ID: props.equipment,
                bd_srno: props.bd_srno
            }
        })
        this.setState({
            selectedData: true,
            data: data.data
        })
    }

    selectedData = () => !this.state.selectedData ? this.props.selectedData : this.state.selectedData

    render() {
        const { classes } = this.props;
        return (
            <AppContext.Consumer>
                {({ getData }) => (
                    <Paper className={classes.fullScreenFormView}>
                        <Tabs
                            value={this.state.value}
                            onChange={this.handleChange}
                            indicatorColor="primary"
                            textColor="primary"
                            centered
                        >
                            <Tab label="Reporting" />
                            {this.selectedData() ? <Tab label="Analysis" /> : null}
                            {this.selectedData() ? <Tab label="Completion" /> : null}
                        </Tabs>
                        {this.state.value === 0 ? <EnhancedReportingForm getData={getData} afterCreate={this.afterCreate} {...this.props}
                            breakdownData={this.state.data ? this.state.data : this.props.data[4]} /> : null}
                        {this.state.value === 1 && this.selectedData() ? <EnhancedAnalysisForm getData={getData} {...this.props}
                            type={this.state.data ? "EDIT" : this.props.type} breakdownData={this.state.data ? this.state.data : this.props.data[4]} /> : null}
                        {this.state.value === 2 && this.selectedData() ? <EnhancedCompletionForm getData={getData} {...this.props}
                            handleOpenDocument={this.props.handleOpenDocument} handleClickOpenManpower={this.props.handleClickOpenManpower}
                            handleClickOpenSparesUsed={this.props.handleClickOpenSparesUsed}
                            breakdownData={this.state.data ? this.state.data : this.props.data[4]} type={this.selectedData()}
                            type={this.state.data ? "EDIT" : this.props.type}
                        /> : null}
                    </Paper>
                )}
            </AppContext.Consumer>
        );
    }
}

const StyledEquipmentwiseBreakdownForm = compose(withStyles(styles), dataProvider({
    queries: (props) => {
        var queries = [
            {
                spName: "SP_Fun_Select_All_From_Equipment_By_Plant_ID",
                payload: {},
                key: "key1"
            },
            {
                spName: "sp_select_all_from_Employee_master",
                payload: {},
                key: "key2"
            },
            {
                spName: "SP_Select_All_From_area_code_master",
                payload: {},
                key: "key3"
            },
            {
                spName: "SP_Select_All_From_failure_code_master_By_Area_Code",
                payload: {
                    area_code_id: null
                },
                key: "key4"
            }
        ]
        if (props.type === "EDIT") {
            return [...queries, {
                spName: "sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID",
                payload: {
                    Eqp_ID: props.selectedData.data.Eqp_ID,
                    bd_srno: props.selectedData.data.bd_srno
                },
                key: "key5"
            }]
        }
        return queries
    }
}))(EquipmentwiseBreakdown)

StyledEquipmentwiseBreakdownForm.propTypes = {
    classes: PropTypes.object.isRequired,
};

class EquipmentwiseBreakdownPage extends React.PureComponent {
    state = {
        selectedEquipment: null,
        dialogOpen: false,
        equipmentWiseBreakDownMaster: [],
        refresh: false,
        documentOpen: false,
        manpowerOpen: false,
        sparesUsedOpen: false,
        breadcrumb: [
            { name: "Home", path: "/" },
            { name: "Equipmentwise Breakdown", path: "/breakdownmaintenance/equipmentwiseBreakdown" }],
        rowData: false
    }

    onSearch = async () => {
        const data = await Promise.all([
            this.props.getData({
                spName: "Sp_Select_All_From_Equipment_Wise_Breakdown_By_Eqp_ID_And_Plant_ID",
                payload: {
                    Eqp_ID: this.state.selectedEquipment
                }
            })
        ])

        const rowData = data[0].data.map((item, i) => ({
            data: item,
            srno: item.bd_srno, breakdownDate: moment(item.bd_date).format("DD/MM/YY") + ", " + moment(item.bd_time).format("HH:mm"),
            breakdownDetails: item.bd_det, enteredBy: item.entered_by_name,
            nature: item.nature == 'M' ? 'Major' : item.nature == 'N' ? 'Minor': item.nature == 'C' ? 'Critcal': '',
            completionDateTime: moment(item.comp_date).format("DD/MM/YY, HH:mm")
        }));
        this.setState({
            equipmentWiseBreakDownMaster: rowData
        })
    }

    onEquipmentSelect = async (val) => {
        this.setState({
            selectedEquipment: val
        })
    }

    handleClickOpen = (selectedData) => {
        this.setState({ dialogOpen: true, selectedData: selectedData, type: selectedData ? "EDIT" : "NEW" });
    };

    handleDocument = () => {
        actions.closeDocument({ page: "EQUIPMENTWISE_BREAKDOWN_DOCUMENT" })
    };

    handleManPower = () => {
        actions.closeManpower({ page: "EQUIPMENTWISE_BREAKDOWN_MANPOWER" })
    };

    handleSpareUsed = () => {
        actions.closeSpareUsed({ page: "EQUIPMENTWISE_BREAKDOWN_SPARESUSED" })
    };

    handleWhyWhyAnalysisOpen = async (selectedData) => {
        console.log("Fetching for ", selectedData.data.bd_srno)
        actions.handleWhyWhyAnalysisDialog({ page: "WHY_WHY_ANALYSIS", payload: selectedData.data })
        actions.getData({
            spName: "SP_Select_All_From_BDM_Why_Why_Analysis_By_Eqp_ID_And_Bd_Srno",
            payload: {
                Eqp_ID: selectedData.data.Eqp_ID,
                bd_srno: selectedData.data.bd_srno
            },
            key: "WHY_WHY_ANALYSIS_DATA"
        })
    };

    handleWhyWhyAnalysisClose = () => {
        actions.closeWhyWhyAnalysis({ page: "WHY_WHY_ANALYSIS" })
    };

    handleClose = () => {
        this.setState({ dialogOpen: false, selectedData: null, type: null });
    };

    componentDidMount() {
    };

    onGridReady = (params) => {
        // params.api.sizeColumnsToFit()
        this.gridParams = params
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    };

    handleRefresh = () => {
        window.location.reload();
    };

    exportData = () => {
        this.gridApi.exportDataAsCsv(this.gridParams);
    };

    getData = () => this.state.rowData ? this.state.rowData : this.props.data

    onDeleteRow = async (selectedData) => {
        const data = await this.props.getData({
            spName: "sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID",
            payload: {
                Eqp_ID: selectedData.data.Eqp_ID,
                bd_srno: selectedData.data.bd_srno
            }
        })

        await this.props.getData({
            spName: "Sp_Insert_Update_Delete_Break_Down_Maint1",
            payload: {
                bd_srno: data.data[0].bd_srno,
                Eqp_ID: data.data[0].Eqp_ID,
                bd_det: data.data[0].bd_det,
                bd_date: data.data[0].bd_date,
                bd_time: data.data[0].bd_time,
                prod_dntime: data.data[0].prod_dntime,
                agency: data.data[0].agency,
                coord_per: data.data[0].coord_per,
                comp_date: data.data[0].comp_date,
                comp_time: data.data[0].comp_time,
                notes: data.data[0].notes,
                area_id: data.data[0].area_id,
                failure_id: data.data[0].failure_id,
                opportunity_loss: data.data[0].opportunity_loss,
                nature: data.data[0].nature,
                Flag: "D",
                reported_By: data.data[0].reported_by,
                con_cost: data.data[0].con_cost,
                Line: data.data[0].Line,
                emp_id: data.data[0].emp_id,
                eqp_unit: data.data[0].eqp_unit,
                entered_by: data.data[0].entered_by,
                bd_approval_status: data.data[0].bd_approval_status,
                bd_approval_date: data.data[0].bd_approval_date,
                bd_approval_name: data.data[0].bd_approval_name,
                assign_to_id1: data.data[0].assign_to_id1,
                assign_to_id2: data.data[0].assign_to_id2,
                assign_to_id3: data.data[0].assign_to_id3,
                assign_to_id4: data.data[0].assign_to_id4,
                assembly_description: data.data[0].assembly_description,
                sub_assembly: data.data[0].sub_assembly,
                Phy_Pheno_Desc: data.data[0].Phy_Pheno_Desc,
                Analysis_rqrd: data.data[0].Analysis_rqrd,
                prod_code: data.data[0].prod_code
            }
        })
        actions.snackBarHandleOpen({ message: "Record Deleted successfully." })

        const newData = this.getData().filter((item) => (item.bd_srno !== selectedData.data.bd_srno))
        this.setState({
            rowData: newData
        })
    }

    BDReportDownload = (selectedData) => {
        console.log(selectedData);
        actions.downloadReport({
            spName: "SP_rpt_Select_data_for_breakdown_report",
            payload: {
                bd_srno: selectedData.data.bd_srno
            }
        })
    }

    render() {
        const columnDefs = [
            {
                headerName: "Sr.No.", field: "srno", sortable: true, filter: true, width: 120,
                cellRenderer: "toolTipRenderer", toolTipText: "srno"
            },
            {
                headerName: "Breakdown Date/Time", field: "breakdownDate", sortable: true, filter: true, width: 160,
                cellRenderer: "toolTipRenderer", toolTipText: "breakdownDate"
            },
            {
                headerName: "Breakdown Details", field: "breakdownDetails", sortable: true, filter: true, width: 160,
                cellRenderer: "toolTipRenderer", toolTipText: "breakdownDetails"
            },
            {
                headerName: "Entered By", field: "enteredBy", sortable: true, filter: true, width: 160,
                cellRenderer: "toolTipRenderer", toolTipText: "enteredBy"
            },
            {
                headerName: "Nature", field: "nature", sortable: true, filter: true, width: 120,
                cellRenderer: "toolTipRenderer", toolTipText: "nature"
            },
            {
                headerName: "Completion Date/Time", field: "completionDateTime", sortable: true, filter: true, width: 160,
                cellRenderer: "toolTipRenderer", toolTipText: "completionDateTime"
            },
            {
                headerName: "", width: 150, cellRenderer: "whyWhyAnalysisRenderer"
            },
            {
                headerName: "", width: 150, cellRenderer: "editRenderer"
            },
        ]

        const { classes } = this.props;
        return (
            <Fragment>
                <Dialog
                    fullScreen
                    className={classes.dialogStyles}
                    open={this.state.dialogOpen}
                    handleClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                    maxWidth="sm"
                    TransitionComponent={Transition}
                >
                    <AppBar className={classes.appBar}>
                        <Toolbar>
                            <div className={classes.alignHeader}></div>
                            <Typography variant="subtitle1" color="inherit" className={classes.flex}>
                                {TextData.equipmentwiseBreakdown}
                            </Typography>
                            <div className={classes.alignHeader}></div>
                            <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                                <CloseIcon />
                            </IconButton>
                        </Toolbar>
                        {this.state.type && <StyledEquipmentwiseBreakdownForm {...this.props} type={this.state.type} selectedData={this.state.selectedData}
                            handleClose={this.handleClose} handleDocument={this.handleDocument} handleManpower={this.handleManpower}
                            handleSpareUsed={this.handleSpareUsed} />}
                    </AppBar>

                    <Consumer>
                        {
                            ({ EQUIPMENTWISE_BREAKDOWN_DOCUMENT }) => {
                                return (
                                    <React.Fragment>
                                        {EQUIPMENTWISE_BREAKDOWN_DOCUMENT.isOpen ? <Dialog
                                            fullScreen
                                            className={classes.dialogStyles}
                                            open={EQUIPMENTWISE_BREAKDOWN_DOCUMENT.isOpen}
                                            handleClose={this.handleClose}
                                            aria-labelledby="form-dialog-title"
                                            maxWidth="sm"
                                            TransitionComponent={Transition}
                                        >
                                            <AppBar className={classes.appBar}>
                                                <Toolbar>
                                                    <div className={classes.alignHeader}></div>
                                                    <Typography variant="subtitle1" color="inherit" className={classes.flex}>
                                                        Document
                                                </Typography>

                                                    <div className={classes.alignHeader}></div>
                                                    <IconButton color="inherit" onClick={this.handleDocument} aria-label="Close">
                                                        <CloseIcon />
                                                    </IconButton>
                                                </Toolbar>
                                                <DocumentsPage documentData={EQUIPMENTWISE_BREAKDOWN_DOCUMENT.payload} />
                                            </AppBar>

                                        </Dialog> : null}
                                    </React.Fragment>
                                )
                            }
                        }
                    </Consumer>

                    <Consumer>
                        {
                            ({ EQUIPMENTWISE_BREAKDOWN_MANPOWER }) => {
                                return (
                                    <React.Fragment>
                                        {EQUIPMENTWISE_BREAKDOWN_MANPOWER.isOpen ? <Dialog
                                            fullScreen
                                            className={classes.dialogStyles}
                                            open={EQUIPMENTWISE_BREAKDOWN_MANPOWER.isOpen}
                                            handleClose={this.handleClose}
                                            aria-labelledby="form-dialog-title"
                                            maxWidth="sm"
                                            TransitionComponent={Transition}
                                        >
                                            <AppBar className={classes.appBar}>
                                                <Toolbar>
                                                    <div className={classes.alignHeader}></div>
                                                    <Typography variant="subtitle1" color="inherit" className={classes.flex}>
                                                        Man Power Used
                                                </Typography>

                                                    <div className={classes.alignHeader}></div>
                                                    <IconButton color="inherit" onClick={this.handleManPower} aria-label="Close">
                                                        <CloseIcon />
                                                    </IconButton>
                                                </Toolbar>
                                                <ManpowerPage manpowerData={EQUIPMENTWISE_BREAKDOWN_MANPOWER.payload} />
                                            </AppBar>

                                        </Dialog> : null}
                                    </React.Fragment>
                                )
                            }
                        }
                    </Consumer>

                    <Consumer>
                        {
                            ({ EQUIPMENTWISE_BREAKDOWN_SPARESUSED }) => {
                                return (
                                    <React.Fragment>
                                        {EQUIPMENTWISE_BREAKDOWN_SPARESUSED.isOpen ? <Dialog
                                            fullScreen
                                            className={classes.dialogStyles}
                                            open={EQUIPMENTWISE_BREAKDOWN_SPARESUSED.isOpen}
                                            handleClose={this.handleClose}
                                            aria-labelledby="form-dialog-title"
                                            maxWidth="sm"
                                            TransitionComponent={Transition}
                                        >
                                            <AppBar className={classes.appBar}>
                                                <Toolbar>
                                                    <div className={classes.alignHeader}></div>
                                                    <Typography variant="subtitle1" color="inherit" className={classes.flex}>
                                                        Spares Consumed In Breakdown
                                                    </Typography>

                                                    <div className={classes.alignHeader}></div>
                                                    <IconButton color="inherit" onClick={this.handleSpareUsed} aria-label="Close">
                                                        <CloseIcon />
                                                    </IconButton>
                                                </Toolbar>
                                                <SparesConsumedPage sparesusedData={EQUIPMENTWISE_BREAKDOWN_SPARESUSED.payload} />
                                            </AppBar>

                                        </Dialog> : null}
                                    </React.Fragment>
                                )
                            }
                        }
                    </Consumer>
                </Dialog>

                <Consumer>
                    {
                        ({ WHY_WHY_ANALYSIS, WHY_WHY_ANALYSIS_DATA }) => {
                            return (
                                <React.Fragment>
                                    {WHY_WHY_ANALYSIS.isOpen ? <Dialog
                                        fullScreen
                                        className={classes.dialogStyles}
                                        open={WHY_WHY_ANALYSIS.isOpen}
                                        handleClose={this.handleClose}
                                        aria-labelledby="form-dialog-title"
                                        maxWidth="sm"
                                        TransitionComponent={Transition}
                                    >
                                        <AppBar className={classes.appBar}>
                                            <Toolbar>
                                                <div className={classes.alignHeader}></div>
                                                <Typography variant="subtitle1" color="inherit" className={classes.flex}>
                                                    Why Why Analyisis
                                                    </Typography>
                                                <div className={classes.alignHeader}></div>
                                                <IconButton color="inherit" onClick={this.handleWhyWhyAnalysisClose} aria-label="Close">
                                                    <CloseIcon />
                                                </IconButton>
                                            </Toolbar>

                                            <WhyWhyAnalysisPage whywhyanalysisData={WHY_WHY_ANALYSIS_DATA} WHY_WHY_ANALYSIS={WHY_WHY_ANALYSIS.payload} {...this.props} selectedEquipment={this.state.selectedEquipment}
                                                bd_srno={WHY_WHY_ANALYSIS.payload.bd_srno} />

                                        </AppBar>

                                    </Dialog> : null}
                                </React.Fragment>
                            )
                        }
                    }
                </Consumer>

                <Header1 breadCrumbText={this.state.breadcrumb} breadCrumbHeader="Equipment Wise Breakdown" />

                <Grid
                    container
                    direction="row"
                >
                    <Equipments {...this.props} selectedEquipment={this.state.selectedEquipment} onEquipmentSelect={this.onEquipmentSelect} />

                    <Tooltip title="Search">
                        <Fab color="primary" type="submit" variant="extended" size={"small"} style={{ "marginTop": "15px", "marginLeft": "10px" }}
                            onClick={this.onSearch}
                        >
                            <SearchIcon />&nbsp;
                        </Fab>
                    </Tooltip>
                    <div className={classes.alignHeader}></div>
                    <div className={classes.toolbar}>
                        <Tooltip title="Add New Breakdown">
                            <Fab size={"small"} color="primary" variant="extended" onClick={() => this.handleClickOpen()} className={classes.toolbarIcon}>
                                <AddIcon />
                            </Fab>
                        </Tooltip>
                        <Tooltip title="Refresh">
                            <Fab size={"small"} color="primary" variant="extended" onClick={this.handleRefresh} className={classes.toolbarIcon}>
                                <RefreshIcon />
                            </Fab>
                        </Tooltip>
                    </div>
                </Grid>

                <Paper>
                    <AgTable
                        loading={this.props.loading}
                        onGridReady={this.onGridReady}
                        columnDefs={columnDefs}
                        rowData={this.state.equipmentWiseBreakDownMaster}
                        onEdit={this.handleClickOpen}
                        onDelete={this.onDeleteRow}
                        onWhyWhyAnalysis={this.handleWhyWhyAnalysisOpen}
                        onBdReport={this.BDReportDownload}
                    />
                </Paper>
            </Fragment>)
    }
}

export default compose(withStyles(styles),
    dataProvider({
        query: async (props) => {
            return ({
                spName: "SP_Fun_Select_All_From_Equipment_By_Plant_ID",
                payload: {}
            })
        }
    })
)(EquipmentwiseBreakdownPage);