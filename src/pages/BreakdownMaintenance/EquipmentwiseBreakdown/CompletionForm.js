import React, { Fragment, useState, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import { withFormik } from "formik";
import * as Yup from "yup";
import { DatePicker, TimePicker } from 'material-ui-pickers';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import TextData from './textData';
import Scroll from './../Scroll';
import styles from "./styles";
import CircularProgress from '@material-ui/core/CircularProgress';
import moment from 'moment';
import { actions } from "../../../actions";
import AutoSuggest from "./../../../components/Select";

const ApprovedBy = ((props) => {
    const {
        values,
        classes,
        touched,
        errors,
        setFieldValue
    } = props;
    return (
        <React.Fragment>
        <AutoSuggest className={classes.autoSuggest}
            name="approvedBy"
            error={errors.approvedBy && (touched.approvedBy)} FormHelperTextProps={{
                error: errors.approvedBy && (touched.approvedBy)
            }}
            label="Approved By"
            helperText={
                errors.approvedBy && (touched.approvedBy)
                    ? errors.approvedBy
                    : null
            }
            value={values.approvedBy}
            placeholder={TextData.approvedBy}
            options={props.loading || props.data[1].map((item, i) => ({
                value: item.emp_id,
                label: item.emp_name,
            }))}
            onChange={(value) => {
                setFieldValue("approvedBy", value)
            }}
        >
        </AutoSuggest>

    </React.Fragment>
    )
})

const CompletionForm = withStyles(styles)((props) => {
    if (props.loading) {
        return (<div
            style={{
                height: window.innerHeight - 250,
                width: '100%',
                paddingTop: '15%'
            }}>
            <CircularProgress />;
        </div>)
    }
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue,
        getData,
    } = props

    const [approvedByMaster, setapprovedByMaster] = useState([]);
    useEffect(() => {
        async function setapprovedByMasterData() {
            const data = await Promise.all([
                getData({
                    spName: "sp_select_all_from_Employee_master",
                    payload: {}
                })
            ])
            if (data && data.length > 0) {
                setapprovedByMaster(data[0].data);
            }
        }
        setapprovedByMasterData();
    }, [props.values.approvalForBreakdown]);

    const handleDocumentOpen = async () => {
        let eqp_desc = "";
        const breakdownData = props.breakdownData;
        props.data[0].forEach(function (eqp) {
            if (eqp.Eqp_ID==props.data[4][0].Eqp_ID) {
              eqp_desc=eqp.eqp_desc;
            }
          });
        actions.handleDocumentDialog({ page:"EQUIPMENTWISE_BREAKDOWN_DOCUMENT", payload: {...props.data, breakdownData ,eqp_desc} })
    }

    const handleManpowerOpen = async () => {
        let eqp_desc = "";
        const breakdownData = props.breakdownData;
        props.data[0].forEach(function (eqp) {
            if (eqp.Eqp_ID==props.data[4][0].Eqp_ID) {
              eqp_desc=eqp.eqp_desc;
            }
          });
        actions.handleManpowerDialog({ page:"EQUIPMENTWISE_BREAKDOWN_MANPOWER", payload: {...props.data, breakdownData ,eqp_desc} })
    }

    const handleSpareUsedOpen = async () => {
        let eqp_desc = "";
        const breakdownData = props.breakdownData;
        props.data[0].forEach(function (eqp) {
            if (eqp.Eqp_ID==props.data[4][0].Eqp_ID) {
              eqp_desc=eqp.eqp_desc;
            }
          });
        actions.handleSpareUsedDialog({ page:"EQUIPMENTWISE_BREAKDOWN_SPARESUSED", payload: {...props.data, breakdownData ,eqp_desc} })
    }
    return (
        <Scroll>
            <Fragment>
                <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
                    <Fragment>
                        <Grid
                            container
                            item xs={12}
                            direction="row"
                        >
                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <DatePicker
                                        keyboard
                                        name="completionDate"
                                        id="completionDate"
                                        label="Completion Date"
                                        format={'DD/MM/YYYY'}
                                        value={values.completionDate}
                                        onChange={(date) => {
                                            setFieldValue("completionDate", date)
                                        }}
                                        disableOpenOnEnter
                                        animateYearScrolling={false}
                                        error={errors.completionDate && (touched.completionDate)}
                                        FormHelperTextProps={{
                                            error: errors.completionDate && (touched.completionDate)
                                        }}
                                        helperText={
                                            errors.completionDate && (touched.completionDate)
                                                ? errors.completionDate
                                                : null
                                        }
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TimePicker fullWidth
                                        keyboard
                                        label="Completion Time"
                                        id="completionTime"
                                        name="completionTime"
                                        value={values.completionTime}
                                        onChange={(date) => {
                                            setFieldValue("completionTime", date)
                                        }}
                                        mask={[/\d/, /\d/, ":", /\d/, /\d/, " ", /a|p/i, "M"]}
                                        error={errors.completionTime && (touched.completionTime)}
                                        FormHelperTextProps={{
                                            error: errors.completionTime && (touched.completionTime)
                                        }}
                                        helperText={
                                            errors.completionTime && (touched.completionTime)
                                                ? errors.completionTime
                                                : null
                                        }
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TextField fullWidth
                                        name="productionDowntime"
                                        placeholder="Production DownTime (In Minute)"
                                        label="Production Downtime"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.productionDowntime}
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TextField fullWidth
                                        name="contractualCostInvolved"
                                        placeholder="Contractual Cost Involved"
                                        label="Contractual Cost Involved"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.contractualCostInvolved}
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TextField fullWidth
                                        name="opportunityLoss"
                                        placeholder="Opportunity Loss"
                                        label="Opportunity Loss"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.opportunityLoss}
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <InputLabel htmlFor="rootcauseAnalysisRequired-simple">{TextData.rootCauseAnalysisRequired}</InputLabel>
                                    <Select
                                        fullWidth
                                        value={values.rootcauseAnalysisRequired}
                                        onChange={handleChange}
                                        inputProps={{
                                            name: 'rootcauseAnalysisRequired',
                                            id: 'rootcauseAnalysisRequired-simple',
                                        }}
                                    >
                                        <MenuItem value="">Select</MenuItem>
                                        <MenuItem value="Y">Yes</MenuItem>
                                        <MenuItem value="N">No</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TextField fullWidth
                                        name="workdone"
                                        multiline
                                        row='1'
                                        placeholder="Work Done"
                                        label="Work Done"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.workdone}
                                    />
                                </FormControl>
                            </Grid>
                        </Grid>
                    </Fragment>
                    <Typography variant="caption" className={classes.tc}>{TextData.breakdownApproval}</Typography>
                    <Fragment>
                        <Grid
                            container
                            direction="row"
                            alignItems="center"
                        >
                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <InputLabel htmlFor="approvalForBreakdown-simple">{TextData.approvalForBreakdown}</InputLabel>
                                    <Select
                                        fullWidth
                                        value={values.approvalForBreakdown}
                                        onChange={handleChange}
                                        inputProps={{
                                            name: 'approvalForBreakdown',
                                            id: 'approvalForBreakdown-simple',
                                        }}
                                    >
                                        <MenuItem value="">Select</MenuItem>
                                        <MenuItem value="Y">Yes</MenuItem>
                                        <MenuItem value="N">No</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <ApprovedBy approvedByMaster={approvedByMaster} {...props} />
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                        <DatePicker
                                            keyboard
                                            name="approvalDate"
                                            id="approvalDate"
                                            label="Approval Date"
                                            placeholder="10/10/2018"
                                            format={'DD/MM/YYYY'}
                                            value={values.approvalDate}
                                            onChange={(date) => {
                                                setFieldValue("approvalDate", date)
                                            }}
                                            disableOpenOnEnter
                                            animateYearScrolling={false}
                                        />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12}>
                                <Button variant="contained" type="submit" color="primary" className={classes.button}>
                                    {TextData.save}
                                </Button>
                                <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                                    {TextData.reset}
                                </Button>
                                <Button type="button" variant="contained" color="primary" className={classes.button}
                                    onClick={handleDocumentOpen}>
                                    {TextData.documents}
                                </Button>
                                <Button type="button" variant="contained" color="primary" className={classes.button}
                                    onClick={handleManpowerOpen}>
                                    {TextData.manpower}
                                </Button>
                                <Button type="button" variant="contained" color="primary" className={classes.button}
                                    onClick={handleSpareUsedOpen}>
                                    {TextData.addSparesUsed}
                                </Button>
                            </Grid>

                        </Grid>
                    </Fragment>
                </form>
            </Fragment>
        </Scroll>
    );
});

const EnhancedCompletionForm = withFormik({
    enableReinitialize: true,
    mapPropsToValues: ({ data, breakdownData }) => {
        if (breakdownData && breakdownData[0]) {
            return ({
                completionDate: moment(breakdownData[0].comp_date), completionTime: moment(breakdownData[0].comp_time),
                productionDowntime: breakdownData[0].prod_dntime, contractualCostInvolved: breakdownData[0].con_cost,
                opportunityLoss: breakdownData[0].opportunity_loss,
                rootcauseAnalysisRequired: breakdownData[0].Analysis_rqrd ? breakdownData[0].Analysis_rqrd.replace(/ /g, "") : null,
                workdone: breakdownData[0].Line,
                approvalForBreakdown: breakdownData[0].bd_approval_status ? breakdownData[0].bd_approval_status.replace(/ /g, "") : '',
                approvedBy: breakdownData[0].bd_approval_name ? breakdownData[0].bd_approval_name.replace(/ /g, "") : null,
                approvalDate: moment(breakdownData[0].bd_approval_date)
            })
        } else {
            return ({
                completionDate: new Date(), completionTime: new Date(), productionDowntime: '', contractualCostInvolved: '',
                opportunityLoss: '', rootcauseAnalysisRequired: '', workdone: '', approvalForBreakdown: '', approvedBy: '',
                approvalDate: new Date()
            })
        }
    },

    validationSchema: Yup.object().shape({
    }),

    handleSubmit: async (values, { setSubmitting, props }) => {
        setSubmitting(true);

        if (props.type === "EDIT") {
            await props.getData({
                spName: "Sp_Insert_Update_Delete_Break_Down_Maint1",
                payload: {
                    bd_srno: props.breakdownData[0].bd_srno,
                    Eqp_ID: props.breakdownData[0].Eqp_ID,
                    bd_det: props.breakdownData[0].bd_det,
                    bd_date: props.breakdownData[0].bd_date,
                    bd_time: props.breakdownData[0].bd_time,
                    prod_dntime: values.productionDowntime,
                    agency: props.breakdownData[0].agency,
                    coord_per: props.breakdownData[0].coord_per,
                    comp_date: values.completionDate,
                    comp_time: values.completionTime,
                    notes: props.breakdownData[0].notes,
                    area_id: props.breakdownData[0].area_id,
                    failure_id: props.breakdownData[0].failure_id,
                    opportunity_loss: values.opportunityLoss,
                    nature: props.breakdownData[0].nature,
                    Flag: "U",
                    reported_By: props.breakdownData[0].reported_by,
                    con_cost: values.contractualCostInvolved,
                    Line: values.workdone,
                    emp_id: props.breakdownData[0].emp_id,
                    eqp_unit: props.breakdownData[0].eqp_unit,
                    entered_by: props.breakdownData[0].entered_by,
                    bd_approval_status: values.approvalForBreakdown,
                    bd_approval_date: values.approvalDate,
                    bd_approval_name: values.approvedBy,
                    assign_to_id1: props.breakdownData[0].assign_to_id1,
                    assign_to_id2: props.breakdownData[0].assign_to_id2,
                    assign_to_id3: props.breakdownData[0].assign_to_id3,
                    assign_to_id4: props.breakdownData[0].assign_to_id4,
                    assembly_description: props.breakdownData[0].assembly_description,
                    sub_assembly: props.breakdownData[0].sub_assembly,
                    Phy_Pheno_Desc: props.breakdownData[0].Phy_Pheno_Desc,
                    Analysis_rqrd: values.rootcauseAnalysisRequired,
                    prod_code: props.breakdownData[0].prod_code
                }
            })
            setSubmitting(false);
            actions.snackBarHandleOpen({
                message: "Record updated successfully."
            })
        }
    },

    displayName: 'CompletionForm',
})(CompletionForm);

export default EnhancedCompletionForm;