const styles = theme => ({
    fullScreenFormView: {
        flexGrow: 1,
        TextAlign: 'center'
    },
    toolbar: {
        background: "#f5f5f5",
        flex: '1',
        display: 'flex',
        paddingTop: "1px",
        paddingBottom: "1px",
    },
    toolbarIcon1: {
        "background-color": "#3f51b5",
        color: "white",
        marginLeft: '50%',
        marginRight: '10px',
        marginTop: '12px',
        marginBottom: '12px'
    },
    toolbarIcon2: {
        "background-color": "#3f51b5",
        color: "white",
        marginLeft: '12px',
        marginRight: '10px',
        marginTop: '12px',
        marginBottom: '12px'
    },
    autoSuggest: {
        margin: theme.spacing.unit,
        maxWidth: 400,
        width: "100%",
        textAlign: "left",
        display: "flex"
    },
    autoSuggest2: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit,
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        maxWidth: 400,
        width: "100%",
        textAlign: 'left',
        display: "flex"
    },
    dialogStyles: {
        paperWidthSm: '400px',
        textAlign: 'center',
        overflow:'visible'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        marginTop: "20px",
        paddingBottom: "10px",
        paddingTop: "10px",
        maxWidth: 420,
        textAlign: 'center'
    },
    datepicker: {
        margin: theme.spacing.unit,
        width: "100%",
        maxWidth: 400
    },
    button: {
        padding: theme.spacing.unit * 2
    },
    gridLabel: {
        // color: "#424242",
        textAlign: "left",
        marginTop: 17,
        marginLeft: 5
    }
});

export default styles;