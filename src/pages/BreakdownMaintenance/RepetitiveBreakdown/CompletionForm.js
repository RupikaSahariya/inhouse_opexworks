import React, { Fragment, useState, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import { withFormik } from "formik";
import * as Yup from "yup";
import { DatePicker, TimePicker } from 'material-ui-pickers';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import Scroll from './../Scroll';
import styles from "./styles";
import TextData from "./textData";
import CircularProgress from '@material-ui/core/CircularProgress';
import moment from 'moment';
import AutoSuggest from "./../../../components/Select";
import { actions } from "./../../../actions";

const ApprovedBy = ((props) => {
    const {
        values,
        classes,
        touched,
        errors,
        setFieldValue
    } = props;
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest}
                name="approvedBy"
                error={errors.approvedBy && (touched.approvedBy)} FormHelperTextProps={{
                    error: errors.approvedBy && (touched.approvedBy)
                }}
                label="Approved By"
                helperText={
                    errors.approvedBy && (touched.approvedBy)
                        ? errors.approvedBy
                        : null
                }
                value={values.approvedBy}
                placeholder={TextData.approvedBy}
                options={props.ALL_EMPLOYEE_MASTER ? props.ALL_EMPLOYEE_MASTER.data.map((item, i) => ({
                    value: item.emp_id,
                    label: item.emp_name,
                })) : []}
                onChange={(value) => {
                    setFieldValue("approvedBy", value)
                }}
            >
            </AutoSuggest>

        </React.Fragment>
    )
})

const CompletionForm = withStyles(styles)((props) => {
    if (props.loading) {
        return (<div
            style={{
                height: window.innerHeight - 250,
                width: '100%',
                paddingTop: '15%'
            }}>
            <CircularProgress />;
        </div>)
    }

    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue,
        getData,
    } = props;

    const [approvedByMaster, setApprovedByMaster] = useState([]);
    useEffect(() => {
        async function setFailureTypeData() {
            const data = await Promise.all([
                getData({
                    spName: "sp_select_all_from_Employee_master",
                    payload: {}
                })
            ])
            if (data && data.length > 0) {
                setApprovedByMaster(data[0].data);
            }
        }
        setFailureTypeData();
    }, [props.values.approvalForBreakdown]);

    const handleDocumentOpen = async () => {
        if (props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID) {
            console.log('line101ppp', props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID)
            const eqp_desc = props.selectedData.data.eqp_desc;
            const breakdownData = props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID.data[0];
            actions.handleDocumentDialog({ page: "REPETITIVE_BREAKDOWN_DOCUMENT", payload: { "4": [props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID.data[0]], breakdownData: [breakdownData], eqp_desc } })
        }
    }

    const handleManpowerOpen = async () => {
        if (props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID) {
            console.log('line101ppp', props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID)
            const eqp_desc = props.selectedData.data.eqp_desc;
            const breakdownData = props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID.data[0];
            actions.handleManpowerDialog({ page: "REPETITIVE_BREAKDOWN_MANPOWER", payload: { "4": [props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID.data[0]], breakdownData: [breakdownData], eqp_desc } })
        }
    }

    const handleSpareUsedOpen = async () => {
        if (props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID) {
            console.log('line101ppp', props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID)
            const eqp_desc = props.selectedData.data.eqp_desc;
            const breakdownData = props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID.data[0];
            actions.handleSpareUsedDialog({ page: "REPETITIVE_BREAKDOWN_SPARESUSED", payload: { "4": [props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID.data[0]], breakdownData: [breakdownData], eqp_desc } })
        }
    }
    return (
        <Scroll>
            <Fragment>
                <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
                    <Fragment>
                        <Grid
                            container
                            item xs={12}
                            direction="row"
                        >
                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <DatePicker
                                        keyboard
                                        name="completionDate"
                                        id="completionDate"
                                        label="Completion Date"
                                        format={'DD/MM/YYYY'}
                                        value={values.completionDate}
                                        onChange={(date) => {
                                            setFieldValue("completionDate", date)
                                        }}
                                        disableOpenOnEnter
                                        animateYearScrolling={false}
                                        error={errors.completionDate && (touched.completionDate)}
                                        FormHelperTextProps={{
                                            error: errors.completionDate && (touched.completionDate)
                                        }}
                                        helperText={
                                            errors.completionDate && (touched.completionDate)
                                                ? errors.completionDate
                                                : null
                                        }
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TimePicker fullWidth
                                        keyboard
                                        label="Completion Time"
                                        id="completionTime"
                                        name="completionTime"
                                        value={values.completionTime}
                                        onChange={(date) => {
                                            setFieldValue("completionTime", date)
                                        }}
                                        mask={[/\d/, /\d/, ":", /\d/, /\d/, " ", /a|p/i, "M"]}
                                        error={errors.completionTime && (touched.completionTime)}
                                        FormHelperTextProps={{
                                            error: errors.completionTime && (touched.completionTime)
                                        }}
                                        helperText={
                                            errors.completionTime && (touched.completionTime)
                                                ? errors.completionTime
                                                : null
                                        }
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TextField fullWidth
                                        name="productionDowntime"
                                        placeholder="Production DownTime (In Minute)"
                                        label="Production Downtime"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.productionDowntime}
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TextField fullWidth
                                        name="contractualCostInvolved"
                                        placeholder="Contractual Cost Involved"
                                        label="Contractual Cost Involved"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.contractualCostInvolved}
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TextField fullWidth
                                        name="opportunityLoss"
                                        placeholder="Opportunity Loss"
                                        label="Opportunity Loss"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.opportunityLoss}
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <InputLabel htmlFor="rootcauseAnalysisRequired-simple">{TextData.rootCauseAnalysisRequired}</InputLabel>
                                    <Select
                                        fullWidth
                                        value={values.rootcauseAnalysisRequired}
                                        onChange={handleChange}
                                        inputProps={{
                                            name: 'rootcauseAnalysisRequired',
                                            id: 'rootcauseAnalysisRequired-simple',
                                        }}
                                    >
                                        <MenuItem value="">Select</MenuItem>
                                        <MenuItem value="Y">Yes</MenuItem>
                                        <MenuItem value="N">No</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <TextField fullWidth
                                        name="workdone"
                                        multiline
                                        row='1'
                                        placeholder="Work Done"
                                        label="Work Done"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.workdone}
                                    />
                                </FormControl>
                            </Grid>
                        </Grid>
                    </Fragment>
                    <Typography variant="caption" className={classes.tc}>{TextData.breakdownApproval}</Typography>
                    <Fragment>
                        <Grid
                            container
                            direction="row"
                            alignItems="center"
                        >
                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <InputLabel htmlFor="approvalForBreakdown-simple">{TextData.approvalForBreakdown}</InputLabel>
                                    <Select
                                        fullWidth
                                        value={values.approvalForBreakdown}
                                        onChange={handleChange}
                                        inputProps={{
                                            name: 'approvalForBreakdown',
                                            id: 'approvalForBreakdown-simple',
                                        }}
                                    >
                                        <MenuItem value="">Select</MenuItem>
                                        <MenuItem value="Y">Yes</MenuItem>
                                        <MenuItem value="N">No</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <ApprovedBy approvedByMaster={approvedByMaster} {...props} />
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest}>
                                    <Grid container justify="space-around">
                                        <DatePicker
                                            keyboard
                                            fullWidth
                                            name="approvalDate"
                                            label="Approval Date"
                                            placeholder="10/10/2018"
                                            format={'DD/MM/YYYY'}
                                            value={values.approvalDate}
                                            onChange={(date) => {
                                                setFieldValue("date", date)
                                            }}
                                            disableOpenOnEnter
                                            animateYearScrolling={false}
                                        />
                                    </Grid>
                                </FormControl>
                            </Grid>

                            <Grid item xs={12}>
                                <Button variant="contained" type="submit" color="primary" className={classes.button}>
                                    {TextData.save}
                                </Button>
                                <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                                    {TextData.reset}
                                </Button>
                                <Button type="button" variant="contained" color="primary" className={classes.button} onClick={handleDocumentOpen}>
                                    {TextData.documents}
                                </Button>
                                <Button type="button" variant="contained" color="primary" className={classes.button} onClick={handleManpowerOpen}>
                                    {TextData.manpower}
                                </Button>
                                <Button type="button" variant="contained" color="primary" className={classes.button} onClick={handleSpareUsedOpen}>
                                    {TextData.addSparesUsed}
                                </Button>
                            </Grid>
                        </Grid>
                    </Fragment>
                </form>
            </Fragment>
        </Scroll>
    );
});

const EnhancedCompletionForm = withFormik({
    enableReinitialize: true,
    mapPropsToValues: ({ ...props }) => {

        if (props.selectedData && props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID) {
            const breakdownData = props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID.data[0]
            return ({
                completionDate: moment(breakdownData.comp_date), completionTime: moment(breakdownData.comp_time),
                productionDowntime: breakdownData.prod_dntime, contractualCostInvolved: breakdownData.con_cost,
                opportunityLoss: breakdownData.opportunity_loss,
                rootcauseAnalysisRequired: breakdownData.Analysis_rqrd ? breakdownData.Analysis_rqrd.replace(/ /g, "") : null,
                workdone: breakdownData.Line,
                approvalForBreakdown: breakdownData.bd_approval_status ? breakdownData.bd_approval_status.replace(/ /g, "") : '',
                approvedBy: breakdownData.bd_approval_name ? breakdownData.bd_approval_name.replace(/ /g, "") : null,
                approvalDate: moment(breakdownData.bd_approval_date)
            })
        } else {
            return ({
                completionDate: new Date(), completionTime: new Date(), productionDowntime: '', contractualCostInvolved: '',
                opportunityLoss: '', rootcauseAnalysisRequired: '', workdone: '', approvalForBreakdown: '', approvedBy: '',
                approvalDate: new Date()
            })
        }
    },

    validationSchema: Yup.object().shape({
    }),

    handleSubmit: async (values, { setSubmitting, props }) => {
        setSubmitting(true);
        if (props.type === "EDIT" || props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID) {
            const breakdownData = props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID.data[0];
            await props.getData({
                spName: "Sp_Insert_Update_Delete_Break_Down_Maint1",
                payload: {
                    bd_srno: breakdownData.bd_srno,
                    Eqp_ID: breakdownData.Eqp_ID,
                    bd_det: breakdownData.bd_det,
                    bd_date: breakdownData.bd_date,
                    bd_time: breakdownData.bd_date,
                    prod_dntime: values.productionDowntime,
                    agency: breakdownData.agency,
                    coord_per: breakdownData.coord_per,
                    comp_date: values.completionDate,
                    comp_time: values.completionTime,
                    notes: breakdownData.notes,
                    area_id: breakdownData.area_id,
                    failure_id: breakdownData.failure_id,
                    opportunity_loss: values.opportunityLoss,
                    nature: breakdownData.nature,
                    userId: breakdownData.userid,
                    Flag: "U",
                    reported_By: breakdownData.reported_by,
                    con_cost: values.contractualCostInvolved,
                    Line: values.workdone,
                    emp_id: breakdownData.emp_id,
                    eqp_unit: breakdownData.eqp_unit,
                    entered_by: breakdownData.entered_by,
                    bd_approval_status: values.approvalForBreakdown,
                    bd_approval_date: values.approvalDate,
                    bd_approval_name: values.approvedBy,
                    assign_to_id1: breakdownData.assign_to_id1,
                    assign_to_id2: breakdownData.assign_to_id2,
                    assign_to_id3: breakdownData.assign_to_id3,
                    assign_to_id4: breakdownData.assign_to_id4,
                    assembly_description: breakdownData.assembly_description,
                    sub_assembly: breakdownData.sub_assembly,
                    Phy_Pheno_Desc: breakdownData.Phy_Pheno_Desc,
                    Analysis_rqrd: values.rootcauseAnalysisRequired,
                    prod_code: breakdownData.prod_code
                }
            })
            await actions.getData({
                spName: "sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID",
                payload: {
                    Eqp_ID: breakdownData.Eqp_ID,
                    bd_srno: breakdownData.bd_srno
                },
                key: "sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID"
            })
            setSubmitting(false);
            await actions.snackBarHandleOpen({
                message: "Repetitive Breakdown Updated."
            })
        }
    },

    displayName: 'CompletionForm',
})(CompletionForm)

export default EnhancedCompletionForm;