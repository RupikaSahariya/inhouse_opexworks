import React, { Fragment, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Dialog from '@material-ui/core/Dialog';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Tooltip from '@material-ui/core/Tooltip';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import TextField from '@material-ui/core/TextField';
import { withFormik } from "formik";
import * as Yup from "yup";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { compose } from "recompose";
import Paper from "@material-ui/core/Paper";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import moment from "moment"
import { DatePicker } from 'material-ui-pickers';
import RefreshIcon from "@material-ui/icons/RefreshOutlined";
import Header1 from './../../../components/Header1/Header1';
import { dataProvider } from "./../../../utils/AppContext"
import AgTable from "./../../../components/AgTable";
import styles from "./styles";
import EnhancedAnalysisForm from "./AnalysisForm";
import EnhancedCompletionForm from "./CompletionForm";
import EnhancedReportingForm from "./ReportingForm";
import TextData from "./textData";
import AppContext from "./../../../utils/AppContext";
import DocumentsPage from "../Documents";
import ManpowerPage from "../ManPower";
import SparesConsumedPage from "../SparesConsumed";
import WhyWhyAnalysisPage from "../WhyWhyAnalysis";
import { Consumer, actions, connect } from "../../../actions";
import AutoSuggest from "./../../../components/Select";

const BreakdownPhenomenon = ((props) => {
    const {
        classes,
        values,
        touched,
        errors,
        handleChange,
        breakdownPhenomenonMaster,
        setFieldValue
    } = props
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest3}
                name="breakdownPhenomenon"
                //  error={errors.breakdownPhenomenon && (touched.breakdownPhenomenon)} FormHelperTextProps={{
                //      error: errors.breakdownPhenomenon && (touched.breakdownPhenomenon)
                //  }}
                label={TextData.breakdownPhenomenon}
                textFieldProps={{
                    helperText:
                        errors.breakdownPhenomenon && (touched.breakdownPhenomenon)
                            ? errors.breakdownPhenomenon
                            : null,
                    error: errors.breakdownPhenomenon && (touched.breakdownPhenomenon)
                }}
                value={values.breakdownPhenomenon}
                placeholder={TextData.breakdownPhenomenon}
                options={breakdownPhenomenonMaster ? breakdownPhenomenonMaster.map((item, i) => ({
                    value: item.Phy_Pheno_Desc,
                    label: item.Phy_Pheno_Desc,
                })) : []}
                onChange={(value) => {
                    setFieldValue("breakdownPhenomenon", value)
                }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class RepetitiveBreakdown extends React.PureComponent {
    state = {
        value: 0,
        documentOpen: false,
        manpowerOpen: false,
        sparesUsed: false,
        data: false,
        selectData: false
    };


    handleChange = (event, value) => {
        this.setState({ value });
    };


    handleRefresh = () => {
        window.location.reload();
    };


    afterCreate = async (data) => {
        this.setState({
            selectedData: data,
        })
    }

    selectedData = () => !this.state.selectedData ? this.props.selectedData : this.state.selectedData

    render() {
        const { classes } = this.props;
        return (
            <AppContext.Consumer>
                {({ getData }) => (
                    <Paper className={classes.fullScreenFormView}>
                        <Tabs
                            value={this.state.value}
                            onChange={this.handleChange}
                            indicatorColor="primary"
                            textColor="primary"
                            centered
                        >
                            <Tab label="Reporting" />
                            {(this.props.selectedData || this.props.createRepetitiveBreakdown) ? <Tab label="Analysis" /> : null}
                            {(this.props.selectedData || this.props.createRepetitiveBreakdown) ? <Tab label="Completion" /> : null}
                        </Tabs>
                        {this.state.value === 0 ? <EnhancedReportingForm {...this.props} selectedData={this.props.selectedData || this.props.createRepetitiveBreakdown} getData={getData} afterCreate={this.afterCreate} /> : null}
                        {this.state.value === 1 && (this.props.selectedData || this.props.createRepetitiveBreakdown) ? <EnhancedAnalysisForm getData={getData} selectedData={this.props.selectedData || this.props.createRepetitiveBreakdown}  {...this.props} /> : null}
                        {this.state.value === 2 && (this.props.selectedData || this.props.createRepetitiveBreakdown) ? <EnhancedCompletionForm getData={getData} selectedData={this.props.selectedData || this.props.SP_Insert_Update_Delete_Break_Down_Maint_Output} {...this.props} /> : null}
                    </Paper>
                )}
            </AppContext.Consumer>
        );
    }
}

const StyledRepetitiveBreakDown = compose(withStyles(styles), connect(state => state))(RepetitiveBreakdown)

StyledRepetitiveBreakDown.propTypes = {
    classes: PropTypes.object.isRequired,
};

const SearchForm = withStyles(styles)((props) => {

    const {
        values,
        touched,
        errors,
        handleChange,
        classes,
        handleSubmit,
        getData
    } = props;
    const [breakdownPhenomenonMaster, setbreakdownPhenomenonMaster] = useState([]);
    useEffect(() => {
        async function setbreakdownPhenomenonMasterData() {
            const data = await Promise.all([
                getData({
                    spName: "SP_Get_All_Repetative_Breakdowns_By_From_Date_To_Date_Repeative_Count",
                    payload: {
                        'From_Date': values.fromDate,
                        'To_Date': values.toDate,
                        'Repeat_Count': values.repetitionNumber
                    },
                    key: "REPETITIVE_DATA_LIST"
                })
            ])
            if (data && data.length > 0) {
                setbreakdownPhenomenonMaster(data[0].data);
            }
        }

        setbreakdownPhenomenonMasterData();
    }, [props.values.fromDate, props.values.toDate, props.values.repetitionNumber]);

    return (
        <Fragment>
            <form onSubmit={handleSubmit} style={{ 'border': '0px solid red', 'width': '70%' }}>
                <Grid
                    container
                    item xs={12}
                    direction="row"
                    justify="space-between"
                >
                    <Grid item xs={12} sm={6} md={3}>
                        <FormControl >
                            <DatePicker
                                className={classes.datepicker2}
                                keyboard
                                name="fromDate"
                                id="fromDate"
                                label="From Date"
                                placeholder="10/10/2018"
                                format={'DD/MM/YYYY'}
                                value={props.values.fromDate}
                                onChange={(date) => {
                                    props.setFieldValue("fromDate", date)
                                }}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <FormControl>
                            <DatePicker
                                className={classes.datepicker2}
                                keyboard
                                name="toDate"
                                id="toDate"
                                label="To Date"
                                placeholder="10/10/2018"
                                format={'DD/MM/YYYY'}
                                value={props.values.toDate}
                                onChange={(date) => {
                                    props.setFieldValue("toDate", date)
                                }}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={4} md={3}>
                        <FormControl >
                            <TextField fullWidth
                                className={classes.datepicker2}
                                name="repetitionNumber"
                                id="repetitionNumber"
                                placeholder="Enter Repetition No"
                                label="Repetition No"
                                onChange={handleChange}
                                value={props.values.repetitionNumber}
                                error={errors.repetitionNumber && (touched.repetitionNumber)}
                                FormHelperTextProps={{
                                    error: errors.repetitionNumber && (touched.repetitionNumber)
                                }}
                                helperText={
                                    errors.repetitionNumber && (touched.repetitionNumber)
                                        ? errors.repetitionNumber
                                        : null
                                }
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={10} sm={6} md={2}>
                        < BreakdownPhenomenon onBreakdownPhenomenonSelect={this.onBreakdownPhenomenonSelect}
                            breakdownPhenomenonMaster={breakdownPhenomenonMaster} {...props} />
                    </Grid>
                    <Grid item xs={2} sm={2} md={1}>
                        <Tooltip title="Search">
                            <Fab color="primary" type="submit" variant="extended" size={"small"} style={{ "marginTop": "15px", "marginLeft": "70px" }}>
                                <SearchIcon />&nbsp;
                        </Fab>
                        </Tooltip>
                    </Grid>

                </Grid>
            </form>
        </Fragment>
    )
})

const EnhancedSearch = compose(dataProvider(
    {
        query: (props) => {
            return (
                {
                    spName: "sp_select_all_from_Employee_master",
                    payload: {},
                    key: "key1"
                }
            )
        }
    }
), withFormik({
    mapPropsToValues: (props) => ({
        fromDate: new Date(), toDate: new Date(), repetitionNumber: '', breakdownPhenomenon: ''
    }),

    validationSchema: Yup.object().shape({
        repetitionNumber: Yup.number("repetitionNumber is required and should be a number").required("Repetition Number Required"),
        breakdownPhenomenon: Yup.string("breakdownPhenomenon is required and should be a string").required("Breakdown Phenomenon Required"),
    }),

    handleSubmit: async (values, { props, setSubmitting }) => {
        setSubmitting(true);
        const data = await Promise.all([
            actions.getData({
                spName: "SP_Select_All_From_Breck_Down_Maint_By_From_Date_To_Date_Phy_Pheno_Desc",
                payload: {
                    From_Date: values.fromDate,
                    To_Date: values.toDate,
                    Phy_Pheno_Desc: values.breakdownPhenomenon
                },
                key: "REPETITIVE_DATA_LIST"
            })
        ])
    },

    handleLoadTable: async (values, { props, setSubmitting }) => {
        setSubmitting(true);
    }
})
)(SearchForm)

class RepetitiveBreakdownPage extends React.PureComponent {
    state = {
        selectedBreakdownPhenomenon: null,
        dialogOpen: false,
        BreakDownPhenomenonMaster: [],
        repetitiveBreakdownList: [],
        refresh: false,
        documentOpen: false,
        manpowerOpen: false,
        sparesUsedOpen: false,
        breadcrumb: [
            { name: "Home", path: "/" },
            { name: "Repetitive Breakdown", path: "/breakdownmaintenance/repetitivebreakdown" }]
    }

    handleClickOpen = (selectedData) => {
        actions.openDialog({
            page: "REPETITIVE_DATA_DIALOG",
            payload: selectedData
        })
    };

    handleDocument = () => {
        actions.closeDocument({ page: "REPETITIVE_BREAKDOWN_DOCUMENT" })
    };

    handleManPower = () => {
        actions.closeManpower({ page: "REPETITIVE_BREAKDOWN_MANPOWER" })
    };

    handleSpareUsed = () => {
        actions.closeSpareUsed({ page: "REPETITIVE_BREAKDOWN_SPARESUSED" })
    };

    handleWhyWhyAnalysisOpen = async (selectedData) => {
        actions.handleWhyWhyAnalysisDialog({ page: "WHY_WHY_ANALYSIS", payload: selectedData.data })
        actions.getData({
            spName: "SP_Select_All_From_BDM_Why_Why_Analysis_By_Eqp_ID_And_Bd_Srno",
            payload: {
                Eqp_ID: selectedData.data.Eqp_ID,
                bd_srno: selectedData.data.bd_srno
            },
            key: "WHY_WHY_ANALYSIS_DATA"
        })

    };

    handleWhyWhyAnalysisClose = () => {
        actions.closeWhyWhyAnalysis({ page: "WHY_WHY_ANALYSIS" })
    };

    handleClose = () => {
        actions.closeDialog({
            page: "REPETITIVE_DATA_DIALOG"
        })
    };

    componentDidMount() {
    };

    onGridReady = (params) => {
        // params.api.sizeColumnsToFit()
        this.gridParams = params
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    };

    exportData = () => {
        this.gridApi.exportDataAsCsv(this.gridParams);
    };

    getData = () => this.state.rowData ? this.state.rowData : this.props.data

    columnDefs = [{
        headerName: "Sr.No.", field: "srno", sortable: true, filter: true, width: 120,
        cellRenderer: "toolTipRenderer", toolTipText: "srno"
    },
    {
        headerName: "Equipment", field: "equipment", sortable: true, filter: true, width: 160,
        cellRenderer: "toolTipRenderer", toolTipText: "equipment"
    },
    {
        headerName: "Breakdown Date / Time", field: "breakdownDate", sortable: true, filter: true, width: 150,
        cellRenderer: "toolTipRenderer", toolTipText: "breakdownDate"
    },
    {
        headerName: "Breakdown Details", field: "breakdownDetails", sortable: true, filter: true, width: 160,
        cellRenderer: "toolTipRenderer", toolTipText: "breakdownDetails"
    },
    {
        headerName: "Reported By", field: "reportedBy", sortable: true, filter: true, width: 150,
        cellRenderer: "toolTipRenderer", toolTipText: "reportedBy"
    },
    {
        headerName: "Entered By", field: "enteredBy", sortable: true, filter: true, width: 150,
        cellRenderer: "toolTipRenderer", toolTipText: "enteredBy"
    },
    {
        headerName: "Nature", field: "nature", sortable: true, filter: true, width: 150,
        cellRenderer: "toolTipRenderer", toolTipText: "nature"
    },
    {
        headerName: "Completion Date / Time", field: "completionDateTime", sortable: true, filter: true, width: 150,
        cellRenderer: "toolTipRenderer", toolTipText: "completionDateTime"
    },
    {
        headerName: "", width: 150, cellRenderer: "whyWhyAnalysisRenderer"
    },
    {
        headerName: "", width: 100, cellRenderer: "editRenderer"
    }
    ]

    BDReportDownload = (selectedData) => {
        console.log(selectedData);
        actions.downloadReport({
            spName: "SP_rpt_Select_data_for_breakdown_report",
            payload: {
                bd_srno: selectedData.data.bd_srno
            }
        })
    }

    render() {
        const { classes } = this.props
        return (
            <Fragment>
                <Consumer>
                    {
                        ({ REPETITIVE_BREAKDOWN_DOCUMENT, REPETITIVE_DATA_DIALOG, REPETITIVE_BREAKDOWN_SPARESUSED, REPETITIVE_BREAKDOWN_MANPOWER }) => {
                            return (
                                <Dialog
                                    fullScreen
                                    className={classes.dialogStyles}
                                    open={REPETITIVE_DATA_DIALOG.isOpen}
                                    handleClose={() => actions.closeDialog({
                                        page: "REPETITIVE_DATA_DIALOG"
                                    })}
                                    aria-labelledby="form-dialog-title"
                                    maxWidth="sm"
                                    TransitionComponent={Transition}
                                >
                                    <AppBar className={classes.appBar}>
                                        <Toolbar>
                                            <div className={classes.alignHeader}></div>
                                            <Typography variant="subtitle1" color="inherit" className={classes.flex}>
                                                {TextData.repetitiveBreakdown}
                                            </Typography>

                                            <div className={classes.alignHeader}></div>
                                            <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                                                <CloseIcon />
                                            </IconButton>
                                        </Toolbar>
                                        {REPETITIVE_DATA_DIALOG.isOpen && <StyledRepetitiveBreakDown type={REPETITIVE_DATA_DIALOG.payload ? "EDIT" : "CREATE"} selectedData={REPETITIVE_DATA_DIALOG.payload} handleClose={this.handleClose} handleClose={this.handleClose} handleDocument={this.handleDocument} handleManpower={this.handleManpower}
                                            handleSpareUsed={this.handleSpareUsed}
                                        />}
                                    </AppBar>
                                    <React.Fragment>
                                        {REPETITIVE_BREAKDOWN_DOCUMENT.isOpen ? <Dialog
                                            fullScreen
                                            className={classes.dialogStyles}
                                            open={REPETITIVE_BREAKDOWN_DOCUMENT.isOpen}
                                            handleClose={this.handleClose}
                                            aria-labelledby="form-dialog-title"
                                            maxWidth="sm"
                                            TransitionComponent={Transition}
                                        >
                                            <AppBar className={classes.appBar}>
                                                <Toolbar>
                                                    <div className={classes.alignHeader}></div>
                                                    <Typography variant="subtitle1" color="inherit" className={classes.flex}>
                                                        Document
                                                            </Typography>

                                                    <div className={classes.alignHeader}></div>
                                                    <IconButton color="inherit" onClick={this.handleDocument} aria-label="Close">
                                                        <CloseIcon />
                                                    </IconButton>
                                                </Toolbar>
                                                <DocumentsPage documentData={REPETITIVE_BREAKDOWN_DOCUMENT.payload} />
                                            </AppBar>

                                        </Dialog> : null}
                                    </React.Fragment>
                                    <React.Fragment>
                                        {REPETITIVE_BREAKDOWN_MANPOWER.isOpen ? <Dialog
                                            fullScreen
                                            className={classes.dialogStyles}
                                            open={REPETITIVE_BREAKDOWN_MANPOWER.isOpen}
                                            handleClose={this.handleClose}
                                            aria-labelledby="form-dialog-title"
                                            maxWidth="sm"
                                            TransitionComponent={Transition}
                                        >
                                            <AppBar className={classes.appBar}>
                                                <Toolbar>
                                                    <div className={classes.alignHeader}></div>
                                                    <Typography variant="subtitle1" color="inherit" className={classes.flex}>
                                                        Man Power Used
                                                            </Typography>

                                                    <div className={classes.alignHeader}></div>
                                                    <IconButton color="inherit" onClick={this.handleManPower} aria-label="Close">
                                                        <CloseIcon />
                                                    </IconButton>
                                                </Toolbar>
                                                <ManpowerPage manpowerData={REPETITIVE_BREAKDOWN_MANPOWER.payload} />
                                            </AppBar>

                                        </Dialog> : null}
                                    </React.Fragment>
                                    <React.Fragment>
                                        {REPETITIVE_BREAKDOWN_SPARESUSED.isOpen ? <Dialog
                                            fullScreen
                                            className={classes.dialogStyles}
                                            open={REPETITIVE_BREAKDOWN_SPARESUSED.isOpen}
                                            handleClose={this.handleClose}
                                            aria-labelledby="form-dialog-title"
                                            maxWidth="sm"
                                            TransitionComponent={Transition}
                                        >
                                            <AppBar className={classes.appBar}>
                                                <Toolbar>
                                                    <div className={classes.alignHeader}></div>
                                                    <Typography variant="subtitle1" color="inherit" className={classes.flex}>
                                                        Spares Consumed In Breakdown
                                                                </Typography>

                                                    <div className={classes.alignHeader}></div>
                                                    <IconButton color="inherit" onClick={this.handleSpareUsed} aria-label="Close">
                                                        <CloseIcon />
                                                    </IconButton>
                                                </Toolbar>
                                                <SparesConsumedPage sparesusedData={REPETITIVE_BREAKDOWN_SPARESUSED.payload} />
                                            </AppBar>
                                        </Dialog> : null}
                                    </React.Fragment>
                                </Dialog>
                            )
                        }
                    }
                </Consumer>
                <Consumer>
                    {
                        ({ WHY_WHY_ANALYSIS, WHY_WHY_ANALYSIS_DATA }) => {
                            return (
                                <React.Fragment>
                                    {WHY_WHY_ANALYSIS.isOpen ? <Dialog
                                        fullScreen
                                        className={classes.dialogStyles}
                                        open={WHY_WHY_ANALYSIS.isOpen}
                                        handleClose={this.handleClose}
                                        aria-labelledby="form-dialog-title"
                                        maxWidth="sm"
                                        TransitionComponent={Transition}
                                    >
                                        <AppBar className={classes.appBar}>
                                            <Toolbar>
                                                <div className={classes.alignHeader}></div>
                                                <Typography variant="subtitle1" color="inherit" className={classes.flex}>
                                                    Why Why Analyisis
                                                    </Typography>
                                                <div className={classes.alignHeader}></div>
                                                <IconButton color="inherit" onClick={this.handleWhyWhyAnalysisClose} aria-label="Close">
                                                    <CloseIcon />
                                                </IconButton>
                                            </Toolbar>

                                            <WhyWhyAnalysisPage whywhyanalysisData={WHY_WHY_ANALYSIS_DATA} />

                                        </AppBar>

                                    </Dialog> : null}
                                </React.Fragment>
                            )
                        }
                    }
                </Consumer>
                <Header1 breadCrumbText={this.state.breadcrumb} breadCrumbHeader="Repetitive Breakdown" />
                <Grid
                    container
                    direction="row"
                    justify="space-between"
                >
                    <EnhancedSearch onSearch={this.onBreakdownPhenomenonSelect} selectedBreakdownPhenomenon={this.state.selectedBreakdownPhenomenon} />

                    <div className={classes.toolbar}>
                        <Tooltip title="Refresh">
                            <Fab size={"small"} color="primary" variant="extended" onClick={this.handleRefresh} className={classes.toolbarIcon}>
                                <RefreshIcon />&nbsp;
                        </Fab>
                        </Tooltip>
                    </div>
                </Grid>
                <Paper>
                    <Consumer>{({ REPETITIVE_DATA_LIST }) => {

                        const rowData = REPETITIVE_DATA_LIST ? REPETITIVE_DATA_LIST.data.map((item, i) => ({
                            data: item,
                            srno: item.bd_srno, equipment: item.eqp_desc, breakdownDate: moment(item.bd_date).format("DD/MM/YY") + ", " + moment(item.bd_time).format("HH:mm"),
                            breakdownDetails: item.bd_det, completionDateTime: moment(item.comp_date).format("DD/MM/YY HH:mm"),
                            reportedBy: item.emp_name, enteredBy: item.entered_by_name, nature: item.nature
                        })) : [];

                        return (
                            <AgTable
                                onGridReady={this.onGridReady}
                                columnDefs={this.columnDefs}
                                rowData={rowData}
                                onEdit={this.handleClickOpen}
                                onWhyWhyAnalysis={this.handleWhyWhyAnalysisOpen}
                                dontShowDelete={true}
                                onBdReport={this.BDReportDownload}
                            />
                        )
                    }}
                    </Consumer>
                </Paper>
            </Fragment >)
    }
}

export default compose(withStyles(styles), dataProvider({
    query: async (props) => {
        return (
            {
                spName: "SP_Fun_Select_All_From_Equipment_By_Plant_ID",
                payload: {}
            }
        )
    }
}))(RepetitiveBreakdownPage);