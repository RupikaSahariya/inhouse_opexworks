const styles = theme => ({
    //fullscreen form
    fullScreenFormView: {
        flexGrow: 1
    },
    autoSuggest: {
        marginTop: theme.spacing.unit * 2,
        maxWidth: 550,
        width: "100%",
        textAlign: 'left',
        display: "flex",
        flexWrap: 'wrap'
    },
    autoSuggest2: {
        marginTop: theme.spacing.unit * 3,
        marginBottom: theme.spacing.unit,
        maxWidth: 550,
        width: "100%",
        textAlign: 'left',
        display: "flex",
        flexWrap: 'wrap'
    },
    autoSuggest3: {
        marginTop: "14px",
        marginBottom: "5px",
        marginLeft: "5px",
        maxWidth: 270,
        minWidth: 175,
        width: "100%",
        textAlign: 'left',
        display: "flex",
        flexWrap: 'wrap'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        paddingBottom: "20px",
        paddingTop: "10px",
        justify: "space-around",
        paddingLeft: "6%"
    },
    datepicker: {
        marginRight: theme.spacing.unit,
        width: "100%",
        maxWidth: 400
    },
    datepicker2: {
        margin: "2px 0px",
        marginLeft:"5px",
        // marginTop: "8px",
        // marginTop: theme.spacing.unit*2,
        marginBottom:"5px",
        width: "100%",
        maxWidth: 150
    },

    button: {
        margin: theme.spacing.unit * 2,
    },

    //header styles
    breakCrumbIcon: {
        marginTop: '3px',
        fontSize: "20px"
    },
    breadCrumbText: {
        marginTop: '4px',
        color: "white",
        fontSize: "13px",
        "font-weight": "normal"
    },
    headerText: {
        color: "white",
        paddingBottom: "4px",
        fontSize: "18px",
    },
    header: {
        "background-color": "#3f51b5",
        color: "white",
    },

    //action style
    dialogStyles: {
        paperWidthSm: '800px',
        textAlign: 'center'
    },
    toolbarIcon: {
        margin: '5px 30px'
    },
    toolbar: {
        margin: '10px',
    },
    tc: {
        marginLeft: '-5px',
        marginTop: '25px',
        marginBottom: '-10px',
        fontWeight: 'bold',
        color: 'black',
        display: 'flex',
        flexWrap:'wrap'
    },
    tc2: {
        marginLeft: "70%",
        marginTop: "28px",
        display: 'flex',
        flexWrap: 'wrap',
    },
    alignHeader: {
        flex: '1 1 0%'
    }
});

export default styles