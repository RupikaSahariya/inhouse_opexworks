import React, { Fragment, useState, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import { withFormik } from "formik";
import * as Yup from "yup";
import { DatePicker, TimePicker } from 'material-ui-pickers';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Scroll from './../Scroll';
import styles from "./styles";
import TextData from "./textData";
import CircularProgress from '@material-ui/core/CircularProgress';
import moment from "moment";
import { actions } from "./../../../actions";
import AutoSuggest from "./../../../components/Select";

const Equipments = ((props) => {
    const {
        values,
        classes,
        touched,
        errors,
        setFieldValue
    } = props
    return (
    <React.Fragment>
        <AutoSuggest className={classes.autoSuggest}
                name="equipment"
                label="Equipment"
                // FormHelperTextProps={{
                //     error: errors.equipment && (touched.equipment)
                // }}
                error={errors.equipment && (touched.equipment)} FormHelperTextProps={{
                    error: errors.equipment && (touched.equipment)
                }}                
                helperText={
                    errors.equipment && (touched.equipment)
                        ? errors.equipment
                        : null
                }
                value={values.equipment}
                placeholder={TextData.equipment}
                options={props.ALL_EQUIPMENT_MASTER && props.ALL_EQUIPMENT_MASTER.data.map((item, i) => ({
                    value: item.Eqp_ID,
                    label: item.eqp_desc,
                }))}
                onChange={(value) => {
                    setFieldValue("equipment", value)
                    setFieldValue("breakdownPhenomenon", null)
                }}
            ></AutoSuggest>
    </React.Fragment>
    )
})

const ReportedBy = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props;
    console.log("ReportedBy",props)
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest}
                name="reportedby"
                // FormHelperTextProps={{
                //     error: errors.reportedby && (touched.reportedby)
                // }}
                label="Reported By"
                error={errors.reportedby && (touched.reportedby)} FormHelperTextProps={{
                    error: errors.reportedby && (touched.reportedby)
                }}                
                helperText={
                    errors.reportedby && (touched.reportedby)
                        ? errors.reportedby
                        : null
                }
                value={values.reportedby}
                placeholder={TextData.reportedBy}
                options={props.ALL_EMPLOYEE_MASTER ? props.ALL_EMPLOYEE_MASTER.data.map((item, i) => ({
                    value: item.emp_id,
                    label: item.emp_name,
                })) : []}
                onChange={(value) => {
                    setFieldValue("reportedby", value)
                }}
            >
            </AutoSuggest>

        </React.Fragment>
    )
})

const BreakdownPhenomenon = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        breakdownPhenomenonMaster,
        setFieldValue
    } = props
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest}
                name="breakdownPhenomenon"
                error={errors.breakdownPhenomenon && (touched.breakdownPhenomenon)} FormHelperTextProps={{
                    error: errors.breakdownPhenomenon && (touched.breakdownPhenomenon)
                }}
                label="breakdown Phenomenon"
                helperText={
                    errors.breakdownPhenomenon && (touched.breakdownPhenomenon)
                        ? errors.breakdownPhenomenon
                        : null
                }
                value={values.breakdownPhenomenon}
                placeholder={TextData.breakdownPhenomenon}
                isDisabled = {breakdownPhenomenonMaster === undefined || breakdownPhenomenonMaster.length==0}
                options={breakdownPhenomenonMaster ? breakdownPhenomenonMaster.map((item, i) => ({
                    value: item.sr_no,
                    label: item.Phy_Pheno_Desc,
                })) : []}
                onChange={(value) => {
                    setFieldValue("breakdownPhenomenon", value)
                }}
            >
            </AutoSuggest>

        </React.Fragment>
    )
})

const EnteredBy = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest}
                name="enteredBy"
                error={errors.enteredBy && (touched.enteredBy)} FormHelperTextProps={{
                    error: errors.enteredBy && (touched.enteredBy)
                }}
                label="Entered By"
                helperText={
                    errors.enteredBy && (touched.enteredBy)
                        ? errors.enteredBy
                        : null
                }
                value={values.enteredBy}
                placeholder={TextData.enteredBy}
                options={props.ALL_EMPLOYEE_MASTER ? props.ALL_EMPLOYEE_MASTER.data.map((item, i) => ({
                    value: item.emp_id,
                    label: item.emp_name,
                })) : []}
                onChange={(value) => {
                    setFieldValue("enteredBy", value)
                }}
            >
            </AutoSuggest>

        </React.Fragment>
        )
})

const AssignedTo = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props
    return (
        <React.Fragment>
        <AutoSuggest className={classes.autoSuggest}
            name="assignedTo"
            error={errors.assignedTo && (touched.assignedTo)} FormHelperTextProps={{
                error: errors.assignedTo && (touched.assignedTo)
            }}
            label="Assigned to"
            helperText={
                errors.assignedTo && (touched.assignedTo)
                    ? errors.assignedTo
                    : null
            }
            value={values.assignedTo}
            placeholder={TextData.assignedTo}
            options={props.ALL_EMPLOYEE_MASTER ? props.ALL_EMPLOYEE_MASTER.data.map((item, i) => ({
                value: item.emp_id,
                label: item.emp_name,
            })) : []}
            onChange={(value) => {
                setFieldValue("assignedTo", value)
            }}
        >
        </AutoSuggest>
    </React.Fragment>
        )
})

const ReportingForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue,
        getData,
        status
    } = props;

    const [breakdownPhenomenonMaster, setbreakdownPhenomenonMaster] = useState([]);
    useEffect(() => {
        async function setbreakdownPhenomenonMasterData() {
            const data = await Promise.all([
                getData({
                    spName: "SP_Select_All_From_Bd_Physical_Pheno_By_Eqp_ID",
                    payload: {
                        Eqp_id: values.equipment
                    }
                })
            ])
            if (data && data.length > 0) {
                setbreakdownPhenomenonMaster(data[0].data);
            }
        }
        setbreakdownPhenomenonMasterData();
    }, [props.values.equipment]);

    if (props.loading) {
        return (<div
            style={{
                height: window.innerHeight - 250,
                width: '100%',
                paddingTop: '15%'
            }}>
            <CircularProgress />;
        </div>)
    }
    console.log(values, errors);

    return (
        <Scroll >
            <Fragment>
                <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
                    <Grid
                        container
                        item xs={12}
                        direction="row"
                        justify="space-around"
                    >
                        <Grid
                            container
                            direction="row"
                            alignItems="center"
                        >
                            <Grid item xs={12} md={6}>

                                <FormControl className={classes.autoSuggest2}>
                                    <TextField fullWidth
                                        name="breakdownNo"
                                        placeholder="Enter Breakdown No"
                                        label="Breakdown Number"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.breakdownNo}
                                        disabled={status.isDisabled.breakdownNo}
                                        inputProps={{ style: { "color": "black" } }}
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <Equipments {...props} />
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <ReportedBy {...props} />
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest2}>
                                    <DatePicker
                                        keyboard
                                        name="breakdownDate"
                                        id="breakdowndate"
                                        label="Breakdown Date"
                                        format={'DD/MM/YYYY'}
                                        value={values.breakdownDate}
                                        onChange={(date) => {
                                            setFieldValue("breakdownDate", date)
                                        }}
                                        disableOpenOnEnter
                                        animateYearScrolling={false}
                                        error={errors.breakdownDate && (touched.breakdownDate)}
                                        FormHelperTextProps={{
                                            error: errors.breakdownDate && (touched.breakdownDate)
                                        }}
                                        helperText={
                                            errors.breakdownDate && (touched.breakdownDate)
                                                ? errors.breakdownDate
                                                : null
                                        }
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest2}>
                                    <TimePicker fullWidth
                                        keyboard
                                        label="Breakdown Time"
                                        id="breakdownTime"
                                        name="breakdownTime"
                                        value={values.breakdownTime}
                                        onChange={(date) => {
                                            setFieldValue("breakdownTime", date)
                                        }}
                                        mask={[/\d/, /\d/, ":", /\d/, /\d/, " ", /a|p/i, "M"]}
                                        error={errors.breakdownTime && (touched.breakdownTime)}
                                        FormHelperTextProps={{
                                            error: errors.breakdownTime && (touched.breakdownTime)
                                        }}
                                        helperText={
                                            errors.breakdownTime && (touched.breakdownTime)
                                                ? errors.breakdownTime
                                                : null
                                        }
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest2}>

                                    <InputLabel htmlFor="productionStatus-simple">{TextData.productionStatus}</InputLabel>
                                    <Select fullWidth
                                        value={values.productionStatus}
                                        onChange={handleChange}
                                        inputProps={{
                                            name: 'productionStatus',
                                            id: 'productionStatus-simple',
                                        }}
                                        error={errors.productionStatus && (touched.productionStatus)}
                                        FormHelperTextProps={{
                                            error: errors.productionStatus && (touched.productionStatus)
                                        }}
                                        helperText={
                                            errors.productionStatus && (touched.productionStatus)
                                                ? errors.productionStatus
                                                : null
                                        }>
                                        <MenuItem value={0}>Select</MenuItem>
                                        <MenuItem value={1}>Stopped</MenuItem>
                                        <MenuItem value={2}>Not stopped</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <FormControl className={classes.autoSuggest2}>
                                    <TextField fullWidth
                                        id="breakdownDetails"
                                        label="Breakdown Details"
                                        multiline
                                        rows='1'
                                        value={values.breakdownDetails}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={errors.breakdownDetails && (touched.breakdownDetails)}
                                        FormHelperTextProps={{
                                            error: errors.breakdownDetails && (touched.breakdownDetails)
                                        }}
                                        helperText={
                                            errors.breakdownDetails && (touched.breakdownDetails)
                                                ? errors.breakdownDetails
                                                : null
                                        }
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <BreakdownPhenomenon breakdownPhenomenonMaster={breakdownPhenomenonMaster} {...props} />
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <EnteredBy {...props} />
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <AssignedTo {...props} />
                            </Grid>

                            <Grid item xs={12}>
                                <Button variant="contained" type="submit" color="primary" className={classes.button}>
                                    {TextData.save}
                                </Button>
                                <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                                    {TextData.reset}
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </form>
            </Fragment>
        </Scroll>
    );
});

const EnhancedReportingForm = withFormik({
    enableReinitialize: true,
    mapPropsToStatus: (props) => ({
        isDisabled: {
            breakdownNo: true
        }}),
    mapPropsToValues: ({ data, ...props }) => {
        if (props.selectedData && props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID) {
            const breakdownData = props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID.data[0]
            return ({
                breakdownNo: breakdownData.bd_srno,
                equipment: breakdownData.Eqp_ID,
                reportedby: breakdownData.reported_by ? breakdownData.reported_by.replace(/ /g, "") : null,
                breakdownDate: moment(breakdownData.bd_date), breakdownTime: moment(breakdownData.bd_time),
                productionStatus: breakdownData.prod_code ? breakdownData.prod_code.replace(/ /g, "") : null,
                breakdownDetails: breakdownData.bd_det, breakdownPhenomenon: breakdownData.Phy_Pheno_Desc,
                enteredBy: breakdownData.entered_by ? breakdownData.entered_by.replace(/ /g, "") : null,
                assignedTo: breakdownData.assign_to_id1
            })
        } else {
            return ({
                breakdownNo: '', equipment: '', reportedby: '', breakdownDate: new Date(), breakdownTime: new Date(),
                productionStatus: '', breakdownDetails: '', breakdownPhenomenon: '', enteredBy: '', assignedTo: ''
            })
        }
    },

    validationSchema: Yup.object().shape({
        equipment: Yup.string("Equipment is required and should be a String").required("Equipment Required"),
        reportedby: Yup.string("Reportedby is required and should be a String").required("Reportedby is required"),
        breakdownDetails: Yup.string("breakdownDetails is required and should be a String").required("BreakdownDetails Required")
    }),

    handleSubmit: async (values, { setSubmitting, props }) => {
        setSubmitting(true);
        if (props.type === "CREATE" && !props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID) {
            const r = await actions.createRepetitiveBreakdown({
                spName: "SP_Insert_Update_Delete_Break_Down_Maint_Output",
                payload: {
                    bd_srno: null,
                    Eqp_ID: values.equipment,
                    bd_det: values.breakdownDetails,
                    bd_date: values.breakdownDate,
                    bd_time: values.breakdownTime,
                    prod_dntime: 0,
                    agency: "",
                    coord_per: "",
                    comp_date: moment().toISOString(),
                    comp_time: moment().toISOString(),
                    notes: "",
                    area_id: "",
                    failure_id: "",
                    opportunity_loss: 2,
                    nature: "",
                    userId: "",
                    Flag: "I",
                    reported_By: values.reportedby,
                    con_cost: 0.0,
                    Line: "",
                    emp_id: "",
                    eqp_unit: "",
                    entered_by: values.enteredBy,
                    bd_approval_status: "",
                    bd_approval_date: moment().toISOString(),
                    bd_approval_name: "",
                    assign_to_id1: values.assignedTo,
                    assign_to_id2: "",
                    assign_to_id3: "",
                    assign_to_id4: "",
                    assembly_description: "",
                    sub_assembly: "",
                    Phy_Pheno_Desc: values.breakdownPhenomenon,
                    Analysis_rqrd: "",
                    prod_code: values.productionStatus
                },
                key: "REP"
            })
            await actions.snackBarHandleOpen({
                message: "Repetitive Breakdown Created."
            })
            setSubmitting(false);
        }
        if (props.type === "EDIT" || props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID) {
            const breakdownData = props.sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID.data[0]
            await props.getData({
                spName: "Sp_Insert_Update_Delete_Break_Down_Maint1",
                payload: {
                    bd_srno: values.breakdownNo,
                    Eqp_ID: values.equipment,
                    bd_det: values.breakdownDetails,
                    bd_date: values.breakdownDate,
                    bd_time: values.breakdownTime,
                    prod_dntime: breakdownData.prod_dntime,
                    agency: breakdownData.agency,
                    coord_per: breakdownData.coord_per,
                    comp_date: moment().toISOString(),
                    comp_time: moment().toISOString(),
                    notes: breakdownData.notes,
                    area_id: breakdownData.area_id,
                    failure_id: breakdownData.failure_id,
                    opportunity_loss: breakdownData.opportunity_loss,
                    nature: breakdownData.nature,
                    userId: breakdownData.userid,
                    Flag: "U",
                    reported_By: values.reportedby,
                    con_cost: breakdownData.con_cost,
                    Line: breakdownData.Line,
                    emp_id: breakdownData.emp_id,
                    eqp_unit: breakdownData.eqp_unit,
                    entered_by: values.enteredBy,
                    bd_approval_status: breakdownData.bd_approval_status,
                    bd_approval_date: moment().toISOString(),
                    bd_approval_name: breakdownData.bd_approval_name,
                    assign_to_id1: values.assignedTo,
                    assign_to_id2: breakdownData.assign_to_id2,
                    assign_to_id3: breakdownData.assign_to_id3,
                    assign_to_id4: breakdownData.assign_to_id4,
                    assembly_description: breakdownData.assembly_description,
                    sub_assembly: breakdownData.sub_assembly,
                    Phy_Pheno_Desc: values.breakdownPhenomenon,
                    Analysis_rqrd: breakdownData.Analysis_rqrd,
                    prod_code: values.productionStatus
                }
            })
           await actions.getData({
                spName: "sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID",
                payload: {
                    Eqp_ID: breakdownData.Eqp_ID,
                    bd_srno: breakdownData.bd_srno
                },
                key: "sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID"
            })

            setSubmitting(false);
            await actions.snackBarHandleOpen({
                message: "Repetitive Breakdown Updated."
            })
        }


    },

    displayName: 'ReportingForm',
})(ReportingForm)

function ContainerReportingForm(props) {
    useEffect(() => {
        var queries = [
            {
                spName: "SP_Fun_Select_All_From_Equipment_By_Plant_ID",
                payload: {},
                key: "ALL_EQUIPMENT_MASTER"
            },
            {
                spName: "sp_select_all_from_Employee_master",
                payload: {},
                key: "ALL_EMPLOYEE_MASTER"
            },
            {
                spName: "SP_Select_All_From_area_code_master",
                payload: {},
                key: "FAILURE_AREA_DATA"
            },
            {
                spName: "SP_Select_All_From_failure_code_master_By_Area_Code",
                payload: {
                    area_code_id: "Select"
                },
                key: "SP_Select_All_From_failure_code_master_By_Area_Code"
            }
        ]
        if (props.selectedData) {
            const newQueries = [...queries, {
                spName: "sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID",
                payload: {
                    Eqp_ID: props.selectedData.data.Eqp_ID,
                    bd_srno: props.selectedData.data.bd_srno
                },
                key: "sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID"
            }]
            actions.getDatas(newQueries)
        } else {
            actions.getDatas(queries)
        }
    }, [])

    return <EnhancedReportingForm {...props} />
}

export default ContainerReportingForm;