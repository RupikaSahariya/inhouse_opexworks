const TextData = {
    uploadedBy: 'Uploaded By',
    addDocument: 'Add New Breakdown Documents',
    selectEmployee: 'Select Employee',
    addManpower: 'Add New Man Power Used',
    selectSparePart: 'Select Spare Part',
    AddSparesConsumedinBreakdown: 'Add New Spare Consumed',
    save: 'Save',
    cancel: 'Cancel',

    breakdownReporting: 'Breakdown Reporting',
    department: 'Department',
    cell: 'Cell',
    equipment: 'Equipment',
    reportedBy: 'Reported By',
    productionStatus: 'Production Status',
    breakdownPhenomenon: 'Breakdown Phenomenon',
    enteredBy: 'Entered By',
    BreakdownMemo: 'Breakdown Memo',
    UploadDocuments:'Upload Documents'
}

export default TextData;