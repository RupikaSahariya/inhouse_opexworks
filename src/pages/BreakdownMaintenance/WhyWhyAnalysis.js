import React, { Fragment, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withFormik } from "formik";
import * as Yup from "yup";
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import styles from "./../BreakdownMaintenance/EquipmentwiseBreakdown/styles";
import TextData from "./../BreakdownMaintenance/EquipmentwiseBreakdown/textData";
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Scroll from "./Scroll";
import moment from "moment";
import { actions } from "../../actions";
import AutoSuggest from "./../../components/Select";
import { truncate } from 'fs';

const WhyWhyAnalysisForm = withStyles(styles)((props) => {
    const {
        values,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        status,
        handleReset,
        setFieldValue,
        errors,
        touched
    } = props

    return (
        <Fragment>
            <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container2} autoComplete="off">
                <Grid
                    container
                    item xs={12}
                    direction="row"
                    justify="space-around"
                >
                    <Grid item xs={12}>

                        <AutoSuggest className={classes.formControl5}
                            name="equipment"
                            label="Equipment"
                            error={errors.equipment && (touched.equipment)} FormHelperTextProps={{
                                error: errors.equipment && (touched.equipment)
                            }}
                            helperText={
                                errors.equipment && (touched.equipment)
                                    ? errors.equipment
                                    : null
                            }
                            isDisabled={status.isDisabled.equipment}
                            value={values.equipment}
                            placeholder={TextData.equipment}
                            options={props && props.data ? props.data.map((item, i) => ({
                                value: item.Eqp_ID,
                                label: item.eqp_desc,
                            })) : null}
                        ></AutoSuggest>
                    </Grid>

                    <Grid item xs={12} lg={12}>
                        <FormControl className={classes.formControl2}>
                            <TextField fullWidth
                                name="kaizenregno"
                                placeholder="Enter Kaizen Reg. No"
                                label="Kaizen Reg. No"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.kaizenregno}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={12}>
                        <FormControl className={classes.formControl2}>
                            <TextField fullWidth
                                id="analysisfor"
                                label="Analysis For"
                                multiline
                                rows='1'
                                value={values.analysisfor}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={12}>
                        <FormControl className={classes.formControl2}>
                            <TextField fullWidth
                                id="finalaction"
                                label="Final Action"
                                multiline
                                rows='1'
                                value={values.finalaction}
                                onChange={handleChange}
                                // onChange={(e,value)=>{setFieldValue("finalaction",value); setFieldValue("whydidyoutakeaboveaction1", value)}}
                                onBlur={() => { setFieldValue("whydidyoutakeaboveaction1", values.finalaction ? "Why " + values.finalaction : '') }}
                            />
                        </FormControl>
                    </Grid>

                    {/* <Grid item xs={12} md={1} lg={1}>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={values.checkedSpare}
                                        onChange={handleChange('checkedSpare')}
                                        value="checkedSpare"
                                        color="primary"
                                    />
                                }
                            />
                    </Grid> */}
                    <Grid item xs={12} md={12} lg={12}>
                        <FormControl className={classes.formControl2}>
                            <TextField fullWidth
                                id="sparepartreplace"
                                label="Spare Part Replace"
                                multiline
                                rows='1'
                                value={values.sparepartreplace}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={1}>
                        <Typography variant="subtitle1" className={classes.tc2}>1</Typography>
                    </Grid>

                    <Grid item xs={12} lg={5}>
                        <FormControl className={classes.formControl3}>
                            <TextField fullWidth
                                name="whydidyoutakeaboveaction1"
                                id="whydidyoutakeaboveaction1"
                                placeholder="Why Did You Take Above Action"
                                label="Why Did You Take Above Action"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.whydidyoutakeaboveaction1}
                                disabled={status.isDisabled.whydidyoutakeaboveaction1}
                                inputProps={{ style: { "color": "black" } }}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={3}>
                        <FormControl className={classes.formControl3}>
                            <TextField fullWidth
                                name="dueto1"
                                placeholder="Due To"
                                label="Due To"
                                onChange={handleChange}
                                onBlur={() => { setFieldValue("whydidyoutakeaboveaction2", values.dueto1 ? "Why " + values.dueto1 : '') }}
                                value={values.dueto1}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={3}>
                        <FormControl className={classes.formControl3}>
                            <TextField fullWidth
                                name="action1"
                                placeholder="Action"
                                label="Action"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.action1}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={1}>
                        <Typography variant="subtitle1" className={classes.tc2}>2</Typography>
                    </Grid>

                    <Grid item xs={12} lg={5}>
                        <FormControl className={classes.formControl3}>
                            <TextField fullWidth
                                name="whydidyoutakeaboveaction2"
                                placeholder="Why Did You Take Above Action"
                                label="Why Did You Take Above Action"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.whydidyoutakeaboveaction2}
                                disabled={status.isDisabled.whydidyoutakeaboveaction2}
                                inputProps={{ style: { "color": "black" } }}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={3}>
                        <FormControl className={classes.formControl3}>
                            <TextField fullWidth
                                name="dueto2"
                                placeholder="Due To"
                                label="Due To"
                                onChange={handleChange}
                                onBlur={() => { setFieldValue("whydidyoutakeaboveaction3", values.dueto2 ? "Why " + values.dueto2 : '') }}
                                value={values.dueto2}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={3}>
                        <FormControl className={classes.formControl3}>
                            <TextField fullWidth
                                name="action2"
                                placeholder="Action"
                                label="Action"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.action2}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={1}>
                        <Typography variant="subtitle1" className={classes.tc2}>3</Typography>
                    </Grid>

                    <Grid item xs={12} lg={5}>
                        <FormControl className={classes.formControl3}>
                            <TextField fullWidth
                                name="whydidyoutakeaboveaction3"
                                placeholder="Why Did You Take Above Action"
                                label="Why Did You Take Above Action"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.whydidyoutakeaboveaction3}
                                disabled={status.isDisabled.whydidyoutakeaboveaction3}
                                inputProps={{ style: { "color": "black" } }}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={3}>
                        <FormControl className={classes.formControl3}>
                            <TextField fullWidth
                                name="dueto3"
                                placeholder="Due To"
                                label="Due To"
                                onChange={handleChange}
                                onBlur={() => { setFieldValue("whydidyoutakeaboveaction4", values.dueto3 ? "Why " + values.dueto3 : "") }}
                                value={values.dueto3}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={3}>
                        <FormControl className={classes.formControl3}>
                            <TextField fullWidth
                                name="action3"
                                placeholder="Action"
                                label="Action"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.action3}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={1}>
                        <Typography variant="subtitle1" className={classes.tc2}>4</Typography>
                    </Grid>

                    <Grid item xs={12} lg={5}>
                        <FormControl className={classes.formControl3}>
                            <TextField fullWidth
                                name="whydidyoutakeaboveaction4"
                                placeholder="Why Did You Take Above Action"
                                label="Why Did You Take Above Action"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.whydidyoutakeaboveaction4}
                                disabled={status.isDisabled.whydidyoutakeaboveaction4}
                                inputProps={{ style: { "color": "black" } }}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={3}>
                        <FormControl className={classes.formControl3}>
                            <TextField fullWidth
                                name="dueto4"
                                placeholder="Due To"
                                label="Due To"
                                onChange={handleChange}
                                onBlur={() => { setFieldValue("whydidyoutakeaboveaction5", values.dueto4 ? "Why " + values.dueto4 : "") }}
                                value={values.dueto4}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={3}>
                        <FormControl className={classes.formControl3}>
                            <TextField fullWidth
                                name="action4"
                                placeholder="Action"
                                label="Action"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.action4}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={1}>
                        <Typography variant="subtitle1" className={classes.tc2}>5</Typography>
                    </Grid>

                    <Grid item xs={12} lg={5}>
                        <FormControl className={classes.formControl3}>
                            <TextField fullWidth
                                name="whydidyoutakeaboveaction5"
                                placeholder="Why Did You Take Above Action"
                                label="Why Did You Take Above Action"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.whydidyoutakeaboveaction5}
                                disabled={status.isDisabled.whydidyoutakeaboveaction5}
                                inputProps={{ style: { "color": "black" } }}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={3}>
                        <FormControl className={classes.formControl3}>
                            <TextField fullWidth
                                name="dueto5"
                                placeholder="Due To"
                                label="Due To"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.dueto5}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} lg={3}>
                        <FormControl className={classes.formControl3}>
                            <TextField fullWidth
                                name="action5"
                                placeholder="Action"
                                label="Action"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.action5}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8} lg={10} container direction="row" style={{ "marginTop": "30px", "marginBottom": "30px", "paddingLeft": "50px" }}>
                        <Typography variant="subtitle1">* Root causes is one of the five items</Typography>
                    </Grid>
                    <Grid item xs={12} md={4} lg={2} style={{ "marginTop": "30px", "paddingRight": "50px" }}>
                        <Typography variant="subtitle1">PM</Typography>
                    </Grid>

                    <Grid item xs={12} md={8} lg={10} container direction="row" style={{ "paddingLeft": "100px" }}>
                        <Typography variant="caption">( 1 ) poor Basic Condition</Typography>
                    </Grid>
                    <Grid item xs={12} md={4} lg={2}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={values.PM1}
                                    onChange={handleChange('PM1')}
                                    name="PM1"
                                    id="PM1"
                                    color="primary"
                                />
                            }
                        />
                    </Grid>

                    <Grid item xs={12} md={8} lg={10} container direction="row" style={{ "paddingLeft": "100px" }}>
                        <Typography variant="caption">( 2 ) Poor Operation Condition</Typography>
                    </Grid>
                    <Grid item xs={12} md={4} lg={2}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={values.PM2}
                                    onChange={handleChange('PM2')}
                                    name="PM2"
                                    id="PM2"
                                    color="primary"
                                />
                            }
                        />
                    </Grid>

                    <Grid item xs={12} md={8} lg={10} container direction="row" style={{ "paddingLeft": "100px" }}>
                        <Typography variant="caption">( 3 ) Detarioration</Typography>
                    </Grid>
                    <Grid item xs={12} md={4} lg={2}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={values.PM3}
                                    onChange={handleChange('PM3')}
                                    name="PM3"
                                    id="PM3"
                                    color="primary"
                                />
                            }
                        />
                    </Grid>

                    <Grid item xs={12} md={8} lg={10} container direction="row" style={{ "paddingLeft": "100px" }}>
                        <Typography variant="caption">( 4 ) Weak Design</Typography>
                    </Grid>
                    <Grid item xs={12} md={4} lg={2}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={values.PM4}
                                    onChange={handleChange('PM4')}
                                    name="PM4"
                                    id="PM4"
                                    color="primary"
                                />
                            }
                        />
                    </Grid>

                    <Grid item xs={12} md={8} lg={10} container direction="row" style={{ "paddingLeft": "100px" }}>
                        <Typography variant="caption">( 5 ) Poor Skills</Typography>
                    </Grid>
                    <Grid item xs={12} md={4} lg={2}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={values.PM5}
                                    onChange={handleChange('PM5')}
                                    name="PM5"
                                    id="PM5"
                                    color="primary"
                                />
                            }
                        />
                    </Grid>

                    <Grid item xs={12} md={12} lg={12} container direction="row" style={{ "marginTop": "30px", "marginBottom": "30px", "paddingLeft": "50px" }}>
                        <Typography variant="subtitle2">(Note : Please fill up form in pencil immediatly after m/c is started)</Typography>
                    </Grid>

                    <Grid item xs={12} md={12} lg={12}>
                        <Button variant="contained" type="submit" color="primary" className={classes.button}>
                            {TextData.save}
                        </Button>
                        <Button type="reset" variant="contained" color="secondary" className={classes.button}>
                            {TextData.reset}
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </Fragment>
    );
});

const EnhancedWhyWhyAnalysisForm = withFormik({
    enableReinitialize: true,
    mapPropsToStatus: (props) => ({
        isDisabled: {
            equipment: true,
            whydidyoutakeaboveaction1: true,
            whydidyoutakeaboveaction2: true,
            whydidyoutakeaboveaction3: true,
            whydidyoutakeaboveaction4: true,
            whydidyoutakeaboveaction5: true,
        }
    }),
    mapPropsToValues: (props) => {
        console.log("Props on whywhy: ", props)
        const { whywhyanalysisData } = props
        if (!whywhyanalysisData || (whywhyanalysisData && whywhyanalysisData.data && whywhyanalysisData.data.length === 0)) {
            return ({
                equipment: props.selectedEquipment, kaizenregno: '', analysisfor: '', finalaction: '', sparepartreplace: '', whydidyoutakeaboveaction1: '',
                dueto1: '', action1: '', whydidyoutakeaboveaction2: '', dueto2: '', action2: '', whydidyoutakeaboveaction3: '', dueto3: '', action3: '',
                whydidyoutakeaboveaction4: '', dueto4: '', action4: '', whydidyoutakeaboveaction5: '', dueto5: '', action5: '', PM1: '',
                PM2: '', PM3: '', PM4: '', PM5: ''
            })
        }
        return ({
            equipment: whywhyanalysisData.data[0].Eqp_ID,
            kaizenregno: whywhyanalysisData.data[0].kaizen_reg_number,
            analysisfor: whywhyanalysisData.data[0].Breakdown_Physical_Phenomenon,
            finalaction: whywhyanalysisData.data[0].Final_Action,
            sparepartreplace: whywhyanalysisData.data[0].Spare_Part_Replacement,
            whydidyoutakeaboveaction1: whywhyanalysisData.data[0].Ans1,
            dueto1: whywhyanalysisData.data[0].Why1,
            action1: whywhyanalysisData.data[0].Action1,
            whydidyoutakeaboveaction2: whywhyanalysisData.data[0].Ans2,
            dueto2: whywhyanalysisData.data[0].Why2,
            action2: whywhyanalysisData.data[0].Action2,
            whydidyoutakeaboveaction3: whywhyanalysisData.data[0].Ans3,
            dueto3: whywhyanalysisData.data[0].Why3,
            action3: whywhyanalysisData.data[0].Action3,
            whydidyoutakeaboveaction4: whywhyanalysisData.data[0].Ans3,
            dueto4: whywhyanalysisData.data[0].Why4,
            action4: whywhyanalysisData.data[0].Action4,
            whydidyoutakeaboveaction5: whywhyanalysisData.data[0].Ans4,
            dueto5: whywhyanalysisData.data[0].Why5,
            action5: whywhyanalysisData.data[0].Action5,
            PM1: whywhyanalysisData.data[0].PM1,
            PM2: whywhyanalysisData.data[0].PM2,
            PM3: whywhyanalysisData.data[0].PM3,
            PM4: whywhyanalysisData.data[0].PM4,
            PM5: whywhyanalysisData.data[0].PM5
        })
    },
    validationSchema: Yup.object().shape({
        // equipment: Yup.string("Equipment is required and should be a String").required("Equipment Required")
    }),

    handleSubmit: async (values, { setSubmitting, props }) => {
        setSubmitting(true);
        console.log("props.whywhyanalysisData", props.whywhyanalysisData)
        console.log("Local Values", values)

        if (props.whywhyanalysisData.data.length > 0) {
            console.log("Updating Why Why Record", values)
            actions.getData({
                spName: "sp_Insert_Update_Delete_BDM_Why_Why_Analysis",
                payload: {
                    Registration_No: props.whywhyanalysisData.data[0].Registration_No,
                    Que_No: 0,
                    comp_code: 0,
                    Eqp_ID: values.equipment,
                    Breakdown_Physical_Phenomenon: values.analysisfor,
                    Final_Action: values.finalaction,
                    Spare_Part_Replacement: values.sparepartreplace,
                    Countermeasure_Details: '',
                    Why1: values.whydidyoutakeaboveaction1,
                    Why2: values.whydidyoutakeaboveaction2,
                    Why3: values.whydidyoutakeaboveaction3,
                    Why4: values.whydidyoutakeaboveaction4,
                    Why5: values.whydidyoutakeaboveaction5,
                    Ans1: values.Action1,
                    Ans2: values.Action2,
                    Ans3: values.Action3,
                    Ans4: values.Action4,
                    Ans5: values.Action5,
                    Action1: values.dueto1,
                    Action2: values.dueto2,
                    Action3: values.dueto3,
                    Action4: values.dueto4,
                    Action5: values.dueto5,
                    Root_Causes1: props.whywhyanalysisData.data[0].Root_Causes1,
                    Root_Causes2: props.whywhyanalysisData.data[0].Root_Causes2,
                    Root_Causes3: props.whywhyanalysisData.data[0].Root_Causes3,
                    Root_Causes4: props.whywhyanalysisData.data[0].Root_Causes4,
                    Root_Causes5: props.whywhyanalysisData.data[0].Root_Causes5,
                    JH1: props.whywhyanalysisData.data[0].JH1,
                    JH2: props.whywhyanalysisData.data[0].JH2,
                    JH3: props.whywhyanalysisData.data[0].JH3,
                    JH4: props.whywhyanalysisData.data[0].JH4,
                    JH5: props.whywhyanalysisData.data[0].JH5,
                    PM1: values.PM1,
                    PM2: values.PM2,
                    PM3: values.PM3,
                    PM4: values.PM4,
                    PM5: values.PM5,
                    Design1: props.whywhyanalysisData.data[0].Design1,
                    Design2: props.whywhyanalysisData.data[0].Design2,
                    Design3: props.whywhyanalysisData.data[0].Design3,
                    Design4: props.whywhyanalysisData.data[0].Design4,
                    Design5: props.whywhyanalysisData.data[0].Design5,
                    ET_Skill1: props.whywhyanalysisData.data[0].ET_Skill1,
                    ET_Skill2: props.whywhyanalysisData.data[0].ET_Skill2,
                    ET_Skill3: props.whywhyanalysisData.data[0].ET_Skill3,
                    ET_Skill4: props.whywhyanalysisData.data[0].ET_Skill4,
                    ET_Skill5: props.whywhyanalysisData.data[0].ET_Skill5,
                    Kaizen_Idea: null,
                    Schedule_Date: moment().toISOString(),
                    Login_Name: null,
                    Login_DateTime: moment().toISOString(),
                    bd_srno: props.bd_srno,
                    kaizen_reg_number: values.kaizenregno,
                    Flag: 'U',
                    wwa_sr_no: null
                }
            })
        }
        else {
            console.log("Creating Why Why Record", values)
            actions.getData({
                spName: "sp_Insert_Update_Delete_BDM_Why_Why_Analysis",
                payload: {
                    Registration_No: '',
                    Que_No: 0,
                    comp_code: 0,
                    Eqp_ID: values.equipment,
                    Breakdown_Physical_Phenomenon: values.analysisfor,
                    Final_Action: values.finalaction,
                    Spare_Part_Replacement: values.sparepartreplace,
                    Countermeasure_Details: '',
                    Why1: values.whydidyoutakeaboveaction1,
                    Why2: values.whydidyoutakeaboveaction2,
                    Why3: values.whydidyoutakeaboveaction3,
                    Why4: values.whydidyoutakeaboveaction4,
                    Why5: values.whydidyoutakeaboveaction5,
                    Ans1: values.Action1,
                    Ans2: values.Action2,
                    Ans3: values.Action3,
                    Ans4: values.Action4,
                    Ans5: values.Action5,
                    Action1: values.dueto1,
                    Action2: values.dueto2,
                    Action3: values.dueto3,
                    Action4: values.dueto4,
                    Action5: values.dueto5,
                    Root_Causes1: '',
                    Root_Causes2: '',
                    Root_Causes3: '',
                    Root_Causes4: '',
                    Root_Causes5: '',
                    JH1: '',
                    JH2: '',
                    JH3: '',
                    JH4: '',
                    JH5: '',
                    PM1: values.PM1,
                    PM2: values.PM2,
                    PM3: values.PM3,
                    PM4: values.PM4,
                    PM5: values.PM5,
                    Design1: '',
                    Design2: '',
                    Design3: '',
                    Design4: '',
                    Design5: '',
                    ET_Skill1: '',
                    ET_Skill2: '',
                    ET_Skill3: '',
                    ET_Skill4: '',
                    ET_Skill5: '',
                    Kaizen_Idea: null,
                    Schedule_Date: moment().toISOString(),
                    Login_Name: null,
                    Login_DateTime: moment().toISOString(),
                    bd_srno: props.bd_srno,
                    kaizen_reg_number: values.kaizenregno,
                    Flag: 'I',
                    wwa_sr_no: null
                }
            })
        }

        setSubmitting(false);
    },

    displayName: 'WhyWhyAnalysisForm',
})(WhyWhyAnalysisForm)

const WhyWhyAnalysisPage = withStyles(styles)((props) => {
    console.log(props)
    const { classes } = props
    return (
        <Fragment>
            <Card className={classes.card}>
                <Scroll>
                    <EnhancedWhyWhyAnalysisForm {...props} />
                </Scroll>
            </Card>
        </Fragment >)
})

export default WhyWhyAnalysisPage;