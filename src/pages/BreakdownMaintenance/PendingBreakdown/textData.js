const TextData = {
    equipment: 'Equipment',
    reportedBy: 'Reported By',
    productionStatus: 'Production Status',
    breakdownPhenomenon: 'Breakdown Phenomenon',
    enteredBy: 'Entered By',
    assignedTo: 'Assigned To',
    allocation: 'Allocation',
    coordinator: 'Co-Ordinator',
    assignedTo1: 'Assigned To 1',
    assignedTo2: 'Assigned To 2',
    affectedParts: 'Affected Parts',
    breakdownAnalysis: 'Breakdown Analysis',
    nature: 'Nature',
    failureArea: 'Failure Area',
    failureType: 'Failure Type',
    rootCauseAnalysisRequired: 'Root Cause Analysis Required',
    breakdownApproval: 'Breakdown Approval',
    approvalForBreakdown: 'Approval For Breakdown',
    approvedBy: 'Approved By',
    pendingBreakdown: 'Pending Breakdown',
    save: 'Save',
    cancel: 'Cancel',
    reset: 'Reset',
    documents: 'Documents',
    manpower: 'Add Man Power',
    addSparesUsed: 'Add Spares Used'
}

export default TextData