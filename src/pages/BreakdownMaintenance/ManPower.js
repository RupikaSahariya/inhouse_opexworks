import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withFormik } from "formik";
import * as Yup from "yup";
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import AgTable from "./../../components/AgTable";
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import RefreshIcon from "@material-ui/icons/RefreshOutlined";
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { compose } from "recompose";
import { Consumer, actions } from "./../../actions";
import styles from "./styles1";
import TextData from "./textData1";
import AutoSuggest from "./../../components/Select";
import Typography from '@material-ui/core/Typography';

const SelectedEmployee = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props;
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest2}
                name="selectedEmployee"
                label="Select Employee"
                error={errors.selectedEmployee && (touched.selectedEmployee)} FormHelperTextProps={{
                    error: errors.selectedEmployee && (touched.selectedEmployee)
                }}
                helperText={
                    errors.selectedEmployee && (touched.selectedEmployee)
                        ? errors.selectedEmployee
                        : null
                }
                value={values.selectedEmployee}
                placeholder={TextData.selectedEmployee}
                options={props.ALL_EMPLOYEE_MASTER ? props.ALL_EMPLOYEE_MASTER.data.map((item, i) => {
                    return ({
                        value: item.emp_id,
                        label: item.emp_name,
                    })
                }) : []}
                onChange={(value) => {
                    setFieldValue("selectedEmployee", value)
                }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})

const ManpowerForm = withStyles(styles)((props) => {
    const {
        values,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        errors,
        touched,
    } = props
    return (
        <Fragment>

            <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid item xs={12}>
                        <SelectedEmployee disabled={props.payload} {...props} />
                    </Grid>

                    <Grid item xs={12}>
                        <FormControl className={classes.autoSuggest}>
                            <TextField fullWidth
                                name="hoursUsed"
                                placeholder="Enter Hours Used"
                                label="Hours Used"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                error={errors.hoursUsed && (touched.hoursUsed)}
                                FormHelperTextProps={{
                                    error: errors.hoursUsed && (touched.hoursUsed)
                                }}
                                helperText={
                                    errors.hoursUsed && (touched.hoursUsed)
                                        ? errors.hoursUsed
                                        : null
                                }
                                value={values.hoursUsed}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12}>
                        <FormControl className={classes.autoSuggest}>
                            <TextField fullWidth
                                name="cost"
                                placeholder="Enter Cost"
                                label="Cost"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.cost}
                                error={errors.cost && (touched.cost)}
                                FormHelperTextProps={{
                                    error: errors.cost && (touched.cost)
                                }}
                                helperText={
                                    errors.cost && (touched.cost)
                                        ? errors.cost
                                        : null
                                }
                            />
                        </FormControl>
                    </Grid>

                    <Grid
                        item xs={12}
                        justify="center"
                        className={classes.button}
                    >
                        <Button variant="contained" type="submit" color="primary" style={{ marginLeft: "10px" }}>
                            {TextData.save}
                        </Button>
                        <Button type="button" variant="contained" color="secondary" onClick={() => actions.closeManpower({
                            page: 'PENDING_BREAKDOWN_MANPOWER_DIALOG'
                        })} style={{ marginLeft: "10px" }}>
                            {TextData.cancel}
                        </Button>
                    </Grid>

                </Grid>
            </form>
        </Fragment>
    )
});

const EnhancedManpowerForm = withFormik({
    mapPropsToValues: (props) => {
        const { payload } = props
        if (!payload) {
            return ({ selectedEmployee: '', hoursUsed: '', cost: '' })
        }
        return ({ selectedEmployee: payload.data.emp_id, hoursUsed: payload.hoursUsed, cost: payload.cost })
    },
    validationSchema: Yup.object().shape({
        cost: Yup.number("Should be a Numerical Value").required("Required").typeError("Should be a Numerical Value").min(1, "Should be more than 0"),
        hoursUsed: Yup.number("Should be a Numerical Value").typeError("Should be a Numerical Value").required("Required").min(1, "Should be more than 0"),
        selectedEmployee: Yup.string("Employee is required and should be a String").required("SelectedEmployee Required"),
    }),

    handleSubmit: async (values, { props, setSubmitting }) => {
        setSubmitting(true);
        await actions.getData({
            spName: "SP_Insert_Update_Delete_Resource_Used",
            payload: {
                Eqp_ID: props.manpowerData && props.manpowerData[4] && props.manpowerData[4][0]? props.manpowerData[4][0].Eqp_ID:null,
                BD_Srno: props.manpowerData && props.manpowerData[4] && props.manpowerData[4][0]? props.manpowerData[4][0].bd_srno:null,
                Emp_id: values.selectedEmployee,
                Hrs_used: values.hoursUsed,
                Cost: values.cost,
                Flag: props.payload ? "U" : "I"
            },
            key: "SP_Insert_Update_Delete_Resource_Used" + props.manpowerData && props.manpowerData[4] && props.manpowerData[4][0]? props.manpowerData[4][0].Eqp_ID:null + values.selectedEmployee
        })
        await actions.getData({
            spName: "SP_Select_All_From_Resource_Used_By_Eqp_ID_And_Br_SrNo",
            payload: {
                Eqp_ID: props.manpowerData && props.manpowerData[4] && props.manpowerData[4][0]? props.manpowerData[4][0].Eqp_ID:null,
                BD_Srno: props.manpowerData && props.manpowerData[4] && props.manpowerData[4][0]? props.manpowerData[4][0].bd_srno:null
            },
            key: "PENDING_BREAKDOWN_MANPOWER_DATA"
        })
        actions.snackBarHandleOpen({
            message: props.payload ? "Record Updated" : "Record Created"
        })
        await actions.closeManpower({ page: 'PENDING_BREAKDOWN_MANPOWER_DIALOG' })
    },

    displayName: 'ManpowerForm',
})(ManpowerForm)

class ManpowerPage extends React.PureComponent {
    state = {
        // dialogOpen: false
    }

    handleClickOpen = () => {
        // this.setState({ dialogOpen: true });
    };

    handleClose = () => {
        // this.setState({ dialogOpen: false });
        actions.closeManpower({ page: 'PENDING_BREAKDOWN_MANPOWER_DIALOG' })
    };

    async componentDidMount() {
        console.log(this.props)
        actions.getDatas([{
            spName: "sp_select_all_from_Employee_master",
            payload: {},
            key: "ALL_EMPLOYEE_MASTER"
        }, {
            spName: "SP_Select_All_From_Resource_Used_By_Eqp_ID_And_Br_SrNo",
            payload: {
                Eqp_ID: this.props.manpowerData && this.props.manpowerData[4] && this.props.manpowerData[4][0] ? this.props.manpowerData[4][0].Eqp_ID : null,
                BD_Srno: this.props.manpowerData && this.props.manpowerData[4] && this.props.manpowerData[4][0] ? this.props.manpowerData[4][0].bd_srno : null
            },
            key: 'PENDING_BREAKDOWN_MANPOWER_DATA'
        }]);

    }

    onGridReady = (params) => {
        params.api.sizeColumnsToFit()
        this.gridParams = params
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

    exportData = () => {
        this.gridApi.exportDataAsCsv(this.gridParams);
    }

    onEdit = (selectedData) => {
        actions.handleManpowerDialog({
            page: "PENDING_BREAKDOWN_MANPOWER_DIALOG",
            payload: selectedData
        })
    }

    onDelete = async (selectedData) => {
        await actions.getData({
            spName: "SP_Insert_Update_Delete_Resource_Used",
            payload: {
                Eqp_ID: selectedData.data.Eqp_ID,
                BD_Srno: selectedData.data.bd_srno,
                Emp_id: selectedData.data.emp_id,
                Hrs_used: selectedData.data.hrs_used,
                Cost: selectedData.cost,
                Flag: "D"
            },
            key: "SP_Insert_Update_Delete_Resource_Used" + "D" + this.props.manpowerData.Eqp_ID + "" + selectedData.data.emp_id
        })
        actions.snackBarHandleOpen({
            message: "Record deleted"
        })
        await actions.getData({
            spName: "SP_Select_All_From_Resource_Used_By_Eqp_ID_And_Br_SrNo",
            payload: {
                Eqp_ID: selectedData.data? selectedData.data.Eqp_ID:null,
                BD_Srno: selectedData.data? selectedData.data.bd_srno:null
            },
            key: "PENDING_BREAKDOWN_MANPOWER_DATA"
        })
    }

    handleRefresh = async (selectedData) => {
        await actions.getData({
            spName: "SP_Select_All_From_Resource_Used_By_Eqp_ID_And_Br_SrNo",
            payload: {
                Eqp_ID: this.props.manpowerData.Eqp_ID,
                BD_Srno: this.props.manpowerData.bd_srno
            },
            key: "PENDING_BREAKDOWN_MANPOWER_DATA"
        })
    }

    render() {
        const columnDefs = [{
            headerName: "Sr.No.", field: "srno", sortable: true, filter: true, width: 45,
            cellRenderer: "toolTipRenderer", toolTipText: "srno", cellClass: "centered-content"
        },
        {
            headerName: "Employee", field: "employee", sortable: true, filter: true, width: 90,
            cellRenderer: "toolTipRenderer", toolTipText: "employee", cellClass: "left-aligned-content"
        },
        {
            headerName: "Category", field: "category", sortable: true, filter: true, width: 70,
            cellRenderer: "toolTipRenderer", toolTipText: "category", cellClass: "left-aligned-content"
        },
        {
            headerName: "Hourly Salary", field: "hourlySalary", sortable: true, filter: true, width: 50,
            cellRenderer: "toolTipRenderer", toolTipText: "hourlySalary", cellClass: "centered-content"
        },
        {
            headerName: "Hours Used", field: "hoursUsed", sortable: true, filter: true, width: 50,
            cellRenderer: "toolTipRenderer", toolTipText: "hoursUsed", cellClass: "centered-content"
        },
        {
            headerName: "Cost", field: "cost", sortable: true, filter: true, width: 50,
            cellRenderer: "toolTipRenderer", toolTipText: "cost", cellClass: "right-aligned-content"
        },
        {
            headerName: "", width: 50, cellRenderer: "editRenderer"
        }
        ]

        const { classes } = this.props
        console.log(this.props)
        return (
            <Fragment>
                <Consumer>{React.memo(({ ALL_EMPLOYEE_MASTER, PENDING_BREAKDOWN_MANPOWER_DIALOG }) => (<Dialog
                    disableBackdropClick
                    className={classes.dialogStyles}
                    open={PENDING_BREAKDOWN_MANPOWER_DIALOG.isOpen}
                    handleClose={() => actions.closeManpower({
                        page: 'PENDING_BREAKDOWN_MANPOWER_DIALOG'
                    })}
                    aria-labelledby="form-dialog-title"
                    maxWidth="sm"
                >
                    <DialogTitle id="form-dialog-title">{TextData.addManpower}</DialogTitle>
                    <DialogContent>
                        <EnhancedManpowerForm {...this.props} payload={PENDING_BREAKDOWN_MANPOWER_DIALOG.payload} ALL_EMPLOYEE_MASTER={ALL_EMPLOYEE_MASTER} handleClose={this.handleClose} />
                    </DialogContent>
                </Dialog>
                ))}</Consumer>

                <Grid
                    container
                    direction="row"
                >
                    <div className={classes.toolbar}>
                        <Grid item xs={12} direction="row" className={classes.gridLabel}>
                            <Typography variant="subtitle2">
                                Equipment : {this.props.manpowerData.eqp_desc}
                            {/* </Typography>
                            <Typography variant="subtitle2"> */}
                            &nbsp; &nbsp;   |   &nbsp;&nbsp;
                                Nature of Breakdown : {this.props.manpowerData.breakdownData[0].bd_det}
                            </Typography>
                        </Grid>
                        <Tooltip title="Add New Man Power Used">
                            <Fab size={"small"} color="primary" variant="extended" onClick={() => actions.handleManpowerDialog({
                                page: "PENDING_BREAKDOWN_MANPOWER_DIALOG",
                                payload: null
                            })} className={classes.toolbarIcon1}>
                                <AddIcon />&nbsp;
                        </Fab>
                        </Tooltip>
                        <Tooltip title="Refresh">
                            <Fab size={"small"} color="primary" variant="extended" onClick={this.handleRefresh} className={classes.toolbarIcon2}>
                                <RefreshIcon />&nbsp;
                        </Fab>
                        </Tooltip>
                    </div>
                </Grid>

                <Paper style={{ height: window.innerHeight - 160, paddingTop: '10px' }}>
                    <Consumer>
                        {({ PENDING_BREAKDOWN_MANPOWER_DATA }) => {
                            if (!PENDING_BREAKDOWN_MANPOWER_DATA) {
                                return <div style={{ height: window.innerHeight - 250, width: '100%', paddingTop: '15%' }} ><CircularProgress /></div>
                            }
                            const rowData = PENDING_BREAKDOWN_MANPOWER_DATA.data ? PENDING_BREAKDOWN_MANPOWER_DATA.data.map((item, i) => ({
                                data: item,
                                srno: item.bd_srno, employee: item.emp_name, category: item.category, hourlySalary: item.hourly_salary, hoursUsed: item.hrs_used, cost: item.cost
                            })) : []
                            return (
                                <React.Fragment>{PENDING_BREAKDOWN_MANPOWER_DATA ? <AgTable
                                    loading={this.props.loading}
                                    onGridReady={this.onGridReady}
                                    columnDefs={columnDefs}
                                    rowData={rowData}
                                    onEdit={this.onEdit}
                                    onDelete={this.onDelete}
                                /> : null
                                }
                                </React.Fragment>
                            )
                        }}
                    </Consumer>
                </Paper>
            </Fragment >)
    }
}

export default compose(withStyles(styles))(ManpowerPage)