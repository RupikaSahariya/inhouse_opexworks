import React, { Fragment, useState, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import { withFormik } from "formik";
import * as Yup from "yup";
import { DatePicker, TimePicker } from 'material-ui-pickers';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import { dataProvider } from '../../utils/AppContext';
import { compose } from 'react-apollo';
import { Consumer, actions, connect } from "./../../actions";
import TextData from "./textData1";
import DocumentsPage from "./Documents";
import AppBar from '@material-ui/core/AppBar';
import Dialog from '@material-ui/core/Dialog';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import AutoSuggest from "./../../components/Select";
import Scroll from './Scroll';

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

//your styles here
const styles = theme => ({
    formControl: {
        margin: theme.spacing.unit * 2,
        maxWidth: 400,
        width: "100%",
        textAlign: "left"
    },
    Grid: {
        padding: "10px"
    },
    card: {
        maxWidth: "92%",
        width: "100%",
        padding: 30,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20,
        textAlign: "center"
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        paddingBottom: "20px",
        paddingTop: "10px",
        justify: "space-around"
    },
    datepicker: {
        width: "100%",
        maxWidth: 400
    },
    button: {
        marginLeft: theme.spacing.unit * 2,
        marginTop: theme.spacing.unit * 2
    },
    alignHeader: {
        flex: '1 1 0%'
    },
    autoSuggest: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit,
        maxWidth: 470,
        width: "100%",
        textAlign: 'left',
        display: "flex",
        flexWrap: 'wrap'
    },
    autoSuggest2: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit,
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        maxWidth: 470,
        width: "100%",
        textAlign: 'left',
        display: "flex",
        flexWrap: 'wrap'
    },
});

const Departments = ((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        classes,
        setFieldValue,
        setValues
    } = props;
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest}
                name="department"
                id="department"
                label="Department"
                value={values.department}
                placeholder={TextData.department}
                options={props.data[1] ? props.data[1].map((item, i) => {
                    return ({
                        value: item.dept_id,
                        label: item.Department_Name,
                    })
                }) : []}
                onChange={(value) => {
                    setValues({ department: value, cell: null, equipment: null, breakdownPhenomenon: null })
                }}
                textFieldProps={{
                    helperText: errors.department && (touched.department)
                        ? errors.department
                        : null,
                    error: errors.department && (touched.department),
                }}
                value={values.department}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})

const Cell = ((props) => {
    const {
        values,
        handleChange,
        classes,
        errors,
        touched,
        cellMaster,
        setFieldValue,
        setValues
    } = props
    // console.log("cellMaster", cellMaster)
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest}
                name="cell"
                label="Cell"
                value={values.cell}
                placeholder={TextData.cell}
                isDisabled={cellMaster === undefined || cellMaster.length == 0}
                options={cellMaster ? cellMaster.map((item, i) => ({
                    value: item.Cell_ID,
                    label: item.Cell_Name,
                })) : []}
                onChange={(value) => {
                    setValues({ ...values, cell: value, equipment: null, breakdownPhenomenon: null })
                }}
                textFieldProps={{
                    helperText: errors.cell && (touched.cell)
                        ? errors.cell
                        : null,
                    error: errors.cell && (touched.cell),
                }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})
const Equipments = ((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        classes,
        equipmentMaster,
        setFieldValue,
        setValues
    } = props
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest}
                name="equipment"
                label="Equipment"
                value={values.equipment}
                placeholder={TextData.equipment}
                isDisabled={equipmentMaster === undefined || equipmentMaster.length == 0}
                options={equipmentMaster ? equipmentMaster.map((item, i) => ({
                    value: item.Eqp_ID,
                    label: item.Eqp_ID,
                })) : []}
                onChange={(value) => {
                    setValues({ ...values, equipment: value, breakdownPhenomenon: null })
                }}
                textFieldProps={{
                    helperText: errors.equipment && (touched.equipment)
                        ? errors.equipment
                        : null,
                    error: errors.equipment && (touched.equipment),
                }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})

const ReportedBy = ((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        classes,
        setFieldValue
    } = props;
    return (

        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest}
                name="reportedby"
                label="Reported By"
                value={values.reportedby}
                placeholder={TextData.reportedby}
                // isDisabled={equipmentMaster === undefined || equipmentMaster.length == 0}
                options={props.data[2] ? props.data[2].map((item, i) => ({
                    value: item.emp_id,
                    label: item.emp_name,
                })) : []}
                onChange={(value) => {
                    setFieldValue("reportedby", value)
                }}
                textFieldProps={{
                    helperText: errors.reportedby && (touched.reportedby)
                        ? errors.reportedby
                        : null,
                    error: errors.reportedby && (touched.reportedby),
                }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})

const BreakdownPhenomenon = ((props) => {
    const {
        values,
        handleChange,
        classes,
        errors,
        touched,
        breakdownPhenomenonMaster,
        setFieldValue
    } = props
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest}
                name="breakdownPhenomenon"
                label="Breakdown Phenomenon"
                error={errors.breakdownPhenomenon && (touched.breakdownPhenomenon)} FormHelperTextProps={{
                    error: errors.breakdownPhenomenon && (touched.breakdownPhenomenon)
                }}
                helperText={
                    errors.breakdownPhenomenon && (touched.breakdownPhenomenon)
                        ? errors.breakdownPhenomenon
                        : null
                }
                value={values.breakdownPhenomenon}
                placeholder={TextData.breakdownPhenomenon}
                isDisabled={breakdownPhenomenonMaster === undefined || breakdownPhenomenonMaster.length == 0}
                options={breakdownPhenomenonMaster ? breakdownPhenomenonMaster.map((item, i) => ({
                    value: item.sr_no,
                    label: item.Phy_Pheno_Desc,
                })) : []}
                onChange={(value) => {
                    setFieldValue("breakdownPhenomenon", value)
                }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})

const EnteredBy = ((props) => {
    const {
        values,
        handleChange,
        classes,
        errors,
        touched,
        setFieldValue

    } = props
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest}
                name="enteredBy"
                label="Entered By"
                error={errors.enteredBy && (touched.enteredBy)} FormHelperTextProps={{
                    error: errors.enteredBy && (touched.enteredBy)
                }}
                helperText={
                    errors.enteredBy && (touched.enteredBy)
                        ? errors.enteredBy
                        : null
                }
                value={values.enteredBy}
                placeholder={TextData.enteredBy}
                // disabled={equipmentMaster === undefined || equipmentMaster.length == 0}
                options={props.data[2] ? props.data[2].map((item, i) => ({
                    value: item.emp_id,
                    label: item.emp_name,
                })) : []}
                onChange={(value) => {
                    setFieldValue("enteredBy", value)
                }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})

const BreakdownReportingForm = withStyles(styles)((props) => {
    console.log(props)
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        handleClose,
        setFieldValue,
        getData,
    } = props;

    const [cellMaster, setCellMaster] = useState([]);
    useEffect(() => {
        async function setCellMasterData() {
            const data = await Promise.all([
                getData({
                    spName: "SP_Select_All_From_Cells_by_dept_id",
                    payload: {
                        dept_id: values.department
                    }
                })
            ])
            if (data && data.length > 0) {
                setCellMaster(data[0].data)
            }
        }
        setCellMasterData()
    }, [props.values.department])

    const handleBreakdownMemo = async () => {

        const checkMemo = await props.getData({
            spName: "SP_select_pending_breakdown_count",
            payload: {
                eqp_code: props.values.equipment
            }
        })

        //******    THIS CODE MIGHT BE NEEDED FURTHER AS PER CLIENT'S REQUIREMENT!! PLEASE DO NOT DELETE IT */

        // if (checkMemo) {
        //     await actions.snackBarHandleOpen({
        //         message: "Breadkdowns are pendings for approvals. \nCan't add any breadkdown for this equipment unless all the breakdowns are approved."
        //     })
        // }
        // else{
        // async const checkMemo = await Promise.all([
        actions.downloadReport({
            spName: "SP_rpt_Select_data_for_breakdown_memo",
            payload: {
                bd_srno: props.PENDING_BREAKDOWN_ONCREATE_DATA.bd_srno
            }
        })
        // ])
        // }
    };
    const handleUploadDocument = async () => {
        console.log('bdrepppp', props)
        if (props.PENDING_BREAKDOWN_ONCREATE_DATA) {
            let eqp_desc = props.data[0].eqp_desc;
            const breakdownData = props.PENDING_BREAKDOWN_ONCREATE_DATA;
            props.data[0].forEach(function (eqp) {
                if (eqp.Eqp_ID == props.PENDING_BREAKDOWN_ONCREATE_DATA.Eqp_ID) {
                    eqp_desc = eqp.eqp_desc;
                }
            });
            actions.handleDocumentDialog({ page: "REPORTING_BREAKDOWN_DOCUMENT_POPUP", payload: { "4": [props.PENDING_BREAKDOWN_ONCREATE_DATA], breakdownData: [breakdownData], eqp_desc } }) //props.PENDING_BREAKDOWN_ONCREATE_DATA
        }
    };

    const [equipmentMaster, setequipmentMaster] = useState([]);
    useEffect(() => {
        async function setequipmentMasterData() {
            const data = await Promise.all([
                getData({
                    spName: "SP_Select_All_From_Equipments_by_dept_id_cell_id",
                    payload: {
                        dept_id: values.department,
                        cell_id: values.cell
                    }
                })
            ])
            if (data && data.length > 0) {
                setequipmentMaster(data[0].data)
            }
        }
        setequipmentMasterData()
    }, [props.values.cell])

    const [breakdownPhenomenonMaster, setbreakdownPhenomenonMaster] = useState([]);
    useEffect(() => {
        async function setbreakdownPhenomenonMasterData() {
            const data = await Promise.all([
                getData({
                    spName: "SP_Select_All_From_Bd_Physical_Pheno_By_Eqp_ID",
                    payload: {
                        Eqp_id: values.equipment
                    }
                })
            ])
            if (data && data.length > 0) {
                setbreakdownPhenomenonMaster(data[0].data)
            }
        }
        setbreakdownPhenomenonMasterData()
    }, [props.values.equipment])
    return (
        <Fragment>
            <Typography variant="h6">{TextData.breakdownReporting}</Typography>
            <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid item xs={6} className={classes.Grid}>
                        <Departments {...props} />

                    </Grid>

                    <Grid item xs={6} className={classes.Grid}>
                        <Cell cellMaster={cellMaster} {...props} />
                    </Grid>
                    <Grid item xs={6} className={classes.Grid}>
                        <Equipments equipmentMaster={equipmentMaster} {...props} />
                    </Grid>

                    <Grid item xs={6} className={classes.Grid}>
                        <ReportedBy {...props} />
                    </Grid>

                    <Grid item xs={6}>
                        <FormControl className={classes.autoSuggest2}>
                            <DatePicker
                                keyboard
                                name="breakdownDate"
                                id="breakdownDate"
                                label="Breakdown Date"
                                placeholder="10/10/2018"
                                format={'DD/MM/YYYY'}
                                value={values.breakdownDate}
                                onChange={(date) => {
                                    setFieldValue("breakdownDate", date)
                                }}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                                error={errors.breakdownDate && (touched.breakdownDate)}
                                FormHelperTextProps={{
                                    error: errors.breakdownDate && (touched.breakdownDate)
                                }}
                                helperText={
                                    errors.breakdownDate && (touched.breakdownDate)
                                        ? errors.breakdownDate
                                        : null
                                }
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={6} >
                        <FormControl className={classes.autoSuggest2}>
                            <TimePicker
                                keyboard
                                label="Breakdown Time"
                                id="breakdownTime"
                                name="breakdownTime"
                                value={values.breakdownTime}
                                onChange={(date) => {
                                    setFieldValue("breakdownTime", date)
                                }}
                                mask={[/\d/, /\d/, ":", /\d/, /\d/, " ", /a|p/i, "M"]}
                                error={errors.breakdownTime && (touched.breakdownTime)}
                                FormHelperTextProps={{
                                    error: errors.breakdownTime && (touched.breakdownTime)
                                }}
                                helperText={
                                    errors.breakdownTime && (touched.breakdownTime)
                                        ? errors.breakdownTime
                                        : null
                                }
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={6} className={classes.Grid}>
                        <FormControl className={classes.autoSuggest}>
                            <InputLabel htmlFor="productionStatus-simple">{TextData.productionStatus}</InputLabel>
                            <Select fullWidth
                                value={values.productionStatus}
                                onChange={handleChange}
                                inputProps={{
                                    name: 'productionStatus',
                                    id: 'productionStatus-simple',
                                }}>
                                <MenuItem value={0}>Select</MenuItem>
                                <MenuItem value={1}>Stopped</MenuItem>
                                <MenuItem value={2}>Not Stopped</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>

                    <Grid item xs={6} className={classes.Grid}>
                        <FormControl className={classes.autoSuggest}>
                            <TextField fullWidth
                                name="breakdownDetails"
                                multiline
                                row='1'
                                placeholder="Enter Breakdown Details"
                                label="Breakdown Details"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.breakdownDetails}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={6} className={classes.Grid}>
                        <BreakdownPhenomenon breakdownPhenomenonMaster={breakdownPhenomenonMaster} {...props} />
                    </Grid>

                    <Grid item xs={6} className={classes.Grid}>
                        <EnteredBy {...props} />
                    </Grid>

                    <Grid item xs={12}>
                        <Button variant="contained" type="submit" color="primary" className={classes.button}>
                            {TextData.save}
                        </Button>
                        <Button type="button" variant="contained" color="secondary" className={classes.button} onClick={handleClose}>
                            {TextData.cancel}
                        </Button>
                        {props.status.showActions ?
                            (<React.Fragment><Button type="button" variant="contained" color="primary" className={classes.button} onClick={handleBreakdownMemo}>
                                {TextData.BreakdownMemo}
                            </Button>
                                <Button type="button" variant="contained" color="primary" className={classes.button} onClick={() => handleUploadDocument()}>
                                    {TextData.UploadDocuments}
                                </Button></React.Fragment>
                            ) : null}
                    </Grid>

                </Grid>
            </form>
        </Fragment>
    )
});

const EnhancedBreakdownReportingForm = compose(connect(state => ({ PENDING_BREAKDOWN_ONCREATE_DATA: state.PENDING_BREAKDOWN_ONCREATE_DATA })), withFormik({
    mapPropsToStatus: (props) => { return { showActions: false } },
    mapPropsToValues: () => ({
        department: '', cell: '', equipment: '', reportedby: '', breakdownDate: new Date(), breakdownTime: new Date(), productionStatus: '',
        breakdownDetails: '', breakdownPhenomenon: '', enteredBy: ''
    }),
    validationSchema: Yup.object().shape({
        department: Yup.string("department is required and should be a String").required("department Required"),
        // cell: Yup.string("equipment is required and should be a String").required("cell Required"),
        equipment: Yup.string("equipment is required and should be a String").required("equipment Required"),
        reportedby: Yup.string("reportedby is required and should be a String").required("reportedby is required"),
        // breakdownDate: Yup.date("breakdownDate is required and should be a valid Date").required("breakdownDate Required"),
        // breakdownTime: Yup.date("breakdownTime Person is required and should be a valid Date").required("breakdownTime Required"),
        // productionStatus: Yup.string("productionStatus is required and should be a String").required("productionStatus Required"),
        // breakdownDetails: Yup.string("breakdownDetails is required and should be a String").required("BreakdownDetails Required"),
        // breakdownPhenomenon: Yup.string("breakdownPhenomenon is required and should be a String").required("breakdownPhenomenon Required"),
    }),
    // Custom sync validation

    handleSubmit: (values, formActions) => actions.handleBreakdownSubmit({ values, formActions }),

    // handleDocument: () => {
    //     console.log('closingmmmmm')
    //     actions.closeDocument({ page: 'REPORTING_BREAKDOWN_DOCUMENT_POPUP' })
    // },

    displayName: 'SupplierForm',
}))(BreakdownReportingForm)

const BreakdownReportingPage = withStyles(styles)((props) => {
    const { classes } = props
    return (
        <Fragment>
            <Card className={classes.card} alignItems="center">
                <Scroll>
                    <EnhancedBreakdownReportingForm {...props} />
                </Scroll>
            </Card>
            <Consumer>
                {
                    ({ REPORTING_BREAKDOWN_DOCUMENT_POPUP }) => {
                        return (
                            <React.Fragment>
                                {REPORTING_BREAKDOWN_DOCUMENT_POPUP.isOpen ? <Dialog
                                    fullScreen
                                    className={classes.dialogStyles}
                                    open={REPORTING_BREAKDOWN_DOCUMENT_POPUP.isOpen}
                                    handleClose={() => actions.closeDocument({ page: "REPORTING_BREAKDOWN_DOCUMENT_POPUP" })}
                                    aria-labelledby="form-dialog-title"
                                    maxWidth="sm"
                                    TransitionComponent={Transition}
                                >
                                    <AppBar className={classes.appBar}>
                                        <Toolbar>
                                            <div className={classes.alignHeader}></div>
                                            <Typography variant="subtitle1" color="inherit" className={classes.flex}>
                                                Document
                            </Typography>

                                            <div className={classes.alignHeader}></div>
                                            <IconButton color="inherit" onClick={() => actions.closeDocument({ page: "REPORTING_BREAKDOWN_DOCUMENT_POPUP" })} aria-label="Close">
                                                <CloseIcon />
                                            </IconButton>
                                        </Toolbar>
                                        <DocumentsPage documentData={REPORTING_BREAKDOWN_DOCUMENT_POPUP.payload} />
                                    </AppBar>

                                </Dialog> : null}
                            </React.Fragment>
                        )
                    }
                }
            </Consumer>
        </Fragment >)
})

export default compose(
    dataProvider({
        queries: (props) => {
            return ([
                {
                    spName: "SP_Fun_Select_All_From_Equipment_By_Plant_ID",
                    payload: {},
                    key: "key1"
                },
                {
                    spName: "SP_Select_All_From_Departments",
                    payload: {},
                    key: "key2"
                },
                {
                    spName: "sp_select_all_from_Employee_master",
                    payload: {},
                    key: "key3"
                }
            ])
        }
    }),
)(BreakdownReportingPage);