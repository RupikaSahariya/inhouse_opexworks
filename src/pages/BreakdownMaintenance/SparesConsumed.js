import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withFormik } from "formik"
import * as Yup from "yup";
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import AgTable from "./../../components/AgTable";
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import RefreshIcon from "@material-ui/icons/RefreshOutlined";
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { Consumer, actions } from "./../../actions";
import styles from "./styles1";
import TextData from "./textData1";
import AutoSuggest from "./../../components/Select";

const SeletedSparepart = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props;
    let key1=0;
    console.log("ALL_SPARE_PARTS:",props.ALL_SPARE_PARTS)
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest2}
                name="seletedSparepart"
                id="selectedSparepart"
                label="Select Spare Part"
                error={errors.seletedSparepart && (touched.seletedSparepart)} FormHelperTextProps={{
                    error: errors.seletedSparepart && (touched.seletedSparepart)
                }}
                helperText={
                    errors.seletedSparepart && (touched.seletedSparepart)
                        ? errors.seletedSparepart
                        : null
                }
                value={values.seletedSparepart}
                placeholder={TextData.seletedSparepart}
                options={props.ALL_SPARE_PARTS ? props.ALL_SPARE_PARTS.data.map((item, i) => {
                    return ({
                        value: item.comp_code,
                        label: item.description,
                        key1:i
                    })
                }) : []}
                onChange={(value) => {
                    setFieldValue("seletedSparepart", value)
                    const thisSpareDetails = props.ALL_SPARE_PARTS && props.ALL_SPARE_PARTS.data ? (props.ALL_SPARE_PARTS.data.find( item => (value == item.comp_code))) : ''
                    setFieldValue("details", thisSpareDetails ? thisSpareDetails.make : '')
                }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})

const SparesConsumedForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset
    } = props
    return (
        <Fragment>
            <Typography variant="h6">{TextData.AddSparesConsumedinBreakdown}</Typography>
            <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                >
                    <Grid item xs={12}>
                        <SeletedSparepart {...props} />
                    </Grid>

                    <Grid item xs={12}>
                        <FormControl className={classes.autoSuggest}>
                            <TextField fullWidth
                                name="quantity"
                                placeholder="Enter Quantity"
                                label="Quantity"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.quantity}
                                error={errors.quantity && (touched.quantity)}
                                FormHelperTextProps={{
                                    error: errors.quantity && (touched.quantity)
                                }}
                                helperText={
                                    errors.quantity && (touched.quantity)
                                        ? errors.quantity
                                        : null
                                }
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12}>
                        <FormControl className={classes.autoSuggest}>
                            <TextField fullWidth
                                name="value"
                                placeholder="Enter Value" value
                                label="Value"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.value}
                                error={errors.value && (touched.value)}
                                FormHelperTextProps={{
                                    error: errors.value && (touched.value)
                                }}
                                helperText={
                                    errors.value && (touched.value)
                                        ? errors.value
                                        : null
                                }
                            />
                        </FormControl>
                    </Grid>

                    <Grid
                        item xs={12}
                        justify="center"
                        className={classes.button}
                    >
                        <Button variant="contained" type="submit" color="primary" style={{ marginLeft: "10px" }}>
                            {TextData.save}
                        </Button>
                        <Button type="button" variant="contained" color="secondary" onClick={() => actions.closeSpareUsed({
                            page: 'PENDING_BREAKDOWN_SPARESUSED_DIALOG'
                        })} style={{ marginLeft: "10px" }}>
                            {TextData.cancel}
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </Fragment>
    )
});

const EnhancedSparesConsumedForm = withFormik({
    mapPropsToValues: (props) => {
        console.log(props)
        const { payload } = props
        if (!payload) {
            return ({ seletedSparepart: '', quantity: '', value: '', details:'' })
        }
        return ({ seletedSparepart: payload.data ? payload.data.spr_srno : null, quantity: payload.quantity, value: payload.value })
    },

    validationSchema: Yup.object().shape({
        seletedSparepart: Yup.string("Seleted Sparepart is required and should be a String").required("Seleted Sparepart Required"),
        quantity: Yup.string("Quantity Sparepart is required and should be a String").required("Quantity Sparepart Required"),
        // cost: Yup.string("Cost Sparepart is required and should be a String").required("Cost Sparepart Required"),
    }),

    handleSubmit: async (values, { props, setSubmitting }) => {
        console.log(props)
        setSubmitting(true);
        const r = await actions.getData({
            spName: "SP_Insert_Update_Delete_Spares_Consumed_BD",
            payload: {
                Eqp_ID: props.sparesusedData && props.sparesusedData.breakdownData && props.sparesusedData.breakdownData[0].Eqp_ID,
                BD_SrNo: props.sparesusedData && props.sparesusedData.breakdownData && props.sparesusedData.breakdownData[0].bd_srno,
                Spr_srno: values.seletedSparepart,
                Detail: values.details,
                Cons_qty: values.quantity,
                Value: values.value,
                Rate: props.sparesusedData && props.sparesusedData.breakdownData && props.sparesusedData.breakdownData[0].con_cost,
                Flag: props.payload ? "U" : "I"
            },
            key: "SP_Insert_Update_Delete_Spares_Consumed_BD" + props.sparesusedData && props.sparesusedData.breakdownData && props.sparesusedData.breakdownData[0] ? props.sparesusedData.Eqp_ID: null + values.seletedSparepart
        })
        await actions.getData({
            spName: "SP_Select_All_From_Spr_Cons_BD_By_Eqp_ID_And_BD_Sr_No",
            payload: {
                Eqp_ID: props.sparesusedData && props.sparesusedData.breakdownData && props.sparesusedData.breakdownData[0] ? props.sparesusedData.breakdownData[0].Eqp_ID : null,
                BD_SrNo: props.sparesusedData && props.sparesusedData.breakdownData && props.sparesusedData.breakdownData[0] ? props.sparesusedData.breakdownData[0].bd_srno : null
            },
            key: "PENDING_BREAKDOWN_SPARESUSED_DATA"
        })
        actions.snackBarHandleOpen({
            message: props.payload ? "Record Updated" : "Record Created"
        })
        await actions.closeSpareUsed({
            page: 'PENDING_BREAKDOWN_SPARESUSED_DIALOG'
        })
    },
    displayName: 'SparesConsumedForm',
})(SparesConsumedForm)

class SparesConsumedPage extends React.PureComponent {
    state = {
        // dialogOpen: false
    }

    handleClickOpen = () => {
        // this.setState({ dialogOpen: true });
    };

    handleClose = () => {
        actions.closeSpareUsed({ page: "PENDING_BREAKDOWN_SPARESUSED_DIALOG" })
    };

    componentDidMount() {
        actions.getDatas([{
            spName: "SP_Select_All_From_Spare_Details_By_Equipment_ID",
            payload: {
                Eqp_ID: this.props.sparesusedData && this.props.sparesusedData[4] && this.props.sparesusedData[4][0] ? this.props.sparesusedData[4][0].Eqp_ID:null
            },
            key: "ALL_SPARE_PARTS"
        }, {
            spName: "SP_Select_All_From_Spr_Cons_BD_By_Eqp_ID_And_BD_Sr_No",
            payload: {
                Eqp_ID: this.props.sparesusedData[4][0].Eqp_ID ? this.props.sparesusedData[4][0].Eqp_ID : null,
                BD_SrNo: this.props.sparesusedData[4][0].bd_srno ? this.props.sparesusedData[4][0].bd_srno : null
            },
            key: 'PENDING_BREAKDOWN_SPARESUSED_DATA'
        }]);
    }

    onGridReady = (params) => {
        params.api.sizeColumnsToFit()
        this.gridParams = params
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

    exportData = () => {
        this.gridApi.exportDataAsCsv(this.gridParams);
    }

    onEdit = (selectedData) => {
        actions.handleSpareUsedDialog({
            page: "PENDING_BREAKDOWN_SPARESUSED_DIALOG",
            payload: selectedData
        })
    }

    onDelete = async (selectedData) => {
        console.log(selectedData)
        await actions.getData({
            spName: "SP_Insert_Update_Delete_Spares_Consumed_BD",
            payload: {
                Eqp_ID: selectedData.data.Eqp_ID,
                BD_SrNo: selectedData.data.bd_srno,
                Spr_srno: selectedData.data.spr_srno,
                Detail: selectedData.data.details,
                Cons_qty: selectedData.data.cons_qty,
                Value: selectedData.data.value,
                Rate: selectedData.rate,
                Flag: "D"
            },
            key: "SP_Insert_Update_Delete_Spares_Consumed_BD" + "D" + selectedData.data.Eqp_ID + selectedData.data.spareDescription
        })
        actions.snackBarHandleOpen({
            message: "Record deleted"
        })
        await actions.getData({
            spName: "SP_Select_All_From_Spr_Cons_BD_By_Eqp_ID_And_BD_Sr_No",
            payload: {
                Eqp_ID: selectedData.data.Eqp_ID,
                BD_SrNo: selectedData.data.bd_srno
            },
            key: "PENDING_BREAKDOWN_SPARESUSED_DATA"
        })
    }

    handleRefresh = async (selectedData) => {
        await actions.getData({
            spName: "SP_Select_All_From_Spr_Cons_BD_By_Eqp_ID_And_BD_Sr_No",
            payload: {
                Eqp_ID: selectedData.data.Eqp_ID,
                BD_SrNo: selectedData.data.bd_srno
            },
            key: "PENDING_BREAKDOWN_SPARESUSED_DATA"
        })
    }

    render() {
        const columnDefs = [{
            headerName: "Sr.No.", field: "srno", sortable: true, filter: true, width: 45,
            cellRenderer: "toolTipRenderer", toolTipText:"srno", cellClass:"centered-content"
        },
        {
            headerName: "Spare Parts", field: "spareParts", sortable: true, filter: true, width: 90,
            cellRenderer: "toolTipRenderer", toolTipText:"spareParts"
        },
        {
            headerName: "Spare Description", field: "spareDescription", sortable: true, filter: true, width: 90,
            cellRenderer: "toolTipRenderer", toolTipText:"spareDescription", cellClass:"left-aligned-content"
        },
        {
            headerName: "Quantity", field: "quantity", sortable: true, filter: true, width: 80,
            cellRenderer: "toolTipRenderer", toolTipText:"quantity"
        },
        {
            headerName: "Rate", field: "rate", sortable: true, filter: true, width: 80,
            cellRenderer: "toolTipRenderer", toolTipText:"rate"
        },
        {
            headerName: "Value", field: "value", sortable: true, filter: true, width: 80,
            cellRenderer: "toolTipRenderer", toolTipText:"value"
        },
        {
            headerName: "", width: 50, cellRenderer: "editRenderer"
        }
        ]
        const { classes } = this.props
        return (
            <Fragment>
                <Consumer>{React.memo(({ ALL_SPARE_PARTS, PENDING_BREAKDOWN_SPARESUSED_DIALOG }) => (
                    <Dialog
                        disableBackdropClick
                        className={classes.dialogStyles}
                        open={PENDING_BREAKDOWN_SPARESUSED_DIALOG ? PENDING_BREAKDOWN_SPARESUSED_DIALOG.isOpen : false}
                        handleClose={() => actions.hancloseSpareUsed({ page: "PENDING_BREAKDOWN_SPARESUSED_DIALOG" })}
                        aria-labelledby="form-dialog-title"
                        maxWidth="sm"
                    >
                        <DialogContent>
                            <EnhancedSparesConsumedForm {...this.props} payload={PENDING_BREAKDOWN_SPARESUSED_DIALOG ? PENDING_BREAKDOWN_SPARESUSED_DIALOG.payload : null}
                                ALL_SPARE_PARTS={ALL_SPARE_PARTS} handleClose={this.handleClose} />
                        </DialogContent>
                    </Dialog>
                ))}</Consumer>
                <Grid
                    container
                    direction="row"
                >
                    <div className={classes.toolbar}>
                    <Grid item xs={12} direction="row" className={classes.gridLabel}>
                            <Typography variant="subtitle2">
                                Equipment : {this.props.sparesusedData.eqp_desc}
                            {/* </Typography>
                            <Typography variant="subtitle2"> */}
                            &nbsp; &nbsp;   |   &nbsp;&nbsp;
                                Nature of Breakdown : {this.props.sparesusedData.breakdownData[0].bd_det}
                            </Typography>
                        </Grid>
                        <Tooltip title="Add New Spare Consumed">
                            <Fab size={"small"} color="primary" variant="extended" onClick={() => { actions.handleSpareUsedDialog({ page: "PENDING_BREAKDOWN_SPARESUSED_DIALOG" }) }}
                                className={classes.toolbarIcon1}>
                                <AddIcon />&nbsp;
                        </Fab>
                        </Tooltip>
                        <Tooltip title="Refresh">
                            <Fab size={"small"} color="primary" variant="extended" onClick={this.handleRefresh} className={classes.toolbarIcon2}>
                                <RefreshIcon />&nbsp;
                        </Fab>
                        </Tooltip>
                    </div>
                </Grid>

                <Paper style={{ height: window.innerHeight - 160, paddingTop: '10px' }}>
                    <Consumer>
                        {({ PENDING_BREAKDOWN_SPARESUSED_DATA }) => {
                            if (!PENDING_BREAKDOWN_SPARESUSED_DATA) {
                                return <div style={{ height: window.innerHeight - 250, width: '100%', paddingTop: '15%' }} ><CircularProgress /></div>
                            }
                            console.log(PENDING_BREAKDOWN_SPARESUSED_DATA)

                            const rowData = PENDING_BREAKDOWN_SPARESUSED_DATA.data ? PENDING_BREAKDOWN_SPARESUSED_DATA.data.map((item, i) => ({
                                data: item,
                                srno: item.bd_srno, spareParts: item.spr_srno, spareDescription: item.detail, quantity: item.cons_qty, rate: item.rate, value: item.value
                            })) : null
                            return (
                                <AgTable
                                    loading={this.props.loading}
                                    onGridReady={this.onGridReady}
                                    columnDefs={columnDefs}
                                    onEdit={this.onEdit}
                                    onDelete={this.onDelete}
                                    rowData={rowData} />
                            )
                        }}
                    </Consumer>
                </Paper>
            </Fragment >)
    }
}

export default withStyles(styles)(SparesConsumedPage);

