const TextData = {
    AddMTBFAndMTTR: 'Add MTBF And MTTR',
    AddMonth: 'Add Month',
    MtbfAndMttrAnalysis: 'MTBF And MTTR Analysis',
    equipment: 'Equipment',
    month: 'Month',
    save: 'Save',
    cancel: 'Cancel'
}

export default TextData;