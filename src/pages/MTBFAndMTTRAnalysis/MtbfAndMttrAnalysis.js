import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import AgTable from "../../components/AgTable";
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from "@material-ui/core/Grid";
import RefreshIcon from "@material-ui/icons/RefreshOutlined";
import { compose } from "recompose";
import Header1 from '../../components/Header1/Header1';
import MtbfAndMttrForm from "./MtbfAndMttrForm";
import NewMonthForm from "./NewMonthForm";
import styles from "./styles";
import TextData from "./textData";
import { Consumer, actions } from "./../../actions";
import CircularProgress from '@material-ui/core/CircularProgress';

class MtbfAndMttrAnalysis extends React.PureComponent {
    state = {
        dialogOpen: false,
        dialogOpen2: false,
        breadcrumb: [
            { name: "Home", path: "/" },
            { name: TextData.MtbfAndMttrAnalysis, path: "/MtbfAndMttrAnalysis" }]
    }

    handleMtbfMttrOpen = () => {
        actions.handleMtbfMttrDialog({ page: "MTBFMTTR_PAGE_DIALOG" })
    };

    handleMonthOpen = () => {
        actions.handleMtbfMttrDialog({ page: "MONTH_PAGE_DIALOG" })
    };

    handleMtbfMttrClose = () => {
        actions.closeMtbfMttr({ page: "MTBFMTTR_PAGE_DIALOG" })
    };

    handleMonthClose = () => {
        actions.closeMtbfMttr({ page: "MONTH_PAGE_DIALOG" })
    };

    async componentDidMount() {
        actions.getDatas([{
            spName: "SP_Fun_Select_All_From_Equipment_By_Plant_ID",
            payload: {},
            key: "ALL_EQUIPMENT_MASTER"
        }, {
            spName: "SP_Select_All_From_OverallEqptEfficiency_By_Plant_ID",
            payload: {},
            key: 'ALL_OVERALLEQPTEFFICIENCY_MASTER'
        }]);
    }

    async handleRefresh() {
        await actions.getData({
            spName: "SP_Select_All_From_OverallEqptEfficiency_By_Plant_ID",
            payload: {},
            key: "ALL_OVERALLEQPTEFFICIENCY_MASTER"
        })
    };

    onGridReady = (params) => {
        // params.api.sizeColumnsToFit()
        this.gridParams = params
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

    exportData = () => {
        this.gridApi.exportDataAsCsv(this.gridParams);
    }

    onEdit = (selectedData) => {
        actions.handleMtbfMttrDialog({
            page: "MTBFMTTR_PAGE_DIALOG",
            payload: selectedData
        })
    }

    render() {
        const columnDefs = [
        {
            headerName: "Sr. No.", field: "srno", sortable: true, filter: true, width: 120, cellClass:'centered-content',
            cellRenderer: "toolTipRenderer", toolTipText:"srno"
        },
        {
            headerName: "Equipment", field: "equipment", sortable: true, filter: true, width: 160,
            cellRenderer: "toolTipRenderer", toolTipText:"equipment"
        },
        {
            headerName: "Month", field: "month", sortable: true, filter: true, width: 150, cellClass:'centered-content',
            cellRenderer: "toolTipRenderer", toolTipText:"month"
        },
        {
            headerName: "Year", field: "year", sortable: true, filter: true, width: 150, cellClass:'centered-content',
            cellRenderer: "toolTipRenderer", toolTipText:"year"
        },
        {
            headerName: "Running Time (Hrs)", field: "runningTime", sortable: true, filter: true, width: 150, cellClass:'centered-content',
            cellRenderer: "toolTipRenderer", toolTipText:"runningTime"
        },
        {
            headerName: "No. Of Failures", field: "numberOfFailures", sortable: true, filter: true, width: 150, cellClass:'centered-content',
            cellRenderer: "toolTipRenderer", toolTipText:"numberOfFailures"
        },
        {
            headerName: "Down Time", field: "downTime", sortable: true, filter: true, width: 150, cellClass:'centered-content',
            cellRenderer: "toolTipRenderer", toolTipText:"downTime"
        },
        {
            headerName: "MTBF (In-Days)", field: "mtbf", sortable: true, filter: true, width: 150, cellClass:'centered-content',
            cellRenderer: "toolTipRenderer", toolTipText:"mtbf"
        },
        {
            headerName: "MTTR (In-Hours)", field: "mttr", sortable: true, filter: true, width: 150, cellClass:'centered-content',
            cellRenderer: "toolTipRenderer", toolTipText:"mttr"
        },
        {
            headerName: "% Downtime", field: "downtime2", sortable: true, filter: true, width: 150, cellClass:'centered-content',
            cellRenderer: "toolTipRenderer", toolTipText:"downtime2"
        },
        {
            headerName: "", width: 100, cellRenderer: "editRenderer"
        },
        ]
        const { classes } = this.props
        return (
            <Fragment>
                <Consumer>
                    {
                        ({ MTBFMTTR_PAGE_DIALOG, ALL_EQUIPMENT_MASTER }) => {
                            return (
                                <React.Fragment>
                                    {MTBFMTTR_PAGE_DIALOG.isOpen ?
                                        <Dialog
                                            disableBackdropClick
                                            className={classes.dialogStyles}
                                            open={MTBFMTTR_PAGE_DIALOG.isOpen}
                                            aria-labelledby="form-dialog-title"
                                            maxWidth="sm"
                                        >
                                            <DialogTitle id="form-dialog-title">{TextData.AddMTBFAndMTTR}</DialogTitle>
                                            <DialogContent>
                                                <MtbfAndMttrForm {...this.props} onCancel={this.handleMtbfMttrClose} selectedData={MTBFMTTR_PAGE_DIALOG.payload}
                                                    ALL_EQUIPMENT_MASTER={ALL_EQUIPMENT_MASTER} />
                                            </DialogContent>
                                        </Dialog> : null}
                                </React.Fragment>
                            )
                        }
                    }
                </Consumer>

                <Consumer>
                    {
                        ({ MONTH_PAGE_DIALOG }) => {
                            return (
                                <React.Fragment>
                                    {MONTH_PAGE_DIALOG.isOpen ?
                                        <Dialog
                                            disableBackdropClick
                                            className={classes.dialogStyles}
                                            open={MONTH_PAGE_DIALOG.isOpen}
                                            aria-labelledby="form-dialog-title"
                                            maxWidth="sm"
                                        >
                                            <DialogTitle id="form-dialog-title">{TextData.AddMonth}</DialogTitle>
                                            <DialogContent>
                                                <NewMonthForm {...this.props} onCancel={this.handleMonthClose} />
                                            </DialogContent>
                                        </Dialog> : null}
                                </React.Fragment>
                            )
                        }
                    }
                </Consumer>

                <Header1 breadCrumbText={this.state.breadcrumb} breadCrumbHeader={TextData.MtbfAndMttrAnalysis} />
                <Grid
                    container
                    direction="row"
                    justify="flex-end"
                    alignItems="right"
                >
                    <Grid item key={4}>
                        <div className={classes.toolbar}>
                            <Tooltip title="Add MTBF And MTTR">
                                <Fab size={"small"} color="primary" variant="extended" onClick={this.handleMtbfMttrOpen} className={classes.toolbarIcon}>
                                    <AddIcon />&nbsp;
                        </Fab>
                            </Tooltip>
                            <Tooltip title="Add Month">
                                <Fab size={"small"} color="primary" variant="extended" onClick={this.handleMonthOpen} className={classes.toolbarIcon}>
                                    <AddIcon />&nbsp;Add Month&nbsp;
                        </Fab>
                            </Tooltip>
                            <Tooltip title="Refresh">
                                <Fab size={"small"} color="primary" variant="extended" onClick={this.handleRefresh} className={classes.toolbarIcon}>
                                    <RefreshIcon />&nbsp;
                        </Fab>
                            </Tooltip>
                        </div>
                    </Grid>
                </Grid>

                <Paper>
                    <Consumer>
                        {({ ALL_OVERALLEQPTEFFICIENCY_MASTER }) => {
                            if (!ALL_OVERALLEQPTEFFICIENCY_MASTER) {
                                return <div style={{ height: window.innerHeight - 250, width: '100%', paddingTop: '18%', paddingLeft: '50%' }} ><CircularProgress /></div>
                            }
                            console.log("ALL_OVERALLEQPTEFFICIENCY_MASTER", ALL_OVERALLEQPTEFFICIENCY_MASTER)
                            const rowData = ALL_OVERALLEQPTEFFICIENCY_MASTER ? ALL_OVERALLEQPTEFFICIENCY_MASTER.data.map((item, i) => ({
                                srno: i+1, equipment: item.eqp_desc, month: item.oee_month, year: item.oee_year,
                                runningTime: item.running_time, numberOfFailures: item.failure_no, downTime: item.down_time,
                                mtbf: item.MTBF, mttr: item.MTTR, downtime2: item.Percent_Down_Time,
                                data: item
                            })) : []

                            return (
                                <React.Fragment>{ALL_OVERALLEQPTEFFICIENCY_MASTER ? <AgTable
                                    loading={this.props.loading}
                                    onGridReady={this.onGridReady}
                                    columnDefs={columnDefs}
                                    rowData={rowData}
                                    onEdit={this.onEdit}
                                    dontShowDelete={true}
                                /> : null
                                }
                                </React.Fragment>
                            )
                        }}
                    </Consumer>
                </Paper>
            </Fragment >)
    }
}
export default compose(withStyles(styles))(MtbfAndMttrAnalysis);