const styles = theme => ({
    //header styles
    breakCrumbIcon: {
        marginTop: '3px',
        fontSize: "20px"
    },
    breadCrumbText: {
        marginTop: '4px',
        color: "white",
        fontSize: "13px",
        "font-weight": "normal"
    },
    headerText: {
        color: "white",
        paddingBottom: "4px",
        fontSize: "18px",
    },
    header: {
        "background-color": "#3f51b5",
        color: "white",
    },

    //action style
    dialogStyles: {
        paperWidthSm: '400px',
        textAlign: 'center'
    },
    toolbarIcon: {
        margin: '10px'
    },
    toolbar: {
        margin: '5px',
    },

    autoSuggest: {
        marginTop: theme.spacing.unit * 2,
        maxWidth: 550,
        width: "100%",
        textAlign: 'left',
        display: "flex",
        flexWrap: 'wrap'
    },
    autoSuggest2: {
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit,
        maxWidth: 550,
        width: "100%",
        textAlign: 'left',
        display: "flex",
        flexWrap: 'wrap'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        paddingBottom: "20px",
        paddingTop: "10px",
        justify: "space-around"
    },
    button: {
        padding: theme.spacing.unit * 2
    }
});

export default styles;