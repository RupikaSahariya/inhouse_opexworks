import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withFormik } from "formik";
import * as Yup from "yup";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from "@material-ui/core/Grid";
import FormControl from '@material-ui/core/FormControl';
import styles from "./styles";
import TextData from "./textData";
import { actions } from "./../../actions";
import AutoSuggest from "./../../components/Select";

const Equipments = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props
    console.log("ALL_EQUIPMENT_MASTER", props.ALL_EQUIPMENT_MASTER)
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest2}
                name="equipment"
                label="Equipment"
                textFieldProps={{
                    helperText: errors.equipment && (touched.equipment)
                        ? errors.equipment
                        : null,
                    error: errors.equipment && (touched.equipment),
                }}
                value={values.equipment}
                placeholder={TextData.equipment}
                options={props.ALL_EQUIPMENT_MASTER ? props.ALL_EQUIPMENT_MASTER.data.map((item, i) => ({
                    value: item.Eqp_ID,
                    label: item.eqp_desc,
                })) : []}
                onChange={(value) => {
                    setFieldValue("equipment", value)
                }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
});

const MtbfAndMttrForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        handleClose,
        onCancel
    } = props
    return (
        <Fragment>
            <form onReset={handleReset} onSubmit={handleSubmit} onClick={handleClose} className={classes.container} autoComplete="off">
                <Grid
                    item xs={12}
                    justify="space-around"
                >
                    <Grid item xs={12}>
                        <Equipments {...props} />
                    </Grid>
                    <Grid item xs={12}>
                        <FormControl className={classes.autoSuggest}>
                            <InputLabel htmlFor="month-simple">{TextData.month}</InputLabel>
                            <Select
                                fullWidth
                                value={values.month}
                                onChange={handleChange}
                                inputProps={{
                                    name: 'month',
                                    id: 'month-simple',
                                }}
                            >
                                <MenuItem value={null}>Select</MenuItem>
                                <MenuItem value={'JANUARY'}>JANUARY</MenuItem>
                                <MenuItem value={'FEBRUARY'}>FEBRUARY</MenuItem>
                                <MenuItem value={'MARCH'}>MARCH</MenuItem>
                                <MenuItem value={'APRIL'}>APRIL</MenuItem>
                                <MenuItem value={'MAY'}>MAY</MenuItem>
                                <MenuItem value={'JUNE'}>JUNE</MenuItem>
                                <MenuItem value={'JULY'}>JULY</MenuItem>
                                <MenuItem value={'AUGUST'}>AUGUST</MenuItem>
                                <MenuItem value={'SEPTEMBER'}>SEPTEMBER</MenuItem>
                                <MenuItem value={'OCTOBER'}>OCTOBER</MenuItem>
                                <MenuItem value={'NOVEMBER'}>NOVEMBER</MenuItem>
                                <MenuItem value={'DECEMBER'}>DECEMBER</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        <FormControl className={classes.autoSuggest}>
                            <TextField
                                name="year"
                                placeholder="Enter Year"
                                label="Year"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.year}
                                error={errors.year && (touched.year)}
                                FormHelperTextProps={{
                                    error: errors.year && (touched.year)
                                }}
                                helperText={
                                    errors.year && (touched.year)
                                        ? errors.year
                                        : null
                                }
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        <FormControl className={classes.autoSuggest}>
                            <TextField
                                name="runningTime"
                                placeholder="Enter Running Time"
                                label="Running Time"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.runningTime}
                                error={errors.runningTime && (touched.runningTime)}
                                FormHelperTextProps={{
                                    error: errors.runningTime && (touched.runningTime)
                                }}
                                helperText={
                                    errors.runningTime && (touched.runningTime)
                                        ? errors.runningTime
                                        : null
                                }
                            />
                        </FormControl>
                    </Grid>
                </Grid>

                <Grid
                    item xs={12}
                    justify="center"
                    className={classes.button}
                >
                    <Button type="submit" variant="contained" color="primary" style={{ marginLeft: "10px" }}>
                        {TextData.save}
                    </Button>
                    <Button variant="contained" color="secondary" onClick={onCancel} style={{ marginLeft: "10px" }}>
                        {TextData.cancel}
                    </Button>
                </Grid>
            </form>
        </Fragment>
    );
});

const EnhancedMtbfAndMttrForm = withFormik({
    mapPropsToValues: (props) => {
        const { selectedData } = props
        console.log('selectedddd', selectedData);
        if (!selectedData) {
            return ({ equipment: '', month: '', year: '', runningTime: '' })
        }
        return ({ equipment: selectedData.data.Eqp_ID, month: selectedData.month, year: selectedData.year, runningTime: selectedData.runningTime })
    },

    validationSchema: Yup.object().shape({
        equipment: Yup.string("Equipment is required and should be a String").required("Equipment Required"),
    }),

    handleSubmit: async (values, { props, setSubmitting }) => {
        setSubmitting(true);
        await actions.getData({
            spName: "SP_Insert_Update_Delete_OverallEqptEfficiency",
            payload: {
                Eqp_ID: values.equipment,
                oee_month: values.month,
                oee_year: values.year,
                running_time: values.runningTime,
                failure_no: 0,
                down_time: null,
                break_down_cost: 0,
                Flag: props.selectedData ? 'U' : 'I'
            },
            key: "SP_Insert_Update_Delete_OverallEqptEfficiency"
        })
        actions.snackBarHandleOpen({
            message: props.selectedData ? "Record Updated" : "Record Created"
        })
        await actions.closeMtbfMttr({ page: 'MTBFMTTR_PAGE_DIALOG' })
        await actions.getData({
            spName: "SP_Select_All_From_OverallEqptEfficiency_By_Plant_ID",
            payload: {},
            key: "ALL_OVERALLEQPTEFFICIENCY_MASTER"
        })
        setSubmitting(false);
    },
    displayName: 'MtbfAndMttrForm',
})(MtbfAndMttrForm)

export default EnhancedMtbfAndMttrForm;