import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withFormik } from "formik";
import * as Yup from "yup";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from "@material-ui/core/Grid";
import FormControl from '@material-ui/core/FormControl';
import styles from "./styles";
import TextData from "./textData";
import { actions } from "./../../actions";

const NewMonthForm = withStyles(styles)((props) => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        classes,
        handleReset,
        handleClose2,
        onCancel
    } = props
    return (
        <Fragment>
            <form onReset={handleReset} onSubmit={handleSubmit} onClick={handleClose2} className={classes.container} autoComplete="off">
                <Grid
                    item xs={12}
                    justify="space-around"
                >
                    <Grid item xs={12}>
                        <FormControl className={classes.autoSuggest}>
                            <InputLabel htmlFor="month-simple">{TextData.month}</InputLabel>
                            <Select
                                fullWidth
                                value={values.month}
                                onChange={handleChange}
                                inputProps={{
                                    name: 'month',
                                    id: 'month-simple',
                                }}
                            >
                                <MenuItem value={null}>Select</MenuItem>
                                <MenuItem value={1}>JANUARY</MenuItem>
                                <MenuItem value={2}>FEBRUARY</MenuItem>
                                <MenuItem value={3}>MARCH</MenuItem>
                                <MenuItem value={4}>APRIL</MenuItem>
                                <MenuItem value={5}>MAY</MenuItem>
                                <MenuItem value={6}>JUNE</MenuItem>
                                <MenuItem value={7}>JULY</MenuItem>
                                <MenuItem value={8}>AUGUST</MenuItem>
                                <MenuItem value={9}>SEPTEMBER</MenuItem>
                                <MenuItem value={10}>OCTOBER</MenuItem>
                                <MenuItem value={11}>NOVEMBER</MenuItem>
                                <MenuItem value={12}>DECEMBER</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        <FormControl className={classes.autoSuggest2}>
                            <TextField
                                name="year"
                                placeholder="Enter Year"
                                label="Year"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.year}
                                error={errors.year && (touched.year)}
                                FormHelperTextProps={{
                                    error: errors.year && (touched.year)
                                }}
                                helperText={
                                    errors.year && (touched.year)
                                        ? errors.year
                                        : null
                                }
                            />
                        </FormControl>
                    </Grid>
                </Grid>

                <Grid
                    item xs={12}
                    justify="center"
                    className={classes.button}
                >
                    <Button type="submit" variant="contained" color="primary" style={{ marginLeft: "10px" }}>
                        {TextData.save}
  </Button>
                    <Button variant="contained" color="secondary" onClick={onCancel} style={{ marginLeft: "10px" }}>
                        {TextData.cancel}
  </Button>
                </Grid>
            </form>
        </Fragment>
    );
});

const EnhancedNewMonthForm = withFormik({
    mapPropsToValues: () => ({ month:'', year: '' }),
    validationSchema: Yup.object().shape({
    }),

    handleSubmit: async (values, formikBag) => actions.createNewMonth({values, formikBag}),

    displayName: 'NewMonthForm',
})(NewMonthForm)

export default EnhancedNewMonthForm;