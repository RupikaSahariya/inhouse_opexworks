import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import SessionStyles from '../../styles/Session';
import { withFormik } from 'formik';
import * as Yup from "yup";
import { jwtDecode } from "./../../utils/jwt-decode";
import client from "./../../utils/client";
import { withRouter } from "react-router-dom";
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import CallIcon from '@material-ui/icons/Call';
import EmailIcon from '@material-ui/icons/Email';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Fab from '@material-ui/core/Fab';
import { Consumer, actions } from "../../actions";

const messages = {
  AUTH_ERROR: "Invalid Username or Password"
}

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2,
    maxWidth: '95%'
  },
  backArrowIcon: {
    fontSize: "15px",
    marginTop: "1px",
    marginRight: "5px",
    marginLeft: "2px",
    display: "flex",
    flexWrap: 'wrap'
  },
  userText: {
    marginRight: "2px",
    color: "black",
    fontSize: "13px",
    textAlign: "left",
    display: "flex",
    flexWrap: 'wrap'
  }
});

const signinForm = (props) => {
  const {
    values,
    classes,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
  } = props;
  return (
    <Fragment>
      <form onSubmit={handleSubmit}>
        <TextField
          id="username"
          label="Username/Mobile no."
          name="username"
          className={classes.textField}
          fullWidth
          {...this.props}
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.username}
          error={errors.username && (touched.username)}
          FormHelperTextProps={{
            error: errors.username && (touched.username)
          }}
          helperText={
            errors.username && (touched.username)
              ? errors.username
              : null
          }
        />
        <Button variant="raised" color="primary" fullWidth type="submit">Submit</Button>
      </form>
    </Fragment>
  );
}

const enhancedSigninForm = withFormik({
  mapPropsToValues: () => ({ username: '' }),
  validationSchema: Yup.object().shape({
    username: Yup.string("Username is required and should be a String").required("Please enter username")
  }),
  handleSubmit: (values, formActions) => actions.handleSignIn({ values, formActions }),
  // handleSubmit: (values, { props, setSubmitting }) => {
  //   console.log("hess",values.username);
  //   props.handleSubmitId(values.username)
  //   setSubmitting(false);
  // },
  displayName: 'enhancedSigninForm'
})(signinForm)

const StyledSigninForm = withStyles(styles)(enhancedSigninForm)
StyledSigninForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

const passwordForm = (props) => {
  const {
    values,
    id,
    classes,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    resetVerified
  } = props;
  return (

    <Fragment>
      <Fab variant="extended" size="small" onClick={resetVerified} style={{ marginBottom: "7px" }}>
        <Typography variant="caption" className={classes.userText}>
          <AccountCircleIcon className={classes.backArrowIcon} /> {id}</Typography>
      </Fab>
      <form onSubmit={handleSubmit}>
        <TextField
          id="password"
          label="Password / OTP"
          className={classes.textField}
          type="password"
          fullWidth
          {...this.props}
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.password}
          error={errors.password && touched.password}
          FormHelperTextProps={{
            error: errors.password && touched.password
          }}
          helperText={
            errors.password && touched.password ? errors.password : null
          }
        />
        <Button variant="raised" color="primary" fullWidth type="submit">Submit</Button>
      </form>
    </Fragment>
  );
}

const enhancedPasswordForm = withRouter(withFormik({
  mapPropsToValues: () => ({ password: '' }),
  validationSchema: Yup.object().shape({
    password: Yup.string("Password is required and should be a String").required("Please enter password"),
  }),
  handleSubmit: (values, formActions) => actions.handlePassword({ values, formActions }),
  // handleSubmit: (values, { props, setSubmitting }) => {
  //   console.log("hess");
  //   props.handleSubmitPassword(values.password)
  //   setSubmitting(false);
  // },
})(passwordForm))

const StyledpasswordForm = withStyles(styles)(enhancedPasswordForm)
StyledpasswordForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

class FormUi extends React.PureComponent {
  state = {
    breadcrumbdata: this.props.breadCrumbText,
    header: this.props.breadCrumbHeader,
    IDVerified: false
  }

  handleId = (id) => {
    this.setState({ IDVerified: true, id }, () => {
    })
  }

  resetVerified = (values, formActions) => {
    actions.resetUserLogin({ values, formActions })
  }

  handlePasswordId = (password) => {
    this.setState({ password }, () => {
      this.login()
    })
  }
  handleSubmit = () => {
    this.setState({ IDVerified: true })
  }

  login = async () => {
    try {
      const { company, data, token, userName } = await fetch(`${process.env.REACT_APP_API_URL}/login`, {
        "credentials": "omit",
        "headers": {
          "accept": "application/json, text/plain, */*",
          "content-type": "application/json"
        },
        "body": JSON.stringify({ userId: this.state.id, password: this.state.password }),
        "method": "POST",
        "mode": "cors"
      }).then(r => r.json());
      if (!token) {
        throw "AUTH_ERROR"
      }
      const { payload: { plantId } } = jwtDecode(token)
      localStorage.setItem("token", token)
      client.writeData({
        data: {
          user: {
            userName,
            company,
            image: data,
            plant_id: plantId,
            token: token,
            __typename: "User"
          }
        }
      })
      this.props.history.replace("/")
    } catch (e) {
      if (e === "AUTH_ERROR") {
        // setFieldError("password", messages.AUTH_ERROR)
      } else {
        // setFieldError("password", "Unknown error occured")
      }
    }
  }
  render() {

    const {
      classes,
      values,
      touched,
      errors,
      handleChange,
      handleBlur,
      handleSubmit,
    } = this.props;

    return (
      <Fragment>
        <Fragment>
          <Typography variant="h5" className={classes.welcome}>Welcome</Typography>
        </Fragment>
        <Fragment>
          {/* <form onSubmit={this.handleSubmit}> */}
          <div className="text-xs-center pb-xs">
            <img style={{
              height: "85px", marginBottom: "25px"
            }} src="/static/images/logo.png" alt="" />
          </div>

          {/* {!this.state.IDVerified ?
            <StyledSigninForm handleSubmitId={this.handleId} /> :
            <StyledpasswordForm handleSubmitPassword={this.handlePassword} resetVerified={this.resetVerified} id={this.state.id} />
          } */}

          <Consumer>
            {
              ({ handleSignIn }) => {
                return (
                  <div>
                    {!handleSignIn.username ?
                      <StyledSigninForm /> :
                      <StyledpasswordForm resetVerified={this.resetVerified} id={handleSignIn.user_id} />
                    }
                  </div>
                )
              }
            }
          </Consumer>

          <div className="pt-1 text-md-left">
            <Link to="/forgot">
              <Button style={{ "color": "#3f51b5" }}>Forgot password?</Button>
            </Link>
          </div>
          {/* </form> */}
        </Fragment>

        <Fragment>
          <div className={classes.tags} >
            <a target="_blank" href="http://opexworks.com">
              <Button style={{ "color": "#fff", "backgroundcolor": "primary", "margin": "3px" }}><QuestionAnswerIcon style={{ "font-size": "18px" }} />&nbsp;Need Help ?</Button>
            </a>
            <a href="tel:5551234567">
              <Button style={{ "color": "#fff", "margin": "3px" }}><CallIcon style={{ "font-size": "18px" }} />&nbsp;Call</Button>
            </a>
            <a href="mailto:info@opexworks.com?subject = Feedback&body = Message">
              <Button style={{ "color": "#fff", "margin": "3px" }}><EmailIcon style={{ "font-size": "18px" }} />&nbsp;Mail</Button>
            </a>
          </div>
        </Fragment>
      </Fragment>
    );
  }
}

const StyledFormUi = withStyles(styles)(FormUi)

StyledFormUi.propTypes = {
  classes: PropTypes.object.isRequired,
};

const Signin = (props) => {
  const { classes } = props;
  return (
    <div className={classNames(classes.session, classes.background)}>
      <div className={classes.content}>
        <div className={classes.wrapper}>
          <Card>
            <CardContent>
              <StyledFormUi {...props} />
            </CardContent>
          </Card>
        </div>
      </div>
    </div>
  );
}

Signin.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(SessionStyles)(Signin));