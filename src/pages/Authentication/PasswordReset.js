import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import SessionStyles from '../../styles/Session';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import CallIcon from '@material-ui/icons/Call';
import EmailIcon from '@material-ui/icons/Email';

const PasswordReset = (props) => {
  const { classes } = props;
  return (
    <div className={classNames(classes.session, classes.background)}>
      <div className={classes.content}>
        <div className={classes.wrapper}>
          <Card>
            <CardContent>
              <Fragment>
                <Fragment>
                  <Typography variant="h5" className={classes.welcome}>Forgot Password</Typography>
                </Fragment>
                <Fragment>
                  <form>
                    <div className="text-xs-center pb-xs">
                      <img src="/static/images/logo-dark.svg" alt="" />
                      <Typography variant="caption">Please enter your log in credentials</Typography>
                    </div>
                    <TextField
                      id="username"
                      label="Username"
                      placeholder="Enter Username"
                      className={classes.textField}
                      fullWidth
                    />
                    <Button variant="raised" color="primary" fullWidth className="mt-1" type="submit">Submit</Button>
                    <div className="pt-1 text-xs-left">
                      <Link to="/signin">
                        <Button style={{ "color": "#3f51b5" }}>Sign In</Button>
                      </Link>
                    </div>
                  </form>
                </Fragment>

                <Fragment>
                  <div className={classes.tags}>
                    <Link to="/forgot">
                      <Button style={{ "color": "#fff", "margin": "3px" }}><QuestionAnswerIcon style={{ "font-size": "18px" }} />&nbsp;Need Help ?</Button>
                    </Link>
                    <Link to="/forgot">
                      <Button style={{ "color": "#fff", "margin": "3px" }}><CallIcon style={{ "font-size": "18px" }} />&nbsp;Call</Button>
                    </Link>
                    <Link to="/forgot">
                      <Button style={{ "color": "#fff", "margin": "3px" }}><EmailIcon style={{ "font-size": "18px" }} />&nbsp;Mail</Button>
                    </Link>
                  </div>
                </Fragment>

              </Fragment>
            </CardContent>
          </Card>
        </div>
      </div>
    </div>
  );
}

PasswordReset.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(SessionStyles)(PasswordReset);