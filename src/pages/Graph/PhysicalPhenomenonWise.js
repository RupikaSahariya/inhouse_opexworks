import React, { Fragment, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import { withFormik } from "formik";
import * as Yup from "yup";
import { DatePicker } from 'material-ui-pickers';
import Header1 from './../../components/Header1/Header1';
import Grid from "@material-ui/core/Grid";
import Chart from "./Charts";
import { actions, Consumer } from "./../../actions/index";
import moment from "moment";
import TextData from "./textData";
import styles from "./styles";
import AutoSuggest from "./../../components/Select";

const BreakdownPhenomenon = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest2}
                name="breakdownPhenomenon"
                id="breakdownPhenomenon"
                label="breakdown Phenomenon"
                value={values.breakdownPhenomenon}
                placeholder={TextData.breakdownPhenomenon}
                options={props.physicalphenomenonwiseData ? props.physicalphenomenonwiseData.data.map((item, i) => {
                    return ({
                        value: item.Phy_Pheno_Desc,
                        label: item.Phy_Pheno_Desc,
                    })
                }) : []}
                onChange={(value) => {
                    setFieldValue("breakdownPhenomenon", value)
                }}
                textFieldProps={{
                    helperText: errors.breakdownPhenomenon && (touched.breakdownPhenomenon)
                        ? errors.breakdownPhenomenon
                        : null,
                    error: errors.breakdownPhenomenon && (touched.breakdownPhenomenon),
                }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})

const Form = withStyles(styles)((props) => {
    const {
        values,
        handleSubmit,
        classes,
        handleReset,
        errors,
        touched,
        setFieldValue
    } = props
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid
                container
                direction="row"
                alignItems="center"
            >
                <Grid item xs={12} md={5} lg={5}>
                    <FormControl className={classes.autoSuggest}>
                        <DatePicker
                            keyboard
                            name="fromdate"
                            label="From Date"
                            format={'DD/MM/YYYY'}
                            value={values.fromdate ? values.fromdate : null}
                            onChange={(date) => {
                                setFieldValue("fromdate", date)
                            }}
                            error={errors.fromdate && (touched.fromdate)}
                            FormHelperTextProps={{
                                error: errors.fromdate && (touched.fromdate)
                            }}
                            helperText={
                                errors.fromdate && (touched.fromdate)
                                    ? errors.fromdate
                                    : null
                            }
                            disableOpenOnEnter
                            animateYearScrolling={false}
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={12} md={5} lg={5}>
                    <BreakdownPhenomenon {...props} />
                </Grid>

                <Grid item xs={10} md={2} lg={2}>
                    <Button variant="contained" type="submit" color="primary" className={classes.button}>
                        {TextData.graph}
                    </Button>
                </Grid>
            </Grid>
        </form >
    );
})

const PhysicalPhenomenonWiseForm = withFormik({
    mapPropsToValues: () => ({ fromdate: '', todate: new Date(), breakdownPhenomenon: '' }),
    validationSchema: Yup.object().shape({
        fromdate: Yup.date("Date is required and should be a Date").required("Date Required"),
        breakdownPhenomenon: Yup.string("Breakdown Phenomenon is required and should be a String").required("Breakdown Phenomenon is required")
    }),

    handleSubmit: async (values, { setSubmitting }) => {
        setSubmitting(true)
        await actions.getData({
            spName: "SP_Rpt_BM_Down_Time_Physical_Phenomenon_Wise_By_From_Dt_Failure_ID",
            payload: {
                Phy_Pheno_Desc: values.breakdownphenomenon,
                From_Date: moment(values.fromdate).toISOString(),
                To_Date: moment(values.todate).toISOString(),
                Grapg_Name: "Down Time Due To Breakdown Phenomenon : " + values.breakdownphenomenon
            },
            key: "physicalphenomenonwise"
        })
        setSubmitting(false);
    },
    displayName: 'PhysicalPhenomenonWiseForm',
})(Form)

const PhysicalPhenomenonWisePage = withStyles(styles)((props) => {
    const { classes } = props
    const breadcrumb = [
        { name: "Home", path: "/" },
        { name: TextData.PhenomenonWiseDowntime, path: "/graph/physicalphenomenonwise" }]
    const physicalphenomenonwiseData = useEffect(() => {
        //action
        actions.getData({
            spName: "SP_Get_All_Phy_Pheno_Desc_From_Bd_Physical_Pheno",
            payload: {},
            key: "PHYSICAL_PHENOMENON_WISE_DATA"
        })
    }, [])
    return (
        <Fragment>
            <Header1 breadCrumbText={breadcrumb} breadCrumbHeader={TextData.PhenomenonWiseDowntime} />
            <Card className={classes.card}>
                <Consumer>
                    {({ PHYSICAL_PHENOMENON_WISE_DATA }) => <PhysicalPhenomenonWiseForm {...props} physicalphenomenonwiseData={PHYSICAL_PHENOMENON_WISE_DATA} />}
                </Consumer>
                <Consumer >{({ physicalphenomenonwise }) => {
                    if (!physicalphenomenonwise) {
                        return null
                    }
                    console.log('physicalphenomenonwise', physicalphenomenonwise)
                    return (<Chart
                        yAxislabel="Downtime Hours"
                        xAxislabel="Month"
                        data={{
                            datasets: [{
                                backgroundColor: "#7986cb",
                                borderColor: "#3f51b5",
                                borderWidth: 0,
                                data: physicalphenomenonwise.data ? physicalphenomenonwise.data.map(item => parseFloat(item.prod_downtime)) : null,
                                pointBackgroundColor: "#3f51b5",
                                pointBorderColor: "#fff"
                            }],
                            labels: physicalphenomenonwise.data ? physicalphenomenonwise.data.map(item => item.month_year) : null
                        }} />
                    )
                }}
                </Consumer>
            </Card>
        </Fragment >)
})

export default PhysicalPhenomenonWisePage;