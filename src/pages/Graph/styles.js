const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    card: {
        padding: 25,
        marginLeft: 40,
        marginRight: 40,
        marginTop: 40,
        marginBottom: 40,
        textAlign: "center",
        overflow:"visible"
    },
    card2: {
        padding: 25,
        marginLeft: 40,
        marginRight: 40,
        marginTop: 40,
        marginBottom: 40,
        maxWidth:500,
        textAlign: "center",
        overflow:"visible"
    },
    button: {
        margin: theme.spacing.unit,
    },
    button2: {
        margin: 45,
        marginBottom:100
    },
    autoSuggest: {
        marginBottom: theme.spacing.unit,
        marginLeft: theme.spacing.unit * 1.5,
        minWidth: 150,
        width: "95%",
        textAlign: 'left',
        display: "flex"
    },
    autoSuggest2: {
        marginTop: theme.spacing.unit * 1,
        marginBottom: theme.spacing.unit,
        marginLeft: theme.spacing.unit * 1.5,
        minWidth: 150,
        width: "95%",
        textAlign: 'left',
        display: "flex"
    },
});

export default styles;