import React, { Fragment, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import { withFormik } from "formik";
import * as Yup from "yup";
import { DatePicker } from 'material-ui-pickers';
import Header1 from './../../components/Header1/Header1';
import Grid from "@material-ui/core/Grid";
import Chart from "./Charts";
import { actions, Consumer } from "./../../actions/index";
import moment from "moment";
import TextData from "./textData";
import styles from "./styles";
import AutoSuggest from "./../../components/Select";

const FailureArea = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props
    console.log(props);
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest2}
                name="failurearea"
                label="Failure Area"
                textFieldProps={{
                    helperText: errors.failurearea && (touched.failurearea)
                        ? errors.failurearea
                        : null,
                    error: errors.failurearea && (touched.failurearea),
                }}
                value={values.failurearea}
                placeholder={TextData.failureAreaLabel}
                options={props.failureAreaData ? props.failureAreaData.data.map((item, i) => {
                    return ({
                        value: item.area_id,
                        label: item.area_desc,
                    })
                }) : []}
                onChange={(value) => {
                    setFieldValue("failurearea", value)
                }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})

const Form = withStyles(styles)((props) => {
    const {
        values,
        handleSubmit,
        classes,
        handleReset,
        errors,
        touched,
        setFieldValue
    } = props
    console.log(props)
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid
                container
                direction="row"
                alignItems="center"
            >
                <Grid item xs={12} md={5} lg={5}>
                    <FormControl className={classes.autoSuggest}>
                        <DatePicker
                            keyboard
                            name="fromdate"
                            label="From Date"
                            format={'DD/MM/YYYY'}
                            value={values.fromdate ? values.fromdate : null}
                            onChange={(date) => {
                                setFieldValue("fromdate", date)
                            }}
                            error={errors.fromdate && (touched.fromdate)}
                            FormHelperTextProps={{
                                error: errors.fromdate && (touched.fromdate)
                            }}
                            helperText={
                                errors.fromdate && (touched.fromdate)
                                    ? errors.fromdate
                                    : null
                            }
                            disableOpenOnEnter
                            animateYearScrolling={false}
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={12} md={5} lg={5}>
                    <FailureArea {...props} />
                </Grid>

                <Grid item xs={10} md={2} lg={2}>
                    <Button variant="contained" type="submit" color="primary" className={classes.button}>
                        {TextData.graph}
                    </Button>
                </Grid>
            </Grid>
        </form >
    );
})

const FailureAreaWiseForm = withFormik({
    mapPropsToValues: () => ({ fromdate: '',todate: new Date(), failurearea: '' }),
    validationSchema: Yup.object().shape({
        fromdate: Yup.date("Date is required and should be a Date").required("Date Required"),
        failurearea: Yup.string("Failure Area is required and should be a String").required("Failure Area is required")
    }),

    handleSubmit: async ( values, { setSubmitting }, props) => {
        setSubmitting(true)
        await actions.getData({
            spName: "SP_Rpt_BM_Down_Time_Failuer_Area_Wise_By_From_Dt_Area_ID",
            payload: {
                Area_ID: values.failurearea,
                From_Date: moment(values.fromdate).toISOString(),
                To_Date: moment(values.todate).toISOString(),
                Graph_Name: "Down Time Due To Failure Area : " + values.failurearea +""
            },
            key: "failureareawise"
        })
        setSubmitting(false);
    },
    displayName: 'FailureAreaWiseForm',
})(Form)

const FailureAreaWisePage = withStyles(styles)((props) => {
    const { classes } = props
    const breadcrumb = [
        { name: "Home", path: "/" },
        { name: TextData.failureArea, path: "/graph/failureareawise" }]
    const failureAreaData = useEffect(() => {
        //action
        actions.getData({
            spName: "SP_Select_All_From_area_code_master",
            payload: {},
            key: "FAILURE_AREA_DATA"
        })
    }, [])
    return (
        <Fragment>
            <Header1 breadCrumbText={breadcrumb} breadCrumbHeader={TextData.failureArea} />
            <Card className={classes.card}>
                <Consumer>
                    {({ FAILURE_AREA_DATA }) => <FailureAreaWiseForm {...props} failureAreaData={FAILURE_AREA_DATA} />}
                </Consumer>
                <Consumer >{({ failureareawise, FAILURE_AREA_DATA }) => {
                    if (!failureareawise) {
                        return null
                    }
                    return (<Chart
                        yAxislabel="Downtime Hours"
                        xAxislabel="Month"
                        data={{
                            datasets: [{
                                backgroundColor: "#7986cb",
                                borderColor: "#3f51b5",
                                borderWidth: 0,
                                data: failureareawise.data ? failureareawise.data.map(item => parseFloat(item.prod_downtime)) : null,
                                pointBackgroundColor: "#3f51b5",
                                pointBorderColor: "#fff"
                            }],
                            labels: failureareawise.data ? failureareawise.data.map(item => item.month_year) : null
                        }} />
                    )
                }}
                </Consumer>
            </Card>
        </Fragment >)
})

export default FailureAreaWisePage;