import React, { Fragment, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import { withFormik } from "formik";
import * as Yup from "yup";
import FormControl from '@material-ui/core/FormControl';
import Header1 from './../../components/Header1/Header1';
import { DatePicker } from 'material-ui-pickers';
import Grid from "@material-ui/core/Grid";
import Chart from "./Charts";
import { actions, Consumer } from "../../actions/index";
import moment from "moment";
import TextData from "./textData";
import styles from "./styles";

const Form = withStyles(styles)((props) => {
    const {
        values,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue,
        errors,
            touched
    } = props
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid
                container
                direction="row"
                alignItems="center"
            >
                <Grid item xs={12} md={5} lg={5}>
                <FormControl className={classes.autoSuggest}>
                    <DatePicker
                        keyboard
                        name="fromdate"
                        label="From Date"
                        format={'DD/MM/YYYY'}
                        value={values.fromdate ? values.fromdate : null}
                        onChange={(date) => {
                            setFieldValue("fromdate", date)
                        }}
                        error={errors.fromdate && (touched.fromdate)}
                        FormHelperTextProps={{
                            error: errors.todate
                        }}
                        helperText={
                            errors.todate
                        }
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                    </FormControl>
                </Grid>

                <Grid item xs={12} md={5} lg={5}>
                <FormControl className={classes.autoSuggest}>
                    <DatePicker
                        keyboard
                        name="todate"
                        label="To Date"
                        format={'DD/MM/YYYY'}
                        value={values.todate ? values.todate : null}
                        onChange={(date) => {
                            setFieldValue("todate", date)
                        }}
                        error={errors.todate && (touched.todate)}
                            FormHelperTextProps={{
                                error: errors.todate && (touched.todate)
                            }}
                            helperText={
                                errors.todate && (touched.todate)
                                    ? errors.todate
                                    : null
                            }
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                    </FormControl>
                </Grid>
                <Grid item xs={10} md={2} lg={2}>
                    <Button variant="contained" type="submit" color="primary" className={classes.button}>
                        {TextData.graph}
  </Button>
                </Grid>
            </Grid>
        </form >
    );
})

const MTTRForm = withFormik({
    mapPropsToValues: () => ({ fromdate: '', todate: '' }),
    validationSchema: Yup.object().shape({
        fromdate: Yup.date("Date is required and should be a Date").required("Date Required"),
        todate: Yup.date("Date is required and should be a Date").required("Date Required"),
    }),

    handleSubmit: async (values, { setFieldError, setSubmitting }) => {
        setSubmitting(true)
        if(values.fromdate>=values.todate){
            setFieldError('todate',"Should be after from date")
            return;
        }
        await actions.getData({
            spName: "SP_Rpt_BM_Trends_For_Period_Breakdown_MTTR_By_From_Date_To_Date",
            payload: {
                From_Date: moment(values.fromdate).toISOString(),
                To_Date: moment(values.todate).toISOString()
            },
            key: "mttr"
        })
        setSubmitting(false);
    },
    displayName: 'MTTRForm',
})(Form)

const MTTRPage = withStyles(styles)((props) => {
    useEffect(() => {
        return () => {
            actions.setState({
                mttr: null
            })
        }
    })
    const { classes } = props
    const breadcrumb = [
        { name: "Home", path: "/" },
        { name: TextData.MTTR, path: "/graph/mttr" }]
    return (
        <Fragment>
            <Header1 breadCrumbText={breadcrumb} breadCrumbHeader={TextData.MTTR} />
            <Card className={classes.card}>
                <MTTRForm {...props} />
                <Consumer >{({ mttr }) => {
                    if (!mttr) {
                        return null
                    }
                    console.log('mttr', mttr)
                    return (<Chart
                        yAxislabel="Hours"
                        xAxislabel="Month"
                        data={{
                            datasets: [{
                                backgroundColor: "#7986cb",
                                borderColor: "#3f51b5",
                                borderWidth: 0,
                                data:mttr.data ? mttr.data.map(item=>parseFloat(item.cost)) : null,
                                pointBackgroundColor: "#3f51b5",
                                pointBorderColor: "#fff"
                            }],
                            labels:mttr.data ? mttr.data.map(item=>item.Months) : null
                        }} />
                    )
                }}
                </Consumer>
            </Card>
        </Fragment >)
})

export default MTTRPage;