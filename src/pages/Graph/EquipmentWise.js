import React, { Fragment, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import { withFormik } from "formik"
import * as Yup from "yup";
import { DatePicker } from 'material-ui-pickers';
import Header1 from './../../components/Header1/Header1';
import Grid from "@material-ui/core/Grid";
import Chart from "./Charts";
import { actions, Consumer } from "./../../actions/index";
import moment from "moment";
import TextData from "./textData";
import styles from "./styles";
import AutoSuggest from "./../../components/Select";

const Equipment = ((props) => {
    const {
        values,
        touched,
        errors,
        classes,
        setFieldValue
    } = props
    return (
        <React.Fragment>
            <AutoSuggest className={classes.autoSuggest2}
                name="equipment"
                label="Equipment"
                textFieldProps={{
                    helperText: errors.equipment && (touched.equipment)
                        ? errors.equipment
                        : null,
                    error: errors.equipment && (touched.equipment),
                }}
                value={values.equipment}
                placeholder={TextData.equipment}
                options={props.equipmentwiseData ? props.equipmentwiseData.data.map((item, i) => {
                    return ({
                        value: item.Eqp_ID,
                        label: item.eqp_desc,
                    })
                }) : []}
                onChange={(value) => {
                    setFieldValue("equipment", value)
                }}
            >
            </AutoSuggest>
        </React.Fragment>
    )
})

const Form = withStyles(styles)((props) => {
    const {
        values,
        handleSubmit,
        classes,
        handleReset,
        errors,
        touched,
        setFieldValue
    } = props
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid
                container
                direction="row"
                alignItems="center"
            >
                <Grid item xs={12} md={5} lg={5}>
                    <FormControl className={classes.autoSuggest}>
                        <DatePicker
                            keyboard
                            name="fromdate"
                            label="From Date"
                            format={'DD/MM/YYYY'}
                            value={values.fromdate ? values.fromdate : null}
                            onChange={(date) => {
                                setFieldValue("fromdate", date)
                            }}
                            error={errors.fromdate && (touched.fromdate)}
                            FormHelperTextProps={{
                                error: errors.fromdate && (touched.fromdate)
                            }}
                            helperText={
                                errors.fromdate && (touched.fromdate)
                                    ? errors.fromdate
                                    : null
                            }
                            disableOpenOnEnter
                            animateYearScrolling={false}
                        />
                    </FormControl>  
                </Grid>

                <Grid item xs={12} md={5} lg={5}>
                    <Equipment {...props} />
                </Grid>

                <Grid item xs={10} md={2} lg={2}>
                    <Button variant="contained" type="submit" color="primary" className={classes.button}>
                        {TextData.graph}
                    </Button>
                </Grid>
            </Grid>
        </form >
    );
})

const EquipmentWiseForm = withFormik({
    mapPropsToValues: () => ({ fromdate: '', todate: new Date(), equipment: '' }),
    validationSchema: Yup.object().shape({
        fromdate: Yup.date("Date is required and should be a Date").required("Date Required"),
        equipment: Yup.string("Equipment is required and should be a String").required("Equipment is required")
    }),

    handleSubmit: async (values, { setSubmitting }) => {
        setSubmitting(true)
        await actions.getData({
            spName: "SP_Rpt_BM_Down_Time_Equipment_Wise_By_From_Dt_Equipment_ID",
            payload: {
                From_Date: moment(values.fromdate).toISOString(),
                To_Date: moment(values.todate).toISOString(),
                Equipment_ID: values.equipment,
                Graph_Name:  "Down Time Of : "+ values.equipment
            },
            key: "equipmentwise"
        })
        setSubmitting(false);
    },
    displayName: 'EquipmentWiseForm',
})(Form)

const EquipmentWisePage = withStyles(styles)((props) => {
    const { classes } = props
    const breadcrumb = [
        { name: "Home", path: "/" },
        { name: TextData.EquipmentWiseDowntime, path: "/graph/equipmentwise" }]
    const equipmentwiseData = useEffect(() => {
        //action
        actions.getData({
            spName: "SP_Fun_Select_All_From_Equipment_By_Plant_ID",
            payload: {},
            key: "EQUIPMENT_WISE_DATA"
        })
    }, [])
    return (
        <Fragment>
            <Header1 breadCrumbText={breadcrumb} breadCrumbHeader={TextData.EquipmentWiseDowntime} />
            <Card className={classes.card}>
                <Consumer>
                    {({ EQUIPMENT_WISE_DATA }) => <EquipmentWiseForm {...props} equipmentwiseData={EQUIPMENT_WISE_DATA} />}
                </Consumer>
                <Consumer >{({ equipmentwise }) => {
                    if (!equipmentwise) {
                        return null
                    }
                    console.log(equipmentwise)
                    return (<Chart
                        yAxislabel="Downtime Hours"
                        xAxislabel="Month"
                        data={{
                            datasets: [{
                                backgroundColor: "#7986cb",
                                borderColor: "#3f51b5",
                                borderWidth: 0,
                                data: equipmentwise.data? equipmentwise.data.map(item => parseFloat(item.prod_downtime)) : null,
                                pointBackgroundColor: "#3f51b5",
                                pointBorderColor: "#fff"
                            }],
                            labels: equipmentwise.data ? equipmentwise.data.map(item => item.month_year) : null
                        }} />
                    )
                }}
                </Consumer>
            </Card>
        </Fragment >)
})

export default EquipmentWisePage;