const TextData = {
    WaterCostvsSales: 'Water Cost vs Sales',
    PowerCostvsSales: 'Power Cost vs Sales',
    TotalMaintenanceCostVsSales: 'Total Maintenance Cost Vs Sales',
    TotalBreakdownCostVsSales: 'Total Breakdown Cost Vs Sales',
    Downtime: 'Downtime Percentage',
    MTBF: 'MTBF',
    MTTR: 'MTTR',
    AreaWiseDowntime: 'Area Wise Downtime',
    PhenomenonWiseDowntime: 'Phenomenon Wise Downtime',
    EquipmentWiseDowntime: 'Equipment Wise Downtime',
    failureArea: 'Area Wise Downtime',
    failureAreaLabel: 'Failure Area',
    breakdownPhenomenon: 'Breakdown Phenomenon',
    equipment: 'Equipment',
    graph: 'Graph',
    cancel: 'Cancel'
}

export default TextData;