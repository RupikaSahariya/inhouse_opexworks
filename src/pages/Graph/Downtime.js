import React, { Fragment, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import { withFormik } from "formik";
import * as Yup from "yup";
import FormControl from '@material-ui/core/FormControl';
import { DatePicker } from 'material-ui-pickers';
import Header1 from './../../components/Header1/Header1';
import Grid from "@material-ui/core/Grid";
import Chart from "./Charts";
import { actions, Consumer } from "../../actions/index";
import moment from "moment";
import TextData from "./textData";
import styles from "./styles";

const Form = withStyles(styles)((props) => {
    const {
        values,
        handleSubmit,
        classes,
        handleReset,
        setFieldValue,
        errors,
        touched
    } = props
    return (
        <form onReset={handleReset} onSubmit={handleSubmit} className={classes.container} autoComplete="off">
            <Grid
                container
                direction="row"
                alignItems="center"
            >
                <Grid item xs={12} md={5} lg={5}>
                <FormControl className={classes.autoSuggest}>
                    <DatePicker
                        keyboard
                        name="fromDate"
                        label="From Date"
                        format={'DD/MM/YYYY'}
                        value={values.fromDate ? values.fromDate : null}
                        onChange={(date) => {
                            setFieldValue("fromDate", date)
                        }}
                        error={errors.fromDate && (touched.fromDate)}
                        FormHelperTextProps={{
                            error: errors.fromdate
                        }}
                        helperText={
                            errors.fromdate 
                        }
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                    </FormControl>
                </Grid>

                <Grid item xs={12} md={5} lg={5}>
                <FormControl className={classes.autoSuggest}>
                    <DatePicker
                        keyboard
                        name="toDate"
                        label="To Date"
                        format={'DD/MM/YYYY'}
                        value={values.toDate ? values.toDate : null}
                        onChange={(date) => {
                            setFieldValue("toDate", date)
                        }}
                        error={errors.toDate && (touched.toDate)}
                        FormHelperTextProps={{
                            error: errors.toDate && (touched.toDate)
                        }}
                        helperText={
                            errors.toDate && (touched.toDate)
                                ? errors.toDate
                                : null
                        }
                        disableOpenOnEnter
                        animateYearScrolling={false}
                    />
                    </FormControl>
                </Grid>
                <Grid item xs={10} md={2} lg={2}>
                    <Button variant="contained" type="submit" color="primary" className={classes.button}>
                        {TextData.graph}
  </Button>
                </Grid>
            </Grid>
        </form >
    );
})

const DowtimeForm = withFormik({
    mapPropsToValues: () => ({ fromDate: '', toDate: '' }),
    validationSchema: Yup.object().shape({
        fromDate: Yup.date("From Date is required and should be a Date").required("Date Required"),
        toDate: Yup.date("Date is required and should be a Date").required("Date Required"),
    }),

    handleSubmit: async (values, { setFieldError, setSubmitting }) => {
        setSubmitting(true)
        if(values.fromdate>=values.todate){
            setFieldError('todate',"Should be after from date")
            return;
        }
        await actions.getData({
            spName: "SP_Rpt_BM_Trends_For_Breakdown_Percentage_By_From_Dt_To_Date_Equipment_IDS",
            payload: {
                From_Date: moment(values.fromDate).toISOString(),
                To_Date: moment(values.toDate).toISOString()
            },
            key: "downtime"
        })
        setSubmitting(false);
    },
    displayName: 'DowtimeForm',
})(Form)

const DowtimePage = withStyles(styles)((props) => {
    useEffect(() => {
        return () => {
            actions.setState({
                downtime: null
            })
        }
    })
    const { classes } = props
    const breadcrumb = [
        { name: "Home", path: "/" },
        { name: TextData.Downtime, path: "/graph/downtime" }]
    return (
        <Fragment>
            <Header1 breadCrumbText={breadcrumb} breadCrumbHeader={TextData.Downtime} />
            <Card className={classes.card}>
                <DowtimeForm {...props} />
                <Consumer >{({ downtime }) => {
                    console.log(downtime)
                    if (!downtime) {
                        return null
                    }
                    return (<Chart
                        yAxislabel="Downtime %"
                        xAxislabel="Month"
                        data={{
                            datasets: [{
                                backgroundColor: "#7986cb",
                                borderColor: "#3f51b5",
                                borderWidth: 0,
                                data:downtime.data? downtime.data.map(item=>parseFloat(item.cost)): null,
                                pointBackgroundColor: "#3f51b5",
                                pointBorderColor: "#fff"
                            }],
                            labels:downtime.data ? downtime.data.map(item=>item.Months) : null
                        }} />
                    )
                }}
                </Consumer>
            </Card>
        </Fragment >)
})

export default DowtimePage;