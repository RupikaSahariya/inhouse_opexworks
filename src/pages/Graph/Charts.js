import React from 'react';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import { Bar } from 'react-chartjs-2';
import { Wrapper } from '../../components';

const Charts = (props) => (
  <Wrapper>
    <Grid container>
      <Grid item xs={12} sm={12} md={12} lg={12} >
        <CardContent>
          <Bar
            data={props.data}
            height={100}
            options={{
              scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero: true
                  },
                  scaleLabel: {
                    display: true,
                    fontStyle:"bold",
                    labelString: props.yAxislabel,
                    fontSize:16
                  }
                }],
                xAxes: [{
                  scaleLabel: {
                    display: true,
                    fontStyle:"bold",
                    labelString: props.xAxislabel,
                    fontSize:16
                  }
                }]
              },
              legend: {
                display: false,
              },
              maintainAspectRatio: true,
              responsive: true
            }}
          />
        </CardContent>
      </Grid>
    </Grid>
  </Wrapper>
);

export default Charts;