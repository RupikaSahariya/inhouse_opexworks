import { jwtDecode } from "./../utils/jwt-decode";
import moment from "moment"
import { showFile } from "./../utils/showFile"
const getData = ({ spName, payload }) => fetch(`${process.env.REACT_APP_API_URL}/sp/${spName}`, {
    body: JSON.stringify(payload),
    headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json"
    },
    method: "POST"
}).then(r => {
    if (r.ok) { return r.json() } else {
        console.log(r)
        throw r.body
    }
})

const getReport = async (state, actions, { spName, payload, key }) => {
    const data = await fetch(`${process.env.REACT_APP_API_URL}/reports/${spName}`, {
        body: JSON.stringify(payload),
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json",
            "render": "application/pdf"
        },
        method: "POST"
    }).then(r => r.blob())
    return {
        [key ? key : spName]: new Blob([data], { type: "application/pdf" })
    }
}

const downloadReport = async (state, actions, { spName, payload, key }) => {
    // actions.snackBarHandleOpen({
    //     message: "Downloading PDF document. May take sometime"
    // })
    await fetch(`${process.env.REACT_APP_API_URL}/reports/${spName}`, {
        body: JSON.stringify(payload),
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json",
            "render": "application/pdf"
        },
        method: "POST"
    }).then(r => r.blob()).then(showFile)

    return ({})
}

const createNewMonth = async (state, actions, { formikBag, values }) => {
    const { setSubmitting, props } = formikBag
    setSubmitting(true);
    console.log("Create Month")
    try {
        const check = await getData({
            spName: "SP_Create_New_Refresh_Month_Break_Down_Maint",
            payload: {
                from_month: values.month,
                from_year: values.year
            },
            key: 'NEW_MONTH'
        })
        console.log("checking", check)
        //check if  error based on check data returned
        //if error show already exists message
        //else

        if (check.status === 500) {
            actions.snackBarHandleOpen({
                message: "Month Already Exists"
            })
        }
        else {
            console.log("newmonth values", values);
            console.log("props values", props);
            await actions.getData({
                spName: "SP_Insert_Update_Delete_OverallEqptEfficiency",
                payload: {
                    Eqp_ID: null,
                    oee_month: values.month,
                    oee_year: values.year,
                    running_time: 0,
                    failure_no: 0,
                    down_time: null,
                    break_down_cost: 0,
                    Flag: 'U'
                },
                key: "SP_Insert_Update_Delete_OverallEqptEfficiency"
            })
            actions.snackBarHandleOpen({
                message: "Month Added"
            })
        }
        await actions.closeMtbfMttr({ page: 'MONTH_PAGE_DIALOG' })
        await actions.getData({
            spName: "SP_Select_All_From_OverallEqptEfficiency_By_Plant_ID",
            payload: {},
            key: "ALL_OVERALLEQPTEFFICIENCY_MASTER"
        })

        setSubmitting(false);

        return {}
    } catch (e) {
        actions.snackBarHandleOpen({
            message: "Month Already Exists"
        })
        return {}
    }
}

const openDialog = (state, { getData }, args) => {
    return {
        [args.page]: {
            isOpen: true,
            payload: args.payload
        }
    }
}

const closeDialog = (state, actions, args) => {
    return {
        [args.page]: {
            isOpen: false,
            payload: null
        }
    }
}

const snackBarHandleOpen = async (_s, _a, { message }) => {
    return {
        SNACKBAR: {
            isOpen: true,
            message: message
        }
    }
}

const createRepetitiveBreakdown = async (_s, actions, { spName, payload }) => {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/sp/${spName}`, {
        body: JSON.stringify(payload),
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json"
        },
        method: "POST"
    }).then(r => r.json())
    actions.getData({
        spName: "sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID",
        payload: {
            Eqp_ID: payload.Eqp_ID,
            bd_srno: response.data[0].bd_srno
        },
        key: "sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID"
    })

    return {
        createRepetitiveBreakdown: {
            data: {
                Eqp_ID: payload.Eqp_ID,
                bd_srno: response.data[0].bd_srno
            }
        }
    }
}

const snackBarHandleClose = async (_s, _a, _args) => {
    return {
        SNACKBAR: {
            isOpen: false,
            message: ''
        }
    }
}

const documentInsert = async (state, actions, { values, formikBag: { props, ...formikBag } }) => {
    const fileUpload = async () => {
        const formData = new FormData()
        formData.append('document', values.file)
        const uploadedFile = await fetch(`${process.env.REACT_APP_API_URL}/uploadFile/bm`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
            body: formData
        }).then(r => r.json())
        return uploadedFile
    }

    if (!props.payload) {
        if (!values.file) {
            return formikBag.setFieldError('file', "File Needs To Be Uploaded")
        }
        const uploadFile = await fileUpload();
        const insertData = await actions.getData({
            spName: "SP_Insert_Update_Delete_Break_Down_Maint_Documents",
            payload: {
                Document_Srno: props.payload ? props.payload.data.Document_srno : 0,
                Eqp_ID: props.documentData && props.documentData[4] ? props.documentData[4][0].Eqp_ID : null,
                BD_SrNo: props.documentData && props.documentData[4] ? props.documentData[4][0].bd_srno : null,
                Document_Name: values.file.name,
                Document_Description: values.description,
                Document_Uploaded_by: values.uploadedBy,
                Uploaded_Date: values.uploadedDate,
                Document_Data: uploadFile.location,
                Flag: "I"
            },
            key: "SP_Insert_Update_Delete_Break_Down_Maint_Documents" + new Date().toISOString()
        })
        actions.snackBarHandleOpen({
            message: "Document created"
        })

    } else {
        console.log(props)

        if (values.file) {
            //edit payload when different file is uploaded
            const uploadFile = await fileUpload();
            const insertData = await actions.getData({
                spName: "SP_Insert_Update_Delete_Break_Down_Maint_Documents",
                payload: {
                    Document_Srno: props.payload ? props.payload.data.Document_srno : 0,
                    Eqp_ID: props.documentData && props.documentData[4] ? props.documentData[4][0].Eqp_ID : null,
                    BD_SrNo: props.documentData && props.documentData[4] ? props.documentData[4][0].bd_srno : null,
                    Document_Name: values.file.name,
                    Document_Description: values.description,
                    Document_Uploaded_by: values.uploadedBy,
                    Uploaded_Date: values.uploadedDate,
                    Document_Data: uploadFile.location,
                    Flag: "U"
                },
                key: "SP_Insert_Update_Delete_Break_Down_Maint_Documents" + new Date().toISOString()
            })
            actions.snackBarHandleOpen({
                message: "Document updated"
            })
        } else {
            //edit when no file is uploaded
            const editData = await actions.getData({
                spName: "SP_Insert_Update_Delete_Break_Down_Maint_Documents",
                payload: {
                    Document_Srno: props.payload ? props.payload.data.Document_srno : 0,
                    Eqp_ID: props.documentData && props.documentData[4] ? props.documentData[4][0].Eqp_ID : null,
                    BD_SrNo: props.documentData && props.documentData[4] ? props.documentData[4][0].bd_srno : null,
                    Document_Name: props.payload.data.Document_Name,
                    Document_Description: values.description,
                    Document_Uploaded_by: values.uploadedBy,
                    Uploaded_Date: values.uploadedDate,
                    Document_Data: props.payload.data.Document_Data,
                    Flag: "U"
                },
                key: "SP_Insert_Update_Delete_Break_Down_Maint_Documents" + new Date().toISOString()
            })
            actions.snackBarHandleOpen({
                message: "Document updated"
            })
        }


    }
    await actions.getData({
        spName: "SP_Select_All_From_Break_Down_Maint_Documents_By_Eqp_ID_And_BD_SrNo",
        payload: {
            Eqp_ID: props.documentData && props.documentData[4] ? props.documentData[4][0].Eqp_ID : null,
            BD_SrNo: props.documentData && props.documentData[4] ? props.documentData[4][0].bd_srno : null,
        },
        key: "PENDING_BREAKDOWN_DOCUMENT_DATA"
    })

    await actions.closeDocument({ page: 'DOCUMENTS_PAGE_DIALOG' })

    return ({
    })
}

const percentagedowntimedata = async (state, actions, { from_date, to_date }) => {
    const percentagedowntimedata = (await getData({
        spName: "SP_Rpt_BM_Trends_For_Breakdown_Percentage_By_From_Dt_To_Date_Equipment_IDS",
        payload: {
            From_Date: from_date,
            To_Date: to_date
        }
    })).data
    return ({
        percentagedowntimedata
    })
}

const mtbftrenddata = async (state, actions, { from_date, to_date }) => {
    const mtbftrenddata = (await getData({
        spName: "SP_Rpt_BM_Trends_For_Period_Breakdown_MTBF_By_From_Date_To_Date",
        payload: {
            From_Date: from_date,
            To_Date: to_date
        }
    })).data
    return ({
        mtbftrenddata
    })
}

const mttrtrenddata = async (state, actions, { from_date, to_date }) => {
    const mttrtrenddata = (await getData({
        spName: "SP_Rpt_BM_Trends_For_Period_Breakdown_MTTR_By_From_Date_To_Date",
        payload: {
            From_Date: from_date,
            To_Date: to_date
        }
    })).data
    return ({
        mttrtrenddata
    })
}

const highdowntimedata = async (state, actions, { from_date, to_date }) => {
    const highdowntimedata = (await getData({
        spName: "SP_Rpt_BM_Down_Time_Failuer_Top_Ten_Machines_By_From_Dt",
        payload: {
            From_Date: from_date,
            To_Date: to_date
        }
    })).data
    return ({
        highdowntimedata
    })
}

const highdowntimefailureareadata = async (state, actions, { from_date, to_date }) => {
    const highdowntimefailureareadata = (await getData({
        spName: "SP_Rpt_BM_Down_Time_Failuer_Top_Five_By_From_Dt_Area_ID",
        payload: {
            From_Date: from_date,
            To_Date: to_date
        }
    })).data
    return ({
        highdowntimefailureareadata
    })
}

const toptendowntimedata = async (state, actions, { from_date, to_date }) => {
    const toptendowntimedata = (await getData({
        spName: "SP_Rpt_BM_Down_Time_Failuer_Top_Ten_Machines_By_From_Dt",
        payload: {
            From_Date: from_date,
            To_Date: to_date
        }
    })).data
    return ({
        toptendowntimedata
    })
}

const numberofbreakdownsdata = async (state, actions, { from_date, to_date }) => {
    const numberofbreakdownsdata = (await getData({
        spName: "SP_Get_Breakdowns_By_Day",
        payload: {
            From_Date: from_date,
            To_Date: to_date
        }
    })).data
    return ({
        numberofbreakdownsdata
    })
}

const totaldowntimedata = async (state, actions, { from_date, to_date }) => {
    const totaldowntimedata = (await getData({
        spName: "SP_Rpt_BM_Maintenance_KRA_Power_Cost_Vs_Sales_By_From_Dt_To_Date",
        payload: {
            From_Date: from_date,
            To_Date: to_date
        }
    })).data
    return ({
        totaldowntimedata
    })
}

const percentofpowercostdata = async (state, actions, { from_date, to_date }) => {
    const percentofpowercostdata = (await getData({
        spName: "SP_Rpt_BM_Maintenance_KRA_Total_Maintenance_Cost_Vs_Sales_By_From_Dt_To_Date",
        payload: {
            From_Date: from_date,
            To_Date: to_date
        }
    })).data
    return ({
        percentofpowercostdata
    })
}

const totalmaintenancecostdata = async (state, actions, { from_date, to_date }) => {
    const totalmaintenancecostdata = (await getData({
        spName: "SP_Rpt_BM_Maintenance_KRA_Total_Maintenance_Cost_Vs_Sales_By_From_Dt_To_Date",
        payload: {
            From_Date: from_date,
            To_Date: to_date
        }
    })).data
    return ({
        totalmaintenancecostdata
    })
}

const RCADonedata = async (state, actions, { from_date, to_date }) => {
    const RCADonedata = (await getData({
        spName: "SP_rpt_BM_RCA_Pending",
        payload: {
            bdm_cbm_flag: 'cbm'
        }
    })).data
    return ({
        RCADonedata
    })
}

const maintenancekaizendata = async (state, actions, { from_date, to_date }) => {
    const maintenancekaizendata = (await getData({
        spName: "SP_rpt_Select_All_From_Maintenance_Kaizen_Done",
        payload: {
            From_Date: from_date,
            To_Date: to_date
        }
    })).data
    return ({
        maintenancekaizendata
    })
}

const isLoggedIn = async (state, actions, args) => {
    await actions.setState({
        isLoggedIn: {
            loading: true,
            isLoggedIn: false,
        }
    })
    if (!localStorage.getItem("token")) {
        return {
            isLoggedIn: {
                loading: false,
                isLoggedIn: false
            }
        }
    }
    actions.setState({
        isLoggedIn: {
            loading: false,
            isLoggedIn: true
        }
    })
    try {
        const loggedInData = await fetch(`${process.env.REACT_APP_API_URL}/is-logged-in`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json",
            },
            method: "POST"
        }).then(r => {
            if (r.ok) {
                return {}
            } else {
                throw {
                    "message": "unauthenticated"
                }
            }
        })
        return {
            isLoggedIn: {
                loading: false,
                isLoggedIn: true,
            }
        }
    } catch (e) {
        return {
            isLoggedIn: {
                loading: false,
                isLoggedIn: false,
                error: e
            }
        }
    }
}

const actions = {
    setState: (state, actions, newState) => {
        return newState
    },
    isLoggedIn,
    createNewMonth,
    downloadReport,
    snackBarHandleClose,
    snackBarHandleOpen,
    getReport,
    documentInsert,
    getDatas: async (state, actions, sps) => {
        // console.log
        const datas = await Promise.all(sps.map(({ key, spName, payload }) => fetch(`${process.env.REACT_APP_API_URL}/sp/${spName}`, {
            body: JSON.stringify(payload),
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json"
            },
            method: "POST"
        }).then(async (r) => ({ [key ? key : spName]: await r.json() }))))
        // console.log(newState)
        return datas.reduce((memo, value) => Object.assign(memo, value), {})
    },
    getData: async (state, actions, { spName, payload, key }) => {
        return {
            [key ? key : spName]: await getData({ spName, payload })
        }
    },
    handleBreakdownSubmit: async (state, actions, { values, formActions }) => {
        console.dir(formActions)
        console.dir(values)
        const props = formActions.props
        const onCreate = await getData({
            spName: "SP_Insert_Update_Delete_Break_Down_Maint_Output",
            payload: {
                bd_srno: null,
                Eqp_ID: values.equipment,
                bd_det: values.breakdownDetails,
                bd_date: moment(values.breakdownDate).toISOString(),
                bd_time: moment(values.breakdownTime).toISOString(),
                prod_dntime: 0,
                agency: "",
                coord_per: "",
                comp_date: moment().toISOString(),
                comp_time: moment().toISOString(),
                notes: "",
                area_id: "",
                failure_id: "",
                opportunity_loss: 2,
                nature: "",
                Flag: "I",
                reported_By: values.reportedby,
                con_cost: 0.0,
                Line: "",
                emp_id: "",
                eqp_unit: "",
                entered_by: values.enteredBy,
                bd_approval_status: "",
                bd_approval_date: moment().toISOString(),
                bd_approval_name: "",
                assign_to_id1: '',
                assign_to_id2: "",
                assign_to_id3: "",
                assign_to_id4: "",
                assembly_description: "",
                sub_assembly: "",
                Phy_Pheno_Desc: values.breakdownPhenomenon,
                Analysis_rqrd: "",
                prod_code: values.productionStatus
            }
        })
        console.log("onCreate", onCreate.data[0].bd_srno)
        formActions.setStatus({
            showActions: true
        })
        const data = await getData({
            spName: "sp_Select_All_From_Breck_Down_Maint_By_Eqp_ID",
            payload: {
                Eqp_ID: values.equipment,
                bd_srno: onCreate.data[0].bd_srno
            },
        })
        return {
            PENDING_BREAKDOWN_ONCREATE_DATA: { ...data.data[0], ...onCreate.data[0].bd_srno }
        }
    },
    handleSignOut: (_s, _a, { history }) => {
        localStorage.removeItem('token');
        history.replace('/signin')
        return {
            user: null
        }
    },

    resetUserLogin: async (state, { getData }, { values, formActions }) => {
        return {
            handleSignIn: {
                error: null,
                username: null,
                user_id: null
            },
        }
    },

    handlePassword: async (state, { getData, ...actions }, { values, formActions }) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_API_URL}/login`, {
                body: JSON.stringify({
                    "userId": state.user_ID,
                    "password": values.password
                }),
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST"
            }).then(r => r.json())
            console.log('passs1111', values)
            if (!res.token) { throw "Wrong Password" }
            else {
                console.log('pass22222', formActions)
                const { payload: { plantId } } = jwtDecode(res.token)
                localStorage.setItem("token", res.token)
                await actions.isLoggedIn()
                formActions.props.history.replace("/")
                return {
                    handleSignIn: {
                        error: null,
                        username: null,
                        user_id: null
                    },
                    user: { ...res, plantId }
                }
            }
        } catch (e) {
            console.error(e)
            formActions.setFieldError('password', "Wrong password")
        }
    },

    handleSignIn: async (state, { getData }, { values, formActions }) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_API_URL}/verify-user`, {
                body: JSON.stringify({
                    "userId": values.username
                }),
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST"
            }).then(r => r.json())
            state.user_ID = res.recordset[0].User_ID
            return {
                handleSignIn: {
                    error: null,
                    username: res.recordset[0].User_Name,
                    user_id: res.recordset[0].User_ID
                }
            }
        } catch (e) {
            console.error(e)
            formActions.setFieldError('username', "Does Not Exist")
        }

    },
    createRepetitiveBreakdown,
    closeDocument: closeDialog,

    handleMtbfMttrDialog: openDialog,

    closeMtbfMttr: closeDialog,

    handleDocumentDialog: openDialog,

    closeManpower: closeDialog,

    handleManpowerDialog: openDialog,

    closeSpareUsed: closeDialog,

    handleSpareUsedDialog: openDialog,

    closeWhyWhyAnalysis: closeDialog,
    closeDialog,
    openDialog,
    handleWhyWhyAnalysisDialog: openDialog,
    percentagedowntimedata,
    mtbftrenddata,
    mttrtrenddata,
    highdowntimedata,
    highdowntimefailureareadata,
    toptendowntimedata,
    numberofbreakdownsdata,
    totaldowntimedata,
    percentofpowercostdata,
    totalmaintenancecostdata,
    RCADonedata,
    maintenancekaizendata,
    increment: ({ count }) => ({ count: count + 1 }),
}

export default actions;

