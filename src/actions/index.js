import React from "react"
import createStore from 'react-waterfall'
import actionsCreators from "./actions"
import initialState from "./initState";

const config = {
    initialState,
    actionsCreators,
}

export const { Provider, connect, actions } = createStore(config)

export const Consumer = connect((state) => (state))(({ children: Child, ...props }) => {
    return (<Child {...props} />)
});