const init = {
    count: 0,
    departments: null,
    handleSignIn: false,
    user_ID: "",
    REPORTING_BREAKDOWN_DOCUMENT_POPUP: { isOpen: false, payload: null },
    PENDING_BREAKDOWN_DOCUMENT: { isOpen: false, payload: null },
    PENDING_BREAKDOWN_MANPOWER: { isOpen: false, payload: null },
    PENDING_BREAKDOWN_SPARESUSED: { isOpen: false, payload: null },
    EQUIPMENTWISE_BREAKDOWN_DOCUMENT: { isOpen: false, payload: null },
    EQUIPMENTWISE_BREAKDOWN_MANPOWER: { isOpen: false, payload: null },
    EQUIPMENTWISE_BREAKDOWN_SPARESUSED: { isOpen: false, payload: null },
    REPETITIVE_BREAKDOWN_DOCUMENT: { isOpen: false, payload: null },
    REPETITIVE_BREAKDOWN_MANPOWER: { isOpen: false, payload: null },
    REPETITIVE_BREAKDOWN_SPARESUSED: { isOpen: false, payload: null },
    PENDING_BREAKDOWN_MANPOWER_DIALOG: {
        isOpen: false, payload: null
    },
    PENDING_BREAKDOWN_SPARESUSED_DIALOG: {
        isOpen: false, payload: null
    },
    DOCUMENTS_PAGE_DIALOG: {
        isOpen: false,
        payload: null
    },
    WHY_WHY_ANALYSIS: { isOpen: false, payload: null },
    REPETITIVE_DATA_DIALOG: {
        isOpen: false,
        payload: null,
    },
    SNACKBAR: {
        isOpen: false,
        message: ''
    },
    MTBFMTTR_PAGE_DIALOG: {
        isOpen: false,
        payload: null
    },
    MONTH_PAGE_DIALOG: {
        isOpen: false,
        payload: null
    },
    REPETITIVE_BREAKDOWN_VIEW_DIALOG: {
        isOpen: false,
        payload: null
    },
    isLoggedIn: {
        loading: true,
        isLoggedIn: false,
    }
}
export default init