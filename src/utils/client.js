import { ApolloClient } from 'apollo-client';
import { withClientState } from 'apollo-link-state';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { defaults, resolvers, typeDefs } from "./../services/index"
import { RestLink } from 'apollo-link-rest';
import { ApolloLink, } from "apollo-boost";
import { setContext } from 'apollo-link-context';
import apolloLogger from 'apollo-link-logger';
import { persistCache } from 'apollo-cache-persist';
const authLink = setContext((_, { headers }) => {
    return {
        headers: {
            ...headers,
            authorization: "Bearer " + localStorage.getItem('token'),
        }
    }
});

const link = ApolloLink.from([
    apolloLogger,
    authLink,
    withClientState({ resolvers, defaults, cache, typeDefs }),
    new RestLink({ uri: process.env.REACT_APP_API_URL })
]);

const cache = new InMemoryCache();

persistCache({
    cache,
    storage: window.localStorage,
});

const client = new ApolloClient({
    cache,
    link,
});

// client.query({
//     query: AVAILABILITY_QUERY,
// })

export default client;

