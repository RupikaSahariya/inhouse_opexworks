import React from "react";
import { Redirect } from "react-router-dom";
import { actions, connect } from "./../../actions"

export default function (ComposedComponent) {
    class Authenticate extends React.PureComponent {
        constructor(props) {
            super(props);
            this.state = {
                loading: true,
                user: null
            };
        }
        async componentDidMount() {
            actions.isLoggedIn()
        }

        render() {
            const token = localStorage.getItem("token")
            console.log(this.props)
            if (this.props.isLoggedIn.loading) {
                return <h1>Loading</h1>
            }
            if (!this.props.isLoggedIn.isLoggedIn) {
                return <Redirect to="/signin" />;
            }
            return <ComposedComponent  {...this.props} />;

            // return (<Query
            //     query={GET_USER}>
            //     {({ loading, error, data: { user } }) => {
            //         if (loading) return null;
            //         if (error) return `Error!: ${error}`;
            //         if (user && user.token) {
            //            
            //         }

            //         
            //     }}
            // </Query>)
        }
    }
    return connect(({ isLoggedIn }) => ({ isLoggedIn }))(Authenticate);
}
