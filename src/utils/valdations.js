import * as Yup from "yup"
import moment from "moment"

export const VALID_YEAR = Yup.string().test(
    'VALID_YEAR',"Invalid Year",(value)=>{
        if(moment(value,"YYYY",true).isValid()){
            console.log(value)
            return true
        }
    return false
    }
)