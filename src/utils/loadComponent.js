import React from 'react'
import Loadable from 'react-loadable';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
const Loading = (props) => {
    return <Grid container
        direction="row"
        justify="center"
        alignItems="center"
        style={{
            alignContent: 'center',
            justifyContent: 'center',
            height: window.innerHeight
        }}
    >
        <CircularProgress />
        {props.error && <Typography variant="subheading">Error!</Typography>}
        {/* {props.error && <Typography variant="subheading">Error! <button onClick={() => window.location.reload()}>Retry</button></Typography >}
        {props.timedOut && <Typography variant="subheading">Taking a long time... <button onClick={() => window.location.reload()}>Retry</button></Typography >} */}
    </Grid>
}
setTimeout(() => {
    console.log("loading chunks")
    Loadable.preloadAll()
}, 500)
export default function MyLoadable(opts) {
    return Loadable(Object.assign({
        loading: () => (<Loading />),
        delay: 200,
        timeout: 10000,
    }, opts));
};