import React, { Fragment } from 'react';

const AppContext = React.createContext();

export const dataProvider = settings => (HocComponent) => (props) => {
    class WithData extends React.PureComponent {
        constructor(props) {
            super(props)
            this.state = {
                loading: true,
                success: false,
                data: false
            }
        }
        async componentDidMount() {

            if (settings.queries) {
                this.queries = await settings.queries(this.props)
                const datas = await Promise.all(this.queries.map(query => this.props.getData(query)))
                this.setState({
                    "data": datas.map(data => data.data),
                    loading: false,
                    success: true
                })
            } else {
                this.query = await settings.query(this.props)
                const data = await this.props.getData(this.query)
                this.setState({
                    [this.query.key || "data"]: data.data,
                    loading: false,
                    success: true
                })
            }



        }
        render() {
            return <HocComponent {...this.props} {...this.state} />
        }
    }
    return (
        <Fragment>
            <AppContext.Consumer>
                {(context) => {
                    return (<WithData {...context} {...props} />)
                }}
            </AppContext.Consumer>
        </Fragment>
    )
}

export default AppContext;