import gql from "graphql-tag"

export const AVAILABILITY_QUERY = gql`
query apiInteraction($body:any) {
    AvailabilityData(body: $body) @rest(
      type: "AvailabilityData", 
      path:"/data/SP_App_Get_No_of_Avilability_Performance_Quality_OEE_By_Day" 
      bodyKey: "body"  
      method: "POST"
    ) {
      recordset @type(name:"DashboardData"){
          OEE,
          P,
          A,
          LT,
          AT,
          Date
      }
  }
}
`;

export const GET_USER = gql`
query getUser{
    user @client{
        token
        plant_id
    }
}
`

