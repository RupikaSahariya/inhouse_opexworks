// Icons
import ExploreIcon from '@material-ui/icons/Explore';

// Pages
import {
  Home,
  // Departments,
  // Cells,
  // Equipments,
  // EmployeeDesignation,
  // Employees,
  // LossesInOperation,
  // Suppliers,
  // Customers,
  // Products,
  // IncomingParts,
  // EventsForAlerts,
  // DeleteInspectionData,
  // DeleteParts,
  // DeleteSuppliers,
  // Configuration,
  InwardInspection,
  FetchPDI,
  RaiseConcern,
  KRA,
  PendingBreakdown,
  EquipmentwiseBreakdown,
  RepetitiveBreakdown,
  BreakdownReporting,
  Documents,
  ManPower,
  SparesConsumed,
  WhyWhyAnalysis,
  MtbfAndMttrAnalysis,
  ReportedBreakdowns,
  PendingBreakdowns,
  MTBFAndMTTR,
  BreakdownHistory,
  PhenomenonWiseBreakdown,
  BreakdownRootCauseAnalysisPending,
  WaterCostvsSales,
  PowerCostvsSales,
  TotalMaintenanceCostvsSales,
  TotalBreakdownCostvsSales,
  Downtime,
  MTBF,
  MTTR,
  FailureAreaWise,
  PhysicalPhenomenonWise,
  EquipmentWise,

  Calendar,
  Chat,
  Media,
  Messages,
  Social,
  Blank,
  Invoice,
  PricingPage,
  TimelinePage,
  Charts,
  Taskboard,
  Widgets,
  Lockscreen,
  PasswordReset,
  Signin,
  Signup,
  Leaflet,
  Google,
  Detail,
  Editor,
  NotFound,
  BackendError,
  // Material Examples
  AppBar,
  Autocomplete,
  Avatars,
  Badges,
  ButtonNavigation,
  Buttons,
  Cards,
  Chips,
  Dialogs,
  Dividers,
  Drawers,
  ExpansionPanels,
  GridList,
  Lists,
  Menus,
  Paper,
  Pickers,
  Progress,
  SelectionControls,
  Selects,
  Snackbars,
  Steppers,
  Tables,
  Tabs,
  TextFields,
  Tooltips
} from './../pages';
import WhyWhyAnalysisRenderer from "../components/AgGridRenders/whyWhyAnalysisRenderer";

export default {
  items: [{
    path: '/',
    name: 'Home',
    type: 'link',
    icon: ExploreIcon,
    component: Home
  },
  // {
  //   path: '/master',
  //   name: 'Master',
  //   type: 'submenu',
  //   icon: ExploreIcon,
  //   // component: ()=> <div> <h2>Master Page</h2></div>,
  //   children: [{
  //     path: '/departments',
  //     name: 'Departments',
  //     component: Departments
  //   },
  //   {
  //     path: '/cells',
  //     name: 'Cells',
  //     component: Cells
  //   },
  //   {
  //     path: '/equipments',
  //     name: 'Equipments',
  //     component: Equipments
  //   },
  //   {
  //     path: '/employeedesignation',
  //     name: 'Employee Designation',
  //     component: EmployeeDesignation
  //   },
  //   {
  //     path: '/employees',
  //     name: 'Employees',
  //     component: Employees
  //   },
  //   {
  //     path: '/lossesinoperation',
  //     name: 'Losses In Operation',
  //     component: LossesInOperation
  //   },
  //   {
  //     path: '/suppliers',
  //     name: 'Suppliers',
  //     component: Suppliers
  //   },
  //   {
  //     path: '/customers',
  //     name: 'Customers',
  //     component: Customers
  //   },
  //   {
  //     path: '/products',
  //     name: 'Products',
  //     component: Products
  //   },
  //   {
  //     path: '/incomingparts',
  //     name: 'Incoming Parts',
  //     component: IncomingParts
  //   },
  //   {
  //     path: '/eventsforalerts',
  //     name: 'Events For Alerts',
  //     component: EventsForAlerts
  //   },
  //   {
  //     path: '/utility',
  //     name: 'Utility',
  //     type: 'submenu',
  //     icon: AppsIcon,
  //     badge: {
  //       type: 'secondary',
  //       value: '3'
  //     },
  //     children: [{
  //       path: '/deleteinspectiondata',
  //       name: 'Delete Inspection Data',
  //       component: DeleteInspectionData
  //     },
  //     {
  //       path: '/deleteparts',
  //       name: 'Delete Parts',
  //       component: DeleteParts
  //     },
  //     {
  //       path: '/deletesupplier',
  //       name: 'Delete Supplier',
  //       component: DeleteSuppliers
  //     }
  //     ],
  //   },
  //   {
  //     path: '/configuration',
  //     name: 'Configuration',
  //     component: Configuration
  //   }
  //   ]
  // },
  {
    path: '/breakdownmaintenance',
    name: 'Breakdown Maintenance',
    type: 'submenu',
    icon: ExploreIcon,
    // component: ()=> <div> <h2>Master Page</h2></div>,
    children: [{
      path: '/pendingbreakdown',
      name: 'Pending Breakdown',
      component: PendingBreakdown
    },
    {
      path: '/equipmentwiseBreakdown',
      name: 'Equipmentwise Breakdown',
      component: EquipmentwiseBreakdown
    },
    {
      path: '/repetitiveBreakdown',
      name: 'Repetitive Breakdown',
      component: RepetitiveBreakdown
    },
    {
      path: '/breakdownReporting',
      name: 'Breakdown Reporting',
      component: BreakdownReporting
    },
    // {
    //   path: '/documents',
    //   name: 'Documents',
    //   component: Documents
    // },
    // {
    //   path: '/manpower',
    //   name: 'Add Manpower',
    //   component: ManPower
    // },
    // {
    //   path: '/sparesConsumed',
    //   name: 'Spares Consumed',
    //   component: SparesConsumed
    // },
    // {
    //   path: '/whywhyanalysis',
    //   name: 'Why Why Analysis',
    //   component: WhyWhyAnalysis
    // },
    ]
  },
  {
    path: '/MtbfAndMttrAnalysis',
    name: 'MTBF and MTTR Analysis',
    type: 'link',
    icon: ExploreIcon,
    component: MtbfAndMttrAnalysis
  },
  {
    path: '/reports',
    name: 'Reports',
    type: 'submenu',
    icon: ExploreIcon,
    children: [{
      path: '/reportedbreakdowns',
      name: 'Reported Breakdowns',
      component: ReportedBreakdowns
    },
    {
      path: '/pendingbreakdowns',
      name: 'Pending Breakdowns',
      component: PendingBreakdowns
    },
    {
      path: '/mtbfandmttr',
      name: 'MTBF And MTTR',
      component: MTBFAndMTTR
    },
    {
      path: '/breakdownhistory',
      name: 'Breakdown History',
      component: BreakdownHistory
    },
    {
      path: '/PhenomenonWiseBreakdown',
      name: 'Phenomenon wise Breakdown',
      component: PhenomenonWiseBreakdown
    },
    {
      path: '/breakdownrootcauseanalysispending',
      name: 'Breakdown Root Cause Analysis Pending',
      component: BreakdownRootCauseAnalysisPending
    }
    ]
  },
  {
    path: '/graph',
    name: 'Graph',
    type: 'submenu',
    icon: ExploreIcon,
    children: [{
      path: '/watercostvssales',
      name: 'Maitenance KRA - Water Cost vs Sales',
      component: WaterCostvsSales
    },
    {
      path: '/powercostvssales',
      name: 'Maitenance KRA - Power Cost vs Sales',
      component: PowerCostvsSales
    },
    {
      path: '/totalmaintenancecostvssales',
      name: 'Maitenance KRA - Total Maintenance Cost vs Sales',
      component: TotalMaintenanceCostvsSales
    },
    {
      path: '/totalbreakdowncostvssales',
      name: 'Maitenance KRA - Breakdown Cost vs Sales',
      component: TotalBreakdownCostvsSales
    },
    {
      path: '/downtime',
      name: 'Down Time Trend %',
      component: Downtime
    },
    {
      path: '/mtbf',
      name: 'MTBF Trend',
      component: MTBF
    },
    {
      path: '/mttr',
      name: 'MTTR Trend',
      component: MTTR
    },
    {
      path: '/failureareawise',
      name: 'Failure Area Wise Down Time',
      component: FailureAreaWise
    },
    {
      path: '/physicalphenomenonwise',
      name: 'Physical Phenomenon Wise Down Time',
      component: PhysicalPhenomenonWise
    },
    {
      path: '/equipmentwise',
      name: 'Equipment Wise Down Time',
      component: EquipmentWise
    },
    ]
  },
  // {
  //   path: '/inwardInspection',
  //   name: 'Inward Inspection',
  //   icon: ExploreIcon,
  //   component: () => <div><h2> Inward Inspection </h2></div>
  // },
  // {
  //   path: '/fetchPDI',
  //   name: 'Fetch PDI',
  //   icon: ExploreIcon,
  //   component: () => <div><h2> Fetch PDI </h2></div>
  // },
  // {
  //   path: '/raiseConcern',
  //   name: 'Raise Concern',
  //   type: 'submenu',
  //   icon: ExploreIcon,
  //   // component: () => <div><h2> Raise Concern </h2></div>
  //   children: [
  //     {
  //       path: '/overall-ppm-monthly',
  //       name: 'Overall PPM Monthly',
  //       component: () => <div><h2> Overall PPM Monthly </h2></div>
  //     },
  //     {
  //       path: '/overall-ppm-daily',
  //       name: 'Overall PPM Daily',
  //       component: () => <div><h2> Overall PPM Daily </h2></div>
  //     },
  //     {
  //       path: '/supplierwisePPMmonthly',
  //       name: 'supplierwisePPMmonthly',
  //       component: () => <div><h2> supplierwise PPM monthly </h2></div>
  //     },
  //     {
  //       path: '/supplierwisePPMdaily',
  //       name: 'supplierwisePPMdaily',
  //       component: () => <div><h2> supplierwise PPM daily </h2></div>
  //     },
  //     {
  //       path: '/partwisePPM',
  //       name: 'Raise partwisePPM',
  //       component: () => <div><h2> partwise PPM </h2></div>
  //     },
  //     {
  //       path: '/top10supplierswithhighppm',
  //       name: 'top10supplierswithhighppm',
  //       component: () => <div><h2> top 10 suppliers with high PPM </h2></div>
  //     },
  //     {
  //       path: '/top10partswithhighppm',
  //       name: 'top10partswithhighppm',
  //       component: () => <div><h2> Top 10 Parts With High PPM </h2></div>
  //     },
  //   ]
  // }
    // {
    //   path: '/apps',
    //   name: 'Apps',
    //   type: 'submenu',
    //   icon: AppsIcon,
    //   badge: {
    //     type: 'primary',
    //     value: '5'
    //   },
    //   children: [{
    //       path: '/calendar',
    //       name: 'Calendar',
    //       component: Calendar
    //     },
    //     {
    //       path: '/media',
    //       name: 'Media',
    //       component: Media
    //     },
    //     {
    //       path: '/messages',
    //       name: 'Messages',
    //       component: Messages
    //     },
    //     {
    //       path: '/social',
    //       name: 'Social',
    //       component: Social
    //     },
    //     {
    //       path: '/chat',
    //       name: 'Chat',
    //       component: Chat
    //     }
    //   ]
    // },
    // {
    //   path: '/widgets',
    //   name: 'Widgets',
    //   type: 'link',
    //   icon: PhotoIcon,
    //   component: Widgets
    // },
    // {
    //   path: '/material',
    //   name: 'Material',
    //   type: 'submenu',
    //   icon: EqualizerIcon,
    //   badge: {
    //     type: 'error',
    //     value: '10'
    //   },
    //   children: [{
    //       path: '/appbar',
    //       name: 'App Bar',
    //       component: AppBar
    //     },
    //     {
    //       path: '/autocomplete',
    //       name: 'Auto Complete',
    //       component: Autocomplete
    //     },
    //     {
    //       path: '/avatars',
    //       name: 'Avatars',
    //       component: Avatars
    //     },
    //     {
    //       path: '/badges',
    //       name: 'Badges',
    //       component: Badges
    //     },
    //     {
    //       path: '/button-navigation',
    //       name: 'Button Navigation',
    //       component: ButtonNavigation
    //     },
    //     {
    //       path: '/buttons',
    //       name: 'Buttons',
    //       component: Buttons
    //     },
    //     {
    //       path: '/cards',
    //       name: 'Cards',
    //       component: Cards
    //     },
    //     {
    //       path: '/chips',
    //       name: 'Chips',
    //       component: Chips
    //     },
    //     {
    //       path: '/dialogs',
    //       name: 'Dialogs',
    //       component: Dialogs
    //     },
    //     {
    //       path: '/dividers',
    //       name: 'Dividers',
    //       component: Dividers
    //     },
    //     {
    //       path: '/drawers',
    //       name: 'Drawers',
    //       component: Drawers
    //     },
    //     {
    //       path: '/expansion-panels',
    //       name: 'Expansion Panels',
    //       component: ExpansionPanels
    //     },
    //     {
    //       path: '/grid-list',
    //       name: 'Grid List',
    //       component: GridList
    //     },
    //     {
    //       path: '/lists',
    //       name: 'Lists',
    //       component: Lists
    //     },
    //     {
    //       path: '/menus',
    //       name: 'Menus',
    //       component: Menus
    //     },
    //     {
    //       path: '/paper',
    //       name: 'Paper',
    //       component: Paper
    //     },
    //     {
    //       path: '/pickers',
    //       name: 'Pickers',
    //       component: Pickers
    //     },
    //     {
    //       path: '/progress',
    //       name: 'Progress',
    //       component: Progress
    //     },
    //     {
    //       path: '/selection-controls',
    //       name: 'Selection Controls',
    //       component: SelectionControls
    //     },
    //     {
    //       path: '/selects',
    //       name: 'Selects',
    //       component: Selects
    //     },
    //     {
    //       path: '/snackbars',
    //       name: 'Snackbars',
    //       component: Snackbars
    //     },
    //     {
    //       path: '/steppers',
    //       name: 'Steppers',
    //       component: Steppers
    //     },
    //     {
    //       path: '/tables',
    //       name: 'Tables',
    //       component: Tables
    //     },
    //     {
    //       path: '/tabs',
    //       name: 'Tabs',
    //       component: Tabs
    //     },
    //     {
    //       path: '/text-fields',
    //       name: 'Text Fields',
    //       component: TextFields
    //     },
    //     {
    //       path: '/tooltips',
    //       name: 'Tooltips',
    //       component: Tooltips
    //     }
    //   ]
    // },
    // {
    //   path: '/editor',
    //   name: 'Editor',
    //   type: 'link',
    //   icon: Looks3Icon,
    //   component: Editor
    // },
    // {
    //   path: '/ecommerce',
    //   name: 'Ecommerce',
    //   type: 'submenu',
    //   icon: Looks4Icon,
    //   badge: {
    //     type: 'secondary',
    //     value: 'N'
    //   },
    //   children: [{
    //       path: '/products',
    //       name: 'Products',
    //       component: Products
    //     },
    //     {
    //       path: '/detail',
    //       name: 'Detail',
    //       component: Detail
    //     },
    //   ]
    // },
    // {
    //   path: '/taskboard',
    //   name: 'Taskboard',
    //   type: 'link',
    //   icon: ViewColumnIcon,
    //   component: Taskboard
    // },
    // {
    //   path: '/charts',
    //   name: 'Charts',
    //   type: 'link',
    //   icon: ShowChartIcon,
    //   component: Charts
    // },
    // {
    //   path: '/maps',
    //   name: 'Maps',
    //   type: 'submenu',
    //   icon: NavigationIcon,
    //   children: [{
    //       path: '/google',
    //       name: 'Google',
    //       component: Google
    //     },
    //     {
    //       path: '/leaflet',
    //       name: 'Leaflet',
    //       component: Leaflet
    //     }
    //   ]
    // },
    // {
    //   path: '/pages',
    //   name: 'Pages',
    //   type: 'submenu',
    //   icon: PagesIcon,
    //   children: [{
    //       path: '/invoice',
    //       name: 'Invoice',
    //       component: Invoice
    //     },
    //     {
    //       path: '/timeline',
    //       name: 'Timeline',
    //       component: TimelinePage
    //     },
    //     {
    //       path: '/blank',
    //       name: 'Blank',
    //       component: Blank
    //     },
    //     {
    //       path: '/pricing',
    //       name: 'Pricing',
    //       component: PricingPage
    //     },
    //   ]
    // },
    // {
    //   name: 'Authentication',
    //   type: 'submenu',
    //   icon: PersonIcon,
    //   children: [{
    //       path: '/signin',
    //       name: 'Signin',
    //       component: Signin
    //     },
    //     {
    //       path: '/signup',
    //       name: 'Signup',
    //       component: Signup
    //     },
    //     {
    //       path: '/forgot',
    //       name: 'Forgot',
    //       component: PasswordReset
    //     },
    //     {
    //       path: '/lockscreen',
    //       name: 'Lockscreen',
    //       component: Lockscreen
    //     },
    //   ]
    // },
    // {
    //   name: 'Error',
    //   type: 'submenu',
    //   icon: FaceIcon,
    //   children: [{
    //       path: '/404',
    //       name: '404',
    //       component: NotFound
    //     },
    //     {
    //       path: '/500',
    //       name: 'Error',
    //       component: BackendError
    //     },
    //   ]
    // },
    // {
    //   path: 'https://iamnyasha.github.io/react-primer-docs/',
    //   name: 'Documentation',
    //   type: 'external',
    //   icon: LocalLibraryIcon
    // },
    // {
    //   path: 'https://primer.nyasha.me',
    //   name: 'Get Angular Version',
    //   type: 'external',
    //   icon: BookmarkIcon
    // }
  ]
};