const styles = theme => ({
    //header styles
    breakCrumbIcon: {
        marginTop: '3px',
        fontSize: "20px"
    },
    breadCrumbText: {
        color: "white",
        fontSize: "15px",
        "font-weight": "normal"
    },
    headerText: {
        color: "white",
        paddingBottom: "4px",
        fontSize: "18px",
    },
    header: {
        "background-color": "#3f51b5",
        color: "white",
    },

    //action style
    dialogStyles: {
        paperWidthSm: '400px',
        textAlign: 'center'
    },
    toolbarIcon: {
        margin: '10px'
    },
    toolbar: {
        margin: '5px',
    },

    //page styles
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        marginLeft: "65px"
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
    card: {
        minWidth: 275,
        "width": "15px",
        "padding": "50px"
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        maxWidth: 400,
        width: "100%"
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
    datepicker: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: "100%",
        maxWidth: 400
    },
    file: {
        marginTop: 20,
        marginBottom: 20
    },
    textField: {
        margin: theme.spacing.unit,
        width: "100%",
        maxWidth: 400
    },
    textField2: {
        margin: theme.spacing.unit,
        width: "100%"
    },
});
export default styles;