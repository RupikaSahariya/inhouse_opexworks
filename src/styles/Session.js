const SessionStyles = theme => ({
  card: {
    overflow: 'visible'
  },
  session: {
    position: 'relative',
    zIndex: 4000,
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column'
  },
  background: {
    backgroundColor: "#fff"
  },
  content: {
    padding: `40px ${theme.spacing.unit}px`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flex: '1 0 auto',
    flexDirection: 'column',
    minHeight: '100%',
    textAlign: 'center'
  },
  wrapper: {
    flex: 'none',
    maxWidth: '430px',
    width: '100%',
    margin: '0 auto',
    "box-shadow": "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
  },
  avatar: {
    position: 'relative',
    display: 'block',
    margin: '-75px auto 0'
  },
  lockscreenWrapper: {
    flex: 'none',
    maxWidth: '280px',
    width: '100%',
    margin: '0 auto',
  },
  title: {
    fontSize: '150px',
    lineHeight: 1.2,
    fontWeight: 100,
    display: 'inline-table',
    position: 'relative',
    background: theme.palette.primary.main,
    color: '#fff',
    padding: `0 ${theme.spacing.unit * 3}px`,
    borderRadius: '60px',
    cursor: 'pointer',
    margin: `0 0 ${theme.spacing.unit}px`,
    '&:after': {
      top: '100%',
      left: '50%',
      border: 'solid transparent',
      content: '""',
      height: 0,
      width: 0,
      position: 'absolute',
      pointerEvents: 'none',
      borderColor: 'rgba(0, 0, 0, 0)',
      borderTopColor: theme.palette.primary.main,
      borderWidth: '8px',
      marginLeft: '-8px'
    }
  },
  subtitle: {
    fontSize: '32px',
    fontWeight: 900
  },
  fullWidth: {
    width: '100%',
  },
  textField: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit * 2
  },
  welcome: {
    backgroundColor: "#3f51b5",
    color: '#fff',
    padding: theme.spacing.unit * 3,
    marginLeft: theme.spacing.unit * -2,
    marginRight: theme.spacing.unit * -2,
    marginTop: theme.spacing.unit * -2,
    marginBottom: theme.spacing.unit * 2,
  },
  tags: {
    backgroundColor: "#3f51b5",
    color: "#fff",
    padding: "5px",
    marginLeft: theme.spacing.unit * -2,
    marginRight: theme.spacing.unit * -2,
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * -3,
  }
});

export default SessionStyles;