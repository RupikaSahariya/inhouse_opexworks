import React from 'react';
import Button from '@material-ui/core/Button';

export default class BdreportRenderer extends React.PureComponent {
    onBdReport = () => {
        this.props.context.onBdReport(this.props.data)
    }

    render() {
        return (
            <span><Button type="submit" color="default">
            B/D Report
        </Button></span>
        );
    }
};
