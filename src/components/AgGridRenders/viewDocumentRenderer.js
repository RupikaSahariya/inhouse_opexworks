import React from 'react';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

export default class ViewDocumentRenderer extends React.PureComponent {
    onViewDocument = () => {
        this.props.context.onViewDocument(this.props.data)
    }

    render() {
        return (
            <div>
                <Tooltip title="View Document">
                    <Button onClick={this.onViewDocument}><FileCopyIcon style={{ "color": "#607d8b" }} /></Button>
                </Tooltip>
            </div>
        );
    }
};
