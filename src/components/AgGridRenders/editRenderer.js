import React from 'react';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

export default class EditRenderer extends React.PureComponent {
    onEdit = () => {
        this.props.context.onEdit(this.props.data)
    }

    onDelete = () => {
        this.props.context.onDelete(this.props.data)
    }

    
    render() {
        return (
            <div>
                <Tooltip title="Edit">
                    <Button onClick={this.onEdit}><EditIcon style={{ "color": "#2196f3" }} /></Button>
                </Tooltip>
                {!this.props.context.dontShowDelete ? <Tooltip title="Delete">
                    <Button onClick={this.onDelete}><DeleteIcon style={{ "color": "#f44336" }} /></Button>
                </Tooltip> : null}
            </div>
        );
    }
};
