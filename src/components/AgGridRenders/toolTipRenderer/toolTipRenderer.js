import React from 'react';
import Tooltip from '@material-ui/core/Tooltip';

export default class ToolTipRenderer extends React.PureComponent {
    render() {
        return (
            <div>
                <Tooltip title={this.props.api.getRowNode(this.props.rowIndex).data[this.props.colDef.toolTipText]}>
                <span>{this.props.api.getRowNode(this.props.rowIndex).data[this.props.colDef.field]}</span>
                </Tooltip>
            </div>
        );
    }
};