import React from 'react';
import ImageSearchIcon from '@material-ui/icons/ImageSearch';
import DescriptionIcon from '@material-ui/icons/Description';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

export default class WhyWhyAnalysisRenderer extends React.PureComponent {
    onWhyWhyAnalysis = () => {
        console.log('hello why',this.props)
        this.props.context.onWhyWhyAnalysis(this.props.data)
    }

    onBdReport = () => {
        this.props.context.onBdReport(this.props.data)
    }

    render() {
        return (
             <div>
                <Tooltip title="B/D Report">
                    <Button onClick={this.onBdReport}><DescriptionIcon style={{ "color": "#ffa726" }} /></Button>
                </Tooltip>
                <Tooltip title="Why Why Analysis">
                    <Button onClick={this.onWhyWhyAnalysis}><ImageSearchIcon style={{ "color": "#009688" }} /></Button>
                </Tooltip>
            </div>
        );
    }
};
