import React from 'react';
import DeleteIcon from '@material-ui/icons/Delete';

export default class DeleteRenderer extends React.PureComponent {
    constructor(props) {
        super(props);

        this.invokeParentMethod = this.invokeParentMethod.bind(this);
    }

    invokeParentMethod() {
        // console.log(this.props.data);
        this.props.context.onDelete(this.props.data);
    }

    render() {
        return (
            <span> <DeleteIcon onClick={this.invokeParentMethod} style={{"color": "#f44336"}}/></span>
        );
    }
};
