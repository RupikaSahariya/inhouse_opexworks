import React from "react";
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import "./table-styles.css";
import EditRenderer from "../AgGridRenders/editRenderer";
import DeleteRenderer from "./../AgGridRenders/deleteRenderer";
import BdreportRenderer from "../AgGridRenders/bdReportRenderer";
import WhyWhyAnalysisRenderer from "../AgGridRenders/whyWhyAnalysisRenderer";
import ViewDocumentRenderer from "../AgGridRenders/viewDocumentRenderer";
import ToolTipRenderer from "../AgGridRenders/toolTipRenderer/toolTipRenderer";
import CircularProgress from '@material-ui/core/CircularProgress';

// function autosizeHeaders(event) {
//     if (event.finished !== false) {
//         event.api.setHeaderHeight(25);
//         const headerCells = document.querySelectorAll('#myGrid .ag-header-cell-label');
//         let minHeight = 25;
//         headerCells.forEach(cell => {
//             minHeight = Math.max(minHeight, cell.scrollHeight);
//         });

//         // set header height to calculated height + padding (top: 8px, bottom: 8px)
//         event.api.setHeaderHeight(minHeight + 16);
//     }
// }

class AgTabe extends React.PureComponent {
    componentDidMount() {

    }

    onGridReadyAPI = (event) => {
        this.props.onGridReady(event)
        if (event.finished !== false) {
            const headerCells = document.querySelectorAll('.myGrid .ag-header-cell-label');
            let minHeight = 35;
            headerCells.forEach(cell => {
                minHeight = cell.scrollHeight * 1.5
            });
        }

    }

    render() {
        const {
            onGridReady,
            columnDefs,
            rowData,
            loading,
            onEdit,
            onDelete,
            onBdReport,
            onWhyWhyAnalysis,
            onViewDocument,
            dontShowDelete
        } = this.props

        if (loading) {
            return (<div
                style={{
                    height: window.innerHeight - 250,
                    width: '100%',
                    paddingLeft: '50%',
                    paddingTop: '15%'
                }}>
                <CircularProgress />
            </div>)
        }

        return (<div
            className="ag-theme-material"
            style={{
                height: window.innerHeight - 190,
                width: '100%'
            }}
        >
            <AgGridReact
                className="myGrid"
                onGridReady={this.onGridReadyAPI}
                paginationAutoPageSize={true}
                defaultColDef={
                    {
                        resizable: true,
                        sortable: true,
                        tooltipComponent: "toolTipRenderer"
                    }
                }
                suppressCellSelection
                rowSelection={false}
                pagination={true}
                columnDefs={columnDefs}
                rowData={rowData}
                context={{ onEdit, onDelete, onBdReport, onWhyWhyAnalysis, onViewDocument, dontShowDelete }}
                frameworkComponents={{
                    editRenderer: EditRenderer,
                    deleteRenderer: DeleteRenderer,
                    bdReportRenderer: BdreportRenderer,
                    whyWhyAnalysisRenderer: WhyWhyAnalysisRenderer,
                    viewDocumentRenderer: ViewDocumentRenderer,
                    toolTipRenderer: ToolTipRenderer

                }}>
            </AgGridReact>
        </div >)
    }
}

export default AgTabe