import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";
import Typography from '@material-ui/core/Typography';
import HomeIcon from '@material-ui/icons/Home';
import { Link } from "react-router-dom";

const styles = theme => ({
    breakCrumbIcon: {
        marginTop: '3px',
        fontSize: "20px"
    },
    breadCrumbText: {
        marginTop: '4px',
        color: "white",
        fontSize: "13px",
        "font-weight": "normal",

    },
    header: {
        "background-color": "#3f51b5",
        color: "white",
    },
    headerText: {
        color: "white",
        paddingBottom: "4px",
        fontSize: "18px",
    }
});


class Header1 extends React.PureComponent {
    state = {
        breadcrumbdata: this.props.breadCrumbText,
        header: this.props.breadCrumbHeader,
        crumbLength: this.props.breadCrumbText.length
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.header}>
                <Grid container
                    direction="row"
                    justify="flex-start"
                    alignItems="flex-start"
                >
                    <Grid item>
                        <HomeIcon className={classes.breakCrumbIcon} />&nbsp; {this.crumbLength}
                </Grid>
                    <Grid item>
                        <Typography className={classes.breadCrumbText} variant="body2" >
                            {this.props.breadCrumbText.map((item, i) => (<Fragment  key={i}><Link to={item.path}>
                                {item.name}
                            </Link>
                            { i < this.state.crumbLength-1 ?
                                <Fragment>
                                 &nbsp; / &nbsp;
                                </Fragment> :
                                 <Fragment> </Fragment>
                            }
                            
                            </Fragment>))}
                        </Typography>
                    </Grid>
                </Grid>

                <Grid container
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <Grid item>
                        <Typography className={classes.headerText} variant="h6" > {this.props.breadCrumbHeader} </Typography>
                    </Grid>
                </Grid>
            </div>
        )
    }
}
export default withStyles(styles)(Header1);