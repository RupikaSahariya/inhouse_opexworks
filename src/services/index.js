import { jwtDecode } from "./../utils/jwt-decode"
export const defaults = {
    networkStatus: {
        __typename: "NetworkStatus",
        isConnected: true
    },
};

// recordtypeid:String,
// createdbyid:String,
// sfid:String,
let nextTodoId = 0;
export const typeDefs = ``
export const resolvers = {
    // Query: {
    //     user: async (_, arg, { cache }) => {
    //         return ({
    //             id: "hello",
    //             plant_id: 25,
    //             __typename: "User"
    //         })
    //     }
    // },
    Mutation: {
        logout: async (_, args, { cache }) => {
        },
        updateNetworkStatus: (_, { isConnected }, { cache }) => {
            cache.writeData({
                data: {
                    networkStatus: {
                        __typename: "NetworkStatus",
                        isConnected
                    }
                }
            });
            return null;
        }
    }
};


export const actions = (providerContext) => {
    return {
        state: providerContext.state,
        getData: ({ spName, payload, key }) => fetch(`${process.env.REACT_APP_API_URL}/sp/${spName}`, {
            body: JSON.stringify(payload),
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json"
            },
            method: "POST"
        }).then(r => r.json()),
        toggleTheme: () => providerContext.setState({
            type: providerContext.state.type === 'light' ? 'dark' : 'light'
        }),
        toggleDirection: () => providerContext.setState({
            direction: providerContext.state.direction === 'ltr' ? 'rtl' : 'ltr',
        }, () => document.body.dir = providerContext.state.direction)
    }
}