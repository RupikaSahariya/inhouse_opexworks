import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { render } from "react-dom";
import client from "./utils/client";
import AppProvider from "./components/AppProvider/AppProvider";
import { ApolloProvider } from "react-apollo";
import Dashboard from "./containers/Dashboard";
import {
  NotFound,
  BackendError,
  Lockscreen,
  PasswordReset,
  Signin,
  Signup
} from "./pages";
import TokenLogin from "./pages/TokenLogin/tokenlogin";
import ensureAuthenticated from "./utils/hoc/ensureAuthenticated";
import registerServiceWorker from "./registerServiceWorker";
import { MuiPickersUtilsProvider } from "material-ui-pickers";
import MomentUtils from "@date-io/moment";

render(
  <AppProvider>
    <ApolloProvider client={client}>
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <BrowserRouter>
          <Switch>
            <Route exact path="/404" component={NotFound} />
            <Route exact path="/tokenlogin" component={TokenLogin} />
            <Route exact path="/500" component={BackendError} />
            <Route exact path="/Lockscreen" component={Lockscreen} />
            <Route exact path="/forgot" component={PasswordReset} />
            <Route exact path="/signin" component={Signin} />
            <Route exact path="/signup" component={Signup} />
            <Route path="/" component={ensureAuthenticated(Dashboard)} />
          </Switch>
        </BrowserRouter>
      </MuiPickersUtilsProvider>
    </ApolloProvider>
  </AppProvider>,

  document.getElementById("root")
);

registerServiceWorker();
